<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    
    <style>
@media screen and (min-width: 601px) {
	.container {
		width: 600px!important;
	}
}
	@media (max-width: 991px) {
    .mobile-only {
        display:block !important;
		font-size:10px;
    }
 
    .desktop-only {
        display:none !important;
    }
}
    </style>

</head>
<body>

  <div class="invoice-box"  style="font-size:14px;max-width:800px;margin:auto;padding:30px;border:1px solid #000;max-width:800px;margin:auto;padding:30px;border:1px solid #eee;box-shadow:0 0 10px rgba(0, 0, 0, .15);font-size:16px;line-height:24px;font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;color:#555;">
        <table cellpadding="0" cellspacing="0" style="width:100%;line-height:inherit;text-align:left;" >
            <tr class="top">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41">
                        <tr>
                            <td class="title">
                                <img src="partner/plogo.png" >
                            </td>
                        
                          
                        </tr>
					
                    </table>			
                </td>
            </tr>
        </table>
	         
			        <table  style="width:100%;line-height:inherit;background-color:#88ba41">
                        <tr>
                            <td class="title" style="text-align:center;border:1px solid #fff;">
                                <img src="partner/phand.png" align="center" style="margin: 0 auto;disply:block;" >
								<h2 style="color:#fff;text-transform:uppercase;word-wrap: break-word;vertical-align: middle;">WELCOME</h2>
                            </td>

                        </tr>
					
                    </table>
					   <table  style="width:100%;line-height:inherit;text-align:left;">
						
                        <tr> 
						<tr><h2 align="middle" style="color:#092f53;text-transform:uppercase;word-wrap: break-word;">Thank You for joining the ulo family!</h2></tr>
						<p align="middle" style="color:#000;font-size:14px;">Thank you for becoming a part of Ulo Hotels. You�re on your way to super-productivity and beyond! 
You'll find that joining us is a guaranteed way to increase revenue, occupancy rate and get great 
reviews from customers.
</p>
						</tr>
						<tr>
						<p align="middle" style="color:#000;font-size:14px;">Ulo  was born because we felt that quality shouldn't be a compromise no matter the budget. Ulo gives your property a better digital presence while simultaneously adhering to excellent quality and service. Our property manager will keep you updated about the progress, which you can track through the software we have developed: Ulotel.
</p>
						</tr>
						<tr>
						<p align="middle" style="color:#000;font-size:14px;"> Ulotel will help identify areas of improvement, customer needs and the USP of your property. 
We have separate teams dedicated to digital marketing, property management, and quality control.
</p>
						</tr>
						
						</table>
															 <table  style="width:100%;line-height:inherit;text-align:left;  ">
						   <tr>
					<td style="text-align: center;">
							  <img src="partner/pbird.png" align="center" style="margin: 0 auto;dispaly:block;" >
							  </td>
							  </tr>
                  
							
					
						</table>
			 <table  style="width:100%;line-height:inherit;text-align:left; border:1px solid #124c71; padding:10px; "  >
					
                        <tr>    <td>  <p style="text-align: center;word-wrap: break-word; color:#000;font-size:14px;">I can't wait to hear about your experience with Ulo. I love getting feedback about our service. 
               Whether you have suggestions, queries, or just want to say hi, you can reach us on Facebook 
          or send an email to <span style="color:#124c71">partners@ulohotels.com</span></p><td>
		     
						</tr>
						<tr>
						  <td align="right" style="font-size:14px;color:#000000;">
                                   <p style="float:right;text-align: center;">Looking Forward To A Fruitful Association.
						   <br>Thanks And Regards,<br>Viswanathan, <br> CEO, <br> Ulo Hotels <br>
						   
						   </p>
                            </td>
						</tr>
						</table>
			<br>
									 <table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41;padding:5px; ">
						 
                          <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                                <b><span style="color:#fff" >Mobile:</span>  +91 9543 592-593 <span style="color:#fff"> Email:</span> <span style="color:#155d90">Support@ulohotels.com </span><span style="color:#fff"> Website:</span> <span style="color:#155d90">www.ulohotels.com </span></b>
                            </td>
                              </tr>
					
						</table>
															 <table  style="width:100%;line-height:inherit;text-align:left; background-color:#FFFFE0;padding:5px; ">
						   <tr>
					<td style="text-align: center;">
							  <img src="https://ulohotels.com/ulowebsite/images/logo.png" align="center" style="margin: 0 auto;dispaly:block;" >
							  </td>
							  </tr>
                          <tr>
						    
                              <td align="middle" style="font-size:14px;color:#000000;">
							  
                              CORPORATE OFFICE: 56,B2,Oyster Apartment, 4th Avenue,19th Street, Ashok Nagar, Chennai, Tamil Nadu 600083
                            </td>
                              </tr>
							
					
						</table>
			
						 
    </div>



</body>
</html>