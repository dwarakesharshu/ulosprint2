<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    
    <style>
    
  
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }
        
        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
    </style>
	<script>
function show(shown, hidden) {
  document.getElementById(shown).style.display='block';
  document.getElementById(hidden).style.display='none';
  return false;
}
</script>
</head>
<body>

  <div class="invoice-box"  style="font-size:14px;max-width:800px;margin:auto;padding:30px;border:1px solid #000;max-width:800px;margin:auto;padding:30px;border:1px solid #eee;box-shadow:0 0 10px rgba(0, 0, 0, .15);font-size:16px;line-height:24px;font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;color:#555;">
        <table cellpadding="0" cellspacing="0" style="width:100%;line-height:inherit;text-align:left;" >
            <tr class="top">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left;">
                        <tr>
                            <td class="title">
                                <img src="https://ulohotels.com/ulowebsite/images/logo.png" style="width:100%; max-width:200px;">
                            </td>
                            <td style="text-align:right;">
                               <h2 style="color:#88ba41;text-transform:uppercase;word-wrap: break-word;vertical-align: middle;">Booking Invoice(test not valid)</h2>
                            </td>
                          
                        </tr>
						<hr>
                    </table>
                </td>
            </tr>
            
            <tr class="informations">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left;border-bottom:1px solid #000">
                        <tr>
                            <td>
                                <b>Voucher No: ${bookingid}</b>
                            </td>
                            
                            <td style="text-align:right;">
                              <b>GST : 33AABCU9979Q2Z8<br>CIN : U55100TN2016PTC113073<br>PAN : AABCU9979Q</b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
			      <tr class="informations">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left; border-bottom:1px solid #000" >
						  <h3 align="middle" style="color:#155d90">Guest Booking Information</h3>
                        <tr>
                            <td style="text-align:center;">
                               <b><span style="color:#155d90">Name:</span> ${guestname}<br><span style="color:#155d90">Mobile:</span> ${mobile}<br><span style="color:#155d90">Booking Id:</span>${bookingid}<br><span style="color:#155d90">Check-in:</span>${checkin} |<span style="color:#155d90"> Check-out:</span>${checkout}</b>
                            </td>
                            
                            <td style="text-align:center;" >
                              <b><span style="color:#155d90">Property Name: </span>${propertyname}<br><span style="color:#155d90">No Of Nights:</span>${days}<br><span style="color:#155d90">Meal Plan:</span>Regular<br><span style="color:#155d90">Adults</span>:${adults} |<span style="color:#155d90"> Child:</span>${child}|<span style="color:#155d90"> Infant:</span>${infant}</b>
                            </td>
                        </tr>
							
                    </table>
                </td>
            </tr>

        </table>
	         
		          <table style="width:100%;line-height:inherit;text-align:left;padding:10px; "   >
			
            <tr style=" background:#eee;font-weight:bold;">
                <th width="35%" style="color:#155d90">
                    Particulars
                </th>
                
                <th width="25%"  style="color:#155d90">
                   Rate
                </th>
				 <th width="25%"  style="color:#155d90">
                    Qty
                </th>
                
                <th width="15%"  style="color:#155d90">
              Total
                </th>
				
            </tr>
			      <tr >
				
                <th style="font-weight:normal;" >
                  ${accommodationtype}
                </th>
                
                <th style="font-weight:normal;">
           ${accommodationRate}
                </th >
				 <th style="font-weight:normal;" >
               ${rooms}
                </th>
                
                <th style="font-weight:normal;" >
             ${amount}
                </th>
            </tr>

    <tr >

       <td style=" color:#155d90; " >Sub-Total</td>
      <td style=" color:#155d90; "></td>
      <td style="color:#155d90;" >GST</td>
        <td style="font-weight:normal;" >${tax}</td>
    </tr>
	
	
   <tr  style="background:#eee;font-weight:bold; ">
   <td style="color:#155d90;">Total</td>
      <td></td>
      <td></td>
      
        <td> ${totalamount}</td>
		
    </tr>
             </table>
		
			 <table  style="width:100%;line-height:inherit;text-align:left;">
						 <hr>
                        <tr> <h6 align="middle" style="color:#000">This is computer Generated Vocher.No Signature Required.Please Carry this voucher to the resort.
This is just a confirmation voucher not an invoice.
Invoice will be provided only during the Check-out.</h6>
						</tr>
						</table>
									 <table  style="width:100%;line-height:inherit;text-align:left;">
						 
                        <tr> <h2 align="middle" style="color:#155d90">Thank You For Choosing Ulo Hotels!</h2>
						</tr>
						</table>
									 <table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41">
						 
                          <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                                <b><span style="color:#fff" >Mobile:</span>  +91 9543 592-593 <span style="color:#fff"> Email:</span> <span style="color:#155d90">Support@ulohotels.com </span></b>
                            </td>
                              </tr>
							    <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                              CORPORATE OFFICE: 56,B2,Oyster Apartment, 4th Avenue,19th Street, Ashok Nagar, Chennai, Tamil Nadu 600083
                            </td>
                        </tr>
						</table>
					<!-- 	 <table  style="width:100%;line-height:inherit;text-align:left;">
						<tr>
						<td align="middle" style="font-size:14px;color:#000000;">
						 <a href="#" onclick="return show('Page2','Page1');">Show page 2</a>
						</td>
						</tr>
						</table> -->
						 
    </div>

  <div class="invoice-box" id="Page3" style="max-width:800px;margin:auto;padding:30px;border:1px solid #000;max-width:800px;margin:auto;padding:30px;border:1px solid #eee;box-shadow:0 0 10px rgba(0, 0, 0, .15);font-size:16px;line-height:24px;font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;color:#555;">
        <table  style="width:100%;line-height:inherit;text-align:left;">
                        <tr>
                            <td class="title">
                                <img src="https://ulohotels.com/ulowebsite/images/logo.png" style="width:100%; max-width:200px;">
                            </td>
                        
                          
                        </tr>
					
                    </table>
  <table  style="width:100%;line-height:inherit;text-align:left;">
<ul style="font-size:10px;">
<h3>General Terms And Conditions</h3>
<li>The primary guest must be at least 18 years of age to be able to check-in the hotel</li>
<li>It is mandatory for guests to present valid photo identification at the time of check-in. According to government regulations, a valid Photo ID has to be carried by every person above the age of 18 staying at the hotel. The identification proofs accepted are Adhar Card, Driving License, Voter ID Card, and Passport. Without valid ID the guest will not be allowed to check-in.</li>
<li>PAN cards are not accepted as a valid ID card.</li>
<li>Stay of 1 child up to 5 years of age is complementary without the use of extra mattress. Breakfast charges may be applicable for the child.</li>
<li>Extra mattress will be provided for triple occupancy bookings.</li>
<li>Pets are not allowed in the hotel premises.</li>
<li>Should any action by a guest be deemed inappropriate by the hotel, or if any inappropriate behaviour is brought to the attention of the hotel, the hotel reserves the right, after the allegations have been investigated, to take action against the guest.</li>
<li>Certain destinations may have different policies for specific times during the year.</li>
<li>Guests shall be liable for any damage, except normal wear and tear to Hotel asset. Guest shall keep the Hotel room in a good condition and maintain hygiene and cleanliness.</li>
<li>Certain policies are booking specific and are informed to the customer while making the booking.</li>

</ul>
 </table>
  <table  style="width:100%;line-height:inherit;text-align:left;">
<ul style="font-size:10px;">
<h3>BOOKING EXTENSION POLICY</h3>
  <li>Extension of stay would be provided on the current room rates, subjected to availability.</li>
  <li>The current room rates can be different from the rates at which the rooms were booked.</li>

  </ul>
  </table>
    <table  style="width:100%;line-height:inherit;text-align:left;">
<ul style="font-size:10px;">
<h3>CANCELLATION POLICY</h3>
  <li>We would love to service service you but in case your plan s change, our simple cancellation process makes sure you receive a quick confirmation and fast refunds. Our standard check-in time is 12 noon and you can check-in any time after that till your reservation is valid.</li>
  <li>We do not charge cancellation fees for bookings cancelled within 60 minutes of creating the same booking. Any amount paid will be refunded after deduction of necessary taxes</li>
   <li>Our cancellation policy changes depending on the city and dates being booked and is mentioned on the hotel details page on our website. Before making a booking, please refer to the below highlighting the cancellation policy text</li>
  <h3>On the date of check-in</h3>
  <li>In case you decide to cancel a booking after check-in time or do not show up at the hotel, charges for the first two nights or the complete booking amount, whichever is lower, shall be deducted.</li>
  <h3>Mid Stay Cancellations</h3>
  <li>In case you decide to shorten your booking post the check-in date, you will be charged for the next 24 hours after the official cancellation. Money for the remaining nights will be refunded.</li>
  <li>You need to pay at least 25% of the booking amount as an advance to make  the booking for rooms.</li>
  <table border="1" align="center">
  <tr>
    <th>Cancellation Time</th>
    <th>Cancellation Fee</th>
  </tr>
  <tr>
    <td>30 days or more prior to check-in date</td>
    <td>FREE Cancellation (100% refund)</td>
  </tr>
  <tr>
    <td>Between 15 to 30 days of check-in date</td>
    <td>50% advance amount</td>
  </tr>
    <tr>
    <td>Less than 15 days of check-in date</td>
    <td>100% advance amount</td>
  </tr>
</table>
</ul>
<ul style="font-size:10px;">
<h3>How to Cancel</h3>
<li>You can cancel your booking using our website. The applicable refund amount will be credited to you within 7-14 working days. ULO HOTELS reserves the right to debit from ULO HOTELS Money account, in case of cancellation amount being higher than money already paid by the customer.</li>
<h3>PAYMENT POLICY</h3> 
<li>For bookings of more than 7 nights, guests have to settle all the outstanding payments on a weekly basis. The hotel will be unable to provide further accommodation prior to settlement of the outstanding amount.</li>
 <h3>EARLY CHECK-IN AND LATE CHECK-OUT</h3>
 <h4>Sunrise Check-in (Guaranteed Early Check-in)</h4>
 <li>Our hotels provide guaranteed early check-in from 6 AM onwards of check-in date. Most of these hotels provide this at free of cost while some charge a nominal amount (up to 30% of average tariff). Please use the filter of Sunrise Check-in  to select hotels that provide guaranteed early check-in. You may also call us on +91 9543-592-593 to book with sunrise check-in.</li>
   <table border="1" align="center">
  <tr>
    <th>Check-in Time</th>
    <th>Early Check-in Charges</th>
  </tr>
  <tr>
    <td>Before 6 AM</td>
    <td>100% charges for one day payable as per room rates of the previous day</td>
  </tr>
  <tr>
    <td>Between 6 AM and 10 AM</td>
    <td>0% to 30% charges payable as per room rates of the previous day, depending on hotel policy</td>
  </tr>
    <tr>
    <td>Between 10 AM and 12 Noon</td>
    <td>Complimentary</td>
  </tr>
  <tr>The below does not include charges for breakfast.</tr>
</table>
<h3>Late Check-out(subjected to availability) -:</h3>
<li>Our standard check-out time is 11 AM. Late check-out is subjected to availability and cannot be confirmed in advance. Extra charges will apply as per the below policy:</li>
 <h3>HOTEL SPECIFIC POLICIES:</h3>
 <li>Hotel specific amenities are captured on the website. Guests are advised to refer the same while booking.</li>
	  <li>Hotels in hill stations may not have air conditioning due to weather conditions of the location.</li>
	  <li>In some hotels, due to local conditions, unmarried/unrelated couples may not be allowed to check-in. Accommodation can be denied to guests posing as a couple if suitable proof of identification is not presented at the time of check-in.</li>
	  <li>Hotels may deny check-in to guests providing ID proof of the same city as the hotel itself. </li>
	  <li>Some hotels may deny entry of visitors to rooms. Please confirm with the hotel before inviting visitors into the rooms</li>
 </ul>
  </table>
				 <table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41">
						 
                          <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                                <b><span style="color:#fff" >Mobile:</span>  +91 9543 592-593 <span style="color:#fff"> Email:</span> <span style="color:#155d90">Support@ulohotels.com </span></b>
                            </td>
                              </tr>
							    <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                              CORPORATE OFFICE: 56,B2,Oyster Apartment, 4th Avenue,19th Street, Ashok Nagar, Chennai, Tamil Nadu 600083
                            </td>
                        </tr>
						</table>
	<!-- 	     <table  style="width:100%;line-height:inherit;text-align:left;">
						<tr>
						<td align="middle" style="font-size:14px;color:#000000;">
						   <a href="#" onclick="return show('Page1','Page2');">Show page 1</a>
						</td>
						</tr>
						</table> -->
  </div>

</body>
</html>