<html>
<body>
Hi Support Team,

<p>You recently requested a contact request.</p>
<p>Name: ${name}</p>
<p>Email: ${email}</p>
<p>Subject: ${subject}</p>
<p>Message: ${message}</p>

Regards,<br/>
${from}.
</body>
</html>