package com.ulopms.view;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;









import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.joda.time.DateTime;

import com.opensymphony.xwork2.ActionSupport;



import com.ulopms.controller.PmsDayManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PromotionDetailManager;
import com.ulopms.controller.PromotionManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.model.User;
import com.ulopms.util.DateUtil;

public class PromotionAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	private Integer promotionId;
	private Integer propertyId;
	private Integer propertyAccommodationId;
	private String  checkedDays;
	private Timestamp  startDate;
	private Timestamp  endDate;
	private String promotionName;
	private String promotionType;
	private String checkedAccommodations;
	private String updatePromotionId;
	
	

	private List<PmsPromotions> promotionList;
	
	private List<PmsPromotionDetails> promotionDetailList;
	
	
	private List<PmsDays> daylist;
	
	

	private static final Logger logger = Logger.getLogger(PropertyItemAction.class);
	
	

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PromotionAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}
	
	public Integer getPromotionId(){
		return promotionId;
	}
	
	public void setPromotionId(Integer promotionId){
		this.promotionId=promotionId;
	}
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}
	
	
	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	
	public String getCheckedDays() {
		return checkedDays;
	}

	public void setCheckedDays(String checkedDays) {
		this.checkedDays = checkedDays;
	}
	 
	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	
	public String getPromotionType(){
		return promotionType;
	}

	public void setPromotionType(String promotionType){
		this.promotionType=promotionType;
	}
	
	public String getCheckedAccommodations(){
		return checkedAccommodations;
	}

	public void setCheckedAccommodations(String checkedAccommodations){
		this.checkedAccommodations=checkedAccommodations;
	}
	
	public String getUpdatePromotionId() {
		return updatePromotionId;
	}

	public void setUpdatePromotionId(String updatePromotionId) {
		this.updatePromotionId = updatePromotionId;
	}
	
	public String getDay() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		 //System.out.println(getPropertyId());
		//System.out.println("all categories");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsDayManager dayController = new PmsDayManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.daylist = dayController.list();
			for (PmsDays days : daylist) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"dayId\":\"" + days.getDayId() + "\"";
				jsonOutput += ",\"daysOfWeek\":\"" + days.getDaysOfWeek()+ "\"";
				jsonOutput += ",\"dayPart\":\"" + days.getDayPart()+ "\"";
				//jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
				//jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	 
	public String addPromotions(){
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		this.startDate = getStartDate();
		sessionMap.put("startDate",startDate);
		 
		this.endDate = getEndDate();
		sessionMap.put("endDate",endDate);
		 
		this.promotionName = getPromotionName();
		sessionMap.put("promotionName",promotionName);
		
		this.promotionType=getPromotionType();
		sessionMap.put("promotionType", promotionType);
		
		this.checkedAccommodations=getCheckedAccommodations();
		sessionMap.put("checkedAccommodations", checkedAccommodations);
		
		String jsonOutput="";
    	HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json");
		
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		PmsPromotions promotions=new PmsPromotions();
		PromotionManager promotionController=new PromotionManager();
		Boolean promotionChk=false;
		int PromotionId=0;
		String strStartDate=null,strEndDate=null;
		List<PmsPromotions> listPromotionCheckAll=null;
		String strPromotionId="";
		ArrayList<Integer> arrlist=new ArrayList<Integer>();
		
		try{
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			
	    	strStartDate=format1.format(getStartDate());
    		strEndDate=format1.format(getEndDate());
    		DateTime FromDate = DateTime.parse(strStartDate);
	        DateTime ToDate = DateTime.parse(strEndDate);
    		
	        List<DateTime> dateBetween = DateUtil.getDateRange(FromDate, ToDate);
	        Iterator<DateTime> iterDateValues=dateBetween.iterator();
	        while(iterDateValues.hasNext()){
	        	DateTime dateValues=iterDateValues.next();
	        	java.util.Date DateValues = dateValues.toDate();
	        	listPromotionCheckAll=promotionController.listPromotions(propertyId, DateValues);
	        	if(listPromotionCheckAll.size()>0 && !listPromotionCheckAll.isEmpty()){
	        		promotionChk=true;
	        	}
	        	
	        	/*if(promotionType.equalsIgnoreCase("F")){
	        		listPromotionCheckAll=promotionController.listPromotions(propertyId, DateValues,promotionType);
		        	if(listPromotionCheckAll.size()>0 && !listPromotionCheckAll.isEmpty()){
		        		promotionChk=true;
		        	}
	        	}else if(!promotionType.equalsIgnoreCase("F")){
	        		String[] arrAccommodation = getCheckedAccommodations().split("\\s*,\\s*");
					int[] intArray = new int[arrAccommodation.length];
					 for (int i = 0; i < arrAccommodation.length; i++) {
					    intArray[i] = Integer.parseInt(arrAccommodation[i]);
					    listPromotionCheckAll=promotionController.listPromotions(propertyId, DateValues,promotionType,intArray[i]);
					    if(listPromotionCheckAll.size()>0 && !listPromotionCheckAll.isEmpty()){
					    	for(PmsPromotions promotion:listPromotionCheckAll){
					    		arrlist.add(promotion.getPropertyAccommodation().getAccommodationId());
					    	}
			        		
			        	}
					    
					 }
					 promotionChk=true;
	        	}*/
	        	
	        }
	        
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			if(promotionChk){
				jsonOutput += "\"check\":\"" + promotionChk+ "\"";
			}
			else if(!promotionChk){
				String[] arrAccommodation = getCheckedAccommodations().split("\\s*,\\s*");
				int[] intArray = new int[arrAccommodation.length];
				 for (int i = 0; i < arrAccommodation.length; i++) {
				    intArray[i] = Integer.parseInt(arrAccommodation[i]);
				    
				    if(!arrlist.contains(intArray[i])){
				    	if(intArray[i]!=0){
					    	promotions.setIsActive(true);
							promotions.setIsDeleted(false);
							promotions.setPromotionIsActive(true);
							PmsProperty property = propertyController.find(propertyId);
							promotions.setPmsProperty(property);
							if(!promotionType.equalsIgnoreCase("F")){
								PropertyAccommodation accommodation=accommodationController.find(Integer.parseInt(arrAccommodation[i]));
								promotions.setPropertyAccommodation(accommodation);
							}
							
							promotions.setStartDate(startDate);
							promotions.setEndDate(endDate);
							promotions.setPromotionName(promotionName);
							promotions.setPromotionType(promotionType);
							promotionController.add(promotions);
						
							PromotionId=promotions.getPromotionId();
							strPromotionId=String.valueOf(PromotionId)+","+strPromotionId;
					    }
				    }
				    
				 }
				
				this.promotionId = promotions.getPromotionId();
				sessionMap.put("promotionId",promotionId);
				sessionMap.put("strPromotionId",strPromotionId);
				jsonOutput += "\"check\":\"" + promotionChk+ "\"";
			}
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public String getPromotions(){
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try{
			Calendar calendar=Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
	 		String currentDate = format1.format(calendar.getTime());
	    	java.util.Date curdate=format1.parse(currentDate);
			String jsonOutput = "";
			String strPromotionsType="";
			HttpServletResponse response = ServletActionContext.getResponse();
			PromotionManager promotionController=new PromotionManager();
			response.setContentType("application/json");
			this.promotionList=promotionController.listPromotionsDetails(propertyId,curdate);
			for(PmsPromotions promotions:promotionList){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				strPromotionsType=promotions.getPromotionType();
				jsonOutput += "\"promotionId\":\"" + promotions.getPromotionId() + "\"";
				String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
				String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
				jsonOutput += ",\"startDate\":\"" + start + "\"";
				jsonOutput += ",\"endDate\":\"" + end + "\"";
				jsonOutput += ",\"promotionName\":\"" + promotions.getPromotionName() + "\"";
				if(strPromotionsType.equalsIgnoreCase("F")){
					jsonOutput += ",\"accommodationType\":\" All\"";
				}else{
					jsonOutput += ",\"accommodationType\":\"" + promotions.getPropertyAccommodation().getAccommodationType().trim() + "\"";
				}
				jsonOutput += "}";

			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			System.out.println(jsonOutput);
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	public String getUpdatePromotions(){
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		this.updatePromotionId = getUpdatePromotionId();
		sessionMap.put("updatePromotionId",updatePromotionId);
		try{
			String jsonOutput = "";
			String strPromotionsType="";
			HttpServletResponse response = ServletActionContext.getResponse();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			response.setContentType("application/json");
			this.promotionList=promotionController.listUpdatePromotions(Integer.parseInt(updatePromotionId));
			if(promotionList.size()>0 && !promotionList.isEmpty()){
				for(PmsPromotions promotions:promotionList){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					strPromotionsType=promotions.getPromotionType();
					if(strPromotionsType.equalsIgnoreCase("F")){
						jsonOutput += "\"accommodationType\":\" All\"";
					}else{
						jsonOutput += "\"accommodationType\":\"" + promotions.getPropertyAccommodation().getAccommodationType().trim() + "\"";
					}
					
					
					if(strPromotionsType.equalsIgnoreCase("L")){
						jsonOutput += ",\"promotionType\":\" Last Minute Promotions\"";
					}else if(strPromotionsType.equalsIgnoreCase("R")){
						jsonOutput += ",\"promotionType\":\" Room Promotions\"";
					}else if(strPromotionsType.equalsIgnoreCase("N")){
						jsonOutput += ",\"promotionType\":\" Night Promotions\"";
					}else if(strPromotionsType.equalsIgnoreCase("F")){
						jsonOutput += ",\"promotionType\":\" Flat Promotions\"";
					}
					String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
					String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
					jsonOutput += ",\"startDate\":\"" + start + "\"";
					jsonOutput += ",\"endDate\":\"" + end + "\"";
					
					
					jsonOutput += "}";

				}
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			System.out.println(jsonOutput);
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	public String getViewPromotions(){
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			this.updatePromotionId = getUpdatePromotionId();
			sessionMap.put("updatePromotionId",updatePromotionId);
			try{
				String jsonOutput = "";
				String strPromotionsType="";
				HttpServletResponse response = ServletActionContext.getResponse();
				PromotionManager promotionController=new PromotionManager();
				PromotionDetailManager promotionDetailController=new PromotionDetailManager();
				response.setContentType("application/json");
				this.promotionList=promotionController.listUpdatePromotions(Integer.parseInt(updatePromotionId));
				if(promotionList.size()>0 && !promotionList.isEmpty()){
					for(PmsPromotions promotions:promotionList){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						strPromotionsType=promotions.getPromotionType();
						if(strPromotionsType.equalsIgnoreCase("F")){
							jsonOutput += "\"accommodationType\":\" All\"";
						}else{
							jsonOutput += "\"accommodationType\":\"" + promotions.getPropertyAccommodation().getAccommodationType().trim() + "\"";
						}
						if(strPromotionsType.equalsIgnoreCase("L")){
							jsonOutput += ",\"promotionType\":\" Last Minute Promotions\"";
						}else if(strPromotionsType.equalsIgnoreCase("R")){
							jsonOutput += ",\"promotionType\":\" Room Promotions\"";
						}else if(strPromotionsType.equalsIgnoreCase("N")){
							jsonOutput += ",\"promotionType\":\" Night Promotions\"";
						}else if(strPromotionsType.equalsIgnoreCase("F")){
							jsonOutput += ",\"promotionType\":\" Flat Promotions\"";
						}
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";
						
						PmsPromotionDetails promotionsDetail=promotionDetailController.list(promotions.getPromotionId());
						if(strPromotionsType.equalsIgnoreCase("L")){
							jsonOutput += ",\"promotionPercent\":\"" + promotionsDetail.getPromotionPercentage() + "\"";
							jsonOutput += ",\"promotionHours\":\"" + promotionsDetail.getPromotionHours() + "\"";
							jsonOutput += ",\"promotionBaseAmount\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionBookRooms\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionGetRooms\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionBookNights\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionGetNights\":\"" + 0.0 + "\"";
						}else if(strPromotionsType.equalsIgnoreCase("R")){
							jsonOutput += ",\"promotionBaseAmount\":\"" + promotionsDetail.getBaseAmount() + "\"";
							jsonOutput += ",\"promotionBookRooms\":\"" + promotionsDetail.getBookRooms() + "\"";
							jsonOutput += ",\"promotionGetRooms\":\"" + promotionsDetail.getGetRooms() + "\"";
							jsonOutput += ",\"promotionPercent\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionHours\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionBookNights\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionGetNights\":\"" + 0.0 + "\"";
						}else if(strPromotionsType.equalsIgnoreCase("N")){
							jsonOutput += ",\"promotionBaseAmount\":\"" + promotionsDetail.getBaseAmount() + "\"";
							jsonOutput += ",\"promotionBookNights\":\"" + promotionsDetail.getBookNights() + "\"";
							jsonOutput += ",\"promotionGetNights\":\"" + promotionsDetail.getGetNights() + "\"";
							jsonOutput += ",\"promotionBookRooms\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionGetRooms\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionPercent\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionHours\":\"" + 0 + "\"";
						}else if(strPromotionsType.equalsIgnoreCase("F")){
							jsonOutput += ",\"promotionPercent\":\"" + promotionsDetail.getPromotionPercentage() + "\"";
							jsonOutput += ",\"promotionHours\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionBaseAmount\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionBookNights\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionGetNights\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionBookRooms\":\"" + 0.0 + "\"";
							jsonOutput += ",\"promotionGetRooms\":\"" + 0.0 + "\"";
						}
						jsonOutput += "}";

					}
				}
				
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				System.out.println(jsonOutput);
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	public String getDisablePromotions(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			this.updatePromotionId = getUpdatePromotionId();
			sessionMap.put("updatePromotionId",updatePromotionId);
			
			String promotionsType=null;
			PromotionManager promotionController=new PromotionManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			this.promotionList=promotionController.listUpdatePromotions(Integer.parseInt(updatePromotionId));
			if(promotionList.size()>0 && !promotionList.isEmpty()){
				for(PmsPromotions promotions:promotionList){
					promotions.setIsActive(false);
					promotions.setIsDeleted(true);
					promotions.setPromotionIsActive(false);
					PmsProperty property = propertyController.find(propertyId);
					promotions.setPmsProperty(property);
					promotionsType=promotions.getPromotionType();
					if(!promotionsType.equalsIgnoreCase("F")){
						PropertyAccommodation accommodation=accommodationController.find(promotions.getPropertyAccommodation().getAccommodationId());
						promotions.setPropertyAccommodation(accommodation);
					}
					
					promotions.setStartDate(promotions.getStartDate());
					promotions.setEndDate(promotions.getEndDate());
					promotions.setPromotionName(promotions.getPromotionName());
					promotions.setPromotionType(promotions.getPromotionType());
					promotionController.edit(promotions);

				}
			}
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		
		return null;
	}
}
