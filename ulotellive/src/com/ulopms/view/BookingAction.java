package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
//import java.sql.Date;
import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.ulopms.util.DateUtil;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;
















































import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.AccommodationAmenityManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.PmsAmenityManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PromotionDetailManager;
import com.ulopms.controller.PromotionManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.PropertyRateDetailManager;
import com.ulopms.controller.PropertyRateDetailManager;
import com.ulopms.controller.PropertyRateManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.DashBoard;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class BookingAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer bookingId;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private Timestamp nightDepartureDate;
	private Integer rooms;
	private Integer total;
	private Integer tax;
	private Long roomCount;
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private double totalAmount;
	private double totalTax;
	private double securityDeposit;
	public Integer propertyId;
	public Integer accommodationId;
	
	public String accommodationTypeId;
	private Integer adult;
	private Integer child;
	private double base;
	private Integer min;
	private double extraadult;
	private double extrachild;
	private Integer minadult;
	private Integer minchild;
	
	
	
	private Integer statusId;
	private Integer sourceId;
	private Double lastAmount;
	private Double lastExtraChild;
	private Double lastExtraAdult;
	private Double roomAmount;
	private Double roomExtraChild;
	private Double roomExtraAdult;
	private Double nightAmount;
	private Double nightExtraChild;
	private Double nightExtraAdult;
	private Double flatAmount;
	private Double flatExtraChild;
	private Double flatExtraAdult;
	private Double getRooms;
	private Double bookRooms;
	private Double getNights;
	private Double bookNights;
	private String promotionsType;
	private String promotionsRoomNight;
	private Integer promotionsRoomCount;
	private Integer promotionAccommodationId;
	
	
	private long roomCnt;
	
	public long getRoomCnt() {
		return roomCnt;
	}

	public void setRoomCnt(long roomCnt) {
		this.roomCnt = roomCnt;
	}

	int totalRoomCount = 0;
	int totalAdultCount = 0;
	int totalChildCount = 0;
	double totalTaxAmount = 0;
	double totalBaseAmount = 0;
	Timestamp arrivalDatey = null;
	
	private PmsBooking booking;
	
	private String chartDate;

	private String strDate;
	//TimeZone timeZone = TimeZone.getDefault();
	
	

	List<BookingListAction> array = new ArrayList<BookingListAction>();
  
	List<BookingListAction> types = new ArrayList<BookingListAction>();
	
	private List<PmsBooking> bookingList;
	
	private List<PmsAvailableRooms> availableList;
	
	private List<PropertyAccommodation> accommodationList;
	
     private List<BookingDetail> bookingDetailList;
	
	private List<DashBoard> dashBoardList;


	private static final Logger logger = Logger.getLogger(BookingAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;
	
	private DashBoard arrival;

	private DashBoard departure;

	private DashBoard occupancy;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookingAction() {

	}

	public String execute() {

		
		return SUCCESS;
	}

	
	
	public String getAvailability() throws IOException {


		//this.bookingId = (Integer) sessionMap.get("bookingId");

		//System.out.println("--->bookingId");
		//System.out.println(getBookingId());

		// String timezone ="+05:30";

		//String arrival = getArrivalDate().toString();
		//arrival = arrival.substring(0, arrival.indexOf('.'));
		this.arrivalDate = getArrivalDate();
		sessionMap.put("arrivalDate",arrivalDate);

		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate);


		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		System.out.println(timestamp);

		System.out.println("get all bookings");
		System.out.println(getArrivalDate());
		System.out.println(getDepartureDate());
		System.out.println("date");
		//System.out.println(dateTimeValue);
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.departureDate);
		cal.add(Calendar.DATE, -1);
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		DateTime start = DateTime.parse(startDate);
		DateTime end = DateTime.parse(endDate);
		java.util.Date arrival = start.toDate();
		java.util.Date departure = end.toDate();


		this.propertyId = (Integer) sessionMap.get("propertyId");

		System.out.println(getPropertyId());

		try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();


		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		PmsBookingManager bookingController = new PmsBookingManager();
		//shiva add taxmanager
		PropertyTaxeManager taxController = new PropertyTaxeManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PmsSourceManager sourceController = new PmsSourceManager();
		PropertyRateDetailManager detailController = new PropertyRateDetailManager();
		PmsSource pmsSource = sourceController.find(1);

		//this.familyRegisterList = familyController.list(getUser().getUserid());
		//model = familyRegisterList;
		response.setContentType("application/json");
		this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate());

		List<DateTime> between = DateUtil.getDateRange(start, end);
		List<Long> myList = new ArrayList<Long>();

		for (PmsAvailableRooms available : availableList) {


		if (!jsonOutput.equalsIgnoreCase(""))
		jsonOutput += ",{";

		else
		jsonOutput += "{";

		int dateCount=0;
		double amount=0;
		double totalAmount=0;
		double totalExtraChildAmount=0;
		double totalExtraAdultAmount=0;
		double totalExtraInfantAmount=0;
		double extraChildAmount=0,extraAdultAmount=0,extraInfantAmount=0;
		System.out.println("between...."+between);
		for (DateTime d : between)
		{

		//System.out.println("dateString" +d.toString());

		List<PropertyRate> propertyRateList = rateController.list(available.getAccommodationId(), pmsSource.getSourceId(), d.toDate());
		// System.out.println("lisssssssssssssss"+propertyRateList.size());
		if(propertyRateList.size()>0)
		{
			
		for (PropertyRate pr : propertyRateList)
		{

		int propertyRateId = pr.getPropertyRateId();
		//System.out.println("propertyRateid" +pr.getPropertyRateId());
		DateFormat f = new SimpleDateFormat("EEEE");
		//System.out.printeln(propertyRateList.getPropertyRateId());
		System.out.println("seding" + f.format(d.toDate()).toLowerCase());
		List<PropertyRateDetail> rateDetailList = detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
		if(rateDetailList.size()>0)
		{
		for (PropertyRateDetail rate : rateDetailList)
		{
		amount += rate.getBaseAmount();
		System.out.println("Offer" +amount);
		extraAdultAmount+=rate.getExtraAdult();
		extraChildAmount+=rate.getExtraChild();
		extraInfantAmount+=rate.getExtraInfant();
		}
		}
		else
		{extraAdultAmount+=available.getExtraAdult();
		extraChildAmount+=available.getExtraChild();
		extraInfantAmount+=available.getExtraInfant();
		amount += available.getBaseAmount();
		}


		}

		}
		else
		{
		amount += available.getBaseAmount();
		extraAdultAmount+=available.getExtraAdult();
		extraChildAmount+=available.getExtraChild();
		extraInfantAmount+=available.getExtraInfant();
		}
		dateCount++;
		}

		totalAmount = amount;

		if(dateCount>1){
		totalExtraAdultAmount=Math.round(extraAdultAmount/dateCount);
		totalExtraChildAmount=Math.round(extraChildAmount/dateCount);
		totalExtraInfantAmount=Math.round(extraInfantAmount/dateCount);
		}
		else{
		totalExtraAdultAmount=extraAdultAmount;
		totalExtraChildAmount=extraChildAmount;
		totalExtraInfantAmount=extraInfantAmount;
		}



		//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
		jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";
		jsonOutput += ",\"baseAmount\":\"" + (totalAmount==0 ? available.getBaseAmount() : totalAmount ) + "\"";
		jsonOutput += ",\"arrivalDate\":\"" + getArrivalDate()+ "\"";
		jsonOutput += ",\"departureDate\":\"" + getDepartureDate()+ "\"";
		jsonOutput += ",\"rooms\":\"" + 1 + "\"";
		//jsonOutput += ",\"roomCount\":\"" + (available.getRoomCount() == null ? available.getNoOfUnits() : available.getRoomCount()) + "\"";
		//jsonOutput += ",\"roomCount\":\"" + available.getRoomCount() + "\"";
		jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
		jsonOutput += ",\"accommodationType\":\"" + available.getAccommodationType() + "\"";
		jsonOutput += ",\"minOccupancy\":\"" + available.getMinOccupancy() + "\"";
		jsonOutput += ",\"maxOccupancy\":\"" + available.getMaxOccupancy() + "\"";
		jsonOutput += ",\"noOfAdults\":\"" + available.getNoOfAdults() + "\"";
		jsonOutput += ",\"noOfChild\":\"" + available.getNoOfChild() + "\"";
		jsonOutput += ",\"extraAdult\":\"" + (totalExtraAdultAmount==0 ? available.getExtraAdult() : totalExtraAdultAmount ) + "\"";
		jsonOutput += ",\"extraChild\":\"" + (totalExtraChildAmount==0 ? available.getExtraChild() : totalExtraChildAmount )+ "\"";
		jsonOutput += ",\"extraInfant\":\"" + (totalExtraInfantAmount==0 ? available.getExtraInfant() : totalExtraInfantAmount )+ "\"";

		//shiva add next 2 lines
		//PropertyTaxe propertyTaxe = taxController.findAccommodationTax(available.getAccommodationId());
		jsonOutput += ",\"tax\":\"" + 0 + "\"";
		//PmsAvailableRooms roomCount = bookingController.findCount(arrival,available.getAccommodationId());
		//
		System.out.println("roomCount " + roomCount);
		System.out.println("available " + available.getAccommodationId());

		//jsonOutput += ",\"available\":\"" + available.getAvailable() + "\"";

		long rmc = 0;
		this.roomCnt = rmc;
		List<DateTime> between1 = DateUtil.getDateRange(start, end);
		List<Long> myList1 = new ArrayList<Long>();
		for (DateTime d : between)
		{
		java.util.Date availDate = d.toDate();
		System.out.println("hello hai" +d.toString());

		PmsAvailableRooms roomCount = bookingController.findCount(availDate, available.getAccommodationId());
		//System.out.println(roomCount == null ? 0 : (roomCount.getRoomCount()));
		if(roomCount.getRoomCount() == null){
		this.roomCnt = (available.getAvailable()-rmc);
		System.out.println("first"+this.roomCnt);

		}
		else{

		this.roomCnt = (available.getAvailable()-roomCount.getRoomCount());
		System.out.println("second"+this.roomCnt);
		}

		myList1.add(this.roomCnt);

		System.out.println("roomCount " + this.roomCnt);

		System.out.println("roomCount " + this.roomCnt);


		}
		/*PmsAvailableRooms roomCount = bookingController.findCount(arrival, departure,available.getAccommodationId());
		System.out.println("roomCount " + roomCount.getRoomCount());*/


		//jsonOutput += ",\"available\":\"" + available.getAvailable() + "\"";
		jsonOutput += ",\"available\":\"" + Collections.min(myList1) + "\"";
		jsonOutput += "}";





		}



		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");


		} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
		} finally {

		}

		return null;

		}
   public String getRoomAvailability() throws IOException {
		
	   
	    //this.bookingId = (Integer) sessionMap.get("bookingId");
		
		//System.out.println("--->bookingId");
		//System.out.println(getBookingId());
		
	   // String timezone ="+05:30";
	     
	   //String arrival = getArrivalDate().toString();
	   //arrival = arrival.substring(0, arrival.indexOf('.'));
	    this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
			
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); 
	      
		System.out.println("final accom "+getAccommodationTypeId());
		//String numberAsString = String.valueOf(getAccommodationId());
		String[] parts = getAccommodationTypeId().split(",");
		//System.out.println("final parts"+parts);
		 int[] intArray = new int[parts.length];
		   for (int i = 0; i < parts.length; i++) {
		         String numberAsString = parts[i];
		         intArray[i] = Integer.parseInt(numberAsString);
		      }
		
		// int[] intArray = new int[parts.length];
		 //System.out.println("final length array"+intArray);
			
	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    
	    System.out.println(timestamp);
	    
		System.out.println("get all bookings");
		System.out.println(getArrivalDate());
		System.out.println(getDepartureDate());
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.departureDate);
	    cal.add(Calendar.DATE, -1);
		
		//System.out.println(dateTimeValue);
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		DateTime start = DateTime.parse(startDate);
		DateTime end = DateTime.parse(endDate);
		java.util.Date arrival = start.toDate();
		java.util.Date departure = end.toDate();
		
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		//System.out.println(getPropertyId());
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			//shiva add taxmanager
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			
			for (int accomId : intArray)
			{ 
				System.out.println("final accom"+accomId);
			
			
		
				this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate(),accomId);
			
			
         for (PmsAvailableRooms available : availableList) {
         
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput += "{";
				//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
				jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";
				jsonOutput += ",\"baseAmount\":\"" + available.getBaseAmount() + "\"";
				jsonOutput += ",\"arrivalDate\":\"" + getArrivalDate()+ "\"";
				jsonOutput += ",\"departureDate\":\"" + getDepartureDate()+ "\"";
				jsonOutput += ",\"rooms\":\"" + 0 + "\"";
				//jsonOutput += ",\"roomCount\":\"" + (available.getRoomCount() == null ? available.getNoOfUnits() : available.getRoomCount()) + "\"";
				//jsonOutput += ",\"roomCount\":\"" + available.getRoomCount() + "\"";
				jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
				jsonOutput += ",\"accommodationType\":\"" + available.getAccommodationType() + "\"";
				jsonOutput += ",\"minOccupancy\":\"" + available.getMinOccupancy() + "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + available.getMaxOccupancy() + "\"";
				jsonOutput += ",\"noOfAdults\":\"" + available.getNoOfAdults() + "\"";
				jsonOutput += ",\"noOfChild\":\"" + available.getNoOfChild() + "\"";
				jsonOutput += ",\"noOfInfant\":\"" + available.getNoOfInfant() + "\"";
				jsonOutput += ",\"extraChild\":\"" + available.getExtraChild() + "\"";
				jsonOutput += ",\"extraAdult\":\"" + available.getExtraAdult() + "\"";
				//jsonOutput += ",\"available\":\"" + available.getRoomAvailable() + "\"";
				//shiva add next 2 lines
				/*PropertyTaxe propertyTaxe = taxController.findAccommodationTax(available.getAccommodationId());
	              jsonOutput += ",\"tax\":\"" + propertyTaxe.getTaxAmount() + "\"";*/
				System.out.println("available " + available.getRoomAvailable());
				long rmc = 0;
				this.roomCnt = rmc;
				List<DateTime> between = DateUtil.getDateRange(start, end);
				List<Long> myList = new ArrayList<Long>();
				for (DateTime d : between)
			     {
					java.util.Date availDate = d.toDate();
					System.out.println("hello hai" +d.toString());				  
					       
						  PmsAvailableRooms roomCount = bookingController.findCount(availDate, available.getAccommodationId());							
							//System.out.println(roomCount == null ? 0 : (roomCount.getRoomCount()));							
							if(roomCount.getRoomCount() == null){								
								this.roomCnt = (available.getRoomAvailable()-rmc);		
								System.out.println("first"+this.roomCnt);
				               
							}
							else{
								
								this.roomCnt = (available.getRoomAvailable()-roomCount.getRoomCount());						
								System.out.println("second"+this.roomCnt);
							}					
							
							myList.add(this.roomCnt);
							
					System.out.println("roomCount " + this.roomCnt);
						
					System.out.println("Minimum : "+Collections.min(myList));
			        //long availableCount = Collections.min(myList);
			        

			        //return availableCount;
			     }
				/*PmsAvailableRooms roomCount = bookingController.findCount(arrival, departure,available.getAccommodationId());
				System.out.println("roomCount " + roomCount.getRoomCount());*/
				
				
				//jsonOutput += ",\"available\":\"" + available.getAvailable() + "\"";
				jsonOutput += ",\"available\":\"" + Collections.min(myList) + "\"";
				jsonOutput += "}";
				
				
				
         }
				

			}
         
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

   
   public String getUloAvailability() throws IOException {
		
	   
	    //this.bookingId = (Integer) sessionMap.get("bookingId");
		
		//System.out.println("--->bookingId");
		//System.out.println(getBookingId());
		
	   // String timezone ="+05:30";
	     
	   //String arrival = getArrivalDate().toString();
	   //arrival = arrival.substring(0, arrival.indexOf('.'));
	    this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
			
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); 
	      
		this.propertyId = getPropertyId();
		sessionMap.put("propertyId",propertyId); 
		
		String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(getArrivalDate());
	    String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(getDepartureDate());
	    sessionMap.put("checkIn",checkIn);
		sessionMap.put("checkOut",checkOut);
		 
		
	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    
	    System.out.println(timestamp);
	    
		System.out.println("get all bookings");
		System.out.println(getArrivalDate());
		System.out.println(getDepartureDate());
		System.out.println("date");
		//System.out.println(dateTimeValue);
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.departureDate);
        cal.add(Calendar.DATE, -1);
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		DateTime start = DateTime.parse(startDate);
        DateTime end = DateTime.parse(endDate);
        java.util.Date arrival = start.toDate();
		java.util.Date departure = end.toDate();
		String iconPath  = "ulowebsite/images/icons/";
		
		
		
		
		System.out.println("propyid" +getPropertyId());
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			AccommodationAmenityManager amenityController = new AccommodationAmenityManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			
			//rate calculation
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PromotionManager promotionController= new PromotionManager();
			PromotionDetailManager promotionDetailController= new PromotionDetailManager();
			
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsSource pmsSource = sourceController.find(1);
			

			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate());
			System.out.println("availableList..."+availableList.toString());
			List<DateTime> between = DateUtil.getDateRange(start, end);
			List<Long> myList = new ArrayList<Long>();
            for (PmsAvailableRooms available : availableList) {
           
        	  /* 
            	
            	long rmc = 0;
				this.roomCnt = rmc;
				
				for (DateTime d : between)
			     {
					java.util.Date availDate = d.toDate();
					System.out.println("hello hai" +d.toString());
					PmsAvailableRooms roomCount = bookingController.findCount(availDate, available.getAccommodationId());
					
					//System.out.println(roomCount == null ? 0 : (roomCount.getRoomCount()));
					
					if(roomCount.getRoomCount() == null){
						
						this.roomCnt = (available.getAvailable()-rmc);
						
		               
					}
					else{
						
						this.roomCnt = (available.getAvailable()-roomCount.getRoomCount());
						
						
					}
					
					
					myList.add(this.roomCnt);
					
					System.out.println("roomCount " + this.roomCnt);
						
			     
			     }
				
				*/ 
				 
				 
				 
		    	
        	  
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput += "{";
				
				//actual calculation 
				 int dateCount=0;
				 double amount=0;
				 double totalAmount=0;
				 double totalExtraChildAmount=0;
				 double totalExtraAdultAmount=0;
				 double totalExtraInfantAmount=0;
				 double extraChildAmount=0,extraAdultAmount=0,extraInfantAmount=0;
				 String promotionType=null;
				 Double lastPercentage=0.0,flatPercentage=0.0;
				 ArrayList arrlist=new ArrayList();
				 HashMap<String,Integer> mapIdType=new HashMap<String,Integer>();
				 HashMap<String, Double> mapPercentType=new HashMap<String, Double>();
				 System.out.println("between...."+between);
				 int lastMinuteHours=0;
				 int promotionId=0;
				 int accommPromotionId=0,promotionAccommId=0,mapPromotionId=0;
				 String accommPromotionType=null;
				 DateFormat f = new SimpleDateFormat("EEEE");
				 Boolean blnPromotionsFlag=false;
				 Boolean blnPromotionIsNotFlag=false;
				 Double typePercent=0.0;
				 Double getRoom=0.0,getNight=0.0,bookRoom=0.0,bookNight=0.0,totalGetNights=0.0;
				 Boolean blnBookedGetRooms=false,blnBookedGetNights=false;
				 System.out.println("accom" +available.getAccommodationId());
				 
			     for (DateTime d : between)
			     {
			    	 
			    	 //System.out.println("dateString" +d.toString());
			    	 List<PmsPromotions> promotionList= promotionController.listPromotions(propertyId,d.toDate());
			    	 
			    	 if(promotionList.size()>0 && !promotionList.isEmpty()){
			    		 for(PmsPromotions promotions: promotionList){
			    			 promotionId =promotions.getPromotionId();
			    			 promotionType=promotions.getPromotionType();
			    			 arrlist.add(promotionType);
			    			 if(promotionType.equalsIgnoreCase("F")){
			    				 mapIdType.put(promotionType,promotionId);
			    			 }
			    			 
		    				 blnPromotionsFlag=true;
			    			
			    		 }
			    	 }
			    	 
			    	 if(blnPromotionsFlag){
			    		 List <PmsPromotions> promotionsList=promotionController.listPromotionsPerAccommodation(available.getAccommodationId(),d.toDate());
			    		 if(promotionsList.size()>0){
			    			 for(PmsPromotions promotion:promotionsList){
			    				 
			    				 accommPromotionId =promotion.getPromotionId();
			    				 accommPromotionType=promotion.getPromotionType();
			    				 promotionAccommId=promotion.getPropertyAccommodation().getAccommodationId();
			    				 List<PmsPromotionDetails> promotionDetailList =   promotionDetailController.listPromotionDetails(accommPromotionId,f.format(d.toDate()).toLowerCase());
			    				 if(promotionDetailList.size()>0 && !promotionDetailList.isEmpty()){
			    					 for(PmsPromotionDetails promotionDetail: promotionDetailList){
			    						 if(accommPromotionType.equalsIgnoreCase("R")){
			    							 	long minimum = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),available.getAvailable(),available.getAccommodationId()); 
			    			    				blnBookedGetRooms=bookGetRooms((int)minimum,getRoom,bookRoom);
			    						    	 if(!blnBookedGetRooms){
			    						    		 blnPromotionIsNotFlag=true;
			    						    	 }else{
			    						    		 amount+=promotionDetail.getBaseAmount();
					    			    			 extraAdultAmount+=promotionDetail.getExtraAdult();
					    			    			 extraChildAmount+=promotionDetail.getExtraChild();
					    			    			 getRoom=promotionDetail.getGetRooms();
					    			    			 bookRoom=promotionDetail.getBookRooms();
			    						    		 
					    			    			 sessionMap.put("roomAmount", amount);
						    					     sessionMap.put("roomExtraAdult", extraAdultAmount);
						    					     sessionMap.put("roomExtraChild", extraChildAmount);
						    					     sessionMap.put("promotionsType", "R");
						    					     sessionMap.put("promotionAccommodationId", promotionAccommId);
						    					     sessionMap.put("getRooms", getRoom);
						    					     sessionMap.put("bookRooms", bookRoom);
			    						    	 }
			    			    				
			    		    			 }
			    						 else if(accommPromotionType.equalsIgnoreCase("N")){
				    			    		     totalGetNights=getNight+bookNight;
				    			    		     Integer intTotalGetNights=totalGetNights.intValue();
				    			    		     SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				    			    			 Calendar calCheckOut=Calendar.getInstance();
				    			    			 calCheckOut.setTime(getDepartureDate());
				    			    			 calCheckOut.add(Calendar.DATE,intTotalGetNights);
				    			    			 java.util.Date checkOutDate = calCheckOut.getTime();             
				    			    			 String departureDate = format1.format(checkOutDate);  
				    			    			 
				    			    			 java.util.Date fromDate = format1.parse(departureDate);
				    			    			 Timestamp tsDepartureDate=new Timestamp(fromDate.getTime());
				    			    			 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),tsDepartureDate,available.getAvailable(),available.getAccommodationId());
				    			    			 
				    			    			 blnBookedGetRooms=bookGetNights((int)minimumCount,getNight,bookNight);
				    					    	 if(!blnBookedGetNights){
				    					    		 blnPromotionIsNotFlag=true;
				    					    	 }
				    					    	 else{
				    					    		 amount+=promotionDetail.getBaseAmount();
					    			    			 extraAdultAmount+=promotionDetail.getExtraAdult();
					    			    			 extraChildAmount+=promotionDetail.getExtraChild();
					    			    			 getNight=promotionDetail.getGetNights();
					    			    			 bookNight=promotionDetail.getBookNights();
					    			    				
				    					    		 sessionMap.put("nightAmount", amount);
						    					     sessionMap.put("nightExtraAdult", extraAdultAmount);
						    					     sessionMap.put("nightExtraChild", extraChildAmount);
						    					     sessionMap.put("promotionsType", "N");
						    					     sessionMap.put("promotionAccommodationId", promotionAccommId);
						    					     sessionMap.put("getNights", getNight);
						    					     sessionMap.put("bookNights", bookNight);
				    					    		 sessionMap.put("nightDepartureDate", tsDepartureDate);
				    				    			 sessionMap.put("promotionsRoomNight", "Nights");
				    					    	 }
			    						 }
			    						 else if(accommPromotionType.equalsIgnoreCase("L")){
			    							 lastMinuteHours=promotionDetail.getPromotionHours();
			    							 int intHoursCount=getCurrentHours(d.toDate(),lastMinuteHours);
			    							 if(intHoursCount<=lastMinuteHours){
			    								 lastPercentage=promotionDetail.getPromotionPercentage();
			    								 mapPercentType.put("L", lastPercentage);
			    							 }
			    							 
			    							 
			    							 List<PropertyRate> propertyRateList = rateController.list(available.getAccommodationId(), pmsSource.getSourceId(), d.toDate());
			    					    	 System.out.println("propertyRateList.."+propertyRateList);
			    					    	// System.out.println("lisssssssssssssss"+propertyRateList.size());
			    					    	 if(propertyRateList.size()>0)
			    					    	 {
			    					    		 for (PropertyRate pr : propertyRateList)
			    				    			 {
			    						    		 int propertyRateId = pr.getPropertyRateId();
			    						    		 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
			    						    		 if(rateDetailList.size()>0){
			    						    			 for (PropertyRateDetail rate : rateDetailList){
			    						    				 amount +=  rate.getBaseAmount();
			    						    				 System.out.println("Offer" +amount);
			    						    				 extraAdultAmount+=rate.getExtraAdult();
			    						    				 extraChildAmount+=rate.getExtraChild();
			    						    				 extraInfantAmount+=rate.getExtraInfant();
			    						    			 }
			    						    		 }
			    						    		 else{
			    						    			 amount += available.getBaseAmount();
			    						    			 extraAdultAmount+=available.getExtraAdult();
			    					    				 extraChildAmount+=available.getExtraChild();
			    					    				 extraInfantAmount+=available.getExtraInfant();
			    						    		 }
			    				    			 }	    		 
			    					    	 }
			    					    	 else{
			    					    		 amount += available.getBaseAmount();
			    					    		 extraAdultAmount+=available.getExtraAdult();
			    			    				 extraChildAmount+=available.getExtraChild();
			    			    				 extraInfantAmount+=available.getExtraInfant();
			    					    	 }
			    				    		 
			    					    	 sessionMap.put("lastExtraAdult", extraAdultAmount);
			    					    	 sessionMap.put("lastExtraChild", extraChildAmount);
			    					    	 sessionMap.put("promotionsType", "L");
			    					    	 sessionMap.put("promotionAccommodationId", promotionAccommId);
			    					    	 
			    					    	 
			    					    	 typePercent=mapPercentType.get("L");
			    					    	 System.out.println("typePercent L.."+typePercent);
			    					    	 String strTypePercent=String.valueOf(typePercent);
			    					    	 if(!strTypePercent.equalsIgnoreCase("null")){
			    					    		 totalAmount = amount-(amount*typePercent/100);
			    					    	 }
			    					    	 else{
			    					    		 totalAmount = amount;
			    					    	 }
			    					    	 sessionMap.put("lastAmount", totalAmount);
			    						 }
			    						 
			    					 }
			    				 }
			    				 else{
			    					 blnPromotionIsNotFlag=true;
			    				 }
			    			 }
			    		 }
			    		 else{
			    			 if(mapIdType.size()>0){
			    				 mapPromotionId=mapIdType.get("F");
			    				 List<PmsPromotionDetails> promotionDetailList =   promotionDetailController.listPromotionDetails(mapPromotionId,f.format(d.toDate()).toLowerCase());
			    				 if(promotionDetailList.size()>0 && !promotionDetailList.isEmpty()){
			    					 for(PmsPromotionDetails pmsDetails: promotionDetailList){
			    						 flatPercentage=pmsDetails.getPromotionPercentage();
			    						 mapPercentType.put("F", flatPercentage);
			    						 
			    					 }
			    					 List<PropertyRate> propertyRateList = rateController.list(available.getAccommodationId(), pmsSource.getSourceId(), d.toDate());
		    				    	 System.out.println("propertyRateList.."+propertyRateList);
		    				    	// System.out.println("lisssssssssssssss"+propertyRateList.size());
		    				    	 if(propertyRateList.size()>0)
		    				    	 {
		    				    		 for (PropertyRate pr : propertyRateList)
		    			    			 {
		    					    		 int propertyRateId = pr.getPropertyRateId();
		    					    		 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
		    					    		 if(rateDetailList.size()>0){
		    					    			 for (PropertyRateDetail rate : rateDetailList){
		    					    				 amount +=  rate.getBaseAmount();
		    					    				 System.out.println("Offer" +amount);
		    					    				 extraAdultAmount+=rate.getExtraAdult();
		    					    				 extraChildAmount+=rate.getExtraChild();
		    					    				 extraInfantAmount+=rate.getExtraInfant();
		    					    			 }
		    					    		 }
		    					    		 else{
		    					    			 amount += available.getBaseAmount();
		    					    			 extraAdultAmount+=available.getExtraAdult();
		    				    				 extraChildAmount+=available.getExtraChild();
		    				    				 extraInfantAmount+=available.getExtraInfant();
		    					    		 }
		    			    			 }	    		 
		    				    	 }
		    				    	 else{
		    				    		 amount += available.getBaseAmount();
		    				    		 extraAdultAmount+=available.getExtraAdult();
		    		    				 extraChildAmount+=available.getExtraChild();
		    		    				 extraInfantAmount+=available.getExtraInfant();
		    				    	 }
		    				    	 sessionMap.put("flatExtraAdult", extraAdultAmount);
		    				    	 sessionMap.put("flatExtraChild", extraChildAmount);
		    				    	 sessionMap.put("promotionsType", "F");
		    				    	 
		    				    	 typePercent=mapPercentType.get("F");
		    				    	 System.out.println("typePercent F.."+typePercent);
		    				    	 
		    				    	 typePercent=mapPercentType.get("F");
		    				    	 System.out.println("typePercent F.."+typePercent);
		    				    	 String strTypePercent=String.valueOf(typePercent);
		    				    	 if(!strTypePercent.equalsIgnoreCase("null")){
		    				    		 totalAmount = amount-(amount*typePercent/100);
		    				    	 }
		    				    	 else{
		    				    		 totalAmount = amount;
		    				    	 }
		    				    	 sessionMap.put("flatAmount", totalAmount);
				    				 
			    				 }
			    				 else{
			    					 blnPromotionIsNotFlag=true;
			    				 }
			    				
			    			 }else{
		    					 blnPromotionIsNotFlag=true;
		    				 }
			    			 
			    		 }
			    	 }
			    	 else
			    	 {
			    		 List<PropertyRate> propertyRateList = rateController.list(available.getAccommodationId(), pmsSource.getSourceId(), d.toDate());
				    	 System.out.println("propertyRateList.."+propertyRateList);
				    	// System.out.println("lisssssssssssssss"+propertyRateList.size());
				    	 if(propertyRateList.size()>0)
				    	 {
				    		 for (PropertyRate pr : propertyRateList)
			    			 {
					    		 int propertyRateId = pr.getPropertyRateId();
					    		 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
					    		 if(rateDetailList.size()>0){
					    			 for (PropertyRateDetail rate : rateDetailList){
					    				 amount +=  rate.getBaseAmount();
					    				 System.out.println("Offer" +amount);
					    				 extraAdultAmount+=rate.getExtraAdult();
					    				 extraChildAmount+=rate.getExtraChild();
					    				 extraInfantAmount+=rate.getExtraInfant();
					    			 }
					    		 }
					    		 else{
					    			 amount += available.getBaseAmount();
					    			 extraAdultAmount+=available.getExtraAdult();
				    				 extraChildAmount+=available.getExtraChild();
				    				 extraInfantAmount+=available.getExtraInfant();
					    		 }
			    			 }	    		 
				    	 }
				    	 else{
				    		 amount += available.getBaseAmount();
				    		 extraAdultAmount+=available.getExtraAdult();
		    				 extraChildAmount+=available.getExtraChild();
		    				 extraInfantAmount+=available.getExtraInfant();
				    	 }
				    	 
			    	 }
			    	 if(blnPromotionIsNotFlag){
			    		 List<PropertyRate> propertyRateList = rateController.list(available.getAccommodationId(), pmsSource.getSourceId(), d.toDate());
				    	 System.out.println("propertyRateList.."+propertyRateList);
				    	// System.out.println("lisssssssssssssss"+propertyRateList.size());
				    	 if(propertyRateList.size()>0)
				    	 {
				    		 for (PropertyRate pr : propertyRateList)
			    			 {
					    		 int propertyRateId = pr.getPropertyRateId();
					    		 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
					    		 if(rateDetailList.size()>0){
					    			 for (PropertyRateDetail rate : rateDetailList){
					    				 amount +=  rate.getBaseAmount();
					    				 System.out.println("Offer" +amount);
					    				 extraAdultAmount+=rate.getExtraAdult();
					    				 extraChildAmount+=rate.getExtraChild();
					    				 extraInfantAmount+=rate.getExtraInfant();
					    			 }
					    		 }
					    		 else{
					    			 amount += available.getBaseAmount();
					    			 extraAdultAmount+=available.getExtraAdult();
				    				 extraChildAmount+=available.getExtraChild();
				    				 extraInfantAmount+=available.getExtraInfant();
					    		 }
			    			 }	    		 
				    	 }
				    	 else{
				    		 amount += available.getBaseAmount();
				    		 extraAdultAmount+=available.getExtraAdult();
		    				 extraChildAmount+=available.getExtraChild();
		    				 extraInfantAmount+=available.getExtraInfant();
				    	 }
			    	 }
			    	 dateCount++;
			    	 
			     }
			     jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";
			     
			     
				if(blnPromotionsFlag  && !blnPromotionIsNotFlag)
				{
					this.promotionsType=(String) sessionMap.get("promotionsType");
					this.promotionAccommodationId=(Integer)sessionMap.get("promotionAccommodationId");
					System.out.println("accom" +available.getAccommodationId());
					if(promotionsType.equalsIgnoreCase("L") && available.getAccommodationId()==promotionAccommodationId){
						this.lastAmount=(Double)sessionMap.get("lastAmount");
				    	this.lastExtraAdult=(Double)sessionMap.get("lastExtraAdult");
				    	this.lastExtraChild=(Double)sessionMap.get("lastExtraChild");
				    	totalAmount = lastAmount;
				    	if(dateCount>1){
					    	 totalExtraAdultAmount=Math.round(lastExtraAdult/dateCount);
					    	 totalExtraChildAmount=Math.round(lastExtraChild/dateCount);
					    	 totalExtraInfantAmount=0; 
					     }
					     else{
					    	 totalExtraAdultAmount=lastExtraAdult;
					    	 totalExtraChildAmount=lastExtraChild;
					    	 totalExtraInfantAmount=0;
					     }
				    	
				    	typePercent=mapPercentType.get("L");
				    	Integer intTypePercent=typePercent.intValue();
				    	jsonOutput += ",\"promotions\":\"" +intTypePercent+"% OFF" + "\"";
				    	
					}else if(promotionsType.equalsIgnoreCase("R") && available.getAccommodationId()==promotionAccommodationId ){
						this.roomAmount=(Double)sessionMap.get("roomAmount");
				    	this.roomExtraAdult=(Double)sessionMap.get("roomExtraAdult");
				    	this.roomExtraChild=(Double)sessionMap.get("roomExtraChild");
				    	this.getRooms=(Double)sessionMap.get("getRooms");
				    	this.bookRooms=(Double)sessionMap.get("bookRooms");
				    	totalAmount = roomAmount;
				    	if(dateCount>1){
					    	 totalExtraAdultAmount=Math.round(roomExtraAdult/dateCount);
					    	 totalExtraChildAmount=Math.round(roomExtraChild/dateCount);
					    	 totalExtraInfantAmount=0;
					     }
					     else{
					    	 totalExtraAdultAmount=roomExtraAdult;
					    	 totalExtraChildAmount=roomExtraChild;
					    	 totalExtraInfantAmount=0;
					     }
				    	jsonOutput += ",\"promotions\":\"Book "+bookRooms.intValue()+" Rooms Get "+getRooms.intValue()+"\"";
				    	
					}else if(promotionsType.equalsIgnoreCase("N") && available.getAccommodationId()==promotionAccommodationId){
						this.nightAmount=(Double)sessionMap.get("nightAmount");
				    	this.nightExtraAdult=(Double)sessionMap.get("nightExtraAdult");
				    	this.nightExtraChild=(Double)sessionMap.get("nightExtraChild");
				    	this.getNights=(Double)sessionMap.get("getNights");
				    	this.bookNights=(Double)sessionMap.get("bookNights");
				    	totalAmount = nightAmount;
				    	if(dateCount>1){
					    	 totalExtraAdultAmount=Math.round(nightExtraAdult/dateCount);
					    	 totalExtraChildAmount=Math.round(nightExtraChild/dateCount);
					    	 totalExtraInfantAmount=0;
					     }
					     else{
					    	 totalExtraAdultAmount=nightExtraAdult;
					    	 totalExtraChildAmount=nightExtraChild;
					    	 totalExtraInfantAmount=0;
					     }
				    	jsonOutput += ",\"promotions\":\"Book "+bookNights.intValue()+" Nights Get "+getNights.intValue()+"\"";
					}
					else if(promotionsType.equalsIgnoreCase("F")){
						this.flatAmount=(Double)sessionMap.get("flatAmount");
				    	this.flatExtraAdult=(Double)sessionMap.get("flatExtraAdult");
				    	this.flatExtraChild=(Double)sessionMap.get("flatExtraChild");
				    	totalAmount = flatAmount;
				    	if(dateCount>1){
					    	 totalExtraAdultAmount=Math.round(flatExtraAdult/dateCount);
					    	 totalExtraChildAmount=Math.round(flatExtraChild/dateCount);
					    	 totalExtraInfantAmount=0;
					     }
					     else{
					    	 totalExtraAdultAmount=flatExtraAdult;
					    	 totalExtraChildAmount=flatExtraChild;
					    	 totalExtraInfantAmount=0;
					     }
				    	typePercent=mapPercentType.get("F");
				    	Integer intTypePercent=typePercent.intValue();
				    	jsonOutput += ",\"promotions\":\"" + intTypePercent+"% OFF" + "\"";
					}
				}
				else if(blnPromotionIsNotFlag){
					totalAmount = amount;
				     if(dateCount>1){
				    	 totalExtraAdultAmount=Math.round(extraAdultAmount/dateCount);
				    	 totalExtraChildAmount=Math.round(extraChildAmount/dateCount);
				    	 totalExtraInfantAmount=Math.round(extraInfantAmount/dateCount);
				     }
				     else{
				    	 totalExtraAdultAmount=extraAdultAmount;
				    	 totalExtraChildAmount=extraChildAmount;
				    	 totalExtraInfantAmount=extraInfantAmount;
				     }
				}
				else{
					
					totalAmount = amount;
				     if(dateCount>1){
				    	 totalExtraAdultAmount=Math.round(extraAdultAmount/dateCount);
				    	 totalExtraChildAmount=Math.round(extraChildAmount/dateCount);
				    	 totalExtraInfantAmount=Math.round(extraInfantAmount/dateCount);
				     }
				     else{
				    	 totalExtraAdultAmount=extraAdultAmount;
				    	 totalExtraChildAmount=extraChildAmount;
				    	 totalExtraInfantAmount=extraInfantAmount;
				     }
				}
			    // totalAmount = amount/dateCount; sep2
				//ends here 
				
				//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
				
				jsonOutput += ",\"baseAmount\":\"" + (totalAmount==0 ? available.getBaseAmount() : totalAmount )+ "\"";
				jsonOutput += ",\"arrivalDate\":\"" + getArrivalDate()+ "\"";
				/*this.promotionsRoomNight=(String)sessionMap.get("promotionsRoomNight");
				if(promotionsRoomNight!=null)
				{
					if(promotionsRoomNight.equalsIgnoreCase("Nights")){
						this.nightDepartureDate=(Timestamp)sessionMap.get("nightDepartureDate");
						jsonOutput += ",\"departureDate\":\"" +nightDepartureDate+ "\"";
					}
				}else{
					jsonOutput += ",\"departureDate\":\"" + getDepartureDate()+ "\"";
				}*/
				
				jsonOutput += ",\"departureDate\":\"" + getDepartureDate()+ "\"";
				jsonOutput += ",\"size\":\"" +  available.getAreaSqft() + "\"";
				jsonOutput += ",\"rooms\":\"" + 1 + "\"";
				
				//jsonOutput += ",\"roomCount\":\"" + (available.getRoomCount() == null ? available.getNoOfUnits() : available.getRoomCount()) + "\"";
				//jsonOutput += ",\"roomCount\":\"" + available.getRoomCount() + "\"";
				
				
				/*if(promotionsRoomNight!=null)
				{
					if(promotionsRoomNight.equalsIgnoreCase("Rooms")){
						this.promotionsRoomCount=(Integer)sessionMap.get("promotionsRoomCount");
						jsonOutput += ",\"promotionsRoomCount\":\"" +promotionsRoomCount+ "\"";
					}
				}*/
				
				jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
				jsonOutput += ",\"accommodationType\":\"" + available.getAccommodationType() + "\"";
				jsonOutput += ",\"minOccupancy\":\"" + available.getMinOccupancy() + "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + available.getMaxOccupancy() + "\"";
				jsonOutput += ",\"noOfAdults\":\"" + available.getNoOfAdults() + "\"";
				jsonOutput += ",\"noOfChild\":\"" + available.getNoOfChild() + "\"";
				jsonOutput += ",\"extraAdult\":\"" + (totalExtraAdultAmount==0 ? available.getExtraAdult() : totalExtraAdultAmount ) + "\"";
				jsonOutput += ",\"extraChild\":\"" + (totalExtraChildAmount==0 ? available.getExtraChild() : totalExtraChildAmount )+ "\"";
				jsonOutput += ",\"extraInfant\":\"" + (totalExtraInfantAmount==0 ? available.getExtraInfant() : totalExtraInfantAmount )+ "\"";
				
				//shiva add next 2 lines
				/*PropertyTaxe propertyTaxe = taxController.findAccommodationTax(available.getAccommodationId());
                jsonOutput += ",\"tax\":\"" + propertyTaxe.getTaxAmount() + "\"";*/
				jsonOutput += ",\"tax\":\"" + 0 + "\"";
				String jsonAmenities ="";
				
				List<AccommodationAmenity> propertyAmenityList =  amenityController.list(available.getAccommodationId());
				if(propertyAmenityList.size()>0)
				{
					for (AccommodationAmenity amenity : propertyAmenityList) {
						if (!jsonAmenities.equalsIgnoreCase(""))
							
							jsonAmenities += ",{";
						else
							jsonAmenities += "{";
						//PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
						
						PmsAmenity pmsAmenity1 =ameController.find(amenity.getPmsAmenity().getAmenityId());
						jsonAmenities += "\"DT_RowId\":\"" + pmsAmenity1.getAmenityId()+ "\"";					
						jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
						jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
						jsonAmenities += ",\"icon\":\"" + iconPath + pmsAmenity1.getAmenityIcon()+ "\"";
						
						jsonAmenities += "}";
					}
					
					jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
				}
			
				 
				 //sessionMap.clear();
				//
				
				
				 long minimum = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),available.getAvailable(),available.getAccommodationId()); 
				
				System.out.println("");
				
				//System.out.println( this.roomCount);
				//shiva add next 2 lines
				
				
				jsonOutput += ",\"available\":\"" + minimum + "\"";
				//jsonOutput += ",\"available\":\"" + (roomCount == null ? available.getAvailable() : (available.getAvailable()-roomCount.getRoomCount() )) + "\"";
				jsonOutput += "}";
				
				
				
				
				

			}
           
            //System.out.println("mylist size" +myList.toString());
           // Optional<Integer> min = myList.stream().min(myList::compareTo);
           // System.out.println("Minimum : "+Collections.min(myList));
             
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			System.out.println("jsonOutput..."+jsonOutput);

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
   


 public Long getAvailableCount(int propertyId,Timestamp arrivalDate,Timestamp departureDate,long available,int accommodationId) throws IOException {
	System.out.println("get available count");
	// System.out.println(jobExecutionList.size());
	
	Calendar cal = Calendar.getInstance();
	cal.setTime(this.departureDate);
    cal.add(Calendar.DATE, -1);
	String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
	String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
	DateTime start = DateTime.parse(startDate);
    DateTime end = DateTime.parse(endDate);
    


		PmsBookingManager bookingController = new PmsBookingManager();
		
		
		List<DateTime> between = DateUtil.getDateRange(start, end);
		List<Long> myList = new ArrayList<Long>();
		
		long rmc = 0;
		this.roomCnt = rmc;
		
		
		
		for (DateTime d : between)
	     {
			java.util.Date availDate = d.toDate();
			System.out.println("hello hai" +d.toString());
			  
			       
				  PmsAvailableRooms roomCount = bookingController.findCount(availDate, accommodationId);
					
					//System.out.println(roomCount == null ? 0 : (roomCount.getRoomCount()));
					
					if(roomCount.getRoomCount() == null){
						
						this.roomCnt = (available-rmc);
						
		               
					}
					else{
						
						this.roomCnt = (available-roomCount.getRoomCount());
						
						
					}
					
					
					myList.add(this.roomCnt);
		        	
		        	
		        	
		        
			
			
			
			
			System.out.println("roomCount " + this.roomCnt);
				
	     
	     }
      
		
        System.out.println("Minimum : "+Collections.min(myList));
        long availableCount = Collections.min(myList);

        return availableCount;
          
	}
   
   
   public String getTypeSelected() throws IOException {
    	
	   try {
		   
		   String jsonOutput = "";
		   HttpServletResponse response = ServletActionContext.getResponse();
		
			
		   PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
	       response.setContentType("application/json");

	       sessionMap.put("types",types);
	       
    		for (int i = 0; i < types.size(); i++) 
  	      {
  			
    	   System.out.println("--->type");
  		   System.out.println(types.get(i).getAccommodationId());
  		   PropertyAccommodation accommodation = accommodationController.find(types.get(i).getAccommodationId());
  		   
  		   if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			
			else
				jsonOutput += "{";
  		    
  		    jsonOutput += "\"accommodationType\":\"" + accommodation.getAccommodationType() + "\"";
	        jsonOutput += ",\"baseAmount\":\"" + accommodation.getBaseAmount() + "\"";
			jsonOutput += ",\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
			jsonOutput += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy() + "\"";
			jsonOutput += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy() + "\"";
			jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults() + "\"";
			jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild() + "\"";
			jsonOutput += ",\"extraChild\":\"" + accommodation.getExtraChild() + "\"";
			jsonOutput += ",\"extraAdult\":\"" + accommodation.getExtraAdult() + "\"";
			
			jsonOutput += "}";
  		   
  		   
  	      }
			
    		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
    		
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return SUCCESS ;

    	
    }
   
   
	
	public String getBookings() throws IOException {
		System.out.println("get all bookings");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsBookingManager bookingController = new PmsBookingManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PmsBooking booking = bookingController.find(getBookingId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + booking.getBookingId() + "\"";
				jsonOutput += ",\"arrivalDate\":\"" + booking.getArrivalDate()+ "\"";
				jsonOutput += ",\"departureDate\":\"" + booking.getDepartureDate()+ "\"";
				jsonOutput += ",\"rooms\":\"" +booking.getRooms()+ "\"";
				jsonOutput += ",\"securityDeposit\":\"" + booking.getSecurityDeposit() + "\"";
				jsonOutput += ",\"totalAmount\":\"" + booking.getTotalAmount() + "\"";
				jsonOutput += ",\"totalRefund\":\"" + booking.getTotalRefund() + "\"";
				jsonOutput += ",\"totalTax\":\"" + booking.getTotalTax() + "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			sessionMap.clear();
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String addBooking() {
		
		PmsBookingManager bookingController = new PmsBookingManager();
		PmsStatusManager statusController = new PmsStatusManager();
		PmsSourceManager sourceController = new PmsSourceManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		PropertyTaxeManager taxController = new PropertyTaxeManager();
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		//UserLoginManager  userController = new UserLoginManager();
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		try {
			
						
			System.out.println("add booking");
			System.out.println(getArrivalDate());
			System.out.println(getDepartureDate());	
			System.out.println(getSourceId());	
			
			
			  for (int i = 0; i < array.size(); i++) 
		      {
				
			   System.out.println("<--taxAmount-->");
			  
			   PropertyAccommodation accommodation = propertyAccommodationController.find(array.get(i).getAccommodationId());
			   PropertyTaxe propertyTaxe = taxController.findAccommodationTax(array.get(i).getAccommodationId());
			   //System.out.println(array.get(i).getArrival());
			   this.arrivalDate = Timestamp.valueOf(array.get(i).getArrival());
			   this.departureDate = Timestamp.valueOf(array.get(i).getDeparture());	  
			  
			   totalRoomCount +=  array.get(i).getRooms();
			   totalAdultCount +=  array.get(i).getAdultsCount();
			   totalChildCount +=  array.get(i).getChildCount();
			   totalBaseAmount +=   array.get(i).getTotal();
			   totalTaxAmount +=   array.get(i).getTax();
			   statusId  =   array.get(i).getStatusId();
			   //sourceId  =  array.get(i).getSourceId();
			   sourceId  =  getSourceId();
			   System.out.println("%%%taxamount" +totalTaxAmount);
		      }
			  
			 // System.out.println("Total baseAmount "+ totalBaseAmount);
			 // System.out.println("arrival date "+ arrivalDatey);
			  System.out.println("statusId"+ getSourceId());
			  
			  
			  sessionMap.put("totalAdultCount",this.totalAdultCount);
			  
			  sessionMap.put("totalChildCount",this.totalChildCount);			  
			 
			  
			  this.arrivalDate = getArrivalDate();
			  sessionMap.put("arrivalDate",arrivalDate); 
			  System.out.println("arrival date/////////////////////////////"+this.arrivalDate);
			  this.departureDate = getDepartureDate();
			  sessionMap.put("departureDate",departureDate); 
			 // System.out.println( sessionMap.put("departureDate",departureDate));
			
			PmsBooking booking = new PmsBooking();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setArrivalDate(this.arrivalDate);
			booking.setDepartureDate(this.departureDate);
			//booking.setTotalTax(getTotalTax());
			booking.setSecurityDeposit(getSecurityDeposit());
			booking.setTotalAmount(totalBaseAmount);
			booking.setIsActive(true);
			booking.setIsDeleted(false);
			booking.setRooms(totalRoomCount);
			booking.setTotalTax(totalTaxAmount);
			PmsSource source = sourceController.find(sourceId);
			booking.setPmsSource(source);
			PmsProperty property = propertyController.find(getPropertyId());
			booking.setPmsProperty(property);
			PmsStatus status = statusController.find(statusId);
			booking.setPmsStatus(status);
			
			
			PmsBooking book = bookingController.add(booking);
			
			
			
			this.bookingId = book.getBookingId();
			sessionMap.put("bookingId",bookingId);
			
			this.bookingId = (Integer) sessionMap.get("bookingId");
			System.out.println("<--booking id-->");
			System.out.println(getBookingId());
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String editBooking() {
		PmsBookingManager bookingController = new PmsBookingManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("edit customer");
			PmsBooking guest = bookingController.find(getBookingId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setArrivalDate(getArrivalDate());
			booking.setDepartureDate(getDepartureDate());
			booking.setIsActive(true);
			booking.setIsDeleted(false);
			booking.setRooms(rooms);
			
			bookingController.edit(booking);
		
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editBookingStatus() {
		PmsBookingManager bookingController = new PmsBookingManager();
		BookingDetailManager detailController = new BookingDetailManager();
		PmsStatusManager statusController = new PmsStatusManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("edit booking status"+getBookingId());
			System.out.println("edit status Id "+getStatusId());			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			if(getStatusId() == 3){
			PmsBooking booking = bookingController.find(getBookingId());
			
			//Date date1 = new Date();			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
			String formattedDate = sdf.format(date);
			//System.out.println("arrival Date"+formattedDate);
			 java.util.Date dailyDate = sdf.parse(formattedDate);
			 Timestamp tsDate=new Timestamp(dailyDate.getTime());
			System.out.println("arrival Date"+tsDate);
			//System.out.println("arrival Date"+booking.getArrivalDate());
			
			booking.setArrivedDate(tsDate);
			PmsStatus status = statusController.find(getStatusId());
			booking.setPmsStatus(status);
			
			bookingController.edit(booking);
			
			}
			else if(getStatusId() == 5) {
				System.out.println("edit Departure date ");
				PmsBooking booking = bookingController.find(getBookingId());
				
				//Date date1 = new Date();			
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
				String formattedDate = sdf.format(date);
				//System.out.println("arrival Date"+formattedDate);
				 java.util.Date dailyDate = sdf.parse(formattedDate);
				 Timestamp tsDate=new Timestamp(dailyDate.getTime());
				System.out.println("Departure Date"+tsDate);
				
				booking.setDeparturedDate(tsDate);
				PmsStatus status = statusController.find(getStatusId());
				booking.setPmsStatus(status);
				
				bookingController.edit(booking);
				
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	
	public String deleteBooking() {
		PmsBookingManager bookingController = new PmsBookingManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("delete booking");
			PmsBooking booking = bookingController.find(getBookingId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setIsActive(false);
			booking.setIsDeleted(true);
			booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			bookingController.edit(booking);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String cancelBooking() {
		
		PmsBookingManager bookingController = new PmsBookingManager();
		PmsStatusManager statusController = new PmsStatusManager();
		UserLoginManager  userController = new UserLoginManager();
		 
		try {
			
						
			System.out.println("delete booking");
			PmsBooking booking = bookingController.find(getBookingId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setIsActive(false);
			booking.setIsDeleted(true);
			booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			PmsStatus status = statusController.find(4);
			booking.setPmsStatus(status);
			bookingController.edit(booking);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getArrival() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		//System.out.println("arrival");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsBookingManager bookingController = new PmsBookingManager();		
			response.setContentType("application/json");
			 DashBoard Arrival = bookingController.findArrivalCount(getPropertyId(),todayDate);
			//System.out.println(bookingList);
			//for (PmsBooking arrival : bookingList) 

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += "{";
				else
					jsonOutput += "{";
				//jsonOutput += "\"DT_RowId\":\"" + arrival.getBookingId() + "\"";
				//jsonOutput += ",\"arrivalDate\":\"" + arrival.getArrivalDate()+ "\"";
				
				jsonOutput += "\"arrivals\":\"" + (Arrival == null ? 0 : (Arrival.getTodayArrival() )) + "\"";
				jsonOutput += "}";

			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			//System.out.println(jsonOutput);

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	@SuppressWarnings("unchecked")
	public String getDeparture() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		//System.out.println("arrival");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsBookingManager bookingController = new PmsBookingManager();		
			response.setContentType("application/json");
			 DashBoard departure =  bookingController.findDepartureCount(getPropertyId(),todayDate);
			//System.out.println(bookingList);
			//for (DashBoard departure : dashBoardList) 

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += "{";
				else
					jsonOutput += "{";
				//jsonOutput += "\"DT_RowId\":\"" + arrival.getBookingId() + "\"";
				//jsonOutput += ",\"arrivalDate\":\"" + arrival.getArrivalDate()+ "\"";
				
				
				jsonOutput += "\"departures\":\"" + (departure == null ? 0 : (departure.getTodayDeparture() )) + "\"";
				jsonOutput += "}";

			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			//System.out.println(jsonOutput);

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getTodayOccupancy() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		//System.out.println("arrival");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsBookingManager bookingController = new PmsBookingManager();		
			response.setContentType("application/json");
			 DashBoard occupancy =  bookingController.findOccupancyCount(getPropertyId(),todayDate);
			//System.out.println(bookingList);
		//	for (DashBoard occupancy : dashBoardList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += "{";
				else
					jsonOutput += "{";
				//jsonOutput += "\"DT_RowId\":\"" + arrival.getBookingId() + "\"";
				//jsonOutput += ",\"arrivalDate\":\"" + arrival.getArrivalDate()+ "\"";
				
				jsonOutput += "\"occupancy\":\"" + (occupancy == null ? 0 : (occupancy.getTodayOccupancy() )) + "\"";

			
				jsonOutput += "}";

			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			//System.out.println(jsonOutput);
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAllBookings() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		System.out.println("all bookings report");
		//System.out.println(propertyId);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		 //System.out.println(todayDate);
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsBookingManager bookingController = new PmsBookingManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.availableList = bookingController.listAllBooking(getPropertyId(),todayDate);
			//System.out.println(bookingList);
			for (PmsAvailableRooms booking : availableList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + booking.getBookingId() + "\"";
				jsonOutput += ",\"arrivalDate\":\"" + booking.getArrivalDate()+ "\"";
				jsonOutput += ",\"departureDate\":\"" + booking.getDepartureDate()+ "\"";
				jsonOutput += ",\"rooms\":\"" +booking.getRooms()+ "\"";
				PmsSource pmsSource = sourceController.find(booking.getSourceId());
				jsonOutput += ",\"sourceId\":\"" + pmsSource.getSourceName() + "\"";
				//jsonOutput += ",\"totalAmount\":\"" + booking.getTotalAmount() + "\"";
				//jsonOutput += ",\"totalRefund\":\"" + booking.getTotalRefund() + "\"";
				//jsonOutput += ",\"totalTax\":\"" + booking.getTotalTax() + "\"";
				
				jsonOutput += "}";

			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			//System.out.println(jsonOutput);

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
public String getBookingchart() throws IOException, ParseException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		
		System.out.println(chartDate);
		
		System.out.println("Booking Chart");
		
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	
		
		BookingDetailManager bookingDetailController = new BookingDetailManager();
		response.setContentType("application/json");
	
		for (int i=0; i<chartDate.split(",").length; i++)
		{
			
			//System.out.println(chartDate.split(",")[i]);
		 
		
//		try {
			//ifchartDate.split(",")[i]
			

			this.bookingDetailList = bookingDetailController.list(getPropertyId(),chartDate.split(",")[i]);
			
			
			//System.out.println(bookingDetailList.size());
	     
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			
			else
				jsonOutput = "{";
			
			    jsonOutput += "\"size\":\"" +bookingDetailList.size()+ "\"";
			    
		        jsonOutput += "}";
			
	//		if(bookingDetailList.size()>=0)
	   //   {
			
			/*for (BookingDetail bookingDetail : bookingDetailList) {

				System.out.println(bookingDetailList.size());
				
			}*/

			

		//} catch (Exception e) {
		//	logger.error(e);
		//	e.printStackTrace();
		//} finally {

		//}
	//	}
		        
		}
		
		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		//System.out.println(jsonOutput);
		
		return null;
		
	}
	
public String getBookingCalendar() throws IOException, ParseException {
	
	this.propertyId = (Integer) sessionMap.get("propertyId");
	
	
	System.out.println(chartDate);
	
	System.out.println("Booking calendar");
	System.out.println("accommodationid"+ getAccommodationId());
	String jsonOutput = "";
	HttpServletResponse response = ServletActionContext.getResponse();

	
	PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
    AccommodationRoom accommodationRoom =  roomController.findCount(getPropertyId(),getAccommodationId());
	BookingDetailManager bookingDetailController = new BookingDetailManager();
	response.setContentType("application/json");

	for (int i=0; i<chartDate.split(",").length; i++)
	{
		
		
		

		this.bookingDetailList = bookingDetailController.list(getPropertyId(),chartDate.split(",")[i]);
		
		
		//System.out.println(bookingDetailList.size());
     
		if (!jsonOutput.equalsIgnoreCase(""))
			jsonOutput += ",{";
		
		else
			jsonOutput = "{";
		
		   // jsonOutput += "\"size\":\"" +bookingDetailList.size()+ "\"";
		    //jsonOutput += ",\"roomcount\":\"" +accommodationRoom.getRoomCount()+ "\"";
		    jsonOutput += "\"title\":\"" +" Available: "+(accommodationRoom.getRoomCount()-bookingDetailList.size())+" / Sold: "+bookingDetailList.size()+ "\"";
		    //jsonOutput += "\"title\":\"" +" Available: "+(accommodationRoom.getRoomCount()-bookingDetailList.size())+"\\n"+"  Sold: "+bookingDetailList.size()+ "\"";
		    //jsonOutput += ",\"description\":\"" +(accommodationRoom.getRoomCount()-bookingDetailList.size())+ "\"";
		    jsonOutput += ",\"start\":\"o\"";
		    
	        jsonOutput += "}";
		

	        
	}
	
	response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	//System.out.println(jsonOutput);
	
	return null;
	
}
  
  public String updateInventory() {
		
		try {
			
		  System.out.println("updateinventory" +getStrDate());
		  
		// String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(getStrDate());
		   sessionMap.put("reserveDate",getStrDate());
		  //DateTime checkIn = DateTime.parse(getStrDate());
		  //System.out.println("checkin" +checkIn);
	      //this.arrivalDate = getArrivalDate();
		 // sessionMap.put("arrivalDate",arrivalDate);				
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
  
	 public String getPropertyAccommodation( ) throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		Date date = new Date();// Now use today date.
		String startDate = sdf.format(date.getTime());
		cal.add(Calendar.DATE, 14); // Adding 14 days
		String endDate = sdf.format(cal.getTime());
		System.out.println("property Accommodation list");
	
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			
			response.setContentType("application/json");

	        this.dashBoardList =  accommodationController.listPropertyAccommodation(getPropertyId());
			
			//System.out.println(accommodationList);
			//System.out.println(accommodationList.size());
			
			for (DashBoard dashboard : dashBoardList) {
       
				//System.out.println("accommodation id");
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"accommodationId\":\"" + dashboard.getAccommodationId() + "\"";
				jsonOutput += ",\"total\":\"" + dashboard.getTotals()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" +dashboard. getAccommodationType()+ "\"";
				
				DashBoard roomCount  = bookingDetailController.findCountAccommodation(startDate,endDate,dashboard.getAccommodationId());
		  	 // jsonOutput += ",\"roomCounts\":\"" + roomCount+ "\"";
		  	   jsonOutput += ",\"roomCounts\":\"" + (roomCount == null ? 0 : (roomCount.getRoomCount() )) + "\"";

               //System.out.println(roomCount);
		  	   //System.out.println("roomCounts"+roomCount);
				jsonOutput += "}";

			}
		
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		  	//System.out.println(jsonOutput);

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}    
	 
	 public String getPropertyRevenue() throws IOException {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			//System.out.println("PropertyRevenue");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			Date date = new Date();// Now use today date.
			String startDate = sdf.format(date.getTime());
			cal.add(Calendar.DATE, 14); // Adding 14 days
			String endDate = sdf.format(cal.getTime());
			
			try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				
				PmsBookingManager bookingController = new PmsBookingManager();
				response.setContentType("application/json");
				this.dashBoardList = bookingController.listRevenue(getPropertyId(),startDate,endDate);
				//System.out.println(bookingList);
				for (DashBoard booking : dashBoardList) {

					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += "{";
					else
						jsonOutput += "{";
					//jsonOutput += "\"DT_RowId\":\"" + booking.getBookingId() + "\"";
					//jsonOutput += ",\"arrivalDate\":\"" + booking.getArrivalDate()+ "\"";
					//jsonOutput += ",\"departureDate\":\"" + booking.getDepartureDate()+ "\"";
					jsonOutput += "\"totalRooms\":\"" +booking.getTotalRooms()+ "\"";
					//jsonOutput += ",\"securityDeposit\":\"" + booking.getSecurityDeposit() + "\"";
					jsonOutput += ",\"totalAmount\":\"" + booking.getTotalRevenue() + "\"";
					//tots +=  booking.getTotalRooms();
					//jsonOutput += ",\"tots\":\"" + tots + "\"";
					//jsonOutput += ",\"totalRefund\":\"" + booking.getTotalRefund() + "\"";
					//jsonOutput += ",\"totalTax\":\"" + booking.getTotalTax() + "\"";
					
					jsonOutput += "}";

				}
				
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				//System.out.println(jsonOutput);

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		}
	 
	 
	 public String getPropertyBookingList() throws IOException {
			System.out.println("PropertyBookingList");
		    this.propertyId = (Integer) sessionMap.get("propertyId");
			 System.out.println(propertyId);
			try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
			
				
				PmsBookingManager bookingController = new PmsBookingManager();
				//this.familyRegisterList = familyController.list(getUser().getUserid());
				//model = familyRegisterList;
				response.setContentType("application/json");

				this.bookingList = bookingController.listBookingreport(getPropertyId(),getArrivalDate(),getDepartureDate());
				for (PmsBooking booking : bookingList) {

					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"DT_RowId\":\"" + booking.getBookingId() + "\"";
					jsonOutput += ",\"arrivalDate\":\"" + booking.getArrivalDate()+ "\"";
					jsonOutput += ",\"departureDate\":\"" + booking.getDepartureDate()+ "\"";
					jsonOutput += ",\"rooms\":\"" +booking.getRooms()+ "\"";
					//jsonOutput += ",\"securityDeposit\":\"" + booking.getSecurityDeposit() + "\"";
					jsonOutput += ",\"totalAmount\":\"" + booking.getTotalAmount() + "\"";
					//jsonOutput += ",\"totalRefund\":\"" + booking.getTotalRefund() + "\"";
					//jsonOutput += ",\"totalTax\":\"" + booking.getTotalTax() + "\"";
					
					jsonOutput += "}";

				}

				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return null;

		}   
	 
	 
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	
	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}
	
	public Long getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}
	
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
	
	public Integer getTax() {
		return tax;
	}

	public void setTax(Integer tax) {
		this.tax = tax;
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
    
	public double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}
	
	public double getSecurityDeposit() {
		return securityDeposit;
	}

	public void setSecurityDeposit(double securityDeposit) {
		this.securityDeposit = securityDeposit;
	}
	
	public  String getChartDate() {
		return chartDate;
	}

	public void setChartDate(String chartDate) {
		this.chartDate = chartDate;
	}
	
	public PmsBooking getBooking() {
		return booking;
	}

	public void setBooking(PmsBooking booking) {
		this.booking = booking;
	}

	public List<PmsBooking> getBookingList() {
		return bookingList;
	}

	public void setBookingList(List<PmsBooking> bookingList) {
		this.bookingList = bookingList;
	}
	
	public List<PropertyAccommodation> getAccommodationList() {
		return accommodationList;
	}

	public void setAccomadationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}
	
	public List<PmsAvailableRooms> getAvailableList() {
		return availableList;
	}

	public void setAvailableList(List<PmsAvailableRooms> availableList) {
		this.availableList = availableList;
	}
	
    public List<BookingListAction> getArray() {
	        return array;
	 }
	    
	 public void setArray(List<BookingListAction> array) {
	        this.array = array;
	  }
		
	 public List<BookingListAction> getTypes() {
	        return types;
	 }
	    
	 public void setTypes(List<BookingListAction> types) {
	        this.types = types;
	  }
	 
	 public String getStrDate() {
			return strDate;
	  }

	 public void setStrDate(String strDate) {
			this.strDate = strDate;
	  }
	 
	 public String getAccommodationTypeId() {
			return accommodationTypeId;
		}

		public void setAccommodationTypeId(String accommodationTypeId) {
			this.accommodationTypeId = accommodationTypeId;
		}

	 public Integer getCurrentHours(Date date,int lastMinuteHours){
    	 Calendar calendar=Calendar.getInstance();
    	 LocalTime currentTime = LocalTime.now();
    	 int currentHours = currentTime.getHour();
    	 
    	
 		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
 		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
 		
    	 DateTime start = DateTime.parse(startDate);
         DateTime end = DateTime.parse(endDate);
         java.util.Date loginDate = start.toDate();
 		 java.util.Date fromDate = end.toDate();
 		
         long diff = fromDate.getTime() - loginDate.getTime();
         
         long diffHours = diff / (60 * 60 * 1000);
         int diffInDays = (int) ((fromDate.getTime() - loginDate.getTime()) / (1000 * 60 * 60 * 24));
    	 int diffInHours=(int)diffHours;
    	 int gethours=lastMinuteHours-currentHours;
    	 int totalHours=0;
    	 if(diffInDays>1){
    		 totalHours= diffInDays*diffInHours;
    		 gethours=lastMinuteHours-currentHours;
    	 }
    	 else if(diffInDays==1){
    		 gethours=lastMinuteHours-currentHours;
    	 }
		 return gethours;
	 }
	 
	 public Boolean bookGetRooms(int availableCount,Double bookRoom,Double getRoom){
		 Double totalGetRooms=0.0;
	     totalGetRooms=bookRoom+getRoom;
	     Integer intTotalGetRooms=totalGetRooms.intValue();
	    	if(intTotalGetRooms<=availableCount){
	    		sessionMap.put("promotionsRoomCount", intTotalGetRooms);
	   	     	sessionMap.put("promotionsRoomNight", "Rooms");
	    		 return true;
	    	}
	    	else{
	    		 return false;
	    	}

		
	 }
	 
	 public Boolean bookGetNights(int availableCount, Double getNight,Double bookNight){
		 Double totalGetNights=0.0;
	     Integer intTotalGetNights=totalGetNights.intValue();
	     if(intTotalGetNights<=availableCount){
    		 return true;
    	}
    	else{
    		 return false;
    	}
	 }
}
