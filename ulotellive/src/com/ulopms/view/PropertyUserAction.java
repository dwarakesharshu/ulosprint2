package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;


















import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.view.UserAddAction;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyItemManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.PropertyUserManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.PropertyUser;
import com.ulopms.model.Role;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;





public class PropertyUserAction extends ActionSupport implements 
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer userId;
	private Integer propertyUserId;
	private Integer roleId;
	private String emailId;
	private String userName;
	public Integer propertyId;
	
	
	private PropertyUser propertyUser;
	
	private PropertyUser userProperty;
	
	private List<PropertyUser> propertyUserList;
	
	private List<PropertyUser> userPropertyList;
	
	private List<User> userList;
   
	private List<PmsProperty> propertyList;
	
	private static final Logger logger = Logger.getLogger(PropertyUserAction.class);

    private User user;
    
	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
    
	
	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyUserAction() {

	}
	

	public String execute() {

		this.propertyId = (Integer) sessionMap.get("propertyId");
		//this.patientId = getPatientId();
		return SUCCESS;
	}
	
	
    
public String getSingleUser() throws IOException {
		
		System.out.println(getPropertyUserId());
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyUserManager propertyUserController = new PropertyUserManager();
			UserLoginManager userController = new UserLoginManager();
			RoleManager roleController = new RoleManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
             
			System.out.println(getPropertyUserId());
			
			PropertyUser propertyUser =  propertyUserController.find(getPropertyUserId());
		    User us = userController.find(propertyUser.getUser().getUserId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"userName\":\"" + us.getUserName() + "\"";
				//jsonOutput += "\"userId\":\"" + us.getUserId() + "\"";
				jsonOutput += ",\"propertyUserId\":\"" + propertyUser.getPropertyUserId() + "\"";
				jsonOutput += ",\"emailId\":\"" + us.getEmailId()+ "\"";
				jsonOutput += ",\"roleId\":\"" + us.getRole().getRoleId()+ "\"";
				Role rl = roleController.find(us.getRole().getRoleId());
				jsonOutput += ",\"roleName\":\"" + rl.getRoleName()+ "\"";
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
    public String getProperties() throws IOException {
		
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyUserManager propertyUserController = new PropertyUserManager();
			UserLoginManager userController = new UserLoginManager();
			RoleManager roleController = new RoleManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.userPropertyList = propertyUserController.listProperty(getUserId());
			for (PropertyUser userProperty : userPropertyList) {
               
				//System.out.println("family register");
				//System.out.println(userProperty.getPropertyUserId());
				
				 // User user1 = userController.find(userProperty.getUser().getUserId());
				 // Role role1 = roleController.find(userProperty.getRole().getRoleId());
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				 jsonOutput += "\"propUserId\":\"" + userProperty.getPropertyUserId() + "\"";
				 jsonOutput += ",\"propertyId\":\"" + userProperty.getPmsProperty().getPropertyId() + "\"";
				 jsonOutput += ",\"propertyName\":\"" + userProperty.getPmsProperty().getPropertyName() + "\"";
				 
				 
				
				
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

    public String changeProperty() throws IOException {
    	
    	//System.out.println("change Property");
    	//System.out.println(getPropertyId());
    	
    	sessionMap.put("propertyId",getPropertyId());
    	
    	PmsPropertyManager propertyController = new PmsPropertyManager();
    	PmsProperty  property = propertyController.find(getPropertyId());
    	
    	//System.out.println("change Property name");
    	
    	sessionMap.put("propertyName", property.getPropertyName());
    	
    	
    	
    	//session.getValue("pu)
        //sessionMap.get("propertyId");
    	
      
        //System.out.println("propId");
        //System.out.println(this.propertyId);
        
    	return SUCCESS;
    	
    }
	
	public String getUsers() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyUserManager propertyUserController = new PropertyUserManager();
			UserLoginManager userController = new UserLoginManager();
			RoleManager roleController = new RoleManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyUserList = propertyUserController.list(getPropertyId());
			for (PropertyUser propertyUser : propertyUserList) {
               
				//System.out.println("family register");
				//System.out.println(propertyUser.getPropertyUserId());
				
				  User user1 = userController.find(propertyUser.getUser().getUserId());
				  Role role1 = roleController.find(propertyUser.getRole().getRoleId());
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				 jsonOutput += "\"propUserId\":\"" + propertyUser.getPropertyUserId() + "\"";
				 jsonOutput += ",\"userId\":\"" + user1.getUserId()+ "\"";
				 jsonOutput += ",\"userName\":\"" + user1.getUserName()+ "\"";
				 jsonOutput += ",\"emailId\":\"" + user1.getEmailId()+ "\"";
				 jsonOutput += ",\"roleId\":\"" + propertyUser.getRole().getRoleName()+ "\"";
				 jsonOutput += ",\"roleName\":\"" + propertyUser.getRole().getRoleName()+ "\"";
				//PropertyUser pu = propertyUserController.find(propertyUser.getRole().getRoleId());
				//jsonOutput += ",\"roleName\":\"" + role1.getRoleName()+ "\"";
				
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String addUser() {

		//PmsGuestManager guestController = new PmsGuestManager();
		this.propertyId = (Integer) sessionMap.get("propertyId");

		PropertyUserManager propertyUserController = new PropertyUserManager();
		UserLoginManager userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();

		try {

		UserAddAction addUser = new UserAddAction();

		System.out.println("property action entry");
		System.out.println(getUserName());
		System.out.println(getEmailId());
		System.out.println(getRoleId());

		String result = addUser.UserAdd(getUserName(),getEmailId(),getRoleId());

		this.userList = userController.findUserByEmail(getEmailId());
		PropertyUser propertyUser = new PropertyUser();

		for (User us : userList) {

		// check userid if it is in property table
		System.out.println("user Iddddddd"+us.getUserId());
		System.out.println("property Iddddddd======="+getPropertyId());
		//PropertyUser searchPropertyUser = propertyUserController.find(us.getUserId());
		List<PropertyUser> propertyUserList = propertyUserController.list(getPropertyId(),us.getUserId() );

		if(propertyUserList.size()>0){

		System.out.println("already exist");

		}
		else{
		//System.out.println(us.getUserId());

		long time = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(time);

		propertyUser.setUser(us);
		propertyUser.setRole(us.getRole());
		propertyUser.setIsActive(true);
		propertyUser.setIsDeleted(false);
		PmsProperty property = propertyController.find(getPropertyId());
		propertyUser.setPmsProperty(property);
		propertyUserController.add(propertyUser);

		}

		}

		//PmsGuest guest = new PmsGuest();
		//long time = System.currentTimeMillis();
		//java.sql.Date date = new java.sql.Date(time);
		//PmsGuest guest1 = guestController.add(guest);


		} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
		} finally {

		}
		return SUCCESS;
		}
	
	public String editUser() {
		
		 
		PropertyUserManager propertyUserController = new PropertyUserManager();
	    UserLoginManager  userController = new UserLoginManager();
	    PmsPropertyManager propertyController = new PmsPropertyManager();
	    RoleManager roleController = new RoleManager();
		try {
			
			System.out.println(getEmailId());			
			System.out.println(getUserName());
			System.out.println(getUserId());
			//PmsGuest guest = guestController.find(getGuestId());
			
			
			System.out.println(getPropertyUserId());
			
			System.out.println(getRoleId());
			
            PropertyUser propertyUser = propertyUserController.find(getPropertyUserId());
            Role role1 = roleController.find(getRoleId());
            User user =  userController.find(propertyUser.getUser().getUserId());
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			propertyUser.setRole(role1);
			propertyUserController.edit(propertyUser);
			user.setEmailId(getEmailId());
			user.setUserName(getUserName());
			userController.edit(user);
			//userController.edit(user);
			//propertyUser.setRole(getRoleId());
			
			
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteUser() {
		PropertyUserManager propertyUserController = new PropertyUserManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("delete customer");
			PropertyUser propertyUser = propertyUserController.find(getPropertyUserId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			propertyUser.setIsActive(false);
			propertyUser.setIsDeleted(true);
			propertyUser.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			propertyUserController.edit(propertyUser);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getPropertyUserId() {
		return propertyUserId;
	}

	public void setPropertyUserId(Integer propertyUserId) {
		this.propertyUserId = propertyUserId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public PropertyUser getPropertyUser() {
		return propertyUser;
	}

	public void setPropertyUser(PropertyUser propertyUser) {
		this.propertyUser = propertyUser;
	}

	public List<PropertyUser> getPropertyUserList() {
		return propertyUserList;
	}

	public void setPropertyUserList(List<PropertyUser> propertyUserList) {
		this.propertyUserList = propertyUserList;
	}
	
	
	
}
