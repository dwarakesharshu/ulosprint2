package com.ulopms.view;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;

//import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;








import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.User;
import com.ulopms.util.Email;

public class DashboardAction extends ActionSupport implements 
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	
	
	private Object model;

	

	private static final Logger logger = Logger.getLogger(DashboardAction.class);

	private Integer propertyId;
	private User user;

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  

	

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public DashboardAction() {

	}

	public String execute() {
		
	
	
	
	
	try {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsPropertyManager propertyController = new PmsPropertyManager();
		PmsProperty property = propertyController.find(this.propertyId);
		Timestamp todayDate = new Timestamp(System.currentTimeMillis());
		String currDate = new SimpleDateFormat("yyyy-MM-dd").format(todayDate);
		String fromDate = new SimpleDateFormat("yyyy-MM-dd").format(property.getTaxValidFrom());
	    String toDate = new SimpleDateFormat("yyyy-MM-dd").format(property.getTaxValidTo());
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    Date FromDate = sdf.parse(fromDate);
	    Date ToDate = sdf.parse(toDate);
	    Date CurrDate = sdf.parse(currDate);
        Date date2 = sdf.parse(toDate);
        int comparedVal1 = CurrDate.compareTo(FromDate);
        int comparedVal2 = CurrDate.compareTo(ToDate);
        
        
        if(comparedVal1 >= 0 && comparedVal2 <=0 ){
        
            System.out.println("Taxactive");	
            property.setTaxIsActive(true);
	        propertyController.edit(property);
         
        }
        
        else{
        
        	 System.out.println("Tax Inactive");
        	 property.setTaxIsActive(false);
 		     propertyController.edit(property);
        	
        }
		
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   
   
		return SUCCESS;
	}

	
	

}
