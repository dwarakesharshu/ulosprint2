package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;































import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyItemManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyDiscountAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	private Integer propertyDiscountId;
	private Integer propertyId;
	private String  discountName;
	private String  discountType;
	private Timestamp startDate;
	private Timestamp endDate;
	private Integer discountUnits;
	private Boolean isSignup;
	private double discountPercentage;
    

	private double discountInr;
	
	
	private PropertyDiscount discount;
	
	private List<PropertyDiscount> DiscountList;
	
	private List<PropertyDiscount> SignupActiveList;
	

	

	private static final Logger logger = Logger.getLogger(PropertyDiscountAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
	

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyDiscountAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	
/*	public String getTaxes() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		 //System.out.println(getPropertyId());
		//System.out.println("all categories");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.taxeList = taxController.list(getPropertyId());
			for (PropertyTaxe taxe : taxeList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"taxId\":\"" + taxe.getPropertyTaxId() + "\"";
				jsonOutput += ",\"taxName\":\"" + taxe.getTaxName()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}  
	
	public String getDiscount() throws IOException {
		
		
		System.out.println(getPropertyDiscountId());
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyItemManager itemController = new PropertyItemManager();
			PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
			PropertyTaxeManager taxController =  new PropertyTaxeManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PropertyItem item = itemController.find(getPropertyItemId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"itemId\":\"" + item.getPropertyItemId() + "\"";
				jsonOutput += ",\"itemCode\":\"" + item.getItemCode()+ "\"";
				jsonOutput += ",\"itemName\":\"" + item.getItemName()+ "\"";
				jsonOutput += ",\"itemDescription\":\"" + item.getItemDescription()+ "\"";
				jsonOutput += ",\"itemSku\":\"" + item.getSku()+ "\"";
				jsonOutput += ",\"itemAmount\":\"" + item.getAmount()+ "\"";
				jsonOutput += ",\"propertyItemCategoryId\":\"" + item.getPropertyItemCategory().getPropertyItemCategoryId()+ "\"";
				PropertyItemCategory category =  categoryController.find(item.getPropertyItemCategory().getPropertyItemCategoryId());
				jsonOutput += ",\"propertyItemCategoryName\":\"" + category.getCategoryName()+ "\"";
					
				jsonOutput += ",\"propertyTaxeId\":\"" + item.getPropertyTaxe().getPropertyTaxId()+ "\"";
				PropertyTaxe tax = taxController.find(item.getPropertyTaxe().getPropertyTaxId());
				jsonOutput += ",\"propertyTaxeName\":\"" + tax.getTaxName()+ "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}      */
	
	public String getDiscount() throws IOException {
		//System.out.println(getPropertyDiscountId());
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

			PmsPropertyManager propertyController = new PmsPropertyManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PropertyDiscount discount = propertyDiscountController.find(getPropertyDiscountId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discount.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discount.getDiscountName()+ "\"";
				String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(discount.getStartDate());
				String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(discount.getEndDate());
				//jsonOutput += ",\"arrivalDate\":\"" + checkIn + "\"";
				//jsonOutput += ",\"departureDate\":\"" + checkOut+ "\"";
				jsonOutput += ",\"startDate\":\"" + checkIn + "\"";
				jsonOutput += ",\"endDate\":\"" + checkOut + "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discount.getDiscountPercentage()+ "\"";
				jsonOutput += ",\"discountUnits\":\"" + discount.getDiscountUnits()+ "\"";
				jsonOutput += ",\"isSignup\":\"" + discount.getIsSignup()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discount.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	
	
	public String getDiscounts() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		// System.out.println("shiva"+getPropertyId());
		//System.out.println("all categories");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			
			response.setContentType("application/json");

			this.DiscountList = propertyDiscountController.list(getPropertyId());
			for (PropertyDiscount discounts : DiscountList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
				jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
				jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
				jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
				jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}   
	
	
public String getResortDiscount() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		System.out.println("all categories"+todayDate);
		
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		// System.out.println("==="+getPropertyId());
		//System.out.println("all categories");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			
			response.setContentType("application/json");

			this.DiscountList = propertyDiscountController.listresort(getPropertyId(),todayDate);
			for (PropertyDiscount discounts : DiscountList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
				jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
				jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
				//jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
				jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
				jsonOutput += ",\"isResort\":\"" + discounts.getIsResort()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				System.out.println("========================="+discounts.getDiscountPercentage());
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}   
	
	
      public String getCurrentDiscounts() throws IOException {
		
		
		
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		
		
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//Date currentDate = sdf.parse(currDate);
		
		// System.out.println("shiva"+getPropertyId());
		//System.out.println("all categories");
		// System.out.println(jobExecutionList.size());
		try {
			
			Timestamp todayDate = new Timestamp(System.currentTimeMillis());
			String currDate = new SimpleDateFormat("yyyy-MM-dd").format(todayDate);
			Date currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(currDate);

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		    
			response.setContentType("application/json");

			this.DiscountList = propertyDiscountController.currentDiscounts(currentDate);
			for (PropertyDiscount discounts : DiscountList) {
            
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
				jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
				jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
				jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
				jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	} 
	

	public String addDiscount() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		
		try {
			
			//getIsLogin()
			
			this.DiscountList = propertyDiscountController.findCount(getDiscountName());
			this.SignupActiveList = propertyDiscountController.findActiveCount();
			
			//this.DiscountList = propertyDiscountController.findCount(getDiscountName());
		
			
			
			if(this.DiscountList.size() >= 1){
				
				System.out.println("Name already exist");
			   
				return null;
			}
			
			else{
			
			//System.out.println("dsd"+getIsSignup());
			//System.out.println("activelistsize"+this.SignupActiveList.size());
		    
		    if((getIsSignup() == true) && (this.SignupActiveList.size()>=1)){
		    
		    //System.out.println("signup already exist");
		    
		    return null; 
		    
		    }		
				
		    else{
		    	
		   
			System.out.println("add item");
			PropertyDiscount discount = new PropertyDiscount();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			/*System.out.println("discount id"+getPropertyId());
			System.out.println(getAccommodationId());
			System.out.println(getDiscountName());
			System.out.println(getEndDate());
			System.out.println(getPercentage());*/
			
			discount.setDiscountName(getDiscountName());
			discount.setIsActive(true);
			discount.setIsDeleted(false);
			discount.setIsSignup(getIsSignup());
			discount.setStartDate(getStartDate());
			discount.setEndDate(getEndDate());
			discount.setDiscountPercentage(getPercentage());
			discount.setDiscountUnits(getDiscountUnits());
			PmsProperty property = propertyController.find(getPropertyId());
			discount.setPmsProperty(property);
			//PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(getAccommodationId());
			//discount.setPropertyAccommodation(propertyAccommodation);
			
			
			
			propertyDiscountController.add(discount);
			
			return SUCCESS;
			
		    }
			
			}
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
		
	}
	
	
	

	public String editDiscount() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		System.out.println("edit discount"+getPropertyDiscountId());
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		try {
			
				
			System.out.println("edit discount");
			PropertyDiscount editdis = propertyDiscountController.find(getPropertyDiscountId());
			//PropertyDiscount nameCount = propertyDiscountController.findCount(getDiscountName());
			this.DiscountList = propertyDiscountController.findCount(getDiscountName());
			this.SignupActiveList = propertyDiscountController.findActiveCount();
			System.out.println(this.DiscountList.size());
			/*if(nameCount.getNameCount() >= 1){
			
			}*/
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			if(this.DiscountList.size() < 1){
			editdis.setDiscountName(getDiscountName());
			}
			editdis.setIsActive(true);
			editdis.setIsDeleted(false);
			if((getIsSignup() == true) && (this.SignupActiveList.size()< 1)){
			editdis.setIsSignup(getIsSignup());
			}
			if((getIsSignup() == false)){
			editdis.setIsSignup(getIsSignup());
			}
			editdis.setStartDate(getStartDate());
			editdis.setEndDate(getEndDate());
			editdis.setDiscountPercentage(getDiscountPercentage());	
			editdis.setDiscountUnits(getDiscountUnits());	
			PmsProperty property = propertyController.find(getPropertyId());
			editdis.setPmsProperty(property);
			//PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(getAccommodationId());
			//editdis.setPropertyAccommodation(propertyAccommodation);
			propertyDiscountController.edit(editdis);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		
		return SUCCESS;
	}
	
	
   public String deleteDiscount() {
		
	   PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		try {
			
						
			System.out.println("delete discount");
			
			//System.out.println(getPropertyDiscountId());
			
			PropertyDiscount deletedis = propertyDiscountController.find(getPropertyDiscountId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			deletedis.setIsActive(false);
			deletedis.setIsDeleted(true);
			deletedis.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			propertyDiscountController.edit(deletedis);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}  

	
   public String applyDiscount() throws IOException {
		//System.out.println(getPropertyDiscountId());
		// System.out.println(jobExecutionList.size());
	   
	   Timestamp todayDate = new Timestamp(System.currentTimeMillis());
	   System.out.println("disName" +getDiscountName());
	   System.out.println("currdate" + todayDate);
	   
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

			PmsPropertyManager propertyController = new PmsPropertyManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

		     //PropertyDiscount discount = propertyDiscountController.findName(getDiscountName(),todayDate);
			
		     //System.out.println(discount.getDiscountName());
			
			this.DiscountList = propertyDiscountController.verifyDiscount(getDiscountName(),todayDate);
			for (PropertyDiscount discounts : DiscountList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
				jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
				jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	

	public Integer getPropertyDiscountId() {
		return propertyDiscountId;
	}

	public void setPropertyDiscountId(Integer propertyDiscountId) {
		this.propertyDiscountId = propertyDiscountId;
	}

	/*public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}*/

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	
	public String getDiscountName() {
		return discountName;
	}

	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}
	
	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}


	public double getPercentage() {
		return discountPercentage;
	}

	public void setPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	
	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getDiscountInr() {
		return discountInr;
	}

	public void setDiscountInr(double discountInr) {
		this.discountInr = discountInr;
	}
	
	public Integer getDiscountUnits() {
			return discountUnits;
		}

	public void setDiscountUnits(Integer discountUnits) {
			this.discountUnits = discountUnits;
		}
	
	public Boolean getIsSignup() {
		return isSignup;
	}

	public void setIsSignup(Boolean isSignup) {
		this.isSignup = isSignup;
	}
	
	
	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	
	public List<PropertyDiscount> getDiscountList() {
		return DiscountList;
	}

	public void setBookingDetailList(List<PropertyDiscount> DiscountList) {
		this.DiscountList = DiscountList;
	}
	
	public List<PropertyDiscount> getSignupActiveList() {
		return SignupActiveList;
	}

	public void setSignupActiveList(List<PropertyDiscount> signupActiveList) {
		SignupActiveList = signupActiveList;
	}
	
	



}
