package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
//import org.json.JSONArray;
//import org.json.JSONObject;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;







import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import jdk.nashorn.internal.parser.JSONParser;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class BookingListAction extends ActionSupport implements
		ServletRequestAware, UserAware {

	private static final long serialVersionUID = 914982665758390091L;

	private Integer accommodationId;
	private String accommodationType;
	private String arrival;
	private String departure;
	private Integer adultsAllow;
	private Integer adultsCount;
	private double baseAmount;
	private Integer childAllow;
	private Integer childCount;
	private Integer infantCount;
	private Integer extraInfant;
    private Integer extraChild;
	private Integer extraAdult;
	private Integer minOccu;
	private double refund;
	private Integer rooms;
	public Integer statusId;
	public Integer sourceId;
	public double tax;
	private double total;
	private double randomNo;
	public Integer available;
	public Integer roomId;
    
	private BookingDetail bookingDetail;
    
	
	//private List<BookingListAction> data;
	
	private String jsonData;
	
	// TimeZone timeZone = TimeZone.getDefault();

	// private String data;

	// JSONObject jsonObj = new JSONObject(data);

	// JSONObject json = (JSONObject) new JSONParser().parse("json_data");

	private static final Logger logger = Logger
			.getLogger(BookingListAction.class);

	private User user;

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookingListAction() {

	}

	public String execute() {
		System.out.println(getJsonData());
		return SUCCESS;
	}
	
	
public String addBookingDetails() {
		
		
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			System.out.println("jsonstring");
			//System.out.println(getJsonData());
		    String a = getJsonData().toString();
			
		    
		    if(a!=null){
		    System.out.println(a);	
			//JSONObject jsonObj = new JSONObject(a);
		    }
			//List<String> list = new ArrayList<String>();
			//JSONArray array = jsonObj.getJSONArray("roomsbooked");
			//for(int i = 0 ; i < array.length() ; i++){
			  //  list.add(array.getJSONObject(i).getString("accommodationType"));
			//}
				
			//System.out.println("accomdation: " + jsonObj.getString("accommodationType"));
			//System.out.println("accomdationID: " + jsonObj.getInt("accommodationId"));
			
			//JSONObject json = (JSONObject)new JSONParser().parse(jsonObj);
			/*System.out.println(data.size());
	        for (int i = 0; i < data.size(); i++) {
	                System.out.println("Data  " + data.get(i).getChildCount() + "-" + data.get(i).getRefund());
	         }
	          
	         */  
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}

	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getRefund() {
		return refund;
	}

	public void setRefund(double refund) {
		this.refund = refund;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getAccommodationType() {
		return accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}

	public Integer getAdultsAllow() {
		return adultsAllow;
	}

	public void setAdultsAllow(Integer adultsAllow) {
		this.adultsAllow = adultsAllow;
	}

	public Integer getAdultsCount() {
		return adultsCount;
	}

	public void setAdultsCount(Integer adultsCount) {
		this.adultsCount = adultsCount;
	}

	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Integer getChildAllow() {
		return childAllow;
	}

	public void setChildAllow(Integer childAllow) {
		this.childAllow = childAllow;
	}

	public Integer getExtraChild() {
		return extraChild;
	}

	public void setExtraChild(Integer extraChild) {
		this.extraChild = extraChild;
	}

	public Integer getExtraAdult() {
		return extraAdult;
	}

	public void setExtraAdult(Integer extraAdult) {
		this.extraAdult = extraAdult;
	}

	public Integer getMinOccu() {
		return minOccu;
	}

	public void setMinOccu(Integer minOccu) {
		this.minOccu = minOccu;
	}

	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}
	
	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
	
	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public BookingDetail getBookingDetail() {
		return bookingDetail;
	}

	public void setBookingDetail(BookingDetail bookingDetail) {
		this.bookingDetail = bookingDetail;
	}

	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		
		this.arrival = arrival;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}
    
	
	 public double getRandomNo() {
			return this.randomNo;
	   }

     public void setRandomNo(double randomNo) {
			this.randomNo = randomNo;
	   }
	
     public Integer getAvailable() {
 		return available;
 	}

 	public void setAvailable(Integer available) {
 		this.available = available;
 	}
 	
 	
	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	public Integer getExtraInfant() {
		return extraInfant;
	}

	public void setExtraInfant(Integer extraInfant) {
		this.extraInfant = extraInfant;
	}
 	
	/*public List < BookingListAction > getData() {
	 * 
	 * 
	 * 
	        System.out.println("Getter Call");
	        return data;
	}
	 
	  public void setData(List < BookingListAction > data) {
	        System.out.println("Setter Call Flow");
	        this.data = data;
	  } 
	  */
	
	  public String getJsonData() {
			return jsonData;
		}

		public void setJsonData(String jsonData) {
			this.jsonData = jsonData;
		}

}
