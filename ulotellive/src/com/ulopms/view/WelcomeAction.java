package com.ulopms.view;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.User;
 
public class WelcomeAction extends ActionSupport implements UserAware {
 
   // private static final long serialVersionUID = 8111120314704779336L;
	private static final Logger logger = Logger.getLogger(WelcomeAction.class);
    private User user;
	
	//@Override
    //public User getModel() {
    //    return user;
    //}

	@Override
	public void setUser(User user) {
		this.user = user;
	}
	public User getUser() {
		return user;
	}
    public String execute(){
    	
    	return SUCCESS;
    }
     
  
    
}