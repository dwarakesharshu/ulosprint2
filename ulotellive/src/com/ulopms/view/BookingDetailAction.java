package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
























import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
//import org.json.JSONObject;












import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ntp.TimeStamp;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;







































import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.view.BookingListAction;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.BookedDetail;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsRoomDetail;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class BookingDetailAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer bookingDetailsId;
	private Integer accommodationId;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private Integer adultCount;
	private Integer childCount;
	private double amount;
	private double totalAmount;
	private double refund;
	public Integer statusId;
	public double tax;
	private Integer bookingId;
	private Integer guestId;
	private Integer roomId;
	private String propertyName;
	private String firstName;
	private String lastName;
	private String phone;
	private String emailId;
	private Integer rooms;
	private Integer days;
	private Integer discountId;
	
	
	
	


	private long adults;
	private long child;
	private long infant;
	private boolean mailFlag;
	int totalAdultCount = 0;
	int totalInfantCount = 0;
	int totalChildCount = 0;
	
	



	private BookingDetail bookingDetail;
	
	 private List<PmsBookedDetails> bookedList;
	 
	 private List<PmsRoomDetail> roomList;
	 
	 List<BookingListAction> array = new ArrayList<BookingListAction>();

     public List<BookingListAction> getArray() {
         return array;
     }
     
     public void setArray(List<BookingListAction> array) {
         this.array = array;
     }
	
    
	
	private List<BookingDetail> bookingDetailList;
	
	private List<BookedDetail> bookList;
	
	private List<PropertyAccommodation> accommodationList;

	private static final Logger logger = Logger.getLogger(BookingDetailAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookingDetailAction() {

	}

	public String execute() {

		
		return SUCCESS;
	}

	
	 public String viewBookingDetails() {
 		
 		
 		try {
 			
 			
 			long time = System.currentTimeMillis();
 			java.sql.Date date = new java.sql.Date(time);
 			
 			this.bookingId = (Integer) sessionMap.get("bookingId");
			this.guestId = (Integer) sessionMap.get("guestId");
			
			System.out.println(getBookingId());
			System.out.println(getGuestId());
 			
 			System.out.println("araylist");
 			System.out.println(array.size());
 			
 	        for (int i = 0; i < array.size(); i++) {
 	                //System.out.println("Data  " + array.get(i).getAccommodationType() + "-" + array.get(i).getRefund());
 	        	System.out.println("accommodation Type  " + array.get(i).getAccommodationType());
 	        	System.out.println("accommodation Id  " + array.get(i).getAccommodationId());
 	        	System.out.println("adultsCount  " + array.get(i).getAdultsCount());
 	        	System.out.println("childCount  " + array.get(i).getChildCount());
 	        	System.out.println("refund  " + array.get(i).getRefund());
 	        	System.out.println("StatusId  " + array.get(i).getStatusId());
 	        	System.out.println("Tax  " + array.get(i).getTax());
 	         }
 	           
 			
 			
 			
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}
 		return SUCCESS;
 	}
  
	 public String getBookedDetails() throws IOException {
			
		    System.out.println(getBookingId());
		    
			System.out.println("get all bookings");
		
			 try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
			
				PmsPropertyManager propertyController = new PmsPropertyManager();
				PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
				BookingDetailManager bookingDetailController = new BookingDetailManager();
				BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
				PmsGuestManager guestController = new PmsGuestManager();
				PmsStatusManager statusController = new PmsStatusManager();
				PmsBookingManager bookingController = new PmsBookingManager();
				
				response.setContentType("application/json");
				this.bookedList = bookingDetailController.bookedList(getBookingId());
				
				StringBuilder accommodationType = new StringBuilder();
				
				StringBuilder accommodationRate = new StringBuilder();
				
				this.totalAdultCount = (Integer) sessionMap.get("totalAdultCount");
				
				//this.totalInfantCount = (Integer) sessionMap.get("totalInfantCount");
				
				this.totalChildCount = (Integer) sessionMap.get("totalChildCount");
				
	            for (PmsBookedDetails booked : bookedList) {
	               
	            	
	            	//System.out.println("bookingId -->" + getBookingId());
	            	
	            	
	            	
	            	//System.out.println("propertyName -->" + pmsProperty.getPropertyName());
	            	this.bookingId = getBookingId();
	            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
	            	this.propertyName = pmsProperty.getPropertyName();
	            	this.arrivalDate = booked.getArrivalDate();
	            	this.departureDate = booked.getDepartureDate();
	            	PmsGuest guest = guestController.find(booked.getGuestId());
	            	this.firstName = guest.getFirstName();
	            	this.lastName = guest.getLastName();
	            	this.phone = guest.getPhone();
	            	this.emailId = guest.getEmailId();
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(booked.getAccommodationId());
	            	System.out.println("Accommodation -->" + accommodation.getAccommodationType());
	            	//this.accommodationType = accommodation.getAccommodationType();
	            	accommodationType.append(accommodation.getAccommodationType().trim());
	            	accommodationType.append(",");
	            	this.bookList =  bookingDetailController.list(getBookingId(),booked.getAccommodationId());
	            	
	            	for (BookedDetail bookedDetail : bookList) {
	            		
	            	System.out.println("detail amount -->" + bookedDetail.getAmount());
	            	accommodationRate.append(bookedDetail.getAmount());
	            	accommodationRate.append(",");
	            	System.out.println("detail accommodation -->"+accommodationRate);
	            	//this.amount = bookedDetail.getAmount();
	            	//System.out.println("detail accommodation -->" + bookingDetail.getPropertyAccommodation().getAccommodationId());
	            		}
	            		
	            	this.rooms = booked.getRooms();
	            	this.adults = this.totalAdultCount;
	            	this.infant = this.totalInfantCount;
	            	this.child = this.totalChildCount;
	            	this.tax = booked.getTax();
	            	this.amount = booked.getAmount();
	            	this.totalAmount = this.tax + this.amount;
	            	//System.out.println("this.totalamount -->" + this.totalAmount);
	            	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
					String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(getDepartureDate());
					
					LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
	                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
	                this.days = Days.daysBetween(date1, date2).getDays();
	                //System.out.println("days" +this.days);
	            	/*System.out.println("Arrival -->" + booked.getArrivalDate());
	            	System.out.println("Departure -->" + booked.getDepartureDate());
	            	System.out.println("guestId -->" + booked.getGuestId());
	            	PmsGuest guest = guestController.find(booked.getGuestId());
	            	System.out.println("firstName -->" + guest.getFirstName());
	            	System.out.println("lastName -->" + guest.getLastName());
	            	System.out.println("mobile -->" + guest.getPhone());
	            	System.out.println("email -->" + guest.getEmailId());
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(booked.getAccommodationId());
					System.out.println("accommodationType -->" + accommodation.getAccommodationType());
	            	System.out.println("rooms -->" + booked.getRooms());
	            	System.out.println("tax -->" + booked.getTax());
	            	System.out.println("amount -->" + booked.getAmount());
	            	System.out.println("<---------------------->");
				    accommodationType.append(accommodation.getAccommodationType());
	            	accommodationType.append(",");
	            	System.out.println("accommodationType -->" + accommodationType.toString());*/

				}
	            
	            
	            
	          /*  System.out.println("bookingId -->" + getBookingId());
	            System.out.println("propertyName -->" + this.propertyName);
	            System.out.println("Arrival -->" + this.arrivalDate);
         	System.out.println("Departure -->" + this.departureDate);
         	System.out.println("firstName -->" + this.firstName);
         	System.out.println("lastName -->" + this.lastName);
         	System.out.println("mobile -->" + this.phone);
         	System.out.println("email -->" + this.emailId);
         	System.out.println("accommodationType -->" + accommodationType.toString());
         	System.out.println("rooms -->" + this.rooms);
         	System.out.println("adults -->" + this.adults);
         	System.out.println("child -->" + this.child);
         	System.out.println("tax -->" + this.tax);
         	System.out.println("amount -->" +  this.amount);
         	*/
         	
         	if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput += "{";
         	System.out.println("name1" +this.firstName);	
				//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
				jsonOutput += "\"bookingId\":\"" + getBookingId() + "\"";
				jsonOutput += ",\"propertyName\":\"" + this.propertyName + "\"";
				String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
				String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(getDepartureDate());
				jsonOutput += ",\"arrivalDate\":\"" + checkIn + "\"";
				jsonOutput += ",\"departureDate\":\"" + checkOut+ "\"";
				LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
                int days = Days.daysBetween(date1, date2).getDays();
                System.out.println("days" +days);
                jsonOutput += ",\"days\":\"" + days+ "\"";
				jsonOutput += ",\"firstName\":\"" + this.firstName+ "\"";
				jsonOutput += ",\"lastName\":\"" + this.lastName+ "\"";
				jsonOutput += ",\"phone\":\"" + this.phone+ "\"";
				jsonOutput += ",\"emailId\":\"" + this.emailId+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodationType+ "\"";
				jsonOutput += ",\"rooms\":\"" + this.rooms+ "\"";
				jsonOutput += ",\"adultCount\":\"" + this.adults+ "\"";
				jsonOutput += ",\"childCount\":\"" + this.child+ "\"";
				//jsonOutput += ",\"infantCount\":\"" + this.infant+ "\"";
				jsonOutput += ",\"tax\":\"" + this.tax+ "\"";
				jsonOutput += ",\"amount\":\"" + this.amount+ "\"";
				jsonOutput += ",\"totalAmount\":\"" + this.totalAmount+ "\"";
				jsonOutput += ",\"accommodationRate\":\"" + accommodationRate + "\"";
				
			/*	String jsonTypes ="";
				
				
				if(bookedList.size()>0)
				{
					for (PmsBookedDetails booked : bookedList) {
						if (!jsonTypes.equalsIgnoreCase(""))
							
							jsonTypes += ",{";
						else
							jsonTypes += "{";
						PropertyAccommodation accommodation = propertyAccommodationController.find(booked.getAccommodationId());
						jsonTypes += "\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
						
						PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
						jsonTypes += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
						jsonTypes += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
						jsonTypes += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
						jsonTypes += ",\"icon\":\"" + pmsAmenity1.getAmenityIcon()+ "\"";
						
						jsonTypes += "}";
					}
					
					jsonOutput += ",\"types\":[" + jsonTypes+ "]";
				}*/
				
				jsonOutput += "}";
				
	            if(this.mailFlag == true)
	            {
	            	try
	 				{
	            		 System.out.println("name2" +this.firstName);	
	 				//email template
	 				Configuration cfg = new Configuration();
	 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
	 				Template template = cfg.getTemplate(getText("reservation.template"));
	 				Map<String, String> rootMap = new HashMap<String, String>();	 				
	 				rootMap.put("bookingid",getBookingId().toString());
	 				rootMap.put("propertyname",this.propertyName);
	 				rootMap.put("guestname",this.firstName);
	 				//rootMap.put("guestname",this.firstName + " " + this.lastName);
	 				rootMap.put("mobile",this.phone);
	 				rootMap.put("emailId",this.emailId);	 				
	 				rootMap.put("checkin",checkIn);
	 				rootMap.put("days",this.days.toString());
	 				rootMap.put("checkout",checkOut);
	 				rootMap.put("accommodationRate",accommodationRate.toString());
	 			/*	for (PmsBookedDetails booked : bookedList) {
	 				PropertyAccommodation accommodation = propertyAccommodationController.find(booked.getAccommodationId());
	 				
	 				System.out.println("Accommodation2 -->" + accommodation.getAccommodationType());
	 				}*/
	 				rootMap.put("accommodationtype", accommodationType.toString());
	 				rootMap.put("rooms", this.rooms.toString());
	 				rootMap.put("adults", String.valueOf(this.adults));
	 				rootMap.put("child",String.valueOf(this.child));
	 				rootMap.put("infant", String.valueOf(this.infant));
	 				rootMap.put("tax",String.valueOf(this.tax));
	 				rootMap.put("amount",String.valueOf(this.amount));
	 				rootMap.put("totalamount",String.valueOf(this.totalAmount));
	 				
	 				rootMap.put("from", getText("notification.from"));
	 				Writer out = new StringWriter();
	 				template.process(rootMap, out);

	 				//send email
	 				Email em = new Email();
	 				em.set_to(this.emailId);
	 				em.set_cc(getText("reservation.notification.email"));
	 				em.set_cc2(getText("bookings.notification.email"));
	 				em.set_from(getText("email.from"));
	 				//em.set_host(getText("email.host"));
	 				//em.set_password(getText("email.password"));
	 				//em.set_port(getText("email.port"));
	 				em.set_username(getText("email.username"));
	 				em.set_subject(getText("reservation.subject"));
	 				em.set_bodyContent(out);
	 				em.send();		
	 				}
	 				catch(Exception e1)
	 				{
	 					e1.printStackTrace();
	 				}
	 		     
	            }
	            

				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		}
	  
	  
	  
	  
	public String getBookingDetails() throws IOException {
		System.out.println("get all booking  details");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			BookingDetail bookingDetail = bookingDetailController.find(getBookingDetailsId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + bookingDetail.getBookingDetailsId() + "\"";
				jsonOutput += ",\"accommodatioId\":\"" + bookingDetail.getPropertyAccommodation().getAccommodationId()+ "\"";
				jsonOutput += ",\"adultCount\":\"" + bookingDetail.getAdultCount()+ "\"";
				jsonOutput += ",\"childCount\":\"" +bookingDetail.getChildCount()+ "\"";
				jsonOutput += ",\"amount\":\"" + bookingDetail.getAmount() + "\"";
				jsonOutput += ",\"refund\":\"" + bookingDetail.getRefund() + "\"";
				jsonOutput += ",\"statusId\":\"" + bookingDetail.getPmsStatus().getStatusId() + "\"";
				jsonOutput += ",\"tax\":\"" + bookingDetail.getTax() + "\"";
				jsonOutput += ",\"bookingId\":\"" + bookingDetail.getPmsBooking().getBookingId() + "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAllBookingDetails() throws IOException {
		System.out.println("all booking  details");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.bookingDetailList = bookingDetailController.list();
			for (BookingDetail bookingDetail : bookingDetailList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + bookingDetail.getBookingDetailsId() + "\"";
				jsonOutput += ",\"accommodatioId\":\"" + bookingDetail.getPropertyAccommodation().getAccommodationId()+ "\"";
				jsonOutput += ",\"adultCount\":\"" + bookingDetail.getAdultCount()+ "\"";
				jsonOutput += ",\"childCount\":\"" +bookingDetail.getChildCount()+ "\"";
				jsonOutput += ",\"amount\":\"" + bookingDetail.getAmount() + "\"";
				jsonOutput += ",\"refund\":\"" + bookingDetail.getRefund() + "\"";
				jsonOutput += ",\"statusId\":\"" + bookingDetail.getPmsStatus().getStatusId() + "\"";
				jsonOutput += ",\"tax\":\"" + bookingDetail.getTax() + "\"";
				jsonOutput += ",\"bookingId\":\"" + bookingDetail.getPmsBooking().getBookingId() + "\"";
				
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

public String addBookingDetails() {  
	 System.out.println("Discount*Id-------------"+getDiscountId());

        PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
        PropertyAccommodationRoomManager propertyAccommodationRoomController = new PropertyAccommodationRoomManager();     
        BookingDetailManager bookingDetailController = new BookingDetailManager();
        BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
        PmsGuestManager guestController = new PmsGuestManager();
        PmsStatusManager statusController = new PmsStatusManager();
        PmsBookingManager bookingController = new PmsBookingManager();
        PropertyTaxeManager taxController = new PropertyTaxeManager();
        PropertyDiscountManager discountController = new PropertyDiscountManager();
        //UserLoginManager  userController = new UserLoginManager();
        try {           

            System.out.println("add booking entry");
            Calendar cal = Calendar.getInstance();    
            long time = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(time);         
           
            this.bookingId = (Integer) sessionMap.get("bookingId");
            this.guestId = (Integer) sessionMap.get("guestId");         
            System.out.println("<!--Date in Final-->");
            this.arrivalDate = (Timestamp) sessionMap.get("arrivalDate");
            this.departureDate = (Timestamp) sessionMap.get("departureDate");     

            cal.setTime(this.departureDate);
            cal.add(Calendar.DATE, -1);
            System.out.println(arrivalDate);
            //System.out.println(cal.getTime()); 
            String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
            String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            System.out.println("final end date"+endDate);
            /*String startDate = this.arrivalDate.toString();
            String endDate =   this.departureDate.toString();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            //String datey = this.arrivalDate;  
            java.util.Date sd = format.parse(startDate);
            java.util.Date ed = format.parse(endDate);
            //String sd = sd.substring(0, this.startDate.indexOf('.'));     

            DateTime start = DateTime.parse(sd.toString());
            System.out.println("Start: " + start);
            DateTime end = DateTime.parse(sd.toString());
            System.out.println("End: " + end);

            */
            DateTime start = DateTime.parse(startDate);
            System.out.println("Start: " + start);
            DateTime end = DateTime.parse(endDate);
            System.out.println("End: " + end);
            //System.out.println(getBookingId());
            //System.out.println(getGuestId());
            BookingGuestDetail bookingGuestDetail = new BookingGuestDetail();
            PmsBooking bookingg = bookingController.find(getBookingId());
            bookingGuestDetail.setPmsBooking(bookingg);
            PmsGuest guest = guestController.find(getGuestId());
            bookingGuestDetail.setPmsGuest(guest);
            bookingGuestDetail.setIsPrimary(true);
            bookingGuestDetail.setIsActive(true);
            bookingGuestDetail.setIsDeleted(false);
            bookingGuestDetailController.add(bookingGuestDetail);
            System.out.println("add booking");
            BookingDetail bookingDetail = new BookingDetail();
            java.util.Date arrival = start.toDate();
            java.util.Date departure = end.toDate();
            /*  double totalBaseAmount = 0;
              Timestamp arrivalDatey = null;
              for (int i = 0; i < array.size(); i++)
              {
               System.out.println("<--baseamount-->");
               PropertyAccommodation accommodation = propertyAccommodationController.find(array.get(i).getAccommodationId());

               //System.out.println(array.get(i).getArrival());
               arrivalDatey = Timestamp.valueOf(array.get(i).getArrival());
               //System.out.println(array.get(i).getDeparture());
               totalBaseAmount +=  accommodation.getBaseAmount();              

              }
              System.out.println("Total baseAmount "+ totalBaseAmount);
              System.out.println("arrival date "+ arrivalDatey);
              

              */
            // PmsRoomDetail roomDetail = bookingDetailController.findRoom(arrival, departure,16);
            // System.out.println(roomDetail.getRoomId());
            // this.roomList = bookingDetailController.roomList(arrival, departure,array.get(i).getAccommodationId());
            // for (PmsBookedDetails booked : bookedList) {}            

             /* for (int i = 0; i < array.size(); i++) 

              {

               PmsRoomDetail roomDetail = bookingDetailController.findRoom(arrival, departure,array.get(i).getAccommodationId());

               System.out.println("roomId");
               array.get(i).setRoomId(roomDetail.getRoomId());
               System.out.println(array.get(i).getRoomId());
               //System.out.println(array.get(i).getAccommodationId());
              
               //System.out.println(roomDetail.getRoomId());


              }*/             

             List<DateTime> between = getDateRange(start, end);
             for (DateTime d : between)
             {

             // DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
             //String bookingDate = fmt.(d);               

             //System.out.println(d.toString()); 
             //DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
             //format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
              java.util.Date bookingDate=  d.toDate();
              for (int i = 0; i < array.size(); i++) 
              {

                    PropertyAccommodation accommodation = propertyAccommodationController.find(array.get(i).getAccommodationId());
                    PropertyTaxe propertyTaxe = taxController.findAccommodationTax(array.get(i).getAccommodationId());                 

                    bookingDetail.setAdultCount(array.get(i).getAdultsCount());
                    bookingDetail.setAmount(array.get(i).getTotal());
                    bookingDetail.setChildCount(array.get(i).getChildCount());
                    System.out.println(""+array.get(i).getChildCount());
                    bookingDetail.setRefund(array.get(i).getRefund());
                    bookingDetail.setTax(array.get(i).getTax());  
                    bookingDetail.setBookingDate(bookingDate);
                    bookingDetail.setIsActive(true);
                    bookingDetail.setIsDeleted(false);
                    PmsStatus status = statusController.find(array.get(i).getStatusId());
                    bookingDetail.setPmsStatus(status);
                    PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(array.get(i).getAccommodationId());

                    bookingDetail.setPropertyAccommodation(propertyAccommodation);
                    PmsBooking booking = bookingController.find(getBookingId());
                    bookingDetail.setPmsBooking(booking);
                    bookingDetail.setRandomNo(array.get(i).getRandomNo());
                    bookingDetail.setInfantCount(array.get(i).getInfantCount());
                    bookingDetail.setRoomCount(array.get(i).getRooms());
                    if(getDiscountId() != null){
                    PropertyDiscount discount = discountController.find(getDiscountId());
                    bookingDetail.setPropertyDiscount(discount);
                    }
                   
                    //PmsRoomDetail roomDetail = bookingDetailController.findRoom(arrival, departure,array.get(i).getAccommodationId());
                    //PropertyAccommodationRoom accommodationRoom = propertyAccommodationRoomController.find(roomDetail.getRoomId());
                    //bookingDetail.setPropertyAccommodationRoom(accommodationRoom);
                    //bookingDetail.set(roomDetail.getRoomId());
                    bookingDetailController.add(bookingDetail);
                    
                    }       

                    

             }            

             //send email         

             //SendConfirmation();        

             //ends here
             this.setMailFlag(true);         

              getBookedDetails(); 

            /*String jsonOutput = "";
            HttpServletResponse response = ServletActionContext.getResponse();

            response.setContentType("application/json");

            if (!jsonOutput.equalsIgnoreCase(""))

                jsonOutput += ",{";

            else

                jsonOutput += "{";

            

            jsonOutput += "\"bookingId\":\"" + getBookingId() + "\"";          
            jsonOutput += ",\"guestId\":\"" + getGuestId()+ "\"";
            jsonOutput += ",\"guestName\":\"" + guest.getFirstName()+ "\"";
            jsonOutput += ",\"guestNumber\":\"" + guest.getPhone()+ "\"";
            jsonOutput += ",\"guestEmail\":\"" + guest.getEmailId()+ "\"";
            jsonOutput += ",\"rooms\":\"" + array.get(i).getAdultsCount() + "\"";
            jsonOutput += ",\"adults\":\"" + array.get(i).getAdultsCount() + "\"";
            jsonOutput += ",\"child\":\"" + array.get(i).getChildCount()+ "\"";
            jsonOutput += ",\"tax\":\"" + array.get(i).getChildCount()+ "\"";
            jsonOutput += ",\"total\":\"" + array.get(i).getTotal()+ "\"";
            jsonOutput += "}";
            response.getWriter().write("{\"data\":[" + jsonOutput + "]}");*/           

        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        } finally {

        }
        return null;

    }
	
	public String editBookingDetails() {
		
		BookingDetailManager bookingDetailController = new BookingDetailManager();
		PmsStatusManager statusController = new PmsStatusManager();
		PmsBookingManager bookingController = new PmsBookingManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("edit customer");
			BookingDetail bookingDetail = bookingDetailController.find(getBookingDetailsId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			bookingDetail.setAdultCount(getAdultCount());
			bookingDetail.setAmount(getAmount());
			bookingDetail.setChildCount(getChildCount());
			bookingDetail.setRefund(getRefund());
			bookingDetail.setTax(getTax());
			bookingDetail.setIsActive(true);
			bookingDetail.setIsDeleted(false);
			PmsStatus status = statusController.find(getStatusId());
			bookingDetail.setPmsStatus(status);
			PmsBooking booking = bookingController.find(getBookingId());
			bookingDetail.setPmsBooking(booking);
			
			bookingDetailController.edit(bookingDetail);
		
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	
	public String editBookingDetailsStatus() {
		PmsBookingManager bookingController = new PmsBookingManager();
		BookingDetailManager detailController = new BookingDetailManager();
		PmsStatusManager statusController = new PmsStatusManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("edit booking details status"+getBookingId());
			System.out.println("edit status details Id "+getStatusId());
			
			if(getStatusId() == 3){
				System.out.println("edit Arrival Date");
			this.bookingDetailList = detailController.findId(getBookingId());
			
			 for (BookingDetail prop : bookingDetailList)
			 {			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			PmsStatus status = statusController.find(getStatusId());
			prop.setPmsStatus(status);
			
			/*booking.setStatusId(getStatusId());
			booking.setDepartureDate(getDepartureDate());
			booking.setIsActive(true);
			booking.setIsDeleted(false);
			booking.setRooms(rooms);*/
			
			detailController.edit(prop);
			
			 
			 }
			}
			else if(getStatusId() == 5){
				System.out.println("edit Departure Date");
				this.bookingDetailList = detailController.findId(getBookingId());
				
				 for (BookingDetail prop : bookingDetailList)
				 {			
				
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				
				
				PmsStatus status = statusController.find(getStatusId());
				prop.setPmsStatus(status);
				
				/*booking.setStatusId(getStatusId());
				booking.setDepartureDate(getDepartureDate());
				booking.setIsActive(true);
				booking.setIsDeleted(false);
				booking.setRooms(rooms);*/
				
				detailController.edit(prop);
				
				 
				 }
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String deleteBookingDetails() {
		BookingDetailManager bookingDetailController = new BookingDetailManager();
		PmsStatusManager statusController = new PmsStatusManager();
		PmsBookingManager bookingController = new PmsBookingManager();
		try {
			
			
			
						
			System.out.println("delete booking");
			BookingDetail bookingDetail = bookingDetailController.find(getBookingDetailsId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			bookingDetail.setIsActive(false);
			bookingDetail.setIsDeleted(true);
			bookingDetail.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			bookingDetailController.edit(bookingDetail);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public static List<DateTime> getDateRange(DateTime start, DateTime end) {

        List<DateTime> ret = new ArrayList<DateTime>();
        DateTime tmp = start;
        while(tmp.isBefore(end) || tmp.equals(end)) {
            ret.add(tmp);
            tmp = tmp.plusDays(1);
        }
        return ret;
    }
	
	
	
	public Integer getBookingDetailsId() {
		return bookingDetailsId;
	}

	public void setBookingDetailId(Integer bookingDetailsId) {
		this.bookingDetailsId = bookingDetailsId;
	}
	
	
	public List<PropertyAccommodation> getAccommodationList() {
		return accommodationList;
	}

	public void setAccomadationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}

	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}
	
	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}
	
	public Integer getDiscountId() {
		return discountId;
	}

	public void setDiscountId(Integer discountId) {
		this.discountId = discountId;
	}
	

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	
	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTax() {
		return tax;
	}
	
	public void setTax(double tax) {
		this.tax = tax;
	}
	
	public double getRefund() {
		return refund;
	}

	public void setRefund(double refund) {
		this.refund = refund;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	
	public Integer getGuestId() {
		return guestId;
	}

	public void setGuestId(Integer guestId) {
		this.guestId = guestId;
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	
	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

	
	/*public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}*/

	public List<BookingDetail> getBookingDetailList() {
		return bookingDetailList;
	}

	public void setBookingDetailList(List<BookingDetail> bookingDetailList) {
		this.bookingDetailList = bookingDetailList;
	}

	public void setBookingDetailsId(Integer bookingDetailsId) {
		this.bookingDetailsId = bookingDetailsId;
	}

	public void setAccommodationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}
	
	public List<PmsBookedDetails> getBookedList() {
		return bookedList;
	}

	public void setBookedList(List<PmsBookedDetails> bookedList) {
		this.bookedList = bookedList;
	}
	
	public List<PmsRoomDetail> getRoomList() {
		return roomList;
	}

	public void setRoomList(List<PmsRoomDetail> roomList) {
		this.roomList = roomList;
	}
	
	/*public List < BookingListAction > getData() {
	        System.out.println("Getter Call");
	        return data;
	}
	 
	public void setData(List < BookingListAction > data) {
	        System.out.println("Setter Call Flow");
	        this.data = data;
	} 
	  */
	 public boolean getMailFlag() {
			return mailFlag;
		}

		public void setMailFlag(boolean mailFlag) {
			this.mailFlag = mailFlag;
		}
	
	
	
}
