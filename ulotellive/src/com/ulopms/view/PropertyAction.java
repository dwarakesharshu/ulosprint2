package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;









//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;










import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsPhotoCategoryManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyTypeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsPhotoCategory;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyType;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	



	private Integer propertyId;
	private Integer propertyTypeId;
	private String address1;
	private String address2;
	private String city;
	private String latitude;
	private String longitude;
	private String propertyContact;
	private String propertyDescription;
	private String propertyEmail;
	private String propertyLogo;
	private String propertyName;
	private String resortManagerName;
	private String resortManagerContact;
	private String resortManagerEmail;
	private String reservationManagerName;
	private String reservationManagerContact;
	private String reservationManagerEmail;
	private String propertyPhone;
	private String zipCode;
	private String stateCode;
	private Timestamp taxValidFrom;
	private Timestamp taxValidTo;
	private Boolean taxIsActive;
	
	private String propertyHotelPolicy;
	private String propertyStandardPolicy;
	private String propertyCancellationPolicy;
	
	private PmsProperty property;
	


	private List<PmsProperty> propertyList;
	
	private List<PmsPhotoCategory> categoryList;
	

	private static final Logger logger = Logger.getLogger(PropertyAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyAction() {
		
	

	}

	public String execute() {//.
//this.propertyId= session("")
		//this.patientId = getPatientId();
		
		
		return SUCCESS;
	}

	
	public String getProperties() throws IOException {
		System.out.println("get package");
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PmsProperty property = propertyController.find(getPropertyId());
			
				
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
			jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId()+ "\"";
			jsonOutput += ",\"address1\":\"" + property.getAddress1().trim()+ "\"";
			jsonOutput += ",\"address2\":\"" + property.getAddress2().trim()+ "\"";
			jsonOutput += ",\"city\":\"" + property.getCity().trim() + "\"";
			jsonOutput += ",\"latitude\":\"" +property.getLatitude().trim()+ "\"";
			jsonOutput += ",\"longitude\":\"" + property.getLongitude().trim()+ "\"";
			jsonOutput += ",\"propertyContact\":\"" + property.getPropertyContact().trim() + "\"";
			//jsonOutput += ",\"propertyDescription\":\"" + property.getPropertyDescription() + "\"";
			jsonOutput += ",\"propertyDescription\":\"" + (property.getPropertyDescription() == null ? "": property.getPropertyDescription().trim()) + "\"";
			jsonOutput += ",\"propertyEmail\":\"" + (property.getPropertyEmail() == null ? "": property.getPropertyEmail().trim())+ "\"";
			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().trim())+ "\"";
			jsonOutput += ",\"propertyPhone\":\"" + (property.getPropertyPhone() == null ? "": property.getPropertyPhone().trim())+ "\"";
			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "": property.getPropertyHotelPolicy().trim())+ "\"";
			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy()== null ? "": property.getPropertyStandardPolicy().trim())+ "\"";
			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "": property.getPropertyCancellationPolicy().trim())+ "\"";
			jsonOutput += ",\"reservationManagerName\":\"" + (property.getReservationManagerName() == null ? "": property.getReservationManagerName().trim())+ "\"";
			jsonOutput += ",\"reservationManagerEmail\":\"" + (property.getReservationManagerEmail() == null ? "": property.getReservationManagerEmail().trim())+ "\"";
			jsonOutput += ",\"reservationManagerContact\":\"" + (property.getReservationManagerContact() == null ? "": property.getReservationManagerContact().trim()) + "\"";
			jsonOutput += ",\"resortManagerName\":\"" + (property.getResortManagerName() == null ? "": property.getResortManagerName().trim())+ "\"";
			jsonOutput += ",\"resortManagerEmail\":\"" + (property.getResortManagerEmail() == null ? "": property.getResortManagerEmail().trim())+ "\"";
			jsonOutput += ",\"resortManagerContact\":\"" + (property.getResortManagerContact() == null?"":property.getResortManagerContact().trim()) + "\"";
			jsonOutput += ",\"zipCode\":\"" + (property.getZipCode() == null ? "": property.getZipCode().trim())+ "\"";
			String from = new SimpleDateFormat("MM/dd/yyyy").format(property.getTaxValidFrom());
			String to = new SimpleDateFormat("MM/dd/yyyy").format(property.getTaxValidTo());
			jsonOutput += ",\"taxValidFrom\":\"" + from + "\"";
			jsonOutput += ",\"taxValidTo\":\"" + to + "\"";
			jsonOutput += ",\"taxIsActive\":\"" + ( property.getTaxIsActive() == true ? "true": "false" )+ "\"";
			jsonOutput += ",\"stateCode\":\"" + (property.getState() == null ? "": property.getState().getStateCode().trim())+ "\"";
			jsonOutput += ",\"countryCode\":\"" + (property.getState() == null ? "": property.getState().getCountry().getCountryCode().trim())+ "\"";
			
			jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getAllProperties() throws IOException {
		System.out.println("all properties");
		this.propertyId = (Integer) sessionMap.get("propertyId");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyList = propertyController.list();
			for (PmsProperty property : propertyList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
				jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId()+ "\"";
				jsonOutput += ",\"address1\":\"" + property.getAddress1().trim()+ "\"";
				jsonOutput += ",\"address2\":\"" + property.getAddress2().trim()+ "\"";
				jsonOutput += ",\"city\":\"" + property.getCity().trim() + "\"";
				jsonOutput += ",\"latitude\":\"" +property.getLatitude().trim()+ "\"";
				jsonOutput += ",\"longitude\":\"" + property.getLongitude().trim()+ "\"";
				jsonOutput += ",\"propertyContact\":\"" + property.getPropertyContact().trim() + "\"";
				//jsonOutput += ",\"propertyDescription\":\"" + property.getPropertyDescription() + "\"";
				jsonOutput += ",\"propertyDescription\":\"" + (property.getPropertyDescription() == null ? "": property.getPropertyDescription().trim()) + "\"";
				jsonOutput += ",\"propertyEmail\":\"" + (property.getPropertyEmail() == null ? "": property.getPropertyEmail().trim())+ "\"";
				jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().trim())+ "\"";
				jsonOutput += ",\"propertyPhone\":\"" + (property.getPropertyPhone() == null ? "": property.getPropertyPhone().trim())+ "\"";
				jsonOutput += ",\"zipCode\":\"" + (property.getZipCode() == null ? "": property.getZipCode().trim())+ "\"";
				String from = new SimpleDateFormat("MM/dd/yyyy").format(property.getTaxValidFrom());
				String to = new SimpleDateFormat("MM/dd/yyyy").format(property.getTaxValidTo());
				jsonOutput += ",\"taxValidFrom\":\"" + from + "\"";
				jsonOutput += ",\"taxValidTo\":\"" + to + "\"";
				jsonOutput += ",\"taxIsActive\":\"" + property.getTaxIsActive() + "\"";
				jsonOutput += ",\"stateCode\":\"" + (property.getState() == null ? "": property.getState().getStateCode().trim())+ "\"";
				jsonOutput += ",\"countryCode\":\"" + (property.getState() == null ? "": property.getState().getCountry().getCountryCode().trim())+ "\"";
				jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "": property.getPropertyHotelPolicy().trim())+ "\"";
				jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy()== null ? "": property.getPropertyStandardPolicy().trim())+ "\"";
				jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "": property.getPropertyCancellationPolicy().trim())+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getPhotoCategory() throws IOException {
		System.out.println("all photo Category");
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPhotoCategoryManager photoCategoryController = new PmsPhotoCategoryManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.categoryList = photoCategoryController.list();
			for (PmsPhotoCategory category : categoryList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"photoCategoryId\":\"" + category.getPhotoCategoryId()+ "\"";
				jsonOutput += ",\"photoCategoryName\":\"" + category.getPhotoCategoryName()+ "\"";
				
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	
	
	

	public String addProperty() {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsPropertyManager propertyController = new PmsPropertyManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("add property");
			PmsProperty property = new PmsProperty();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			property.setAddress1(getAddress1());
			property.setAddress2(getAddress2());
			property.setCity(getCity());
			property.setIsActive(true);
			property.setIsDeleted(false);
			property.setLatitude(getLatitude());
			property.setLongitude(getLongitude());
			property.setPropertyContact(getPropertyContact());
			property.setPropertyDescription(getPropertyDescription());
			property.setPropertyEmail(getPropertyEmail());
			property.setPropertyName(getPropertyName());
			property.setReservationManagerEmail(getReservationManagerEmail());
			property.setReservationManagerName(getReservationManagerName());
			property.setReservationManagerContact(getReservationManagerContact());
			property.setResortManagerEmail(getResortManagerEmail());
			property.setResortManagerName(getResortManagerName());
			property.setResortManagerContact(getResortManagerContact());
			property.setZipCode(getZipCode());		
			
			propertyController.add(property);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editProperty() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsPropertyManager propertyController = new PmsPropertyManager();
		PropertyTypeManager propertyTypeController = new PropertyTypeManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
			
			System.out.println("edit property");
			
			System.out.println("active" +getTaxIsActive());
			
			PmsProperty property = propertyController.find(getPropertyId());
			PropertyType propertyType = propertyTypeController.find(getPropertyTypeId());
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			property.setAddress1(getAddress1());
			property.setAddress2(getAddress2());
			property.setCity(getCity());
			property.setIsActive(true);
			property.setIsDeleted(false);
			property.setLatitude(getLatitude());
			property.setLongitude(getLongitude());
			property.setPropertyContact(getPropertyContact());
			property.setPropertyDescription(getPropertyDescription());
			property.setPropertyEmail(getPropertyEmail());
			property.setPropertyName(getPropertyName());
			property.setZipCode(getZipCode());	
			property.setPropertyType(propertyType);	
			property.setReservationManagerEmail(getReservationManagerEmail());
			property.setReservationManagerName(getReservationManagerName());
			property.setReservationManagerContact(getReservationManagerContact());
			property.setResortManagerEmail(getResortManagerEmail());
			property.setResortManagerName(getResortManagerName());
			property.setResortManagerContact(getResortManagerContact());
			property.setTaxValidFrom(getTaxValidFrom());
			property.setTaxValidTo(getTaxValidTo());
			property.setTaxIsActive(getTaxIsActive());
		    propertyController.edit(property);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	public String editPropertyPolicy() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsPropertyManager propertyController = new PmsPropertyManager();
		try {
			
			PmsProperty property = propertyController.find(getPropertyId());
			
			
			property.setPropertyHotelPolicy(getPropertyHotelPolicy().trim());
			property.setPropertyStandardPolicy(getPropertyStandardPolicy().trim());
			property.setPropertyCancellationPolicy(getPropertyCancellationPolicy().trim());
			
			propertyController.edit(property);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public String deleteProperty() {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsPropertyManager propertyController = new PmsPropertyManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("delete property");
			PmsProperty property = propertyController.find(getPropertyId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			property.setIsActive(false);
			property.setIsDeleted(true);
			property.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			propertyController.edit(property);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
     
	
	public Integer getPropertyTypeId() {
		return propertyTypeId;
	}

	public void setPropertyTypeId(Integer propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}
	
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPropertyContact() {
		return propertyContact;
	}

	public void setPropertyContact(String propertyContact) {
		this.propertyContact = propertyContact;
	}

	public String getPropertyDescription() {
		return propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getPropertyEmail() {
		return propertyEmail;
	}

	public void setPropertyEmail(String propertyEmail) {
		this.propertyEmail = propertyEmail;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyPhone() {
		return propertyPhone;
	}

	public void setPropertyPhone(String propertyPhone) {
		this.propertyPhone = propertyPhone;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	
	public String getResortManagerName() {
		return resortManagerName;
	}

	public void setResortManagerName(String resortManagerName) {
		this.resortManagerName = resortManagerName;
	}

	public String getResortManagerContact() {
		return resortManagerContact;
	}

	public void setResortManagerContact(String resortManagerContact) {
		this.resortManagerContact = resortManagerContact;
	}

	public String getResortManagerEmail() {
		return resortManagerEmail;
	}

	public void setResortManagerEmail(String resortManagerEmail) {
		this.resortManagerEmail = resortManagerEmail;
	}

	public String getReservationManagerName() {
		return reservationManagerName;
	}

	public void setReservationManagerName(String reservationManagerName) {
		this.reservationManagerName = reservationManagerName;
	}

	public String getReservationManagerContact() {
		return reservationManagerContact;
	}

	public void setReservationManagerContact(String reservationManagerContact) {
		this.reservationManagerContact = reservationManagerContact;
	}

	public String getReservationManagerEmail() {
		return reservationManagerEmail;
	}

	public void setReservationManagerEmail(String reservationManagerEmail) {
		this.reservationManagerEmail = reservationManagerEmail;
	}
	

	public PmsProperty getProperty() {
		return property;
	}

	public void setProperty(PmsProperty property) {
		this.property = property;
	}

	public List<PmsProperty> getPropertyList() {
		return propertyList;
	}

	public void setPropertyList(List<PmsProperty> propertyList) {
		this.propertyList = propertyList;
	}
	
	public Timestamp getTaxValidFrom() {
		return taxValidFrom;
	}

	public void setTaxValidFrom(Timestamp taxValidFrom) {
		this.taxValidFrom = taxValidFrom;
	}

	public Timestamp getTaxValidTo() {
		return taxValidTo;
	}

	public void setTaxValidTo(Timestamp taxValidTo) {
		this.taxValidTo = taxValidTo;
	}

	public Boolean getTaxIsActive() {
		return taxIsActive;
	}

	public void setTaxIsActive(Boolean taxIsActive) {
		this.taxIsActive = taxIsActive;
	}
	
	public String getPropertyHotelPolicy()
	{
		return propertyHotelPolicy;
	}
	
	public void setPropertyHotelPolicy(String propertyHotelPolicy){
		this.propertyHotelPolicy=propertyHotelPolicy;
	}
	
	public String getPropertyStandardPolicy()
	{
		return propertyStandardPolicy;
	}
	
	public void setPropertyStandardPolicy(String propertyStandardPolicy){
		this.propertyStandardPolicy=propertyStandardPolicy;
	}
	
	public String getPropertyCancellationPolicy()
	{
		return propertyCancellationPolicy;
	}
	
	public void setPropertyCancellationPolicy(String propertyCancellationPolicy){
		this.propertyCancellationPolicy=propertyCancellationPolicy;
	}

	
}
