package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;




















import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;
import com.ulopms.view.PropertyAccommodationRoomAction;
public class PropertyAccommodationAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;

	private Integer propertyAccommodationId;
	private Integer propertyId;
	private PropertyAccommodation propertyaccommodation = new PropertyAccommodation();
	private String  abbreviation;
	private String  accommodationType;
	private Integer noOfUnits;
	private Integer minOccupancy;
	private Integer maxOccupancy;
	private Integer noOfAdults;
	private Integer noOfChild;
	private double  baseAmount;
	private double  extraChild;
	private double  extraAdult;
	private double  extraInfant;
	private String  description;
	private String accommodationPic;

	private PropertyAccommodation accommodation;
	
	private List<PropertyAccommodation> accommodationList;
	
	private List<PropertyAccommodationRoom> accommodationroomList;
	

	private static final Logger logger = Logger.getLogger(PropertyAccommodationAction.class);
    
	private HttpSession session;
	
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	// @Override
	// public User getModel() {
	// return user;
	// }
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}
	
	 

	public PropertyAccommodation getModel() {
		return propertyaccommodation;
	}
    
	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyAccommodationAction() {
		
	//this.propertyId = (Integer) sessionMap.get("propertyId");
	
	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	public String getAccommodation() throws IOException {
		
		System.out.println(getPropertyAccommodationId());
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();

			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
                                        
			PropertyAccommodation accommodation = accommodationController.find(getPropertyAccommodationId());
			AccommodationRoom accommodationRooms = accommodationRoomController.findCount(this.propertyId,getPropertyAccommodationId());
			//accommodationRooms.getRoomCount();
			
			System.out.println("accommodation room count---------------"+accommodationRooms.getRoomCount());
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"propertyAccommodationId\":\"" + accommodation.getAccommodationId() + "\"";
				jsonOutput += ",\"abbreviation\":\"" + accommodation.getAbbreviation()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
				jsonOutput += ",\"noOfUnits\":\"" + accommodationRooms.getRoomCount()+ "\"";
				jsonOutput += ",\"minimumOccupancy\":\"" + accommodation.getMinOccupancy()+ "\"";
				jsonOutput += ",\"maximumOccupancy\":\"" + accommodation.getMaxOccupancy()+ "\"";
				jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults()+ "\"";
				jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild()+ "\"";
				jsonOutput += ",\"baseAmount\":\"" + accommodation.getBaseAmount()+ "\"";
				jsonOutput += ",\"extraChild\":\"" + accommodation.getExtraChild()+ "\"";
				jsonOutput += ",\"extraAdult\":\"" + accommodation.getExtraAdult()+ "\"";
				jsonOutput += ",\"extraInfant\":\"" + accommodation.getExtraInfant()+ "\"";
				jsonOutput += ",\"description\":\"" + accommodation.getAccommodationDescription()+ "\"";
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAccommodations() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		System.out.println(getPropertyId());
		
		//System.out.println("all categories");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.accommodationList = accommodationController.list(getPropertyId());
			for (PropertyAccommodation accommodation : accommodationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				AccommodationRoom accommodationRooms = accommodationRoomController.findCount(this.propertyId,accommodation.getAccommodationId());
				jsonOutput += "\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
				jsonOutput += ",\"abbreviation\":\"" + accommodation.getAbbreviation()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
				jsonOutput += ",\"noOfUnits\":\"" + accommodationRooms.getRoomCount()+ "\"";
				jsonOutput += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy()+ "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy()+ "\"";
				jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults()+ "\"";
				jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild()+ "\"";
				jsonOutput += ",\"baseAmount\":\"" + accommodation.getBaseAmount()+ "\"";
				jsonOutput += ",\"extraChild\":\"" + accommodation.getExtraChild()+ "\"";
				jsonOutput += ",\"extraAdult\":\"" + accommodation.getExtraAdult()+ "\"";
				jsonOutput += ",\"extraInfant\":\"" + accommodation.getExtraInfant()+ "\"";
				jsonOutput += ",\"description\":\"" + accommodation.getAccommodationDescription()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getPropertyThumb() {
		System.out.println(getPropertyId());
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		HttpServletResponse response = ServletActionContext.getResponse();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		PmsProperty property = propertyController.find(getPropertyId());
		System.out.println("location id----:"+getPropertyId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if ( property.getPropertyThumbPath() != null) {
				imagePath = getText("stroage.propertythumb.photo") + "/"
						+ property.getPropertyId()+ "/"
						+ property.getPropertyThumbPath();
				Image img = new Image();
				System.out.println(imagePath);
				response.getOutputStream().write(
						img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("stroage.propertythumb.photo") + "/emptyprofilepic.png";
				Image img = new Image();
				//System.out.println(imagePath);
				response.getOutputStream().write(
						img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}

	
	public String userProfilePic() throws IOException {
		try {

			//System.out.println(getPropertyAccommodationId());
			System.out.println("profile pic update...");
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();

			//System.out.println(getMyFile());
			//User u = new User();
			// FileInputStream fis;
			// fis = new FileInputStream(myFile);
			// @SuppressWarnings("deprecation")
			// Blob blob = Hibernate.createBlob(fis);
			// user.setProfilePicture(blob);
			// linkController.edit(getModel());
			// this.user = getModel();
			File fileZip1 = new File(getText("stroage.accomodation.photo") + "/"
					+ getPropertyAccommodationId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			// new file
			System.out.println("file det" +this.getMyFileFileName());
			PropertyAccommodation propaccom = accommodationController.find(getPropertyAccommodationId()); 
			
			//System.out.println(this.getMyFileFileName());
			
			propaccom.setAccommodationImgPath(this.getMyFileFileName());
			//System.out.println(fileZip1);
			//System.out.println(getMyFileFileName());
			//System.out.println(getModel());
			//u.setProfilePicName(this.getMyFileFileName());
			accommodationController.edit(propaccom);
			this.propertyaccommodation = getModel();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	
	
	
	public String getAccommodationPic() {
		System.out.println(getPropertyAccommodationId());
		
		HttpServletResponse response = ServletActionContext.getResponse();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();

		String imagePath = "";
		// response.setContentType("");
		try {
			PropertyAccommodation propacco = accommodationController.find(getPropertyAccommodationId()); 
			if ( propacco.getAccommodationImgPath() != null) {
				imagePath = getText("stroage.accomodation.photo") + "/"
						+propacco.getAccommodationId() + "/"
						+ propacco.getAccommodationImgPath();
				Image im = new Image();
				System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("stroage.accomodation.photo") + "/emptyprofilepic.png";
				Image im = new Image();
				System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		}catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	
	
	public String addAccommodation() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		//UserLoginManager  userController = new UserLoginManager();
		
		PmsPropertyManager propertyController = new PmsPropertyManager();
		
		
		try {
			
						
			System.out.println("add category");
			PropertyAccommodation accommodation = new PropertyAccommodation();
			PropertyAccommodationRoomAction accommodationRoom = new PropertyAccommodationRoomAction();
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			accommodation.setAbbreviation(getAbbreviation());
			accommodation.setAccommodationDescription(getDescription());
			accommodation.setAccommodationType(getAccommodationType());
			accommodation.setNoOfAdults(getNoOfAdults());
			accommodation.setBaseAmount(getBaseAmount());
			accommodation.setNoOfChild(getNoOfChild());
			accommodation.setIsActive(true);
			accommodation.setIsDeleted(false);
			accommodation.setExtraAdult(getExtraAdult());
			accommodation.setExtraChild(getExtraChild());
			accommodation.setExtraInfant(getExtraInfant());
			accommodation.setMinOccupancy(getMinOccupancy());
			accommodation.setMaxOccupancy(getMaxOccupancy());
			accommodation.setNoOfUnits(getNoOfUnits());
			PmsProperty property = propertyController.find(getPropertyId());
			accommodation.setPmsProperty(property);
			//accommodationController.add(accommodation);
			
			PropertyAccommodation accom = accommodationController.add(accommodation);
	
			this.propertyAccommodationId = accom.getAccommodationId();
			this.propertyAccommodationId = accom.getAccommodationId();
			System.out.println("accomId" +this.propertyAccommodationId);
			
			for(int i=1; i<=getNoOfUnits(); i++){
			
				accommodationRoom.addAccommodationRoom(this.propertyAccommodationId, "200", getAbbreviation()+i);
				
			}
		    
			
			//sessionMap.put("propertyAccommodationId",propertyAccommodationId);
			
			//this.propertyAccommodationId = (Integer) sessionMap.get("propertyAccommodationId");
			
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"accommodationId\":\"" + this.propertyAccommodationId  + "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			 
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String editAccommodation() {
		
		
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			//System.out.println("edit category");
			PropertyAccommodation accommodation = accommodationController.find(getPropertyAccommodationId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			accommodation.setAbbreviation(getAbbreviation());
			accommodation.setAccommodationDescription(getDescription());
			accommodation.setAccommodationType(getAccommodationType());
			accommodation.setNoOfAdults(getNoOfAdults());
			accommodation.setBaseAmount(getBaseAmount());
			accommodation.setNoOfChild(getNoOfChild());
			accommodation.setIsActive(true);
			accommodation.setIsDeleted(false);
			accommodation.setExtraAdult(getExtraAdult());
			accommodation.setExtraChild(getExtraChild());
			accommodation.setExtraInfant(getExtraInfant());
			accommodation.setMinOccupancy(getMinOccupancy());
			accommodation.setMaxOccupancy(getMaxOccupancy());
			accommodation.setNoOfUnits(getNoOfUnits());
			
			accommodationController.edit(accommodation);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteAccommodation() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		
		PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("delete category");
			
			System.out.println(getPropertyAccommodationId());
			
			PropertyAccommodation accommodation = accommodationController.find(getPropertyAccommodationId());
			System.out.println("delete1 category type"+accommodation.getAccommodationId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			accommodation.setIsActive(false);
			accommodation.setIsDeleted(true);
			accommodation.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			accommodationController.edit(accommodation);
			
			//System.out.println("accomodationid" + getPropertyAccommodationId());
			
			/* this.accommodationroomList = accommodationRoomController.list1(getPropertyAccommodationId());
			
			for(PropertyAccommodationRoom accommodationRooms : accommodationroomList){
				
				//long time = System.currentTimeMillis();
				//java.sql.Date date = new java.sql.Date(time);
				
			System.out.println("delete category type"+accommodationRooms.getIsActive());
			accommodationRooms.setIsActive(false);
			accommodationRooms.setIsDeleted(true);
			//accommodationRooms.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			
			accommodationRoomController.edit(accommodationRooms);
			
			}*/
			
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	

	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getAccommodationType() {
		return accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}
	
	public Integer getNoOfUnits() {
		return noOfUnits;
	}

	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	
	public Integer getMinOccupancy() {
		return minOccupancy;
	}

	public void setMinOccupancy(Integer minOccupancy) {
		this.minOccupancy = minOccupancy;
	}
	
	public Integer getMaxOccupancy() {
		return maxOccupancy;
	}

	public void setMaxOccupancy(Integer maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
	}
	
	public Integer getNoOfAdults() {
		return noOfAdults;
	}

	public void setNoOfAdults(Integer noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	
	public Integer getNoOfChild() {
		return noOfChild;
	}

	public void setNoOfChild(Integer noOfChild) {
		this.noOfChild = noOfChild;
	}
    
	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}
	
	public double getExtraChild() {
		return extraChild;
	}

	public void setExtraChild(double extraChild) {
		this.extraChild = extraChild;
	}
	
	public double getExtraAdult() {
		return extraAdult;
	}

	public void setExtraAdult(double extraAdult) {
		this.extraAdult = extraAdult;
	}
	
	public double getExtraInfant() {
		return extraInfant;
	}

	public void setExtraInfant(double extraInfant) {
		this.extraInfant = extraInfant;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setAccommodationImgPath(String accommodationPic) {
		this.accommodationPic = accommodationPic;
	}

	public String getAccommodationImgPath() {
		return accommodationPic;
	}
	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}
	public void setImgPath(String accommodationPic) {
		this.accommodationPic = accommodationPic;
	}

	public String getImgPath() {
		return accommodationPic;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public List<PropertyAccommodation> getAccommodationList() {
		return accommodationList;
	}

	public void setAccomadationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}

	public void setAccommodation(PropertyAccommodation accommodation) {
		this.accommodation = accommodation;
	}
	
	

	


}
