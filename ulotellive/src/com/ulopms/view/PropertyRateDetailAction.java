package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;






























import org.joda.time.DateTime;

import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsDayManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyRateDetailManager;
import com.ulopms.controller.PropertyRateManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.DateUtil;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyRateDetailAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	
	private Integer  propertyRateId;
	private Integer propertyId;
	private Integer daysOfWeek;
	private Integer baseAmount;
	private String  dayPart;
	private Timestamp  startDate;
	private Timestamp  endDate;
	private String  checkedDays;
	private Double extraChild;
	private Double extraAdult;
	private Double extraInfant;
	private Integer smartPricePropertyAccommodationId;
    private Timestamp smartPriceStartDate;
    private Timestamp smartPriceEndDate;
    private Boolean smartPriceIsActiveOrNot;
	
	
	

	
	private PropertyRateDetail rateDetail;
	
	private PmsDays days;

	
	
	private List<PropertyRateDetail> rateDetailList;

	private static final Logger logger = Logger.getLogger(PropertyItemAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private long roomCnt;
	
	public long getRoomCnt() {
		return roomCnt;
	}

	public void setRoomCnt(long roomCnt) {
		this.roomCnt = roomCnt;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyRateDetailAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}
	
	

	
	
	public String addSmartPriceRateDetail(){
		this.propertyId = (Integer) sessionMap.get("propertyId");
		this.propertyRateId = (Integer) sessionMap.get("propertyRateId");

		this.smartPriceStartDate = getSmartPriceStartDate();
		sessionMap.put("smartPriceStartDate",smartPriceStartDate); 
	      
		this.smartPriceEndDate = getSmartPriceEndDate();
	    sessionMap.put("smartPriceStartDate",smartPriceEndDate); 
			
		this.smartPricePropertyAccommodationId = getSmartPricePropertyAccommodationId();
		sessionMap.put("smartPricePropertyAccommodationId",smartPricePropertyAccommodationId);
		
		this.smartPriceIsActiveOrNot = getSmartPriceIsActiveOrNot();
	    sessionMap.put("smartPriceIsActive",smartPriceIsActiveOrNot); 
	    
	    try{
	    	if(propertyRateId==null || propertyRateId==0){
	    		return null;
	    	}
	    	if(propertyRateId>0){
			PmsDayManager dayController = new PmsDayManager();
	    	
	    	int roomCount=0,availableCount=0,totalCount=0,totalDate=0,dateCount=0;
	    	Double dblTotalBaseAmount=0.0,dblBaseAmount=0.0,dblPercentCount=0.0,dblSmartPricePercent=0.0;
	    	Double dblPercentFrom,dblPercentTo;
	    	String strStartDate,strEndDate,strDateValues,strDay=null,strRateDay=null;
	    	int intPropertyRateId=0,intDay=0;
	    	Boolean isPositive=false,isNegative=false;
	    	HashMap<String, String> mapDateDay=new HashMap<String, String>();
	    	HashMap<String, Double> mapDateAmount=new HashMap<String, Double>();
	    	ArrayList arlBaseAmountList=new ArrayList();
	    	
	    	SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
	    	strStartDate=format1.format(smartPriceStartDate);
    		strEndDate=format1.format(smartPriceEndDate);
    		DateTime FromDate = DateTime.parse(strStartDate);
	        DateTime ToDate = DateTime.parse(strEndDate);
	        
	        PropertyRateDetail rateDetail = new PropertyRateDetail();
	    	PropertyAccommodationManager roomController=new PropertyAccommodationManager();
	    	PropertyAccommodationRoomManager  roomManagerController  = new PropertyAccommodationRoomManager();
	    	PropertyRateManager rateController=new PropertyRateManager();
	    	PropertyRateDetailManager rateDetailController=new PropertyRateDetailManager();
	    	List<PmsSmartPrice> listSmartPriceDetail=null; 
	    	List<AccommodationRoom> listAccommodationRoom=null;
	    	List<DateTime> dateBetween = DateUtil.getDateRange(FromDate, ToDate);
	    	Iterator<DateTime> iterDateValues=dateBetween.iterator();
	    	String strArrays[] = new String[dateBetween.size()];
	        while(iterDateValues.hasNext()){
	        	DateTime dateValues=iterDateValues.next();
	        	java.util.Date DateValues = dateValues.toDate();
	        	strDateValues=format1.format(DateValues);
	        	strArrays[dateCount]=strDateValues;
	        	rateDetail.setIsActive(true);
				rateDetail.setIsDeleted(false);
	        	DateTime checkedDate = DateTime.parse(strDateValues);
	        	List<String> dayBetweenList=DateUtil.getDayRange(checkedDate, checkedDate);
	        	Iterator<String> iterDayValues=dayBetweenList.iterator();
	        	if(iterDayValues.hasNext()){
	        		strDay=iterDayValues.next();
	        	}
	        	//mapDateDay.put(strDateValues, strDay);
				arlBaseAmountList.add(strDateValues);
	        	java.sql.Date sqlDate = new java.sql.Date(DateValues.getTime());
	        	List<PropertyRateDetail> listRateDetail= null;
	        	List<PropertyRate> listDateRate=rateController.listDateDetails(propertyId, smartPricePropertyAccommodationId, sqlDate);
	        	List<PropertyAccommodation> accommodationList= roomManagerController.list(smartPricePropertyAccommodationId,propertyId);
	        	if(listDateRate.size()>1 && !listDateRate.isEmpty()){
	        		for(PropertyRate rateList:listDateRate){
	        			intPropertyRateId=rateList.getPropertyRateId();
	        			intDay=DateUtil.getDay(strDay);
	        			if(intPropertyRateId!=propertyRateId){
	        				PmsDays day = dayController.find(intDay);
		        			rateDetail.setDaysOfWeek(day.getDaysOfWeek());
		        			rateDetail.setDayPart(intDay);
		        			//intPropertyRateId=intPropertyRateId-1;
		        			listRateDetail=rateDetailController.list(intPropertyRateId, intDay);
		        			if(listRateDetail.size()>0 && !listRateDetail.isEmpty()){
		        				for(PropertyRateDetail rateRateDetail:listRateDetail){
		        					dblBaseAmount=rateRateDetail.getBaseAmount();
		        					strRateDay=rateRateDetail.getDaysOfWeek();
		        					
		        					rateDetail.setExtraChild(rateRateDetail.getExtraChild());
		        					rateDetail.setExtraAdult(rateRateDetail.getExtraAdult());
		        					rateDetail.setExtraInfant(rateRateDetail.getExtraInfant());
		        				}
		        			}
		        			break;
	        			}
	        			
	        		}
	        	}
	        	else{
    				if(accommodationList.size()>0 && !accommodationList.isEmpty()){
    					for (PropertyAccommodation accommodationRate : accommodationList) {
    						dblBaseAmount=accommodationRate.getBaseAmount();
    						intDay=DateUtil.getDay(strDay);
    						rateDetail.setDayPart(intDay);
    						PmsDays day = dayController.find(intDay);
    						rateDetail.setDaysOfWeek(day.getDaysOfWeek());
        					rateDetail.setExtraChild(accommodationRate.getExtraChild());
        					rateDetail.setExtraAdult(accommodationRate.getExtraAdult());
        					rateDetail.setExtraInfant(accommodationRate.getExtraInfant());
       					break;
    					}
    				}
    			}
	        	
	        	listAccommodationRoom=roomController.listPropertyTotalRooms(propertyId, smartPricePropertyAccommodationId);
				 System.out.println("size..."+listAccommodationRoom.size());
				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
					 for(AccommodationRoom roomsList:listAccommodationRoom){
						 roomCount=(int) roomsList.getRoomCount();
						 long minimum = getAvailableCount(propertyId,sqlDate,roomCount,smartPricePropertyAccommodationId); 
						 /*availableCount=(int)minimum*100;
						 dblAvblPercent=(double)availableCount/roomCount;*/
						 availableCount=(int)minimum;
						 //totalCount=roomCount-availableCount;
						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
						 totalCount=(int) Math.round(dblPercentCount);
						 
						 listSmartPriceDetail=rateDetailController.listSmartPriceDetail(totalCount);
						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
								 dblSmartPricePercent=smartPrice.getAmountPercent();
								 dblPercentFrom=smartPrice.getPercentFrom();
								 dblPercentTo=smartPrice.getPercentTo();
								 if(dblPercentFrom>=0 && dblPercentTo<=59){
									 isNegative=true;
								 }
								 else if(dblPercentFrom>=60 && dblPercentTo<=99){
									 isPositive=true;
								 }
								 else{
									 
								 }
								 
								 if(isNegative){
									 dblTotalBaseAmount=dblBaseAmount-(dblBaseAmount*dblSmartPricePercent/100);
								 }
								  
								if(isPositive){
									dblTotalBaseAmount=dblBaseAmount+(dblBaseAmount*dblSmartPricePercent/100);
								}
								arlBaseAmountList.add(dblTotalBaseAmount);
								mapDateAmount.put(strDateValues, dblTotalBaseAmount);
								rateDetail.setBaseAmount(dblTotalBaseAmount);
							 }
						 }
					 }
				 }
				 PropertyRate propertyRate = rateController.find(propertyRateId);
				 rateDetail.setPropertyRate(propertyRate);
				 rateDetailController.add(rateDetail);
				 dblTotalBaseAmount=0.0;
	        }

	        System.out.println("mapDateAmount..."+mapDateAmount);
	        System.out.println("mapDateDay..."+mapDateDay);
	        System.out.println("arlBaseAmountList..."+arlBaseAmountList);
	    	}
	        
	    }
	    catch(Exception e)
	    {
	    	logger.error(e);
			e.printStackTrace();
	    }finally{
	    	
	    }
	    
		return null;
	}
	

	public String addRateDetail() {
		
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		this.propertyRateId = (Integer) sessionMap.get("propertyRateId");
		System.out.println("<--propertyRateId id-->");
		System.out.println("<--propertyRateId id-->"+getCheckedDays());
		System.out.println(getPropertyRateId());
		String strRateId=(String)sessionMap.get("RateId");
		
		System.out.println("strRateId..."+strRateId);
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
	    //UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		PmsDayManager dayController = new PmsDayManager();
		
		/*String startDate = new SimpleDateFormat("yyyy-MM-dd").format(getStartDate());
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(getEndDate());
		DateTime start = DateTime.parse(startDate);
	    DateTime end = DateTime.parse(endDate);*/
	    
		try {
			
						
			System.out.println("add Rate");
			PropertyRateDetail rateDetail = new PropertyRateDetail();
			String[] RateIdArray = strRateId.split("\\s*,\\s*");
			
			for (int j = 0; j < RateIdArray.length; j++){  
				
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				String[] stringArray = getCheckedDays().split("\\s*,\\s*");
				System.out.println("selected days" + stringArray);
			    int[] intArray = new int[stringArray.length];
			    for (int i = 0; i < stringArray.length; i++) {
			    	intArray[i] = Integer.parseInt(stringArray[i]);
			    	if(intArray[i]!=0){
					     PmsDays day = dayController.find(Integer.parseInt(stringArray[i]));
					     rateDetail.setDayPart(Integer.parseInt(stringArray[i]));
					     rateDetail.setDaysOfWeek(day.getDaysOfWeek());
					     
					     rateDetail.setIsActive(true);
						 rateDetail.setIsDeleted(false);
						 rateDetail.setBaseAmount(getBaseAmount());
						 rateDetail.setExtraChild(getExtraChild());
						 rateDetail.setExtraAdult(getExtraAdult());
						 rateDetail.setExtraInfant(getExtraInfant());
						 PropertyRate propertyRate = rateController.find(Integer.parseInt(RateIdArray[j]));
						 rateDetail.setPropertyRate(propertyRate);
						
						 rateDetailController.add(rateDetail);
			    	}
			    
			    }
			}
		    
		    //System.out.println("All days" + daysBetween);
		   // System.out.println("size" + items.size());
		   // for (int number : intArray) {
		    	    //if (int number : intArray) 
		    	 /*   {
		    	    	
		    	    	
		    	    	rateDetail.setIsActive(true);
						rateDetail.setIsDeleted(false);
					//	rateDetail.setDaysOfWeek(items.get(i));
						//int  dayNo =  DateUtil.getDay(items.get(i));
						//  System.out.println("shivabalan" + dayNo);
						//Integer bdDayPart = new Integer(dayNo);
						//PmsDays day = dayController.find(dayNo);
						//rateDetail.setDayPart(bdDayPart);
						rateDetail.setBaseAmount(getBaseAmount());
						PropertyRate propertyRate = rateController.find(getPropertyRateId());
						rateDetail.setPropertyRate(propertyRate);
					//	System.out.println(items.get(i));
						//PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
						//taxe.setPropertyTaxe(propertyTaxe);
						//rateDetailController.add(rateDetail);
						
		    	    }*/
		    	//}	
		     
		     
			
			
		    } catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
public String editRateDetail() {
		
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		this.propertyRateId = (Integer) sessionMap.get("propertyRateId");
		System.out.println("<--propertyRateId id-->");
		System.out.println("<--propertyRateId id-->"+getCheckedDays());
		System.out.println(getPropertyRateId());
		
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
	    //UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		PmsDayManager dayController = new PmsDayManager();
		
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(getStartDate());
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(getEndDate());
		DateTime start = DateTime.parse(startDate);
	    DateTime end = DateTime.parse(endDate);
	    
		try {
			
			this.rateDetailList = rateDetailController.list(getPropertyRateId()); 
			System.out.println("selected daysssss" +getPropertyRateId());
			for(PropertyRateDetail rate : rateDetailList){ 
				rate.setIsActive(false);
			  rate.setIsDeleted(true);
			rateDetailController.edit(rate);	
		
		}
			System.out.println("add Rate");
			PropertyRateDetail rateDetail = new PropertyRateDetail();
			
			   
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			String[] stringArray = getCheckedDays().split("\\s*,\\s*");
			System.out.println("selected days" + stringArray);
		    int[] intArray = new int[stringArray.length];
		    for (int i = 0; i < stringArray.length; i++) {
		    //String numberAsString = stringArray[i];
		    // System.out.println("selected daysssss" + Integer.parseInt(stringArray[i]));
		     intArray[i] = Integer.parseInt(stringArray[i]);
		     System.out.println("selected days" + Integer.parseInt(stringArray[i])); 
		     PmsDays day = dayController.find(Integer.parseInt(stringArray[i]));
		     rateDetail.setDayPart(Integer.parseInt(stringArray[i]));
		     rateDetail.setDaysOfWeek(day.getDaysOfWeek());
		     System.out.println("All days" +day.getDaysOfWeek());
		     rateDetail.setIsActive(true);
			rateDetail.setIsDeleted(false);
			rateDetail.setBaseAmount(getBaseAmount());
			PropertyRate propertyRate = rateController.find(getPropertyRateId());
			rateDetail.setPropertyRate(propertyRate);
			rateDetail.setExtraChild(getExtraChild());
			rateDetail.setExtraAdult(getExtraAdult());
			rateDetail.setExtraInfant(getExtraInfant());
			//PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
			//taxe.setPropertyTaxe(propertyTaxe);
			
			rateDetailController.add(rateDetail);
		    }
		    
		    //System.out.println("All days" + daysBetween);
		   // System.out.println("size" + items.size());
		   // for (int number : intArray) {
		    	    //if (int number : intArray) 
		    	 /*   {
		    	    	
		    	    	
		    	    	rateDetail.setIsActive(true);
						rateDetail.setIsDeleted(false);
					//	rateDetail.setDaysOfWeek(items.get(i));
						//int  dayNo =  DateUtil.getDay(items.get(i));
						//  System.out.println("shivabalan" + dayNo);
						//Integer bdDayPart = new Integer(dayNo);
						//PmsDays day = dayController.find(dayNo);
						//rateDetail.setDayPart(bdDayPart);
						rateDetail.setBaseAmount(getBaseAmount());
						PropertyRate propertyRate = rateController.find(getPropertyRateId());
						rateDetail.setPropertyRate(propertyRate);
					//	System.out.println(items.get(i));
						//PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
						//taxe.setPropertyTaxe(propertyTaxe);
						//rateDetailController.add(rateDetail);
						
		    	    }*/
		    	//}	
		     
		     
			
			
		    } catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	
	
	
	


	

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getDaysOfWeek() {
		return daysOfWeek;
	}

	public void setDaysOfWeek(Integer daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}

	public Integer getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(Integer baseAmount) {
		this.baseAmount = baseAmount;
	}

	public String getDayPart() {
		return dayPart;
	}

	public void setDayPart(String dayPart) {
		this.dayPart = dayPart;
	}
	
	
	public Integer getPropertyRateId() {
		return propertyRateId;
	}

	public void setPropertyRateId(Integer propertyRateId) {
		this.propertyRateId = propertyRateId;
	}


	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}
	

	public Timestamp getSmartPriceStartDate() {
		return smartPriceStartDate;
	}

	public void setSmartPriceStartDate(Timestamp smartPriceStartDate) {
		this.smartPriceStartDate = smartPriceStartDate;
	}

	public Timestamp getSmartPriceEndDate() {
		return smartPriceEndDate;
	}

	public void setSmartPriceEndDate(Timestamp smartPriceEndDate) {
		this.smartPriceEndDate = smartPriceEndDate;
	}
	
	public Integer getSmartPricePropertyAccommodationId() {
		return smartPricePropertyAccommodationId;
	}

	public void setSmartPricePropertyAccommodationId(Integer smartPricePropertyAccommodationId) {
		this.smartPricePropertyAccommodationId = smartPricePropertyAccommodationId;
	}
	
	public Boolean getSmartPriceIsActiveOrNot() {
        return smartPriceIsActiveOrNot;
    }  
    public void setSmartPriceIsActiveOrNot(Boolean smartPriceIsActiveOrNot) {
        this.smartPriceIsActiveOrNot = smartPriceIsActiveOrNot;
    }

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getCheckedDays() {
		return checkedDays;
	}

	public void setCheckedDays(String checkedDays) {
		this.checkedDays = checkedDays;
	}

	public PropertyRateDetail getRateDetail() {
		return rateDetail;
	}

	public void setRateDetail(PropertyRateDetail rateDetail) {
		this.rateDetail = rateDetail;
	}
	
	public PmsDays getDays() {
		return days;
	}

	public void setDays(PmsDays days) {
		this.days = days;
	}

	public List<PropertyRateDetail> getRateDetailList() {
		return rateDetailList;
	}

	public void setRateDetailList(List<PropertyRateDetail> rateDetailList) {
		this.rateDetailList = rateDetailList;
	}

	public Double getExtraChild(){
		return extraChild;
	}
	
	public void setExtraChild(Double extraChild){
		this.extraChild=extraChild;
	}
	
	public Double getExtraAdult(){
		return extraAdult;
	}
	
	public void setExtraAdult(Double extraAdult){
		this.extraAdult=extraAdult;
	}
	
	public Double getExtraInfant(){
		return extraInfant;
	}
	
	public void setExtraInfant(Double extraInfant){
		this.extraInfant=extraInfant;
	}
	
	private Long getAvailableCount(int propertyId,Date curdate,long available,int accommodationId) throws IOException {
		System.out.println("get available count");
		PmsBookingManager bookingController = new PmsBookingManager();
		
		List<Long> myList = new ArrayList<Long>();
		
		long rmc = 0;
		this.roomCnt = rmc;
		PmsAvailableRooms roomCount = bookingController.findCount(curdate, accommodationId);
		if(roomCount.getRoomCount() == null){
			
			this.roomCnt = (available-rmc);
		}
		else{
			
			this.roomCnt = (available-roomCount.getRoomCount());
		}
		
		myList.add(this.roomCnt);
		System.out.println("roomCount " + this.roomCnt);
		
        System.out.println("Minimum : "+Collections.min(myList));
        long availableCount = Collections.min(myList);

        return availableCount;
          
	}

	


}
