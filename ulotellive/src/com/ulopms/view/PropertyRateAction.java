package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Calendar;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

import com.ulopms.util.DateUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.derby.tools.sysinfo;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;

























import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.select.Evaluator.IsEmpty;

import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsDayManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PromotionManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyRateDetailManager;
import com.ulopms.controller.PropertyRateManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.model.PropertyRateOrder;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyRateAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	
	private Integer  propertyRateId;
	private Integer propertyId;
	private Integer sourceId;
	private Integer propertyAccommodationId;
	private String  rateName;
	private String  checkedDays;
	private Integer  propertyRateDetailsId;
	private Timestamp  startDate;
	private Timestamp  endDate;
	private Integer  orderBy;
	private Timestamp arrivalDate;
    private Timestamp departureDate;
    private String chartDate;
	public Integer accommodationId;
	private Timestamp modStartDate;
    private Timestamp modEndDate;
    private String modSources;
    private Integer smartPricePropertyAccommodationId;
    private Timestamp smartPriceStartDate;
    private Timestamp smartPriceEndDate;
    private Boolean smartPriceIsActiveOrNot;
    private String resultDate;
    private String updatePropertyRateId;
    
	
	
	

	private PropertyRate rate;
	
	
	private List<PropertyRate> rateList;
	
	private List<PropertyRateDetail> rateDetailList;
	
	private List<PmsSource> sourceList;
	
	private List<PmsDays> daylist;
	
	private List<PropertyRate> listSmartPrices;
	
	private List<PropertyRate> listPropertyRates;
	
	List<PropertyRateOrder> orders = new ArrayList<PropertyRateOrder>();

	private static final Logger logger = Logger.getLogger(PropertyItemAction.class);
	
	private long roomCnt;
	
	public long getRoomCnt() {
		return roomCnt;
	}

	public void setRoomCnt(long roomCnt) {
		this.roomCnt = roomCnt;
	}

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyRateAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}
	
public String getDay() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		 //System.out.println(getPropertyId());
		//System.out.println("all categories");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsDayManager dayController = new PmsDayManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.daylist = dayController.list();
			for (PmsDays days : daylist) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"dayId\":\"" + days.getDayId() + "\"";
				jsonOutput += ",\"daysOfWeek\":\"" + days.getDaysOfWeek()+ "\"";
				jsonOutput += ",\"dayPart\":\"" + days.getDayPart()+ "\"";
				//jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
				//jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

public String getRoomRate() throws IOException {
    
    this.propertyId = (Integer) sessionMap.get("propertyId");
    
    this.arrivalDate = getArrivalDate();
   sessionMap.put("arrivalDate",arrivalDate);
        
    this.departureDate = getDepartureDate();
    sessionMap.put("departureDate",departureDate);
 
    try {        String jsonOutput = "";
        HttpServletResponse response = ServletActionContext.getResponse();
    
        
        PropertyRateManager rateController = new PropertyRateManager();
        PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();        
        response.setContentType("application/json");
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.departureDate);
       cal.add(Calendar.DATE, -1);
        String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
        String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        DateTime arrival = DateTime.parse(startDate);
        DateTime departure = DateTime.parse(endDate);
        java.util.Date arrival1 = arrival.toDate();
        java.util.Date departure1 = departure.toDate();        
                
                int dateCount=0;
                 double amount=0;
                 double totalAmount=0;
                List<DateTime> between = DateUtil.getDateRange(arrival, departure);
                for (DateTime d : between){
                                        if (!jsonOutput.equalsIgnoreCase(""))
                        jsonOutput += ",{";
                    
                    else
                        jsonOutput += "{";
                
                List<PropertyRate> propertyRateList = rateController.list1(getPropertyAccommodationId(),getSourceId(),d.toDate());            
             System.out.println("liiiiiiistttttt"+propertyRateList.size());
            
             if(propertyRateList.size()>0)
             {
                
                 for (PropertyRate rates : propertyRateList)
                    
                    
                {        
                    
                     jsonOutput += "\"rateId\":\"" + rates.getPropertyRateId() + "\"";
                 jsonOutput += ",\"rateName\":\"" + rates.getRateName()+ "\"";
                 jsonOutput += ",\"accommodationType\":\"" + rates.getPropertyAccommodation().getAccommodationType()+ "\"";
                 jsonOutput += ",\"orderBy\":\"" + rates.getOrderBy()+ "\"";
                 String start = new SimpleDateFormat("MM/dd/yyyy").format(rates.getStartDate());
                 String end = new SimpleDateFormat("MM/dd/yyyy").format(rates.getEndDate());
                 jsonOutput += ",\"startDate\":\"" + start + "\"";
                 jsonOutput += ",\"endDate\":\"" + end + "\"";
                 
                 DateFormat f = new SimpleDateFormat("EEEE");
                 //System.out.printeln(propertyRateList.getPropertyRateId());
                 System.out.println("seding" + f.format(d.toDate()).toLowerCase());
                 List<PropertyRateDetail> rateDetailList =   rateDetailController.list(rates.getPropertyRateId(),f.format(d.toDate()).toLowerCase());
                 if(rateDetailList.size()>0)
                 {
                     for (PropertyRateDetail rate : rateDetailList)
                     {
                         amount +=  rate.getBaseAmount();
                         System.out.println("Offer" +amount);
                     }
                 }
                 else
                 {
                    // amount += available.getBaseAmount();
                     System.out.println("Offer" +amount);
                 }                
                
                }
                // totalAmount = amount;
                
                 totalAmount = amount;
                 jsonOutput += ",\"baseAmount\":\"" + totalAmount + "\"";
                    System.out.println("Offer ammount" +totalAmount);
                    
            
             } 
            
            //PropertyRateDetail rateDetail = rateDetailController.find1(rates.getPropertyRateId());        
            //jsonOutput += ",\"baseAmount\":\"" + rateDetail.getBaseAmount()+ "\"";
            //jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
            //jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
            jsonOutput += "}";            
                }    
            
                
                
                
                
                       response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
            } catch (Exception e) {
        logger.error(e);
        e.printStackTrace();
    } finally {  
    	
    }   
    return null;
    }

public String getRates() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		 //System.out.println(getPropertyId());
		//System.out.println("all categories");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.rateList = rateController.list(getPropertyId());
			for (PropertyRate rates : rateList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"rateId\":\"" + rates.getPropertyRateId() + "\"";
				jsonOutput += ",\"rateName\":\"" + rates.getRateName()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + rates.getPropertyAccommodation().getAccommodationType()+ "\"";
				jsonOutput += ",\"orderBy\":\"" + rates.getOrderBy()+ "\"";
				String start = new SimpleDateFormat("MM/dd/yyyy").format(rates.getStartDate());
				String end = new SimpleDateFormat("MM/dd/yyyy").format(rates.getEndDate());
				jsonOutput += ",\"startDate\":\"" + start + "\"";
				jsonOutput += ",\"endDate\":\"" + end + "\"";				
				PropertyRateDetail rateDetail = rateDetailController.find1(rates.getPropertyRateId());        
				jsonOutput += ",\"baseAmount\":\"" + rateDetail.getBaseAmount()+ "\"";
				//jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
				//jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

public String getRate() throws IOException {
	
	this.propertyId = (Integer) sessionMap.get("propertyId");
	 //System.out.println(getPropertyId());
	//System.out.println("all categories");
	// System.out.println(jobExecutionList.size());
	try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	
		
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
		PmsDayManager dayController = new PmsDayManager();
		//this.familyRegisterList = familyController.list(getUser().getUserid());
		//model = familyRegisterList;
		response.setContentType("application/json");

		List<PropertyRate> rateList =  rateController.list1(getPropertyRateId());
		for (PropertyRate rate : rateList) {

			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"rateId\":\"" + rate.getPropertyRateId() + "\"";
			jsonOutput += ",\"rateName\":\"" + rate.getRateName()+ "\"";
			String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(rate.getStartDate());
			String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(rate.getEndDate());
			jsonOutput += ",\"startDate\":\"" + checkIn + "\"";
			jsonOutput += ",\"endDate\":\"" + checkOut + "\"";
			jsonOutput += ",\"sourceId\":\"" + rate.getPmsSource().getSourceId()+ "\"";		
			jsonOutput += ",\"accommodationType\":\"" + rate.getPropertyAccommodation().getAccommodationType()+ "\"";
			jsonOutput += ",\"accommodationId\":\"" + rate.getPropertyAccommodation().getAccommodationId()+ "\"";
			PropertyRateDetail rateDetail = rateDetailController.find1(rate.getPropertyRateId());        
			jsonOutput += ",\"baseAmount\":\"" + rateDetail.getBaseAmount()+ "\"";
			
			
			String jsonDays ="";
			
			//System.out.println(propertyRateDetail);
	//		if(propertyRateDetail.size()>0)
	//		{
			
			this.daylist = dayController.list(); 
				for(PmsDays days : daylist){
					
					List<PropertyRateDetail> rateDetailList = rateDetailController.list(rate.getPropertyRateId(),days.getDayPart());
					if (!jsonDays.equalsIgnoreCase(""))
						
						jsonDays += ",{";
					else
						jsonDays += "{";
					
					//PropertyRateDetail propertyRateDetail1 = rateDetailController.find(ratedetail.getPropertyRateDetailsId());
					
					//System.out.println("baseAmount"+ rateDetailList);
					
					jsonDays += "\"daysOfWeek\":\"" + days.getDaysOfWeek()+ "\"";
					jsonDays += ",\"dayPart\":\"" + days.getDayPart()+ "\"";
					jsonDays += ",\"isActive\":\"" + days.getIsActive()+ "\"";					
					if(rateDetailList.size()>0)
						jsonDays += ",\"status\":\"true\""; else jsonDays += ",\"status\":\"false\"";
					jsonDays += "}";
				
				}
				jsonOutput += ",\"days\":[" + jsonDays+ "]";
//			}
			
			//jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
			//jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
			jsonOutput += "}";


		}


		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

}

	public String addSmartPriceRate(){
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		this.smartPriceStartDate = getSmartPriceStartDate();
		sessionMap.put("smartPriceStartDate",smartPriceStartDate); 
	      
		this.smartPriceEndDate = getSmartPriceEndDate();
	    sessionMap.put("smartPriceStartDate",smartPriceEndDate); 
			
		this.smartPricePropertyAccommodationId = getSmartPricePropertyAccommodationId();
		sessionMap.put("smartPricePropertyAccommodationId",smartPricePropertyAccommodationId);
		
		this.smartPriceIsActiveOrNot = getSmartPriceIsActiveOrNot();
	    sessionMap.put("smartPriceIsActive",smartPriceIsActiveOrNot); 
	    
	    try{
	    	if(this.smartPriceStartDate==null || this.smartPriceEndDate==null || this.smartPricePropertyAccommodationId==null || this.smartPriceIsActiveOrNot==null){
	    		return null;
	    	}
	    	String jsonOutput="";
	    	HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
	    	PropertyRate rate = new PropertyRate();
	    	PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			Boolean rateChk=false;
			String strStartDate=null,strEndDate=null;
			List<PropertyRate> listRateCheckAll=null;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			
	    	strStartDate=format1.format(getSmartPriceStartDate());
    		strEndDate=format1.format(getSmartPriceEndDate());
    		DateTime FromDate = DateTime.parse(strStartDate);
	        DateTime ToDate = DateTime.parse(strEndDate);
    		
	        List<DateTime> dateBetween = DateUtil.getDateRange(FromDate, ToDate);
	        Iterator<DateTime> iterDateValues=dateBetween.iterator();
	        while(iterDateValues.hasNext()){
	        	DateTime dateValues=iterDateValues.next();
	        	java.util.Date DateValues = dateValues.toDate();
	        	listRateCheckAll=rateController.listSmartPriceCheckDetails(propertyId, smartPricePropertyAccommodationId,DateValues);
	        	if(listRateCheckAll.size()>0 && !listRateCheckAll.isEmpty()){
	        		rateChk=true;
	        	}
	        }
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			if(rateChk){
				jsonOutput += "\"check\":\"" + rateChk+ "\"";
			}
			else if(!rateChk){
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				List<PropertyRate> rateList =  rateController.list(propertyId,smartPricePropertyAccommodationId);            
	            Object orderBy = rateList.size();            
	            String converted = orderBy .toString();            
	            int orderBy1 = 0;
	            orderBy1 = Integer.parseInt(converted) + 1;
	            
	            rate.setIsActive(true);
				rate.setIsDeleted(false);
				rate.setStartDate(smartPriceStartDate);
				rate.setEndDate(smartPriceEndDate);
				rate.setOrderBy(orderBy1);
				rate.setRateName("Smart Price");
				rate.setSmartPriceIsActive(getSmartPriceIsActiveOrNot());
				PmsProperty property = propertyController.find(getPropertyId());
				rate.setPmsProperty(property);
				PropertyAccommodation propertyAccommodation = accommodationController.find(smartPricePropertyAccommodationId);
				rate.setPropertyAccommodation(propertyAccommodation);
				PmsSource source = sourceController.find(1);
				rate.setPmsSource(source);
				
				rateController.add(rate);
				this.propertyRateId = rate.getPropertyRateId();
				sessionMap.put("propertyRateId",propertyRateId);
				this.propertyRateId = (Integer) sessionMap.get("propertyRateId");
				
				jsonOutput += "\"check\":\"" + rateChk+ "\"";
				
			}
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	    }
	    catch(Exception e)
	    {
	    	logger.error(e);
			e.printStackTrace();
	    }finally{
	    	
	    }
	    
		return null;
	}
	
	
	public String addRate() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsSourceManager sourceController = new PmsSourceManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		
		try {
	        PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();    
			System.out.println("add Rate");
			PropertyRate rate = new PropertyRate();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			List<PropertyRate> rateList =  rateController.list(getPropertyId(),getPropertyAccommodationId());            
            Object orderBy = rateList.size();            
            String converted = orderBy .toString();            
            int orderBy1 = 0;
            orderBy1 = Integer.parseInt(converted) + 1;
            
			String[] stringArray = getModSources().split("\\s*,\\s*");
			System.out.println("selected sources" + stringArray);
			int RateId=0;
			String strRateId="";
		    for (int i = 0; i < stringArray.length; i++) {
		    	rate.setIsActive(true);
				rate.setIsDeleted(false);
				rate.setStartDate(getModStartDate());
				rate.setEndDate(getModEndDate());
				rate.setOrderBy(orderBy1);
				//rate.setRateName(getRateName());
				rate.setSmartPriceIsActive(false);
				PmsProperty property = propertyController.find(getPropertyId());
				rate.setPmsProperty(property);
				PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
				rate.setPropertyAccommodation(propertyAccommodation);
				int sourceId=Integer.parseInt(stringArray[i]);
				PmsSource source = sourceController.find(sourceId);
				rate.setPmsSource(source);
				//PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
				//taxe.setPropertyTaxe(propertyTaxe);
				rateController.add(rate);
				RateId=rate.getPropertyRateId();
				strRateId=String.valueOf(RateId)+","+strRateId;
		    }
			
			
			//System.out.println(getTaxType());
		    
			System.out.println("strRateId..."+strRateId);
			
			this.propertyRateId = rate.getPropertyRateId();
			sessionMap.put("propertyRateId",propertyRateId);
			sessionMap.put("RateId", strRateId);
			
			this.propertyRateId = (Integer) sessionMap.get("propertyRateId");
			System.out.println("<--propertyRateId id-->");
			System.out.println(getPropertyRateId());
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
public String editRate() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsSourceManager sourceController = new PmsSourceManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		boolean dateChk=false;
		try {
			
			String alldates=null;	
			TreeSet<String> setValues=new TreeSet<String>();
			Set<String> setDateValues=new HashSet<String>();
			System.out.println("edit Rate");
			System.out.println(getPropertyRateId());
			PropertyRate rate = rateController.find(getPropertyRateId());
			Map<String, String> findDateMap = new HashMap<String, String>();
			java.util.Date sysdate=new java.util.Date();
			ArrayList<String> arrListDate=new ArrayList<String>();
			ArrayList<String> arrListDetail=new ArrayList<String>();
			ArrayList<String> arrListDateId=new ArrayList<String>();
			//PropertyDiscount editdis = propertyDiscountController.find(getPropertyDiscountId());
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			/*List<PropertyRate> rateList =  rateController.list(getPropertyId(),getPropertyAccommodationId());            
            Object orderBy = rateList.size();            
            String converted = orderBy .toString();            
            int orderBy1 = 0;
            orderBy1 = Integer.parseInt(converted) + 1;*/
			//System.out.println(getTaxType());
			int fromDay=0,fromMonth=0,fromYear=0;
			int toDay=0,toMonth=0,toYear=0;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(getStartDate());
			java.util.Date startDate = calStart.getTime();             
	    	String displayFromDate = format1.format(startDate);  
	    	String[] fromDays=displayFromDate.split("-");
	    	fromDay=Integer.valueOf(fromDays[2]);
    		fromMonth=Integer.valueOf(fromDays[1]);
    		fromYear=Integer.valueOf(fromDays[0]);
	    	fromDay=calStart.getActualMinimum(calStart.DAY_OF_MONTH);
	    	String strFromDate=fromYear+"-"+fromMonth+"-"+fromDay;
	    	//String startDay=getDayOfWeek(calStart.get(Calendar.DAY_OF_WEEK));
	    	
	    	Calendar calEnd=Calendar.getInstance();
	    	calEnd.setTime(getEndDate());
	    	java.util.Date endDate = calEnd.getTime();  
	    	String displayToDate = format1.format(endDate); 
	    	String[] toDays=displayToDate.split("-");
	    	toDay=Integer.valueOf(toDays[2]);
    		toMonth=Integer.valueOf(toDays[1]);
    		toYear=Integer.valueOf(toDays[0]);
	    	toDay=calEnd.getActualMaximum(calEnd.DAY_OF_MONTH);
	    	String strToDate=toYear+"-"+toMonth+"-"+toDay;
	    	//String endDay=getDayOfWeek(calEnd.get(Calendar.DAY_OF_WEEK));
	    	/*while (calStart.before(calEnd)) {
	    	    System.out.print(printCalendar(calStart));
	    	    if (i % 7 == 0) {   // last day of the week
	    	        System.out.println();
	    	        i  = 1;
	    	    } else {
	    	        System.out.print(" - ");
	    	        i++;
	    	    }
	    	    calStart.add(DATE, 1);
	    	}*/
	    	
	    	for ( int i = 0; i < toDay; i++) {
	    		calStart.set(Calendar.DAY_OF_MONTH, i + 1);
	    		alldates=format1.format(calStart.getTime());
	    		setDateValues.add(alldates);
	            System.out.print(", " + format1.format(calStart.getTime()));
	        }
	    	
			String strStartDate=null,strEndDate=null,strPropertyRateId=null,strAmount=null,strDaysofWeek=null,strDays=null,strDateValues=null;
		    List<PropertyRate> dateList =  rateController.list(getPropertyId(),getPropertyAccommodationId(),getSourceId(),getStartDate(),getEndDate());
		    if(!dateList.isEmpty()){
		    	for (PropertyRate rates : dateList) {
		    		strPropertyRateId=String.valueOf(rates.getPropertyRateId());
		    		strStartDate=format1.format(rates.getStartDate());
		    		strEndDate=format1.format(rates.getEndDate());
		    		DateTime FromDate = DateTime.parse(strStartDate);
			        DateTime ToDate = DateTime.parse(strEndDate);
			        
			        int propertyRatesId=rates.getPropertyRateId();
		    		List<PropertyRateDetail> rateDetailList =  rateDetailController.listRateDetail(propertyRatesId);
		    		
			        List<DateTime> dateBetween = DateUtil.getDateRange(FromDate, ToDate);
			        Iterator<DateTime> iterDateValues=dateBetween.iterator();
			        while(iterDateValues.hasNext()){
			        	DateTime dateValues=iterDateValues.next();
			        	java.util.Date DateValues = dateValues.toDate();
			        	strDateValues=format1.format(DateValues);
			        	if(!arrListDateId.contains(strDateValues)){
			        		arrListDateId.add(strDateValues);
				        	arrListDateId.add(strPropertyRateId);
				        	if(!rateDetailList.isEmpty()){
				    			for(PropertyRateDetail rateDetails: rateDetailList){
				    				strAmount=String.valueOf(rateDetails.getBaseAmount());
				    				strDaysofWeek=rateDetails.getDaysOfWeek().toLowerCase();
				    				List<String> daysBetween = DateUtil.getDayRange(FromDate, ToDate);
				    				Iterator<String> iterDayValues=daysBetween.iterator();
							        while(iterDayValues.hasNext()){
							        	strDays=iterDayValues.next().toLowerCase();
							        	if(strDaysofWeek.contains(strDays)){
							        		arrListDateId.add(strDaysofWeek);
							        		arrListDateId.add(strAmount);
							        	}
							        }
				    			}
				    		}
			        	}
			        	
			        }
		    		dateChk=true;
		    	}
		    }
		    
		    System.out.println(arrListDateId);
		    
		   if(!dateChk){
		    	rate.setIsActive(true);
				rate.setIsDeleted(false);
				rate.setStartDate(getStartDate());
				rate.setEndDate(getEndDate());
				//rate.setOrderBy(orderBy1);
				rate.setRateName(getRateName());
				PmsProperty property = propertyController.find(getPropertyId());
				rate.setPmsProperty(property);
				PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
				rate.setPropertyAccommodation(propertyAccommodation);
				PmsSource source = sourceController.find(getSourceId());
				rate.setPmsSource(source);
				//PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
				//taxe.setPropertyTaxe(propertyTaxe);
				rateController.edit(rate);
		    }
			
			this.propertyRateId = (Integer) sessionMap.get("propertyRateId");
			System.out.println("<--propertyRateId id-->");
			System.out.println(getPropertyRateId());
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteRates() {
		
		PmsSourceManager sourceController = new PmsSourceManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			try {
				
							
				System.out.println("delete discount");
				
				//System.out.println(getPropertyDiscountId());
				
				PropertyRate deleterate = rateController.find(getPropertyRateId());
							
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				
				deleterate.setIsActive(false);
				deleterate.setIsDeleted(true);
				deleterate.setModifiedDate(new java.sql.Timestamp(date.getTime()));
				rateController.edit(deleterate);
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return null;
		}  

	public String editRateOrders() throws IOException {
    	
		   try {
			   
			   
			  System.out.println("<!--rate orders-->");
				
	           
	            
			   String jsonOutput = "";
			   HttpServletResponse response = ServletActionContext.getResponse();
			
				
			   this.propertyId = (Integer) sessionMap.get("propertyId");
			   PmsSourceManager sourceController = new PmsSourceManager();
			   PropertyRateManager rateController = new PropertyRateManager();
			   PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			   //UserLoginManager  userController = new UserLoginManager();
			   PmsPropertyManager propertyController = new PmsPropertyManager();
			   response.setContentType("application/json");			   
			  // System.out.println(orders.size());
			   /*int orderBy1 = 0;
	            orderBy1 = (orders.size()) + 1;
			   System.out.println("shiva"+ orderBy1);*/
			 
	    		for (int i = 0; i < orders.size(); i++) 
	  	      {
	    	  
	    	    //System.out.println("--->orders");
	  		    System.out.println(orders.get(i).getRateId());
	  		    PropertyRate rate = rateController.find(orders.get(i).getRateId());
	  		    rate.setOrderBy(i);
				rateController.edit(rate);
	  		  
	  	      } 				
	    		
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null ;

	    	
	    }
	
    public String getSources() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		 //System.out.println(getPropertyId());
		//System.out.println("all categories");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsSourceManager sourceController = new PmsSourceManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.sourceList = sourceController.list();
			for (PmsSource source : sourceList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"sourceId\":\"" + source.getSourceId() + "\"";
				jsonOutput += ",\"sourceName\":\"" + source.getSourceName()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
    public String getDays() {
		
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsSourceManager sourceController = new PmsSourceManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		
		try {
			
						
			System.out.println("get Days");
			PropertyRate rate = new PropertyRate();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			Calendar c = Calendar.getInstance();
			
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(getStartDate());
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(getEndDate());
			DateTime start = DateTime.parse(startDate);
		    DateTime end = DateTime.parse(endDate);
			//System.out.println(getTaxType());
			 /*List<DateTime> between = DateUtil.getDateRange(start, end);
		     for (DateTime d : between)
		     {
		    	 DateTimeFormatter fmt = DateTimeFormat.forPattern("E");
		    	 String dtStr = fmt.print(d);
		    	 System.out.println(dtStr);
		    	// String df = d.toString();
		    	 //SimpleDateFormat simpleDateformat = new SimpleDateFormat("E"); // the day of the week abbreviated
		         //System.out.println(simpleDateformat.format(df));
		        // String dfs = new SimpleDateFormat("E").format(d);
		    	// System.out.println(d.toString());
		    	 
		     }
		     */
		     List<String> items = Arrays.asList(getCheckedDays().split("\\s*,\\s*"));
		     List<String> daysBetween = DateUtil.getDayRange(start, end);
		     
		   /*  for (int i = 0; i < items.size(); i++) {
		    	    if (daysBetween.contains(items.get(i))) {
		    	    	
		    	        System.out.println("Equals..: " + items.get(i));
		    	    }
		    	}	
		    */
		     
		     for (String days : daysBetween)
		     {
		    	 
		    	 System.out.println("daysonly" +days);
		    	 int dayNo =  DateUtil.getDay(days);
		    	 System.out.println("dayNo" +dayNo);
		    	// String df = d.toString();
		    	 //SimpleDateFormat simpleDateformat = new SimpleDateFormat("E"); // the day of the week abbreviated
		         //System.out.println(simpleDateformat.format(df));
		        // String dfs = new SimpleDateFormat("E").format(d);
		    	// System.out.println(d.toString());
		    	 
		     }
			
			//System.out.println("startdate--enddate");
			//System.out.println(getStartDate());
			//System.out.println(getEndDate());
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	


	

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}
	
	public Integer getSmartPricePropertyAccommodationId() {
		return smartPricePropertyAccommodationId;
	}

	public void setSmartPricePropertyAccommodationId(Integer smartPricePropertyAccommodationId) {
		this.smartPricePropertyAccommodationId = smartPricePropertyAccommodationId;
	}

	public Integer getPropertyRateId() {
		return propertyRateId;
	}

	public void setPropertyRateId(Integer propertyRateId) {
		this.propertyRateId = propertyRateId;
	}
	
	public Integer getPropertyRateDetailsId() {
		return propertyRateDetailsId;
	}

	public void setPropertyRateDetailsId(Integer propertyRateDetailsId) {
		this.propertyRateDetailsId = propertyRateDetailsId;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public String getRateName() {
		return rateName;
	}

	public void setRateName(String rateName) {
		this.rateName = rateName;
	}
	
	
	
	public Timestamp getSmartPriceStartDate() {
		return smartPriceStartDate;
	}

	public void setSmartPriceStartDate(Timestamp smartPriceStartDate) {
		this.smartPriceStartDate = smartPriceStartDate;
	}

	public Timestamp getSmartPriceEndDate() {
		return smartPriceEndDate;
	}

	public void setSmartPriceEndDate(Timestamp smartPriceEndDate) {
		this.smartPriceEndDate = smartPriceEndDate;
	}
	
	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	

	public Timestamp getArrivalDate() {
        return arrivalDate;
    }
    public void setArrivalDate(Timestamp arrivalDate) {
        this.arrivalDate = arrivalDate;
    }    
    public Timestamp getDepartureDate() {
        return departureDate;
    }  
    public void setDepartureDate(Timestamp departureDate) {
        this.departureDate = departureDate;
    }

	public Integer getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

	public PropertyRate getPropertyRate() {
		return rate;
	}

	public void setPropertyRate(PropertyRate rate) {
		this.rate = rate;
	}

	public List<PropertyRate> getRateList() {
		return rateList;
	}

	public void setRateList(List<PropertyRate> rateList) {
		this.rateList = rateList;
	}
	
	public String getCheckedDays() {
		return checkedDays;
	}

	public void setCheckedDays(String checkedDays) {
		this.checkedDays = checkedDays;
	}
	 
	public String getModSources(){
		return modSources;
	}
	
	public void setModSources(String modSources){
		this.modSources=modSources;
	}
	 public List<PmsSource> getSourceList() {
			return sourceList;
		}
	
	 public void setSourceList(List<PmsSource> sourceList) {
			this.sourceList = sourceList;
		}
	 
	 public List<PropertyRateOrder> getOrders() {
	        return orders;
	 }
	    
	 public void setOrders(List<PropertyRateOrder> orders) {
	        this.orders = orders;
	  }
	 
	 public String getChartDate(){
		 return chartDate;
	 }
	 
	 public void setChartDate(String chartDate){
		 this.chartDate=chartDate;
	 }
	 
	 public String getResultDate(){
		 return resultDate;
	 }
	 
	 public void setResultDate(String resultDate){
		 this.resultDate=resultDate;
	 }
	 
	 public Integer getAccommodationId(){
		 return accommodationId;
	 }
	 
	 public void setAccommodationId(Integer accommodationId){
		 this.accommodationId=accommodationId;
	 }
	 
	 public Timestamp getModStartDate() {
        return modStartDate;
    }
    public void setModStartDate(Timestamp modStartDate) {
        this.modStartDate = modStartDate;
    }    
    public Timestamp getModEndDate() {
        return modEndDate;
    }  
    public void setModEndDate(Timestamp modEndDate) {
        this.modEndDate = modEndDate;
    }
    public Boolean getSmartPriceIsActiveOrNot() {
        return smartPriceIsActiveOrNot;
    }  
    public void setSmartPriceIsActiveOrNot(Boolean smartPriceIsActiveOrNot) {
        this.smartPriceIsActiveOrNot = smartPriceIsActiveOrNot;
    }
    public String getUpdatePropertyRateId() {
		return updatePropertyRateId;
	}

	public void setUpdatePropertyRateId(String updatePropertyRateId) {
		this.updatePropertyRateId = updatePropertyRateId;
	}
	
	 public String getBookingRates() throws IOException, ParseException {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
		    sessionMap.put("chartDate",chartDate); 
		    
			if(getAccommodationId()==null){
				accommodationId=0;
			}
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();

			
			PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
//		    AccommodationRoom accommodationRoom =  roomController.findCount(getPropertyId(),accommodationId);
//		    PropertyRateManager rateController = new PropertyRateManager();
			response.setContentType("application/json");

			for (int i=0; i<chartDate.split(",").length; i++)
			{
				List<PropertyAccommodation> accommodationList= roomController.list(accommodationId,getPropertyId());
				
				//System.out.println(bookingDetailList.size());
				for (PropertyAccommodation accommodationRate : accommodationList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput = "{";
				
				    jsonOutput += "\"title\":\"" +" Rent: "+(accommodationRate.getBaseAmount())+" \"";
				    jsonOutput += ",\"start\":\"o\"";
				    
			        jsonOutput += "}";
				
				}
			        
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			System.out.println(jsonOutput);
			
			return null;
			
		}
	
	 public String getRateDetails() throws IOException, ParseException{
		 try {
		 	this.arrivalDate = getArrivalDate();
		    sessionMap.put("startDate",arrivalDate); 
			
			this.departureDate = getDepartureDate();
			sessionMap.put("endDate",departureDate); 
		      
			this.sourceId = getSourceId();
		    sessionMap.put("sourceId",sourceId); 
				
			this.accommodationId = getAccommodationId();
			sessionMap.put("accommodationId",accommodationId);
			
			if(this.arrivalDate==null || this.departureDate==null || this.accommodationId==null){
				return null;
			}
			this.propertyId = (Integer) sessionMap.get("propertyId");
		    
			sessionMap.put("chartDate",chartDate); 
			this.chartDate = (String) sessionMap.get("chartDate");
			
			String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(getArrivalDate());
		    String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(getDepartureDate());
		    sessionMap.put("checkIn",checkIn);
			sessionMap.put("checkOut",checkOut);
			HttpServletResponse response = ServletActionContext.getResponse();
			PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			boolean dateChk=false;
			
			int fromDay=0,fromMonth=0,fromYear=0;
			int toDay=0,toMonth=0,toYear=0;
			Boolean isSmartPriceActive=false;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(getArrivalDate());
			java.util.Date startDate = calStart.getTime();             
	    	String displayFromDate = format1.format(startDate);  
	    	String[] fromDays=displayFromDate.split("-");
	    	fromDay=Integer.valueOf(fromDays[2]);
    		fromMonth=Integer.valueOf(fromDays[1]);
    		fromYear=Integer.valueOf(fromDays[0]);
	    	fromDay=calStart.getActualMinimum(calStart.DAY_OF_MONTH);
	    	String strFromDate=fromYear+"-"+fromMonth+"-"+fromDay;
	    	//String startDay=getDayOfWeek(calStart.get(Calendar.DAY_OF_WEEK));
	    	
	    	Calendar calEnd=Calendar.getInstance();
	    	calEnd.setTime(getDepartureDate());
	    	java.util.Date endDate = calEnd.getTime();  
	    	String displayToDate = format1.format(endDate); 
	    	String[] toDays=displayToDate.split("-");
	    	toDay=Integer.valueOf(toDays[2]);
    		toMonth=Integer.valueOf(toDays[1]);
    		toYear=Integer.valueOf(toDays[0]);
	    	toDay=calEnd.getActualMaximum(calEnd.DAY_OF_MONTH);
	    	String strToDate=toYear+"-"+toMonth+"-"+toDay;
	    	
	    	java.util.Date fromDate = format1.parse(strFromDate);
			Timestamp tsFromDate=new Timestamp(fromDate.getTime());

			java.util.Date toDate = format1.parse(strToDate);
			Timestamp tsToDate=new Timestamp(toDate.getTime());
			
			String jsonOutput = "";
			String jsonSmart ="";
			
			response.setContentType("application/json");
			   
			HashMap<String,String> dateMap=new HashMap<String,String>();
			HashMap<String,String> dayMap=new HashMap<String,String>();
			ArrayList<Integer> listSmartPriceChecked=new ArrayList<Integer>();
			
			ArrayList<String> arrListDateId=new ArrayList<String>();
			SimpleDateFormat formatEE = new SimpleDateFormat("EE", Locale.ENGLISH);
			SimpleDateFormat formatEEEE = new SimpleDateFormat("EEEE", Locale.ENGLISH);
			ArrayList<String> arrListRateDetail=new ArrayList<String>();
			List<PropertyAccommodation> accommodationList= roomController.list(accommodationId,getPropertyId());
			
			String strStartDate=null,strEndDate=null,strPropertyRateId=null,strAmount=null,strDaysofWeek=null,strDays=null,strDateValues=null;
		    List<PropertyRate> dateList =  rateController.list(getPropertyId(),getAccommodationId(),getSourceId(),tsFromDate,tsToDate);
		    if(!dateList.isEmpty()){
		    	for (PropertyRate rates : dateList) {
		    		strPropertyRateId=String.valueOf(rates.getPropertyRateId());
		    		strStartDate=format1.format(rates.getStartDate());
		    		strEndDate=format1.format(rates.getEndDate());
		    		DateTime FromDate = DateTime.parse(strStartDate);
			        DateTime ToDate = DateTime.parse(strEndDate);
			        isSmartPriceActive=rates.getSmartPriceIsActive();
			        
			        int propertyRatesId=rates.getPropertyRateId();
			        if(isSmartPriceActive){
			        	listSmartPriceChecked.add(propertyRatesId);
			        }
		    		List<PropertyRateDetail> rateDetailList =  rateDetailController.listRateDetail(propertyRatesId);
		    		
			        List<DateTime> dateBetween = DateUtil.getDateRange(FromDate, ToDate);
			        Iterator<DateTime> iterDateValues=dateBetween.iterator();
			        while(iterDateValues.hasNext()){
			        	DateTime dateValues=iterDateValues.next();
			        	java.util.Date DateValues = dateValues.toDate();
			        	strDateValues=format1.format(DateValues);
			        	if(!arrListDateId.contains(strDateValues)){
			        		arrListDateId.add(strDateValues);
				        	arrListDateId.add(strPropertyRateId);
				        	if(!rateDetailList.isEmpty()){
				    			for(PropertyRateDetail rateDetails: rateDetailList){
				    				strAmount=String.valueOf(rateDetails.getBaseAmount());
				    				strDaysofWeek=rateDetails.getDaysOfWeek().toLowerCase();
				    				List<String> daysBetween = DateUtil.getDayRange(FromDate, ToDate);
				    				Iterator<String> iterDayValues=daysBetween.iterator();
							        while(iterDayValues.hasNext()){
							        	strDays=iterDayValues.next().toLowerCase();
							        	if(strDaysofWeek.contains(strDays)){
							        		arrListDateId.add(strDaysofWeek);
							        		arrListDateId.add(strAmount);
							        	}
							        }
				    			}
				    		}
			        	}
			        	
			        }
		    		dateChk=true;
		    	}
		    }
		    
		    System.out.println(arrListDateId);
		   
		   int intCount=0;
		   String strArrays[] = new String[arrListDateId.size()];
		    Iterator<String> iterListValues=arrListDateId.iterator();
		    while(iterListValues.hasNext()){
	    		String strTemp=iterListValues.next();
	    		strArrays[intCount]=strTemp;
	    		intCount++;		
		    }
		    
		    String dateval="",doubleval="",rateId="";
		    for(String s : strArrays) {
		          String[] s2 = s.split(" ");
		          for(String results : s2) {
		              System.out.println(results);
		              if(results.contains("-")){//Date check
		            	  dateval=results;
		            	  arrListRateDetail.add(dateval);
		            	  
		              }else{// Number
		            	  if(isNumeric(results)){
		            		  rateId=results;
		            	  }
		            	  else{
		            		  int first = results.indexOf(".");
		            		  if ( (first >= 0) && (first - results.lastIndexOf(".")) == 0) {
		            		      // only one decimal point
		            			  doubleval=results;
		            			  arrListRateDetail.add(doubleval);
		            		  }
		            		  else {
		            			
		            			  // no decimal point or more than one decimal point
		            		  }
		            	  }
		              }
		  		      if(!doubleval.isEmpty() && !dateval.isEmpty()){
		  		    	dateMap.put(dateval, doubleval);
		              }
		  		      if(!rateId.isEmpty() && !dateval.isEmpty()){
		  		    	  dayMap.put(dateval, rateId);
		  		      }
		          }
		      }
		    intCount=0;
            String[] listValues=new String[arrListRateDetail.size()];
            Iterator<String> rateDetailsIter=arrListRateDetail.iterator();
		    while (rateDetailsIter.hasNext()) {
                String strTemp=rateDetailsIter.next();
                listValues[intCount]=strTemp;
	    		intCount++;		
		    }
		    System.out.println(listValues);
		    System.out.println("before hashingMap"+dateMap);
		    String strDay="", strRateId="";
		    int intRateId=0;
            String[] monthDate=chartDate.split(",");
            
            for(String strDate : monthDate){ 
        		if(arrListRateDetail.contains(strDate)){
        			java.util.Date dteReqDate=format1.parse(strDate);
        			
        			strDay=formatEEEE.format(dteReqDate).toLowerCase();
        			strRateId=dayMap.get(strDate);
        			intRateId=Integer.parseInt(strRateId);
        			if(listSmartPriceChecked.contains(intRateId)){
        				List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
        				sessionMap.put("SmartPriceRateId", intRateId);
            			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
            				for (PropertyRateDetail rate : rateDetailList1)
    		    			{
            					if (!jsonOutput.equalsIgnoreCase(""))
                					jsonOutput += ",{";
                				
                				else
                					/*jsonOutput = "{";
                				
            						jsonOutput += "\"title\":\"" +" Rent: "+(rate.getBaseAmount())+" Child: "+(rate.getExtraChild())+" Adult: "+(rate.getExtraAdult())+" Infant: "+(rate.getExtraInfant())+" \"";
            						jsonOutput += ",\"color\":\"red\"";
                				    jsonOutput += ",\"start\":\"o\"";
                				    
                			        jsonOutput += "}";*/
                					
                					jsonOutput = "{";
                				
	            				    jsonOutput += "\"title\":\"" +" Tariff: "+(rate.getBaseAmount())+" \"";
	            				    jsonOutput += ",\"color\":\"red\"";
	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    
	            			        jsonOutput += "}";
	            			        jsonOutput += ",{";
	            				    jsonOutput += "\"title\":\"" +" Child: "+(rate.getExtraChild())+" \"";
	            				    jsonOutput += ",\"color\":\"red\"";
	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    
	            				    jsonOutput += "}";
	            				    jsonOutput += ",{";
	            				    jsonOutput += "\"title\":\"" +" Adult: "+(rate.getExtraAdult())+"\"";
	            				    jsonOutput += ",\"color\":\"red\"";
	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    
	            				    jsonOutput += "}";
	            				    //jsonOutput += ",{";
	            				    //jsonOutput += "\"title\":\"" +" Infant: "+(rate.getExtraInfant())+" \"";
	            				    //jsonOutput += ",\"color\":\"red\"";
	            				    //jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    
	            				    //jsonOutput += "}";
	            				    
    		    			 }
            			}
        			}
        			else{
	        			System.out.println(strDay);
	        			List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
	        			
	        			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
	        				for (PropertyRateDetail rate : rateDetailList1)
			    			 {
	        					if (!jsonOutput.equalsIgnoreCase(""))
	            					jsonOutput += ",{";
	            				
	            				else
	            					/*jsonOutput = "{";
	            				
	        						jsonOutput += "\"title\":\"" +" Rent: "+(rate.getBaseAmount())+" Child: "+(rate.getExtraChild())+" Adult: "+(rate.getExtraAdult())+" Infant: "+(rate.getExtraInfant())+" \"";
	            				    jsonOutput += ",\"start\":\"o\"";
	            				    
	            			        jsonOutput += "}";*/
	            					jsonOutput = "{";
                				
	            				    jsonOutput += "\"title\":\"" +" Tariff: "+(rate.getBaseAmount())+" \"";
	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    jsonOutput += ",\"color\":\"green\"";
	            				    
	            			        jsonOutput += "}";
	            			        jsonOutput += ",{";
	            				    jsonOutput += "\"title\":\"" +" Child: "+(rate.getExtraChild())+" \"";
	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    jsonOutput += ",\"color\":\"orange\"";
	            				    
	            				    jsonOutput += "}";
	            				    jsonOutput += ",{";
	            				    jsonOutput += "\"title\":\"" +" Adult: "+(rate.getExtraAdult())+" \"";
	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    
	            				    jsonOutput += "}";
	            				    //jsonOutput += ",{";
	            				    //jsonOutput += "\"title\":\"" +" Infant: "+(rate.getExtraInfant())+" \"";
	            				    //jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    
	            				    //jsonOutput += "}";
			    			 }
	        			}
	        			else
	        			{
	        				for (PropertyAccommodation accommodationRate : accommodationList) {
	        					if (!jsonOutput.equalsIgnoreCase(""))
	            					jsonOutput += ",{";
	            				
	            				else
	            					/*jsonOutput = "{";
	            				
	        						jsonOutput += "\"title\":\"" +" Rent: "+(accommodationRate.getBaseAmount())+" Child: "+(accommodationRate.getExtraChild())+" Adult: "+(accommodationRate.getExtraAdult())+" Infant: "+(accommodationRate.getExtraInfant())+" \"";
	            				    jsonOutput += ",\"start\":\"o\"";
	            				    
	            			        jsonOutput += "}";*/
	            					jsonOutput = "{";
	            				
		        				    jsonOutput += "\"title\":\"" +" Tariff: "+(accommodationRate.getBaseAmount())+" \"";
		        				    jsonOutput += ",\"start\":\""+strDate+"\"";
		        				    jsonOutput += ",\"color\":\"green\"";
		        				    
		        			        jsonOutput += "}";
		        			        jsonOutput += ",{";
		        				    jsonOutput += "\"title\":\"" +" Child: "+(accommodationRate.getExtraChild())+" \"";
		        				    jsonOutput += ",\"start\":\""+strDate+"\"";
		        				    jsonOutput += ",\"color\":\"orange\"";
		        				    
		        				    jsonOutput += "}";
		        				    jsonOutput += ",{";
		        				    jsonOutput += "\"title\":\"" +" Adult: "+(accommodationRate.getExtraAdult())+" \"";
		        				    jsonOutput += ",\"start\":\""+strDate+"\"";
		        				    
		        				    jsonOutput += "}";
		        				    //jsonOutput += ",{";
		        				    //jsonOutput += "\"title\":\"" +" Infant: "+(accommodationRate.getExtraInfant())+" \"";
		        				   // jsonOutput += ",\"start\":\""+strDate+"\"";
		        				    
		        				    //jsonOutput += "}";
	        				}
	        			}
        			}
        		}
        		else{
    				
    				for (PropertyAccommodation accommodationRate : accommodationList) {
    					if (!jsonOutput.equalsIgnoreCase(""))
        					jsonOutput += ",{";
        				
        				else
        					jsonOutput = "{";
        				
        				    jsonOutput += "\"title\":\"" +" Tariff: "+(accommodationRate.getBaseAmount())+" \"";
        				    jsonOutput += ",\"start\":\""+strDate+"\"";
        				    jsonOutput += ",\"color\":\"green\"";
        				    
        			        jsonOutput += "}";
        			        jsonOutput += ",{";
        				    jsonOutput += "\"title\":\"" +" Child: "+(accommodationRate.getExtraChild())+" \"";
        				    jsonOutput += ",\"start\":\""+strDate+"\"";
        				    jsonOutput += ",\"color\":\"orange\"";
        				    
        				    jsonOutput += "}";
        				    jsonOutput += ",{";
        				    jsonOutput += "\"title\":\"" +" Adult: "+(accommodationRate.getExtraAdult())+" \"";
        				    jsonOutput += ",\"start\":\""+strDate+"\"";
        				    
        				    jsonOutput += "}";
        				    //jsonOutput += ",{";
        				    //jsonOutput += "\"title\":\"" +" Infant: "+(accommodationRate.getExtraInfant())+" \"";
        				    //jsonOutput += ",\"start\":\""+strDate+"\"";
        				    
        				    //jsonOutput += "}";
    				}
        		}
            	
        	}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			System.out.println(jsonOutput);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
		
	 }
	 public String getInactiveRates(){
		 try{
			 sessionMap.put("updatePropertyRateId",updatePropertyRateId); 
			 this.updatePropertyRateId=(String)sessionMap.get("updatePropertyRateId");
			 PropertyRateManager rateController = new PropertyRateManager();
			 PropertyRate deleterate = rateController.find(Integer.parseInt(updatePropertyRateId));
			 long time = System.currentTimeMillis();
			 java.sql.Date date = new java.sql.Date(time);
			
			 deleterate.setIsActive(false);
			 deleterate.setIsDeleted(true);
			 deleterate.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			 rateController.edit(deleterate);
		 }catch(Exception e){
			 logger.equals(e);
			 e.printStackTrace();
		 }finally{
			 
		 }
		 return null;
	 }
	 
	 public String getInactiveSmartPrice(){
		 try{
			 sessionMap.put("updatePropertyRateId",updatePropertyRateId); 
			 this.updatePropertyRateId=(String)sessionMap.get("updatePropertyRateId");
			 PropertyRateManager rateController = new PropertyRateManager();
			 List<PropertyRate> listSmartPriceDetail=rateController.listActiveSmartPricesCheck(Integer.parseInt(updatePropertyRateId));
			 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty())
			 {
				 for(PropertyRate ratelist:listSmartPriceDetail){
				     
					 PropertyRate rateId = rateController.find(ratelist.getPropertyRateId());
					 rateId.setIsActive(false);
					 rateId.setIsDeleted(true);
					 
					 rateId.setStartDate(ratelist.getStartDate());
					 rateId.setEndDate(ratelist.getEndDate());
					 rateId.setOrderBy(ratelist.getOrderBy());
					 rateId.setRateName(ratelist.getRateName());
					 rateId.setPmsProperty(ratelist.getPmsProperty());
					 rateId.setPropertyAccommodation(ratelist.getPropertyAccommodation());
					 rateId.setPmsSource(ratelist.getPmsSource());
					 rateId.setSmartPriceIsActive(false);	
					 rateController.edit(rateId);
					 //break;
				 }
			 }
			 
		 }catch(Exception e){
			 logger.equals(e);
			 e.printStackTrace();
		 }finally{
			 
		 }
		 return null;
	 }
	 
	 public String getSmartPriceCheck(){
		 this.propertyId = (Integer) sessionMap.get("propertyId");
		 this.accommodationId = (Integer) sessionMap.get("accommodationId");
		 
		 sessionMap.put("ResultDate",resultDate); 
		 this.resultDate = (String) sessionMap.get("ResultDate");
		 
		 this.smartPriceStartDate = getSmartPriceStartDate();
		 sessionMap.put("smartPriceStartDate",smartPriceStartDate); 
	      
		 this.smartPriceEndDate = getSmartPriceEndDate();
	     sessionMap.put("smartPriceStartDate",smartPriceEndDate); 
			
		 this.smartPricePropertyAccommodationId = getSmartPricePropertyAccommodationId();
		 sessionMap.put("smartPricePropertyAccommodationId",smartPricePropertyAccommodationId);
		
		 this.smartPriceIsActiveOrNot = getSmartPriceIsActiveOrNot();
	     sessionMap.put("smartPriceIsActive",smartPriceIsActiveOrNot); 
	    
		 System.out.println(resultDate);
		 String strDate="",strStartDate="",strEndDate="",strDateValues="";
		 try{
			 if(resultDate=="" || resultDate==null){
				 resultDate=null;
			 }
			 if(smartPriceIsActiveOrNot==null){
				 smartPriceIsActiveOrNot=true;
			 }
			 SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
			 SimpleDateFormat displayformat = new SimpleDateFormat("yyyy-MM-dd");
			 
			 PropertyRateManager rateController = new PropertyRateManager();
			 if(resultDate!=null && !resultDate.isEmpty()){
				 java.util.Date dteResultDate = format1.parse(resultDate);
				 Timestamp tsFromDate=new Timestamp(dteResultDate.getTime());
				 strDate=displayformat.format(dteResultDate);
				 
				 List<PropertyRate> listSmartPriceDetail=rateController.listSmartPriceCheckDetails(propertyId, accommodationId, tsFromDate);
				 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty())
				 {
					 for(PropertyRate ratelist:listSmartPriceDetail){
					     
						 PropertyRate rateId = rateController.find(ratelist.getPropertyRateId());
						 rateId.setIsActive(false);
						 rateId.setIsDeleted(true);
						 
						 rateId.setStartDate(tsFromDate);
						 rateId.setEndDate(tsFromDate);
						 rateId.setOrderBy(ratelist.getOrderBy());
						 rateId.setRateName(ratelist.getRateName());
						 rateId.setPmsProperty(ratelist.getPmsProperty());
						 rateId.setPropertyAccommodation(ratelist.getPropertyAccommodation());
						 rateId.setPmsSource(ratelist.getPmsSource());
						 rateId.setSmartPriceIsActive(false);	
						 rateController.edit(rateId);
						 //break;
					 }
				 }
			 }
			 else{
				 if(!smartPriceIsActiveOrNot){
					 
					String startDate = new SimpleDateFormat("yyyy-MM-dd").format(smartPriceStartDate);
					String endDate = new SimpleDateFormat("yyyy-MM-dd").format(smartPriceEndDate);
					
					DateTime startdate = DateTime.parse(startDate);
					DateTime enddate = DateTime.parse(endDate);
					
					 List<DateTime> dateBetween = DateUtil.getDateRange(startdate, enddate);
				     Iterator<DateTime> iterDateValues=dateBetween.iterator();
				     while(iterDateValues.hasNext()){
				    	 DateTime dateValues=iterDateValues.next();
				        	java.util.Date DateValues = dateValues.toDate();
				        	strDateValues=format1.format(DateValues);
				        	java.util.Date dteCheckDate = format1.parse(strDateValues);
							Timestamp tsCheckDate=new Timestamp(dteCheckDate.getTime());
							List<PropertyRate> listSmartPriceDetail=rateController.listSmartPriceCheckDetails(propertyId, smartPricePropertyAccommodationId, tsCheckDate);
							if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty())
							{
								 for(PropertyRate ratelist:listSmartPriceDetail){
									 PropertyRate rateId = rateController.find(ratelist.getPropertyRateId());
									 rateId.setIsActive(false);
									 rateId.setIsDeleted(true);
									 rateId.setStartDate(ratelist.getStartDate());
									 rateId.setEndDate(ratelist.getEndDate());
									 rateId.setOrderBy(ratelist.getOrderBy());
									 rateId.setRateName(ratelist.getRateName());
									 rateId.setPmsProperty(ratelist.getPmsProperty());
									 rateId.setPropertyAccommodation(ratelist.getPropertyAccommodation());
									 rateId.setPmsSource(ratelist.getPmsSource());
									 rateId.setSmartPriceIsActive(false);	
									 rateController.edit(rateId);
									 //break;
								 }
							}
				     }
				 }
				 
			 }
			 
		 } catch(Exception e){
			 logger.error(e);
			 e.printStackTrace();
		 }finally{
			 
		 }
		 
		 return null;
	 }
	 public String getSmartPrices(){
		 
		 	this.propertyId = (Integer) sessionMap.get("propertyId");
			try{
				Calendar calendar=Calendar.getInstance();
				SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
		 		String currentDate = format1.format(calendar.getTime());
		    	java.util.Date curdate=format1.parse(currentDate);
		    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());

				String jsonOutput = "",strRateName=null;
				HttpServletResponse response = ServletActionContext.getResponse();
				PropertyRateManager rateController=new PropertyRateManager();
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
				response.setContentType("application/json");
				
				this.listSmartPrices=rateController.listSmartPrices(propertyId,tsCurrentDate);
				if(listSmartPrices.size()>0 && !listSmartPrices.isEmpty()){
					for(PropertyRate rates:listSmartPrices){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						jsonOutput += "\"propertyRateId\":\"" + rates.getPropertyRateId() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";
						strRateName=rates.getRateName();
						if(strRateName==null){
							jsonOutput += ",\"rateName\":\"Modified Rate\"";
						}
						else{
							jsonOutput += ",\"rateName\":\"" + strRateName.trim()+ "\"";
						}
						
						
						jsonOutput += ",\"accommodationType\":\"" + rates.getPropertyAccommodation().getAccommodationType().trim()+ "\"";
						jsonOutput += "}";
					}
				}
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				System.out.println(jsonOutput);
			}
			catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
			finally{
				
			}
			return null;
	 }
	 
	 public String getPropertyRates(){
		 this.propertyId = (Integer) sessionMap.get("propertyId");
			try{
				Calendar calendar=Calendar.getInstance();
				SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
		 		String currentDate = format1.format(calendar.getTime());
		    	java.util.Date curdate=format1.parse(currentDate);
		    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());
		    	
				String jsonOutput = "",strRateName=null;
				HttpServletResponse response = ServletActionContext.getResponse();
				PropertyRateManager rateController=new PropertyRateManager();
				response.setContentType("application/json");
				this.listPropertyRates=rateController.listPropertyRates(propertyId,tsCurrentDate);
				if(listPropertyRates.size()>0 && !listPropertyRates.isEmpty()){
					for(PropertyRate rates:listPropertyRates){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						jsonOutput += "\"propertyRateId\":\"" + rates.getPropertyRateId() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";
						strRateName=rates.getRateName();
						if(strRateName==null){
							jsonOutput += ",\"rateName\":\"Modified Rate\"";
						}
						else{
							jsonOutput += ",\"rateName\":\"" + strRateName.trim()+ "\"";
						}
						
						jsonOutput += ",\"accommodationType\":\"" + rates.getPropertyAccommodation().getAccommodationType().trim()+ "\"";
						jsonOutput += "}";
					}
				}
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				System.out.println(jsonOutput);
			}
			catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
			finally{
				
			}
		 return null;
	 }
	 
	 public static boolean isNumeric(String str)
	 {
	     for (char c : str.toCharArray())
	     {
	         if (!Character.isDigit(c)) return false;
	     }
	     return true;
	 }
	 

}
