package com.ulopms.view;

import java.io.IOException;
import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.joda.time.DateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.catalina.startup.UserConfig;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.HelpTopicManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.HelpTopic;
import com.ulopms.model.Location;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsProperty;
import javax.servlet.http.HttpServletRequest;

public class BookingDetailsUloAction extends ActionSupport implements ServletRequestAware, SessionAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private User user;
	
	private Integer propertyId;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private String accommodationTypes;
	private double baseAmount;
	
	

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	
	private List<PropertyTaxe> taxeList;
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  

	private static final Logger logger = Logger.getLogger(BookingDetailsUloAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }
   
	List<BookingListAction> types = new ArrayList<BookingListAction>();
	
	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookingDetailsUloAction() {

	}

    
    public String execute() {
		
		return SUCCESS;
	}
	
    public String getPropertyDetails() throws IOException {
		// Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		
		   
			try {
                 
				
				this.propertyId  = getPropertyId();
				sessionMap.put("propertyId",this.propertyId);
				
				System.out.println("propid" +this.propertyId);
			   	

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			 return SUCCESS;
			
   
	}
    
	public String getTypeSelected() throws IOException {
	    	
		   try {
			   this.propertyId = (Integer) sessionMap.get("propertyId");
				PmsPropertyManager propertyController = new PmsPropertyManager();
				PmsProperty property = propertyController.find(this.propertyId);
				Timestamp todayDate = new Timestamp(System.currentTimeMillis());
				String currDate = new SimpleDateFormat("yyyy-MM-dd").format(todayDate);
				String fromDate = new SimpleDateFormat("yyyy-MM-dd").format(property.getTaxValidFrom());
			    String toDate = new SimpleDateFormat("yyyy-MM-dd").format(property.getTaxValidTo());
			    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			    Date FromDate = sdf.parse(fromDate);
			    Date ToDate = sdf.parse(toDate);
			    Date CurrDate = sdf.parse(currDate);
		        Date date2 = sdf.parse(toDate);
		        int comparedVal1 = CurrDate.compareTo(FromDate);
		        int comparedVal2 = CurrDate.compareTo(ToDate);
		        
		        
		        if(comparedVal1 >= 0 && comparedVal2 <=0 ){
		        
		            System.out.println("Taxactive");	
		            property.setTaxIsActive(true);
			        propertyController.edit(property);
		         
		        }
		        
		        else{
		        
		        	 System.out.println("Tax Inactive");
		        	 property.setTaxIsActive(false);
		 		     propertyController.edit(property);
		        	
		        }
			   
			   
				
	            System.out.println("<!--Date in Final-->");
				
	            this.propertyId = (Integer) sessionMap.get("propertyId");
	            this.arrivalDate = (Timestamp) sessionMap.get("arrivalDate");
	            this.departureDate = (Timestamp) sessionMap.get("departureDate");
	            
			   String jsonOutput = "";
			   HttpServletResponse response = ServletActionContext.getResponse();
			
				
			   PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		       response.setContentType("application/json");

	    		for (int i = 0; i < types.size(); i++) 
	  	      {
	  			
	    	   System.out.println("--->type");
	  		   System.out.println(types.get(i).getAccommodationId());
	  		   PropertyAccommodation accommodation = accommodationController.find(types.get(i).getAccommodationId());
	  		   
	  		//shiva add taxmanager
				PropertyTaxeManager taxController = new PropertyTaxeManager();
	  		   
	  		   if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput += "{";
	  		   
	  		    jsonOutput += "\"propertyId\":\"" + this.propertyId + "\"";
	  		    jsonOutput += ",\"baseAmount\":\"" + types.get(i).getBaseAmount() + "\"";
	  		    jsonOutput += ",\"arrivalDate\":\"" + this.arrivalDate + "\"";
	  		    jsonOutput += ",\"departureDate\":\"" + this.departureDate + "\"";
	  		    jsonOutput += ",\"rooms\":\"" + 1 + "\"";
	  		    jsonOutput += ",\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
	  		    jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType() + "\"";
		        jsonOutput += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy() + "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy() + "\"";
				jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults() + "\"";
				jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild() + "\"";
				jsonOutput += ",\"extraInfant\":\"" + types.get(i).getExtraInfant() + "\"";
				jsonOutput += ",\"extraChild\":\"" + types.get(i).getExtraChild() + "\"";
				jsonOutput += ",\"extraAdult\":\"" + types.get(i).getExtraAdult() + "\"";
				jsonOutput += ",\"available\":\"" + types.get(i).getAvailable() + "\"";
				jsonOutput += ",\"taxIsActive\":\"" + ( (property.getTaxIsActive() == true) ? "true": "false" )+ "\"";
		        jsonOutput += ",\"tax\":\"" + 100 + "\"";
				//shiva add next 2 lines
//				PropertyTaxe propertyTaxe = taxController.findAccommodationTax(accommodation.getAccommodationId());
//                jsonOutput += ",\"tax\":\"" + propertyTaxe.getTaxAmount() + "\"";
				jsonOutput += "}";
	  		   
	  		   
	  	      }
				
	    		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	    		
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null ;

	    	
	    }
	
	public String setTypeSelected() throws IOException {
    	
		   try {
			   
			 System.out.println("<!---ACC-types --->" + getAccommodationTypes()); 
			 sessionMap.put("accommodationTypes",getAccommodationTypes());
	    		
				
	    		
	    		
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return SUCCESS ;

	    	
	    }
	 
	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}
	
	public String getAccommodationTypes() {
		return accommodationTypes;
	}

	public void setAccommodationTypes(String accommodationTypes) {
		this.accommodationTypes = accommodationTypes;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	 
	
	 public List<BookingListAction> getTypes() {
	        return types;
	 }
	    
	 public void setTypes(List<BookingListAction> types) {
	        this.types = types;
	  }
	 
	 public List<PropertyTaxe> getTaxList() {
			return taxeList;
		}

	public void setTaxList(List<PropertyTaxe> taxeList) {
			this.taxeList = taxeList;
		}

}
