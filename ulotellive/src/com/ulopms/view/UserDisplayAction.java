package com.ulopms.view;

import java.sql.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.User;

public class UserDisplayAction extends ActionSupport implements UserAware {

	// private static final long serialVersionUID = 9149826260758390091L;

	private List<User> userList;

	private User user;

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private static final Logger logger = Logger
			.getLogger(UserDisplayAction.class);
	private UserLoginManager linkController;

	public UserDisplayAction() {
		linkController = new UserLoginManager();
	}

	public String execute() {

		try {
			this.userList = linkController.list();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

}
