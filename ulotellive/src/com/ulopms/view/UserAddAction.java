package com.ulopms.view;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

//import com.whl4.controller.DepartmentManager;
//import com.whl4.controller.CompanyManager;

















//import com.whl4.model.Company;
//import com.whl4.model.Department;
import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.AccessRightManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserAccessRightManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccessRight;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.Role;
import com.ulopms.model.User;
import com.ulopms.model.UserAccessRight;
import com.ulopms.security.passwordDecrypt;
import com.ulopms.util.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

public class UserAddAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware {
	
	private static final long serialVersionUID = 9149826260758390091L;
	private User user;

	private List<Role> roleList;
	// private List<Department> departList;
	private String loginName;
	private String userName;
	private String password;
	private String emailId;
	private String phone;
	private short roleId;
	private Long departmentId;
	private int userId;
	

	private int companyId;
	private String accessRightsId;
	
	private HttpServletRequest request;

	private static final Logger logger = Logger.getLogger(UserAddAction.class);
	
	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	

	private UserLoginManager linkController;
	private RoleManager roleController = new RoleManager();

	// private DepartmentManager departController = new DepartmentManager();
	// private CompanyManager companyController = new CompanyManager();

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public UserAddAction() {
		linkController = new UserLoginManager();
	}
	
	
	private List<PropertyDiscount> DiscountList;

	

	public String execute() {

		this.roleList = roleController.list();
		// this.departList = departController.list();
		return SUCCESS;
	}

	public String UserSave() {
		try {
			System.out.println("here");

			HttpServletRequest request = ServletActionContext.getRequest();
			// System.out.println(request.getParameter("userid").toString());
			User u = new User();
			u = linkController.findUser(Integer.parseInt(request
					.getParameter("userid")));
			// System.out.println(u.getEmailId());

			//this.setLoginName(u.getLoginName());
			this.setEmailId(u.getEmailId());
			//
			//this.setPassword(u.getPassword());
			this.setUserName(u.getUserName());
			this.setPhone(u.getPhone());
			this.setUserId(u.getUserId());

			this.roleList = roleController.list();
			// this.departList = departController.list();
			this.user = u;
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	public String UserAdd(String userName , String emailId , Integer roleId) {
		//System.out.println("useradd");

		System.out.println("User add action entry");
		System.out.println(userName);
		System.out.println(emailId);
		System.out.println(roleId);

		AccessRightManager rightsController = new AccessRightManager();
		UserAccessRightManager userRightsController = new UserAccessRightManager();
		try {

		HttpServletResponse response = ServletActionContext.getResponse();
		//response.reset();
		response.flushBuffer();
		response.setContentType("application/json");

		List<User> searchUser = linkController.findUserByEmail(emailId);
		if(searchUser.size()==0)
		{
		User u = new User();
		HttpServletRequest request = ServletActionContext.getRequest();
		long time = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(time);
		System.out.println("before func");
		if (getUserId() > 0) {


		System.out.println("save");
		// u = linkController.findUser(getUserId());
		u.setUserId(getUserId());
		// u.setModifiedOn(date);
		// u.setModifiedUser(getUser());
		// u.setModificationCounter(u.getModificationCounter()+1);
		}

		Role r = roleController.find(roleId);


		RandomStringUtils rsu = new RandomStringUtils();
		String PUBLIC_SALT = passwordDecrypt.secrandom();
		String tpassword = rsu.random(8,true,true).toString();
		System.out.println("tpassword"+tpassword);
		String hashpassword=passwordDecrypt.getDigestvalid(tpassword,PUBLIC_SALT);
		//System.out.println("hashpassword"+hashpassword);
		String tloginName = rsu.random(10,true,true).toString();

		//u.setLoginName(tloginName);
		//u.setPassword(tpassword);
		//u.setPassword(hashpassword);
		u.setHashpassword(hashpassword);
		u.setSaltkey(PUBLIC_SALT);
		// u.setPhone(phone);
		u.setUserName(userName);
		u.setEmailId(emailId);
		u.setRole(r);
		u.setIsActive(true);
		u.setIsDeleted(false);


		// u.setCompany(c);
		//u.setCreatedOn(date);
		//u.setCreatedUser(getUser());


		User newUser = linkController.add(u);

		/*if(!getAccessRightsId().equalsIgnoreCase(""))
		{
		for (int i = 0; i < getAccessRightsId().split(",").length; i++) {
		String f = getAccessRightsId().split(",")[i].trim();
		System.out.println(f);
		if(!f.equalsIgnoreCase(""))
		{
		UserAccessRight s = new UserAccessRight();
		AccessRight ar = rightsController.find(Integer.parseInt(f));
		s.setAccessRight(ar);
		s.setAccessRightsPermission(true);
		s.setUser(newUser);
		userRightsController.add(s);
		}
		}
		}
		else
		{
		List<AccessRight> accessRightList = rightsController.list("PROJECT");
		for(AccessRight rights : accessRightList)
		{
		UserAccessRight s = new UserAccessRight();
		//AccessRights ar = rightsController.find(Integer.parseInt(f));
		s.setAccessRight(rights);
		s.setAccessRightsPermission(false);
		s.setUser(newUser);
		userRightsController.add(s);
		}
		}

		response.getWriter().write("{" + "\"result\":\"Created Successfully\"" + ",\"tempid\":\""+ request.getParameter("tempid") +"\"}");

		*/
		
		//send email notification
		//email template



		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(UserAddAction.class, "../../../");
		Template template = cfg.getTemplate(getText("userinvitation.template"));
		Map<String, String> rootMap = new HashMap<String, String>();
		rootMap.put("password", tpassword);
		
		//rootMap.put("", getText("url.forgetpassword")+uniqueKey.toString());
		rootMap.put("to", u.getUserName());
		//rootMap.put("createdBy", getUser().getUserName());
		//rootMap.put("logoFileName", c1.getLogoPath());
		rootMap.put("url", getText("app.api.url"));
		rootMap.put("from", getText("notification.from"));
		Writer out = new StringWriter();
		template.process(rootMap, out);

		//send email
		Email em = new Email();
		em.set_to(u.getEmailId());
		em.set_from(getText("email.from"));
		//em.set_host(getText("email.host"));
		//em.set_password(getText("email.password"));
		//em.set_port(getText("email.port"));
		em.set_username(getText("email.username"));
		em.set_subject(getText("forgetpassword.subject"));
		em.set_bodyContent(out);
		em.send();

		//end notification
		//response.getWriter().write("{\"data\":[{" + "\"result\":\"Created Successfully\"" + "}]}");

		//response.setStatus(200);
		}
		else
		{
			System.out.println("else condition entry");
	/*	//response.getWriter().write("{\"data\":[{" + "\"result\":\"Already Exists\"" + "}]}");
		response.getWriter().write("{" + "\"result\":\"Already Exists\"" + ",\"tempid\":\""+ request.getParameter("tempid") +"\"}");
		response.setStatus(412);*/
		}

		} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
		} finally {

		}
		return null;
		}
	
	
	public String customerAdd() {
		//System.out.println("useradd");
		
		System.out.println("User add action entry");
		System.out.println(userName);
		System.out.println(emailId);
		System.out.println(roleId);
		System.out.println(password);
		
		String ret = "error";
		
		
		
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
		AccessRightManager rightsController = new AccessRightManager();
		UserAccessRightManager userRightsController = new UserAccessRightManager();
		try {
			
			Timestamp todayDate = new Timestamp(System.currentTimeMillis());
			String currDate = new SimpleDateFormat("yyyy-MM-dd").format(todayDate);
			Date currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(currDate);
			this.DiscountList = propertyDiscountController.currentDiscounts(currentDate);
			for (PropertyDiscount discounts : DiscountList) {
				
				if(discounts.getIsSignup() == true){
					
					double discountPercentage = discounts.getDiscountPercentage();
					System.out.println(discounts.getDiscountPercentage());
					sessionMap.put("isSignup","active");
					sessionMap.put("signupDiscount",discountPercentage);
					
					
				}
				
				else{
					
					
					sessionMap.put("isSignup","inactive");
					
					
					
				}
				
				
				
			}
			
			HttpServletResponse response = ServletActionContext.getResponse();
			//response.reset();
			response.flushBuffer();
			response.setContentType("application/json");
			
			List<User> searchUser = linkController.findUserByEmail(emailId);
			if(searchUser.size()==0)
			{
				User u = new User();
				HttpServletRequest request = ServletActionContext.getRequest();
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				System.out.println("before func");
				if (getUserId() > 0) {
					
					
					System.out.println("save");
					// u = linkController.findUser(getUserId());
					u.setUserId(getUserId());
					// u.setModifiedOn(date);
					// u.setModifiedUser(getUser());
					// u.setModificationCounter(u.getModificationCounter()+1);
				}
	
				Role r = roleController.find(roleId);
	
				
				RandomStringUtils  rsu = new RandomStringUtils();
				String PUBLIC_SALT = passwordDecrypt.secrandom();
				//String tpassword = rsu.random(8,true,true).toString();
				//System.out.println("tpassword"+tpassword);
				String hashpassword=passwordDecrypt.getDigestvalid(password,PUBLIC_SALT);
				//System.out.println("hashpassword"+hashpassword);
				String tloginName = rsu.random(10,true,true).toString();
				
				//u.setLoginName(tloginName);
				//u.setPassword(tpassword);
				//u.setPassword(hashpassword);
				u.setHashpassword(hashpassword);
				u.setSaltkey(PUBLIC_SALT);
				// u.setPhone(phone);
				u.setUserName(userName);
				u.setEmailId(emailId);
				u.setRole(r);
				u.setIsActive(true);
				u.setIsDeleted(false);
				
				
			//	u.setCompany(c);
				//u.setCreatedOn(date);
				//u.setCreatedUser(getUser());
						
				
				User newUser = linkController.add(u);
				
			   if(!getAccessRightsId().equalsIgnoreCase(""))
				{
					for (int i = 0; i < getAccessRightsId().split(",").length; i++) {
						String f = getAccessRightsId().split(",")[i].trim();
						System.out.println(f);
						if(!f.equalsIgnoreCase(""))
						{
							UserAccessRight s = new UserAccessRight();
							AccessRight ar =  rightsController.find(Integer.parseInt(f));
							s.setAccessRight(ar);
							s.setAccessRightsPermission(true);
							s.setUser(newUser);
							userRightsController.add(s);
						}
					}
				}
				else
				{
					List<AccessRight> accessRightList = rightsController.list("PROJECT");
					for(AccessRight rights : accessRightList)
					{
						UserAccessRight s = new UserAccessRight();
						//AccessRights ar =  rightsController.find(Integer.parseInt(f));
						s.setAccessRight(rights);
						s.setAccessRightsPermission(false);
						s.setUser(newUser);
						userRightsController.add(s);
					}
				}
				
				//response.getWriter().write("{" + "\"result\":\"Created Successfully\""  + ",\"tempid\":\""+ request.getParameter("tempid") +"\"}");
				
				//send email notification 
				//email template
				
				
				
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(UserAddAction.class, "../../../");
				Template template = cfg.getTemplate(getText("userinvitation.template"));
				Map<String, String> rootMap = new HashMap<String, String>();
				//rootMap.put("password", password);
				//rootMap.put("", getText("url.forgetpassword")+uniqueKey.toString());
				rootMap.put("userName", getUserName());
				rootMap.put("from", getText("notification.from"));
				//rootMap.put("createdBy", getUser().getUserName());
				//rootMap.put("logoFileName", c1.getLogoPath());
				rootMap.put("url", getText("app.api.url"));
				Writer out = new StringWriter();
				template.process(rootMap, out);

				//send email
				Email em = new Email();
				em.set_to(getEmailId());
				em.set_from(getText("email.from"));
				em.set_cc(getText("reservation.notification.email"));
				//em.set_host(getText("email.host"));
				//em.set_password(getText("email.password"));
				//em.set_port(getText("email.port"));
				em.set_username(getText("email.username"));
				em.set_subject(getText("userinvitation.subject"));
				em.set_bodyContent(out);
				em.send();
				
				 ret= "success";
				//end notification
				//response.getWriter().write("{\"data\":[{" + "\"result\":\"Created Successfully\""  + "}]}");
				
				//response.setStatus(200);
			}
			else
			{
				
				 addActionError(getText("error.signup"));
				 ret= "error";
				//response.getWriter().write("{\"data\":[{" + "\"result\":\"Already Exists\""  + "}]}");
				
				//response.getWriter().write("{" + "\"result\":\"Already Exists\""  + ",\"tempid\":\""+ request.getParameter("tempid") +"\"}");
				//response.setStatus(412);
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return ret;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public short getRoleId() {
		return roleId;
	}

	public void setRoleId(short roleId) {
		this.roleId = roleId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getAccessRightsId() {
		return accessRightsId;
	}

	public void setAccessRightsId(String accessRightsId) {
		this.accessRightsId = accessRightsId;
	}

	
	public List<PropertyDiscount> getDiscountList() {
		return DiscountList;
	}

	public void setDiscountList(List<PropertyDiscount> discountList) {
		DiscountList = discountList;
	}

	

}
