package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;













import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.AccommodationPhotoManager;
import com.ulopms.controller.PmsPhotoCategoryManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyPhotoManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationPhoto;
import com.ulopms.model.PmsPhotoCategory;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyPhoto;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyPhotoAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer propertyPhotoId;
	private Integer propertyId;
	private Boolean isPrimary;

	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private Integer photoCategoryId;
	
	

	private PropertyPhoto propertyPhoto;
	
	private List<PropertyPhoto> propertyPhotoList;
	

	private static final Logger logger = Logger.getLogger(PropertyPhotoAction.class);
    
	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyPhotoAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	public String getPropertyPhotos() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		System.out.println("get all property photos");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyPhotoList = photoController.list(getPropertyId());
			for (PropertyPhoto photo : propertyPhotoList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
				jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
				
				
				jsonOutput += "}";


			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getCategoryPhotos() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		System.out.println("get all category photos");
		// System.out.println(jobExecutionList.size());
		System.out.println("category Id"+getPhotoCategoryId());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyPhotoList = photoController.list1(getPropertyId(),getPhotoCategoryId());
			for (PropertyPhoto photo : propertyPhotoList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
				jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
				jsonOutput += ",\"photoCategoryId\":\"" + getPhotoCategoryId()+ "\"";
				
				
				jsonOutput += "}";


			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	

	public String addPropertyPhoto() {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PropertyPhotoManager photoController = new PropertyPhotoManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		PmsPhotoCategoryManager categoryController = new PmsPhotoCategoryManager();
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("add photo");
			PropertyPhoto photo = new PropertyPhoto();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);

			if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				PmsProperty property = propertyController.find(getPropertyId());
				photo.setPhotoPath(this.getMyFileFileName());
				photo.setIsActive(true);
				photo.setIsDeleted(false);
				photo.setPmsProperty(property);
				PmsPhotoCategory category = categoryController.find(getPhotoCategoryId());
				photo.setPmsPhotoCategory(category);
			
				PropertyPhoto photo1 = photoController.add(photo);
			
				if(photo1.getPropertyPhotoId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					File file = new File(getText("storage.property.photo") + "/"
							+ getPropertyId() + "/" + photo1.getPropertyPhotoId() + "/", this.getMyFileFileName());
					FileUtils.copyFile(this.myFile, file);// copying image in the
				}
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	
	public String deletePropertyPhoto() {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PropertyPhotoManager photoController = new PropertyPhotoManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("delete customer");
			PropertyPhoto photo = photoController.find(getPropertyPhotoId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			photo.setIsActive(false);
			photo.setIsDeleted(true);
			photo.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			photoController.edit(photo);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	public String getPropertyPicture() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		HttpServletResponse response = ServletActionContext.getResponse();

		PropertyPhotoManager photoController = new PropertyPhotoManager();
		PropertyPhoto photo = photoController.find(getPropertyPhotoId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if (getPropertyPhotoId() >0 ) {
				imagePath = getText("storage.property.photo") + "/"
						+ photo.getPmsProperty().getPropertyId()+ "/"
						+ photo.getPropertyPhotoId() + "/"
						+ photo.getPhotoPath();
				Image im = new Image();
				//System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				//System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	
	

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}

	public Integer getPropertyPhotoId() {
		return propertyPhotoId;
	}

	public void setPropertyPhotoId(Integer propertyPhotoId) {
		this.propertyPhotoId = propertyPhotoId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public PropertyPhoto getPropertyPhoto() {
		return propertyPhoto;
	}

	public void setPropertyPhoto(PropertyPhoto propertyPhoto) {
		this.propertyPhoto = propertyPhoto;
	}
	
	public Integer getPhotoCategoryId() {
		return photoCategoryId;
	}

	public void setPhotoCategoryId(Integer photoCategoryId) {
		this.photoCategoryId = photoCategoryId;
	}

	public List<PropertyPhoto> getPropertyPhotoList() {
		return propertyPhotoList;
	}

	public void setPropertyPhotoList(List<PropertyPhoto> propertyPhotoList) {
		this.propertyPhotoList = propertyPhotoList;
	}
	
	



}
