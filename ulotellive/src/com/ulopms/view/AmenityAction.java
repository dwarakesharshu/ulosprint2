package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;











import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;





















import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.AccommodationAmenityManager;
import com.ulopms.controller.PmsAmenityManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAmenityManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyUserManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyUser;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class AmenityAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer amenityId;
	
	private Integer propertyId;
	
	private String checkedAmenity;
	
	private String checkedAmenity1;
	
	private Integer accommodationId;

	private String amenityDescription;
	private String amenityName;
	
	private PmsAmenity amenity;
	
	private List<PmsAmenity> amenityList;
	
	private List<PropertyAmenity> propertyAmenityList;
	
	private List<AccommodationAmenity> accommodationAmenityList;
  
	
	private static final Logger logger = Logger.getLogger(AmenityAction.class);

	
	

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	// @Override
	// public User getModel() {
	// return user;
	// }
	private User user;
	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public AmenityAction() {
		
	//System.out.println("session check");
	//this.propertyId = (Integer) sessionMap.get("propertyId");
	//System.out.println(this.propertyId);	
		
		
	}

	public String execute() {
        
		
		//this.patientId = getPatientId();
		return SUCCESS;
	}
    
	//System.out.println("session check");
	//this.propertyId = (Integer) sessionMap.get("propertyId");
	//System.out.println(this.propertyId);
	
	public String getSingleAmenity() throws IOException {
		
		System.out.println(getAmenityId());
		this.propertyId = (Integer) sessionMap.get("propertyId");
		// System.out.println(jobExecutionList.size());
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsAmenityManager amenityController = new PmsAmenityManager();
			
			response.setContentType("application/json");

			PmsAmenity amenity = amenityController.find(getAmenityId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
				jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
				jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAmenityPic() {
		System.out.println(getAmenityId());
		
		HttpServletResponse response = ServletActionContext.getResponse();
		PmsAmenityManager amenityController = new PmsAmenityManager();

		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();

		String imagePath = "";
		// response.setContentType("");
		try {
			PmsAmenity pmsam = amenityController.find(getAmenityId()); 
			if ( pmsam.getAmenityIcon() != null) {
				imagePath = getText("stroage.amenityicon.photo") + "/"
						+pmsam.getAmenityId() + "/"
						+ pmsam.getAmenityIcon();
				Image im = new Image();
				System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("stroage.amenityicon.photo") + "/emptyprofilepic.png";
				Image im = new Image();
				System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		}catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	
	public String getAmenities() throws IOException {
		System.out.println("amenities");
		
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsAmenityManager amenityController = new PmsAmenityManager();
			
			response.setContentType("application/json");

			this.amenityList = amenityController.list();
			
			for (PmsAmenity amenity : amenityList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
				jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
				jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";

				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getPropertyAmenities() throws IOException {
		
		
		
		
		System.out.println("propertyamenities");
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsAmenityManager amenityController = new PmsAmenityManager();
			PropertyAmenityManager pAmenityConroller = new PropertyAmenityManager();
			
			
			
			response.setContentType("application/json");

			this.amenityList = amenityController.list();
			
			for (PmsAmenity amenity : amenityList) {
            //System.out.println(amenity.getPropertyId());
            
		    List<PropertyAmenity> propertyAmenityList = pAmenityConroller.listTwo(getPropertyId(), amenity.getAmenityId());
		   
		 //  for(PropertyAmenity pAmenity :propertyAmenityList)
		   //{
			   
		   //}
		   
		    if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
				jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
                jsonOutput += ",\"icon\":\"" + amenity.getAmenityIcon()+ "\"";
				jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
				//jsonOutput += ",\"propertyIsDeleted\":\"" + amenity.getgetIsDeleted()+ "\"";
				//jsonOutput += ",\"isDeleted\":\"" + amenity.getIsDeleted()+ "\"";
				if(propertyAmenityList.size()>0) jsonOutput += ",\"status\":\"true\""; else jsonOutput += ",\"status\":\"false\"";

				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
    public String getAccommodationAmenities() throws IOException {
		
		
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsAmenityManager amenityController = new PmsAmenityManager();
			//PropertyAmenityManager pAmenityConroller = new PropertyAmenityManager();
			AccommodationAmenityManager accAmenityController = new AccommodationAmenityManager();

			
			response.setContentType("application/json");

			 
			//List<PropertyAmenity> propertyAmenityList = pAmenityConroller.listTwo(getPropertyId(), amenity.getAmenityId());
            
			this.amenityList = amenityController.list();
			
			for (PmsAmenity amenity : amenityList) {
            
			 System.out.println(getPropertyId());
			 System.out.println(getAccommodationId());
			 System.out.println(amenity.getAmenityId());
            
				
		    this.accommodationAmenityList = accAmenityController.listTwo(getAccommodationId(), amenity.getAmenityId());
		    //   List<PropertyAmenity> propertyAmenityList = pAmenityConroller.listTwo(getPropertyId(), amenity.getAmenityId());
		   
		    //  for(PropertyAmenity pAmenity :propertyAmenityList)
		   //{
			   
		   //}
		   
		    if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
				jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
                jsonOutput += ",\"icon\":\"" + amenity.getAmenityIcon()+ "\"";
				jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
				//jsonOutput += ",\"propertyIsDeleted\":\"" + amenity.getgetIsDeleted()+ "\"";
				//jsonOutput += ",\"isDeleted\":\"" + amenity.getIsDeleted()+ "\"";
				if(accommodationAmenityList.size()>0) jsonOutput += ",\"status\":\"true\""; else jsonOutput += ",\"status\":\"false\"";

				jsonOutput += "}";

			}

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

    public String getAccommodationAmenities1() throws IOException {
	
	
	
	System.out.println(getAccommodationId());
	System.out.println("accoamenities1");
	
	//this.propertyId = (Integer) sessionMap.get("propertyId");
	// System.out.println(jobExecutionList.size());
	try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	
		
		PmsAmenityManager amenityController = new PmsAmenityManager();
		PropertyAmenityManager pAmenityConroller = new PropertyAmenityManager();
		AccommodationAmenityManager accAmenityController = new AccommodationAmenityManager();

		
		
		response.setContentType("application/json");

		this.amenityList = amenityController.list();
		
		for (PmsAmenity amenity : amenityList) {
        //System.out.println(amenity.getPropertyId());
        
	    List<AccommodationAmenity> accommodationAmenityList = accAmenityController.list(getAccommodationId(), amenity.getAmenityId());
	   
	 //  for(PropertyAmenity pAmenity :propertyAmenityList)
	   //{
		   
	   //}
	   
	    if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			
			jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
			jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
			jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
			//jsonOutput += ",\"propertyIsDeleted\":\"" + amenity.getgetIsDeleted()+ "\"";
			//jsonOutput += ",\"isDeleted\":\"" + amenity.getIsDeleted()+ "\"";
			if(accommodationAmenityList.size()>0) jsonOutput += ",\"status\":\"true\""; else jsonOutput += ",\"status\":\"false\"";

			jsonOutput += "}";

		}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

}



    
	public String savePropertyAmenities() {
		
		
		
		System.out.println("propertyamenities");
		System.out.println(getCheckedAmenity());
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		PmsAmenityManager pmsAmenityController = new PmsAmenityManager();
		PropertyAmenityManager propertyAmenityController = new PropertyAmenityManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		//UserLoginManager  userController = new UserLoginManager();
		
		
		
		try {
			
			this.propertyAmenityList = propertyAmenityController.list(getPropertyId());
			for (PropertyAmenity amen : propertyAmenityList) {
			    
				amen.setIsActive(false);
				amen.setIsDeleted(true);
				propertyAmenityController.edit(amen);	
			}			
			System.out.println("add amenity");
			PropertyAmenity propertyAmenity = new PropertyAmenity();
			//PmsAmenity pmsAmenity = new PmsAmenity();
			//List<String> items = Arrays.asList(getCheckedAmenity().split("\\s*,\\s*"));
			String[] stringArray = getCheckedAmenity().split("\\s*,\\s*");
		    int[] intArray = new int[stringArray.length];
		    for (int i = 0; i < stringArray.length; i++) {
		     String numberAsString = stringArray[i];
		     intArray[i] = Integer.parseInt(numberAsString);
		    }
		      
		      System.out.println("The integers are:");
		      for (int number : intArray) {
		         System.out.println(number);
		        
		    	long time = System.currentTimeMillis();
			    java.sql.Date date = new java.sql.Date(time);
			    propertyAmenity.setIsActive(true);
			    propertyAmenity.setIsDeleted(false);
			    PmsProperty property = propertyController.find(getPropertyId());
			    propertyAmenity.setPmsProperty(property);
			    PmsAmenity pms = pmsAmenityController.find(number);
			    propertyAmenity.setPmsAmenity(pms);
			    propertyAmenityController.add(propertyAmenity);
			    //propertyAmenityController.add(propertyAmenity);  
			    //List<PropertyAmenity> propertyAmenityList = propertyAmenityController.listTwo(getPropertyId(), number);
			    //if(propertyAmenityList.size() >0 ){
			    //propertyAmenityController.edit(propertyAmenity);	
			    //}
			    //else{
			   
			   // }
			    
		      }
		      
			
			
		    /*for(int i = 0; i < items.size(); i++) {
		    System.out.println(items.get(i));
		    propertyAmenity.setIsActive(true);
		    propertyAmenity.setIsDeleted(false);
		    PmsProperty property = propertyController.find(getPropertyId());
		    propertyAmenity.setPmsProperty(property);
		    PmsAmenity pms = pmsAmenityController.find(items.get(i));
		    propertyAmenity.setPmsAmenity(pms);
		    List<PropertyAmenity> propertyAmenityList = propertyAmenityController.list(getPropertyId(), items.get(i));
		    if(propertyAmenityList.size()>0){
		    propertyAmenityController.edit(propertyAmenity);	
		    }
		    else{
		    propertyAmenityController.add(propertyAmenity);
		    }
            }*/
		    
			//long time = System.currentTimeMillis();
			//java.sql.Date date = new java.sql.Date(time);
			
			//amenity.setAmenityName(getAmenityName());
			//amenity.setAmenityDescription(getAmenityDescription());
			//amenity.setIsActive(true);
			//amenity.setIsDeleted(false);
			
			//amenityController.add(amenity);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
public String saveAccommodationAmenities() {
		
		
		
		System.out.println("accommodationamenities");
		System.out.println(getCheckedAmenity1());
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		PmsAmenityManager pmsAmenityController = new PmsAmenityManager();
		PropertyAmenityManager propertyAmenityController = new PropertyAmenityManager();
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		AccommodationAmenityManager accAmenityConroller = new AccommodationAmenityManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		//UserLoginManager  userController = new UserLoginManager();
		
		
		
		try {
			
			this.accommodationAmenityList = accAmenityConroller.list(getAccommodationId());
			for (AccommodationAmenity accamen : accommodationAmenityList) {
			    
				accamen.setIsActive(false);
				accamen.setIsDeleted(true);
				accAmenityConroller.edit(accamen);	
			}			
			System.out.println("add acc amenity");
			AccommodationAmenity accommodationAmenity = new AccommodationAmenity();
			
			String[] stringArray = getCheckedAmenity1().split("\\s*,\\s*");
		    int[] intArray = new int[stringArray.length];
		    for (int i = 0; i < stringArray.length; i++) {
		     String numberAsString = stringArray[i];
		     intArray[i] = Integer.parseInt(numberAsString);
		    }
		      
		      System.out.println("The integers are:");
		      for (int number : intArray) {
		         System.out.println(number);
		        
		    	long time = System.currentTimeMillis();
			    java.sql.Date date = new java.sql.Date(time);
			    accommodationAmenity.setIsActive(true);
			    accommodationAmenity.setIsDeleted(false);
			    PropertyAccommodation property = propertyAccommodationController.find(getAccommodationId());
			    accommodationAmenity.setPropertyAccommodation(property);
			    PmsAmenity pms = pmsAmenityController.find(number);
			    accommodationAmenity.setPmsAmenity(pms);
			    accAmenityConroller.add(accommodationAmenity);
			   
		      }
		  
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	

	public String addAmenity() {
		PmsAmenityManager amenityController = new PmsAmenityManager();
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("add amenity");
			PmsAmenity amenity = new PmsAmenity();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			amenity.setAmenityName(getAmenityName());
			amenity.setAmenityDescription(getAmenityDescription());
			amenity.setIsActive(true);
			amenity.setIsDeleted(false);
			
			amenityController.add(amenity);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editAmenity() {
		PmsAmenityManager amenityController = new PmsAmenityManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("edit customer");
			PmsAmenity amenity = amenityController.find(getAmenityId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			amenity.setAmenityName(getAmenityName());
			amenity.setAmenityDescription(getAmenityDescription());
			amenity.setIsActive(true);
			amenity.setIsDeleted(false);
			
			amenityController.edit(amenity);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteAmenity() {
		PmsAmenityManager amenityController = new PmsAmenityManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("delete customer");
			PmsAmenity amenity = amenityController.find(getAmenityId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			amenity.setIsActive(false);
			amenity.setIsDeleted(true);
			amenity.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			amenityController.edit(amenity);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	
	public Integer getAmenityId() {
		return amenityId;
	}

	public void setAmenityId(Integer amenityId) {
		this.amenityId = amenityId;
	}

	public Integer getAccommodationId() {
		return accommodationId;
	}
	
	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public String getAmenityDescription() {
		return amenityDescription;
	}

	public void setAmenityDescription(String amenityDescription) {
		this.amenityDescription = amenityDescription;
	}

	public String getAmenityName() {
		return amenityName;
	}

	public void setAmenityName(String amenityName) {
		this.amenityName = amenityName;
	}
	
	public String getCheckedAmenity() {
		return checkedAmenity;
	}

	public void setCheckedAmenity(String checkedAmenity) {
		this.checkedAmenity = checkedAmenity;
	}
	
	 public String getCheckedAmenity1() {
		return checkedAmenity1;
	}

	public void setCheckedAmenity1(String checkedAmenity1) {
		this.checkedAmenity1 = checkedAmenity1;
	}     

	public PmsAmenity getAmenity() {
		return amenity;
	}

	public void setAmenity(PmsAmenity amenity) {
		this.amenity = amenity;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	

	public List<PmsAmenity> getAmenityList() {
		return amenityList;
	}

	public void setAmenityList(List<PmsAmenity> amenityList) {
		this.amenityList = amenityList;
	}
	
	public List<PropertyAmenity> getPropertyAmenityList() {
		return propertyAmenityList;
	}

	public void setPropertyAmenityList(List<PropertyAmenity> propertyAmenityList) {
		this.propertyAmenityList = propertyAmenityList;
	}

}
