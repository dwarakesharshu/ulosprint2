package com.ulopms.view;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.startup.UserConfig;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.HelpTopicManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.HelpTopic;
import com.ulopms.model.User;
import com.ulopms.util.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

public class JoinUloAction extends ActionSupport implements ServletRequestAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private User user;
	
	private String hotelName;
	private String room;
	private String name;
	private String email;
	private String mobile;
	
	
	
	

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private HttpSession session;
	
	

	private static final Logger logger = Logger.getLogger(JoinUloAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public JoinUloAction() {

	}


	public String Join()
	{
		try
		{
			ActionSupport actionSupport = new ActionSupport();
			actionSupport.getText("forgetpassword.template");
			
		System.out.println(getText("forgetpassword.template"));
		//email template
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(JoinUloAction.class, "../../../");
		Template template = cfg.getTemplate(getText("partnerwithus.template"));
		Map<String, String> rootMap = new HashMap<String, String>();
		rootMap.put("name",getName());
		rootMap.put("email", getEmail());
		rootMap.put("hotel", getHotelName());
		rootMap.put("mobile", getMobile());
		rootMap.put("room", getRoom());
		rootMap.put("from", getText("notification.from"));
		Writer out = new StringWriter();
		template.process(rootMap, out);

		//send email
		Email em = new Email();
		em.set_to(getEmail());
		em.set_cc(getText("partnerwithus.notification.email"));
		em.set_from(getText("email.from"));
		//em.set_host(getText("email.host"));
		//em.set_password(getText("email.password"));
		//em.set_port(getText("email.port"));
		em.set_username(getText("email.username"));
		em.set_subject(getText("partnerwithus.subject"));
		em.set_bodyContent(out);
		em.send();		
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
		addActionMessage("Your request has been submitted successfully");
		return SUCCESS;
	}

	public String execute() {
		
		return SUCCESS;
	}
	
	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
