package com.ulopms.view;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

//import com.medwecare.model.Company;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.User;



public class UserProfileAction extends ActionSupport implements ServletRequestAware,UserAware,ModelDriven<User>  {

	private static final long serialVersionUID = 4L;
	
	private User user;
	private static final Logger logger = Logger.getLogger(UserProfileAction.class);
	private UserLoginManager linkController;
	
	private HttpServletRequest request;
	private String companyName;
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	private String logoFileName;
	private String titleHeader;
	public String getTitleHeader() {
		return titleHeader;
	}

	public void setTitleHeader(String titleHeader) {
		this.titleHeader = titleHeader;
	}

	public String getLogoFileName() {
		return logoFileName;
	}

	public void setLogoFileName(String logoFileName) {
		this.logoFileName = logoFileName;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private HttpSession session;
	
	@Override
    public User getModel() {
        return user;
    }
	
	public UserProfileAction() {
		linkController = new UserLoginManager();
	}

	@Override
	public String execute() {
			
		
		
		this.user = getModel();
		//this.projectList = linkController.list();
		this.session = request.getSession();
		//Company company  = (Company)session.getAttribute("COMPANY");
		//this.setLogoFileName(company.getLogoPath());
		this.titleHeader = user.getUserName()+ "'s PROFILE";
		//this.setCompanyName(company.getCompanyName());
		return SUCCESS;
	}

	
	 @Override
	    public void setUser(User user) {
	        this.user=user;
	    }
	     
	    public User getUser(User user){
	        return this.user;
	    }

		@Override
		public void setServletRequest(HttpServletRequest request) {
			// TODO Auto-generated method stub
			this.request = request;
			
		}

	
}
