package com.ulopms.view;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.catalina.startup.UserConfig;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.HelpTopicManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.HelpTopic;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.User;

import javax.servlet.http.HttpServletRequest;

public class DetailsAction extends ActionSupport implements ServletRequestAware, SessionAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private User user;
	
	private Integer propertyId;
	
	private String latitude;
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	private String longitude;
	
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  

	private static final Logger logger = Logger.getLogger(DetailsAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public DetailsAction() {

	}



	public String execute() {
		
		this.propertyId  = getPropertyId();
		sessionMap.put("propertyId",this.propertyId);
		if(getPropertyId()!=null && getPropertyId()>0)
        {
        PmsPropertyManager propertyController = new PmsPropertyManager();
        PmsProperty property = propertyController.find(getPropertyId());
        this.setLatitude(property.getLatitude().trim());
        this.setLongitude(property.getLongitude().trim());
        
        System.out.println(this.getLatitude() + "--" + this.getLongitude());
        }
		return SUCCESS;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

}
