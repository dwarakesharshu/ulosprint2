package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;





import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class GuestAction extends ActionSupport implements
ServletRequestAware,SessionAware,UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private int guestId;	
	private String address1;
	private String address2;
	private String city;
	private String countryCode;
	private String emailId;
	private String firstName;
	private String landline;
	private String lastName;
	private String phone;
	private String zipCode;
	private String stateCode;
	
	
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	
	private PmsGuest guest;
	
	private List<PmsGuest> guestList;
	

	private static final Logger logger = Logger.getLogger(GuestAction.class);
    
	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public GuestAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	public String getGuests() throws IOException {
		System.out.println("get all customers");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsGuestManager guestController = new PmsGuestManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PmsGuest guest = guestController.find(getGuestId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + guest.getGuestId() + "\"";
				jsonOutput += ",\"address1\":\"" + guest.getAddress1()+ "\"";
				jsonOutput += ",\"address2\":\"" + guest.getAddress2()+ "\"";
				jsonOutput += ",\"city\":\"" + guest.getCity() + "\"";
				jsonOutput += ",\"countryCode\":\"" + guest.getCountryCode() + "\"";
				jsonOutput += ",\"emailId\":\"" + guest.getEmailId() + "\"";
				jsonOutput += ",\"firstName\":\"" + guest.getFirstName() + "\"";
				jsonOutput += ",\"landline\":\"" + (guest.getLandline()== null ? "": guest.getLandline()) + "\"";
				jsonOutput += ",\"lastName\":\"" + (guest.getLastName() == null ? "": guest.getLastName())+ "\"";
				jsonOutput += ",\"phone\":\"" + (guest.getPhone() == null ? "": guest.getPhone()) + "\"";
				jsonOutput += ",\"zipCode\":\"" + (guest.getZipCode() == null ? "": guest.getZipCode()) + "\"";
				jsonOutput += ",\"stateName\":\"" + (guest.getState().getStateName() == null ? "": guest.getState().getStateName() )+ "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAllCustomer() throws IOException {
		System.out.println("family register");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsGuestManager guestController = new PmsGuestManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.guestList = guestController.list();
			for (PmsGuest guest : guestList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + guest.getGuestId() + "\"";
				jsonOutput += ",\"address1\":\"" + guest.getAddress1()+ "\"";
				jsonOutput += ",\"address2\":\"" + guest.getAddress2()+ "\"";
				jsonOutput += ",\"city\":\"" + guest.getCity() + "\"";
				jsonOutput += ",\"countryCode\":\"" + guest.getCountryCode() + "\"";
				jsonOutput += ",\"emailId\":\"" + guest.getEmailId() + "\"";
				jsonOutput += ",\"firstName\":\"" + guest.getFirstName() + "\"";
				jsonOutput += ",\"landline\":\"" + (guest.getLandline()== null ? "": guest.getLandline()) + "\"";
				jsonOutput += ",\"lastName\":\"" + (guest.getLastName() == null ? "": guest.getLastName())+ "\"";
				jsonOutput += ",\"phone\":\"" + (guest.getPhone() == null ? "": guest.getPhone()) + "\"";
				jsonOutput += ",\"zipCode\":\"" + (guest.getZipCode() == null ? "": guest.getZipCode()) + "\"";
				jsonOutput += ",\"stateName\":\"" + (guest.getState().getStateName() == null ? "": guest.getState().getStateName() )+ "\"";
				
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String addGuest() {
		
		System.out.println("entered Guest add");
		PmsGuestManager guestController = new PmsGuestManager();
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("add guest");
			PmsGuest guest = new PmsGuest();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			List<PmsGuest> searchGuest = guestController.findGuestByEmail(emailId);
			if(searchGuest.size()==0)
			{
			
				guest.setAddress1(getAddress1());
				guest.setAddress2(getAddress2());
				guest.setCity(getCity());
				guest.setEmailId(getEmailId());
				guest.setFirstName(getFirstName());
				guest.setLandline(getLandline());
				guest.setLastName(getLastName());
				guest.setIsActive(true);
				guest.setIsDeleted(false);
				guest.setPhone(getPhone());
				guest.setZipCode(getZipCode());
				
				
				
				PmsGuest guestli = guestController.add(guest);
				
				this.guestId = guestli.getGuestId();
				sessionMap.put("guestId",guestId);
				
			}
			
			else{
				
			for(PmsGuest pg : searchGuest){
			
			System.out.println("<-----Guest Id---->");
				
			sessionMap.put("guestId",pg.getGuestId());
			
			this.guestId = (Integer) sessionMap.get("guestId");
			System.out.println(getGuestId());	
			
			}
			}
			//customer.setIcecontactno(getIcecontactno());
			
			/*if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				guest.setImgPath(this.getMyFileFileName());
			
			PmsGuest guest1 = guestController.add(guest);
			
			if(guest1.getGuestId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				File file = new File(getText("storage.user.logopath") + "/"
					+ guest1.getGuestId() + "/", this.getMyFileFileName());
				FileUtils.copyFile(this.myFile, file);// copying image in the
			}*/
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editGuest() {
		PmsGuestManager guestController = new PmsGuestManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("edit customer");
			PmsGuest guest = guestController.find(getGuestId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			guest.setAddress1(getAddress1());
			guest.setAddress2(getAddress2());
			guest.setCity(getCity());
			guest.setEmailId(getEmailId());
			guest.setFirstName(getFirstName());
			guest.setLandline(getLandline());
			guest.setLastName(getLastName());
			guest.setIsActive(true);
			guest.setIsDeleted(false);
			guest.setPhone(getPhone());
			guest.setZipCode(getZipCode());
			
			/*if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				guest.setImgPath(this.getMyFileFileName());
			PmsGuest guest1 = guestController.edit(guest);
			
			if(guest1.getGuestId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				File file = new File(getText("storage.user.logopath") + "/"
					+ guest1.getGuestId() + "/", this.getMyFileFileName());
				FileUtils.copyFile(this.myFile, file);// copying image in the
			}
			*/
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteGuest() {
		PmsGuestManager guestController = new PmsGuestManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			System.out.println("delete customer");
			PmsGuest guest = guestController.find(getGuestId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			guest.setIsActive(false);
			guest.setIsDeleted(true);
			guest.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			guestController.edit(guest);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	/*public String getGuestPicture() {
		HttpServletResponse response = ServletActionContext.getResponse();

		PmsGuestManager guestController = new PmsGuestManager();
		PmsGuest guest = guestController.find(getGuestId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if (getGuestId() >0 ) {
				imagePath = getText("storage.user.logopath") + "/"
						+ getGuestId() + "/"
						+ guest.getImgPath();
				Image im = new Image();
				//System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				//System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}*/
	public String dconverttime(java.util.Date input)
	{
		String disdatetime="";
		try { 
			DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat outputFormat1 = new SimpleDateFormat("KK:mm a dd-MMM-yyyy");
		//System.out.println(outputFormat1.format(inputFormat.parse(input)));
		disdatetime=outputFormat1.format(input);
		}catch(Exception e)
        {
                        e.printStackTrace();
        }
		return disdatetime.replace("-", " ");
	}

	public  String  dconvert(java.util.Date dformat){
		 String disdate="";
	    try { 
	    SimpleDateFormat  dateFormat = new SimpleDateFormat("dd-MMM-YYYY");
	               
	               
	                 disdate=dateFormat.format(dformat).replace("-"," ");
	                System.out.println(disdate.replace("-",""));
	                
	                }catch(Exception e)
	                {
	                                e.printStackTrace();
	                }
	    return disdate;
	}

	public  String datediffer(java.util.Date dformat){	
		
		String disstatus="";
	    try { 
	    SimpleDateFormat  dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	               
	                java.util.Date date = new java.util.Date();
	                String date1=dateFormat.format(dformat);
	                String date2=dateFormat.format(date);
	                java.util.Date d1=dateFormat.parse(date1);
	                java.util.Date d2=dateFormat.parse(date2);
	                System.out.println(dateFormat.format(date));
	                
	                long diff=d2.getTime() - d1.getTime();
	                if(diff>0)
	                {
	                System.out.println("Pass");
	                disstatus="old";
	                }
	                else
	                {
	                  System.out.println("Fail");   
	                  disstatus="New";
	                }
	                
	                }catch(Exception e)
	                {
	                                e.printStackTrace();
	                }
	    
	    System.out.println("disstatus"+disstatus);
	    
	    return disstatus;
	}
	
	

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public int getGuestId() {
		return guestId;
	}

	public void setGuestId(int guestId) {
		this.guestId = guestId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLandline() {
		return landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}


}
