package com.ulopms.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;
















import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PromotionDetailManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.ReportManager;
import com.ulopms.controller.StateManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.Revenue;
import com.ulopms.model.State;
import com.ulopms.model.User;
import com.ulopms.model.UserGuestDetailReport;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class ReportAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	


	private Date fromDate;
	private Date toDate;
	private int accommodationId;
	
	private Integer propertyId;
	
	private List<BookingDetailReport> listBookingDetails;

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private static final Logger logger = Logger.getLogger(ReportAction.class);

	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@SuppressWarnings("unchecked")
	@Override
	public void setSession(Map<String, Object> map) {
		// TODO Auto-generated method stub
		 sessionMap=(SessionMap)map;  
	}
	

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public ReportAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public String bookingReport() throws IOException {
		System.out.println("booking report");
		
		// System.out.println(jobExecutionList.size());
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			System.out.println(this.getFromDate() + "==" + this.getAccommodationId()+"=="+this.getToDate()+"=="+(Integer) sessionMap.get("propertyId"));
			
			ReportManager reportController = new ReportManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			BookingDetailManager detailController = new BookingDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
			
			PmsProperty pmsProperty=new PmsProperty();
			
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			this.listBookingDetails=reportController.listBookingDetail((Integer) sessionMap.get("propertyId"),this.getFromDate(),this.getToDate(),this.getAccommodationId());
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
					long bookingDate =bookingDetailReport.getBookingDate().getTime();
					java.util.Date dateBooking = new java.util.Date(bookingDate);
					String strBookingDate=format1.format(dateBooking); 
					jsonOutput += ",\"boookingDate\":\"" + strBookingDate+ "\"";
					jsonOutput += ",\"guestName\":\"" + bookingDetailReport.getFirstName() + "\"";
					
					long arrivalDate =bookingDetailReport.getBookingDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					String strArrivalDate=format1.format(dateArrival); 
					
					jsonOutput += ",\"checkIn\":\"" + strArrivalDate+ "\"";
					
					long departureDate =bookingDetailReport.getBookingDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					String strDepartureDate=format1.format(dateDeparture); 
					
					jsonOutput += ",\"checkOut\":\"" + strDepartureDate+ "\"";
					
					jsonOutput += ",\"accommodationType\":\"" + bookingDetailReport.getAccommodationType()+ "\"";
					jsonOutput += ",\"amount\":\"" + bookingDetailReport.getAmount()+ "\"";
					jsonOutput += ",\"source\":\""+bookingDetailReport.getSourceName()+"\"";
					jsonOutput += "}";
				}
			}
			/*List<BookingDetail> bookingDetailList = reportController.BookingDetailsByProperty(this.getAccommodationId(),this.getFromDate(),this.getToDate(),(Integer) sessionMap.get("propertyId"));
			for (BookingDetail bookingDetail : bookingDetailList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				BookingDetail booking = detailController.find(bookingDetail.getBookingDetailsId());
				PmsBooking pmsBooking = bookingController.find(booking.getPmsBooking().getBookingId());
				System.out.println("booking id========="+pmsBooking.getBookingId());
				List<BookingGuestDetail>  bookingGuestDetailList = reportController.BookingGuestDetails(pmsBooking.getBookingId());
				PmsBooking pmsBooking1 = bookingController.find(pmsBooking.getBookingId());
				BookingDetail booking1 = detailController.find(booking.getBookingDetailsId());
				System.out.println("source id===================="+pmsBooking1.getPmsSource().getSourceId());
				PmsSource pmsSource = sourceController.find(pmsBooking1.getPmsSource().getSourceId());
				PropertyAccommodation accommodation =  accommodationController.find(booking1.getPropertyAccommodation().getAccommodationId());
			
				String GuestName="";
				for(BookingGuestDetail bookingGuestDetail : bookingGuestDetailList)
				{
					BookingGuestDetail guestDetail = bookingGuestController.find(bookingGuestDetail.getBookingGuestId());
					PmsGuest pmsGuest = guestController.find(guestDetail.getPmsGuest().getGuestId());
					GuestName = pmsGuest.getFirstName() + " " + pmsGuest.getLastName();
				}
				
				jsonOutput += "\"bookingId\":\"" + pmsBooking.getBookingId()+ "\"";
				jsonOutput += ",\"boookingDate\":\"" + bookingDetail.getBookingDate()+ "\"";
				jsonOutput += ",\"guestName\":\"" + GuestName + "\"";
				jsonOutput += ",\"checkIn\":\"" + pmsBooking.getArrivalDate()+ "\"";
				jsonOutput += ",\"checkOut\":\"" + pmsBooking.getDepartureDate()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
				jsonOutput += ",\"amount\":\"" + bookingDetail.getAmount()+ "\"";
				jsonOutput += ",\"source\":\"" + pmsSource.getSourceName()+ "\"";
				
				jsonOutput += "}";

			}*/

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String generateExportBookingReport() throws IOException{
		try{
			String myHomePath= System.getProperty("user.home");
			
			XSSFWorkbook workbook=new XSSFWorkbook();
			XSSFSheet sheet=workbook.createSheet("Booking Report");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			
			CellStyle headerStyle= getHeaderStyle(workbook);
			CellStyle normalStyle = getNormalStyle(workbook);
			ReportManager reportController = new ReportManager();
			Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
			int index=1;
			int recordCount=1;
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			String strPropertyName=null;
			this.propertyId=(Integer) sessionMap.get("propertyId");
			PmsProperty pmsProperty=propertyController.find(this.propertyId);
			strPropertyName=pmsProperty.getPropertyName().trim();
			strPropertyName = strPropertyName.replaceAll(" ", "_").toLowerCase();
			data.put(index, new Object[] {"BOOKING ORDER", "BOOKING DATE", "ACCOMMODATION TYPE","GUEST NAME","MAIL ID","MOBILE NUMBER","CHECK-IN","CHECK-OUT","AMOUNT","TAX AMOUNT","SOURCE"});
			this.listBookingDetails=reportController.listBookingDetail((Integer) sessionMap.get("propertyId"),this.getFromDate(),this.getToDate(),this.getAccommodationId());
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					long bookingDate =bookingDetailReport.getBookingDate().getTime();
					java.util.Date date = new java.util.Date( bookingDate );
					String strBookingDate=format1.format(date); 
					
					long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					String strArrivalDate=format1.format(dateArrival); 
					
					long departureDate =bookingDetailReport.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					String strDepartureDate=format1.format(dateDeparture); 
					
					
					index++;
					data.put(index, new Object[] {recordCount, strBookingDate, bookingDetailReport.getAccommodationType(),bookingDetailReport.getFirstName(),bookingDetailReport.getEmailId(),
							bookingDetailReport.getMobileNumber(),strArrivalDate,strDepartureDate,bookingDetailReport.getAmount(),bookingDetailReport.getTaxAmount(),bookingDetailReport.getSourceName()});
					recordCount++;
				}
			}
	          
	        //Iterate over data and write to sheet
	        Set<Integer> keyset = data.keySet();
	        int rownum = 0;
	        for (Integer key : keyset){
	        	System.out.println("rownum.."+rownum);
	            Row row = sheet.createRow(rownum++);
	            
	            Object [] objArr = data.get(key);
	            int cellnum = 0;
	            if(rownum==1){
	            	for (Object obj : objArr){
		            	Cell cell = row.createCell(cellnum++);
		               	if(obj instanceof String){
		            	   cell.setCellValue((String)obj);
//		            	   headerStyle.setAlignment(headerStyle.ALIGN_CENTER);
		            	   cell.setCellStyle(headerStyle);
		            	   int columnIndex = cell.getColumnIndex();
		                   sheet.autoSizeColumn(columnIndex);
		               	}
		            }
	            }
	            else{
	            	for (Object obj : objArr){
		            	Cell cell = row.createCell(cellnum++);
		               	if(obj instanceof String){
		            	   cell.setCellValue((String)obj);
//		            	   normalStyle.setAlignment(normalStyle.ALIGN_LEFT);
		            	   cell.setCellStyle(normalStyle);
		            	   int columnIndex = cell.getColumnIndex();
		                   sheet.autoSizeColumn(columnIndex);
		               	}
		                else if(obj instanceof Integer){
		                	 cell.setCellValue((Integer)obj);
//		                	 normalStyle.setAlignment(normalStyle.ALIGN_RIGHT);
		                	 cell.setCellStyle(normalStyle);
		                	 int columnIndex = cell.getColumnIndex();
		                     sheet.autoSizeColumn(columnIndex);
		                }
		                else if(obj instanceof Double){
		                	cell.setCellValue((Double)obj);
//		                	normalStyle.setAlignment(normalStyle.ALIGN_RIGHT);
		                	cell.setCellStyle(normalStyle);
		                	int columnIndex = cell.getColumnIndex();
		                    sheet.autoSizeColumn(columnIndex);
		                }
		                else if(obj instanceof Long){
		                	cell.setCellValue((Long)obj);
//		                	normalStyle.setAlignment(normalStyle.ALIGN_RIGHT);
		                	cell.setCellStyle(normalStyle);
		                	int columnIndex = cell.getColumnIndex();
		                    sheet.autoSizeColumn(columnIndex);
		                }
							
		            }
	            }
	            
	        }
	        String strFileName=strPropertyName+"-"+"bookingreports.xls";
	        File f = new File(myHomePath+"\\Downloads\\"+strFileName);
	        Boolean fileCheck=false;
	        fileCheck=f.exists();
	        if(!f.exists()){
	        	System.out.println("test..");
	        }
	        
	        
	        	            
	        FileOutputStream out = new FileOutputStream(new File(myHomePath+"\\Downloads\\"+strFileName));
	        workbook.write(out);
	        out.close();
	        
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	public String RevenueReport() throws IOException {
		System.out.println("revenue report");
		// System.out.println(jobExecutionList.size());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			System.out.println(this.getFromDate() + "==" + this.getAccommodationId()+"=="+this.getToDate()+"=="+(Integer) sessionMap.get("propertyId"));
			
			ReportManager reportController = new ReportManager();
			
			
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			Integer accommodationId=0;
			String strAccommodationType=null;
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			List<Revenue> revenueList = reportController.RevenueByProperty(this.getAccommodationId(),this.getFromDate(),this.getToDate(),(Integer) sessionMap.get("propertyId"));
			if(revenueList.size()>0 && !revenueList.isEmpty()){
				for (Revenue revenue : revenueList) {

					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					
					jsonOutput += "\"amount\":\"" + revenue.getAmount()+ "\"";
					jsonOutput += ",\"nights\":\"" + revenue.getNights()+ "\"";
					String start = new SimpleDateFormat("dd-MMM-yyyy").format(this.getFromDate());
					String end = new SimpleDateFormat("dd-MMM-yyyy").format(this.getToDate());
					jsonOutput += ",\"startDate\":\"" + start + "\"";
					jsonOutput += ",\"endDate\":\"" + end + "\"";
					accommodationId=revenue.getAccommodationId();
					if(accommodationId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						strAccommodationType=accommodations.getAccommodationType();
						jsonOutput += ",\"accommodationType\":\" "+strAccommodationType+"\"";
					}
					else{
						jsonOutput += ",\"accommodationType\":\" ALL\"";
					}
					
					jsonOutput += "}";

				}
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String generateExportRevenueReport() throws IOException{
		try{
			this.propertyId=(Integer) sessionMap.get("propertyId");
			String myHomePath= System.getProperty("user.home");
			
			
			XSSFWorkbook workbook=new XSSFWorkbook();
			XSSFSheet sheet=workbook.createSheet("Revenue Report");
			
			CellStyle headerStyle= getHeaderStyle(workbook);
			CellStyle normalStyle = getNormalStyle(workbook);
			ReportManager reportController = new ReportManager();
			Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
			int index=1;
			int recordCount=1;
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			Integer accommodationId=0;
			String strAccommodationType=null;
			PmsPropertyManager propertyController=new PmsPropertyManager();
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			String strPropertyName=null;
			this.propertyId=(Integer) sessionMap.get("propertyId");
			PmsProperty pmsProperty=propertyController.find(this.propertyId);
			strPropertyName=pmsProperty.getPropertyName().trim();
			strPropertyName = strPropertyName.replaceAll(" ", "_").toLowerCase();
			
			data.put(index, new Object[] {"INDEX","ACCOMMODATION","FROM DATE","TO DATE","NO OF NIGHTS", "TOTAL REVENUE"});
			List<Revenue> revenueList = reportController.RevenueByProperty(this.getAccommodationId(),this.getFromDate(),this.getToDate(),(Integer) sessionMap.get("propertyId"));
			if(revenueList.size()>0 && !revenueList.isEmpty()){
				for (Revenue revenue : revenueList) {
					index++;
					String start = new SimpleDateFormat("dd-MMM-yyyy").format(this.getFromDate());
					String end = new SimpleDateFormat("dd-MMM-yyyy").format(this.getToDate());
					accommodationId=revenue.getAccommodationId();
					if(accommodationId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						strAccommodationType=accommodations.getAccommodationType();
					}
					else{
						strAccommodationType="ALL";
					}
					
					data.put(index, new Object[] {recordCount,strAccommodationType,start,end,revenue.getNights(),revenue.getAmount()});
					recordCount++;
				}
			}
	          
	        //Iterate over data and write to sheet
	        Set<Integer> keyset = data.keySet();
	        int rownum = 0;
	        for (Integer key : keyset){
	        	System.out.println("rownum.."+rownum);
	            Row row = sheet.createRow(rownum++);
	            
	            Object [] objArr = data.get(key);
	            int cellnum = 0;
	            if(rownum==1){
	            	for (Object obj : objArr){
		            	Cell cell = row.createCell(cellnum++);
		               	if(obj instanceof String){
		            	   cell.setCellValue((String)obj);
//		            	   normalStyle.setAlignment(normalStyle.ALIGN_CENTER);
		            	   cell.setCellStyle(headerStyle);
		            	   int columnIndex = cell.getColumnIndex();
		                   sheet.autoSizeColumn(columnIndex);
		               	}
		            }
	            }
	            else{
	            	for (Object obj : objArr){
		            	Cell cell = row.createCell(cellnum++);
		               	if(obj instanceof String){
		            	   cell.setCellValue((String)obj);
//		            	   normalStyle.setAlignment(normalStyle.ALIGN_LEFT);
		            	   cell.setCellStyle(normalStyle);
		            	   int columnIndex = cell.getColumnIndex();
		                   sheet.autoSizeColumn(columnIndex);
		                   
		               	}
		                else if(obj instanceof Integer){
		                	 cell.setCellValue((Integer)obj);
//		                	 normalStyle.setAlignment(normalStyle.ALIGN_RIGHT);
		                	 cell.setCellStyle(normalStyle);
		                	 int columnIndex = cell.getColumnIndex();
		                     sheet.autoSizeColumn(columnIndex);
		                }
		                else if(obj instanceof Double){
		                	cell.setCellValue((Double)obj);
//		                	normalStyle.setAlignment(normalStyle.ALIGN_RIGHT);
		                	cell.setCellStyle(normalStyle);
		                	int columnIndex = cell.getColumnIndex();
		                    sheet.autoSizeColumn(columnIndex);
		                }
		                else if(obj instanceof Long){
		                	cell.setCellValue((Long)obj);
//		                	normalStyle.setAlignment(normalStyle.ALIGN_RIGHT);
		                	cell.setCellStyle(normalStyle);
		                	int columnIndex = cell.getColumnIndex();
		                    sheet.autoSizeColumn(columnIndex);
		                }
							
		            }
	            }
	            
	        }
	        String strFileName=strPropertyName+"-"+"revenuereports.xls";
	        File f = new File(myHomePath+"\\Downloads\\"+strFileName);
	        Boolean fileCheck=false;
	        fileCheck=f.exists();
	        if(!f.exists()){
	        	System.out.println("test..");
	        }
	        	            
	        FileOutputStream out = new FileOutputStream(new File(myHomePath+"\\Downloads\\"+strFileName));
	        workbook.write(out);
	        out.close();
	        
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	
	public String UserReport() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			ReportManager reportController = new ReportManager();
			
			List<UserGuestDetailReport> listUserGuestAll=new ArrayList<UserGuestDetailReport>();
			response.setContentType("application/json");
			List<UserGuestDetailReport> userList=reportController.UserDetails();
			String strPhoneNumber="",strAddress1="",strAddress2="";
			List<UserGuestDetailReport> guestList=reportController.GuestDetails();
			
			listUserGuestAll.addAll(guestList);
			listUserGuestAll.addAll(userList);
			
			if(listUserGuestAll.size()>0 && !listUserGuestAll.isEmpty()){
				for (UserGuestDetailReport userguest : listUserGuestAll) {

					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					strPhoneNumber=userguest.getPhoneNumber();
					strAddress1=userguest.getAddress1();
					strAddress2=userguest.getAddress2();
					
					if(userguest.getPhoneNumber()==null ){
						strPhoneNumber="-";
					}
					
					
					
					jsonOutput += "\"userName\":\"" + userguest.getUserName()+ "\"";
					jsonOutput += ",\"emailId\":\"" + userguest.getEmailId()+ "\"";
					
					jsonOutput += ",\"phoneNumber\":\"" + strPhoneNumber+ "\"";
					
					
					jsonOutput += "}";

				}
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			System.out.println("jsonOutput..."+jsonOutput);
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String generateExportUserReport() throws IOException{
		try{
			
			String myHomePath= System.getProperty("user.home");
			
			
			XSSFWorkbook workbook=new XSSFWorkbook();
			XSSFSheet sheet=workbook.createSheet("User Guest Report");
			
			HttpServletResponse response = ServletActionContext.getResponse();
			
			CellStyle headerStyle= getHeaderStyle(workbook);
			CellStyle normalStyle = getNormalStyle(workbook);
			ReportManager reportController = new ReportManager();
			Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
			int index=1;
			int recordCount=1;
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			List<UserGuestDetailReport> listUserGuestAll=new ArrayList<UserGuestDetailReport>();
			data.put(index, new Object[] {"INDEX","CUSTOMER NAME","EMAIL ID","PHONE NUMBER"});
			List userList=reportController.UserDetails();
			List guestList=reportController.GuestDetails();
			String strPhoneNumber="",strAddress1="",strAddress2="";
			
			listUserGuestAll.addAll(guestList);
			listUserGuestAll.addAll(userList);
			if(listUserGuestAll.size()>0 && !listUserGuestAll.isEmpty()){
				for (UserGuestDetailReport userguest : listUserGuestAll) {
					index++;
					
					strPhoneNumber=userguest.getPhoneNumber();
					strAddress1=userguest.getAddress1();
					strAddress2=userguest.getAddress2();
					
					if(userguest.getPhoneNumber()==null ){
						strPhoneNumber="-";
					}
					
					if(userguest.getAddress1()==null){
						strAddress1="-";
					}
					
					if(userguest.getAddress2()==null){
						strAddress2="-";
					}
					
					data.put(index, new Object[] {recordCount,userguest.getUserName(),userguest.getEmailId(),strPhoneNumber});
					recordCount++;
				}
			}
	          
	        //Iterate over data and write to sheet
	        Set<Integer> keyset = data.keySet();
	        int rownum = 0;
	        for (Integer key : keyset){
	        	System.out.println("rownum.."+rownum);
	            Row row = sheet.createRow(rownum++);
	            
	            Object [] objArr = data.get(key);
	            int cellnum = 0;
	            if(rownum==1){
	            	for (Object obj : objArr){
		            	Cell cell = row.createCell(cellnum++);
		               	if(obj instanceof String){
		            	   cell.setCellValue((String)obj);
//		            	   normalStyle.setAlignment(normalStyle.ALIGN_CENTER);
		            	   cell.setCellStyle(headerStyle);
		            	   int columnIndex = cell.getColumnIndex();
		                   sheet.autoSizeColumn(columnIndex);
		               	}
		            }
	            }
	            else{
	            	for (Object obj : objArr){
		            	Cell cell = row.createCell(cellnum++);
		               	if(obj instanceof String){
		            	   cell.setCellValue((String)obj);
//		            	   normalStyle.setAlignment(normalStyle.ALIGN_LEFT);
		            	   cell.setCellStyle(normalStyle);
		            	   int columnIndex = cell.getColumnIndex();
		                   sheet.autoSizeColumn(columnIndex);
		                   
		               	}
		                else if(obj instanceof Integer){
		                	 cell.setCellValue((Integer)obj);
//		                	 normalStyle.setAlignment(normalStyle.ALIGN_RIGHT);
		                	 cell.setCellStyle(normalStyle);
		                	 int columnIndex = cell.getColumnIndex();
		                     sheet.autoSizeColumn(columnIndex);
		                }
		                else if(obj instanceof Double){
		                	cell.setCellValue((Double)obj);
//		                	normalStyle.setAlignment(normalStyle.ALIGN_RIGHT);
		                	cell.setCellStyle(normalStyle);
		                	int columnIndex = cell.getColumnIndex();
		                    sheet.autoSizeColumn(columnIndex);
		                }
		                else if(obj instanceof Long){
		                	cell.setCellValue((Long)obj);
//		                	normalStyle.setAlignment(normalStyle.ALIGN_RIGHT);
		                	cell.setCellStyle(normalStyle);
		                	int columnIndex = cell.getColumnIndex();
		                    sheet.autoSizeColumn(columnIndex);
		                }
							
		            }
	            }
	            
	        }
	        
	        File f = new File(myHomePath+"\\Downloads\\customerreports.xls");
	        Boolean fileCheck=false;
	        fileCheck=f.exists();
	        if(!f.exists()){
	        	System.out.println("test..");
	        }
	        	            
	        FileOutputStream out = new FileOutputStream(new File(myHomePath+"\\Downloads\\customerreports.xls"));
	        workbook.write(out);
	        out.close();
	        
	        

	        
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public int getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(int accommodationId) {
		this.accommodationId = accommodationId;
	}

	private CellStyle getHeaderStyle(XSSFWorkbook xwb){
		CellStyle style = xwb.createCellStyle();
		style.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setAlignment(CellStyle.ALIGN_CENTER);
		
		XSSFFont font = xwb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		style.setFont(font);
		
		return style;
	}


	private CellStyle getNormalStyle(XSSFWorkbook xwb)
    {
        CellStyle style = xwb.createCellStyle();
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setAlignment(CellStyle.ALIGN_CENTER);
        
        XSSFFont font = xwb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		style.setFont(font);
        return style;
    }

}
