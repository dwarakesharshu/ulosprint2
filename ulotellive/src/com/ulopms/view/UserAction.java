package com.ulopms.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.hibernate.Hibernate;



//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;




import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.User;
import com.ulopms.security.passwordDecrypt;
import com.ulopms.util.Image;




import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.Location;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyPhoto;
import com.ulopms.model.User;
import com.ulopms.security.passwordDecrypt;
import com.ulopms.util.Email;
import com.ulopms.util.Image;




public class UserAction extends ActionSupport implements
ServletRequestAware,SessionAware,UserAware{

	private static final long serialVersionUID = 914982665758390091L;
	
	private String oper;
	private Integer userId;
	private User user = new User();
	private List<User> userList;
	private Integer roleId;
	private String userName;
	private String address1;
	private String address2;
	private String phone;
	private String emailId;
	private String isActive;
	private String isDelete;
	private Timestamp createdDate;
	private Integer createdBy;
	private Integer modifiedBy;
	private Timestamp modifiedDate;
	private String profilePic;
	private String hashPassword;
	private String saltKey;
	
	
	private Integer accommodationId;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private Integer adultCount;
	private Integer childCount;
	private double amount;
	private double refund;
	public Integer statusId;
	public double tax;
	private Integer bookingId;
	private Integer guestId;
	private Integer roomId;
	private String propertyName;
	private String firstName;
	private String lastName;
	private Integer rooms;
	private long adults;
	private long child;
	
	private List<PmsBookedDetails> bookedList;
	
	public User getModel() {
		return user;
	}  
	
	private HttpServletRequest request;
	
	public HttpSession getSession() {
		return session;
	}
	
	public void setSession(HttpSession session) {
		this.session = session;
	}

	private HttpSession session;
	
	
	
	private SessionMap<String,Object> sessionMap;

	//getters and setters

	@Override
	public void setSession(Map<String, Object> map) {
	sessionMap=(SessionMap)map;
	} 
	

	
	private File myFile;

	private String myFileContentType;
	private String myFileFileName;
	
	private List<BookingGuestDetail> userBookingList;
	
	private static final Logger logger = Logger.getLogger(UserAction.class);

	
	

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
		
	}

	private UserLoginManager linkController;
	

	public UserAction() {
		linkController = new UserLoginManager();
	}

	public String execute() throws Exception {
		try {
			System.out.println(getUserId());
			System.out.println(getOper());

			if (getOper() != null && getOper().equalsIgnoreCase("edit")) {
				System.out.println("edit operation");
				editFamilyUser();

			} else if (getOper() != null && getOper().equalsIgnoreCase("add")) {
				System.out.println("add operation");
				add();
			} else if (getOper() != null
					&& getOper().equalsIgnoreCase("delete")) {
				System.out.println("delete operation");
			}

			this.userList = linkController.list();
			System.out.println(this.userList.size());
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}
	
	public String add() {
		// System.out.println(getUser());
		try {
			// user.setUserId(100L);
		//	user.setPhone(phone);
			//user.setPassword(password);
			user.setUserName(userName);
			user.setEmailId(emailId);
			linkController.add(getUser());
			this.userList = linkController.list();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}

		return SUCCESS;
	}
	
	public String userProfileUpdate() throws Exception {
		try {
			//this.projectList = linkController.list();
			this.session = request.getSession();
			//Company company  = (Company)session.getAttribute("COMPANY");
			
			System.out.println("profile update...");
			// System.out.println(getModel().getPassword());
			// System.out.println(getModel().getUserName());
			// System.out.println(getModel().getPassword());
			// System.out.println(getModel().getPhone());
			// user.setUserId(getModel().getUserId());

		//	user.setPhone(getModel().getPhone());
			// user.setPassword(getModel().getPassword());
			user.setUserName(getModel().getUserName());
			user.setEmailId(getModel().getEmailId());
			linkController.edit(getModel());
			this.user = getModel();
		//	this.companyName = company.getCompanyName();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}
	
	public String userProfilePic() throws IOException {
		try {

			System.out.println("profile pic update...");
			//System.out.println(getMyFile());
			//User u = new User();
			// FileInputStream fis;
			// fis = new FileInputStream(myFile);
			// @SuppressWarnings("deprecation")
			// Blob blob = Hibernate.createBlob(fis);
			// user.setProfilePicture(blob);
			// linkController.edit(getModel());
			// this.user = getModel();
			File fileZip1 = new File(getText("storage.user.logopath") + "/"
					+ getModel().getUserId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			// new file
			//System.out.println();
			User u =linkController.findUser(user.getUserId()); 
			
			//System.out.println(this.getMyFileFileName());
			
			u.setImgPath(this.getMyFileFileName());
			//System.out.println(fileZip1);
			//System.out.println(getMyFileFileName());
			//System.out.println(getModel());
			//u.setProfilePicName(this.getMyFileFileName());
			linkController.edit(u);
			this.user = getModel();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}
	
	@SuppressWarnings("deprecation")
	public String GetProfilePic() {

		HttpServletResponse response = ServletActionContext.getResponse();

		HttpServletRequest request = ServletActionContext.getRequest();
		String imagePath = "";
		try {
			if (request.getParameter("id") != null
					&& request.getParameter("id") != ""
					&& NumberUtils.isNumber(request.getParameter("id"))) {
				System.out.println(request.getParameter("id")
						+ "-----user action");
				User usr = linkController.findUser(Integer.parseInt(request
						.getParameter("id")));
				if (usr.getImgPath() != null) {
					imagePath = getText("storage.path") + "/" + usr.getUserId()							
							+ "/" + usr.getImgPath();
					Image im = new Image();
					System.out.println(imagePath);
					response.getOutputStream().write(
							im.getCustomImageInBytes(imagePath));
					response.getOutputStream().flush();
				} else {
					imagePath = getText("storage.path")
							+ "/emptyprofilepic.png";
					Image im = new Image();
					System.out.println(imagePath);
					response.getOutputStream().write(
							im.getCustomImageInBytes(imagePath));
					response.getOutputStream().flush();
				}
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}

		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;
	}
	
	 public String getUserProfile() throws IOException {

		//System.out.println(userId);
		//System.out.println("shivabalan");
		try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();


		UserLoginManager userController = new UserLoginManager();

		//this.familyRegisterList = familyController.list(getUser().getUserid());
		//model = familyRegisterList;
		response.setContentType("application/json");

		this.userId = (Integer) sessionMap.get("userId");

		System.out.println("-----------------"+getUserId());



		User user = userController.find(userId);


		if (!jsonOutput.equalsIgnoreCase(""))
		jsonOutput += ",{";
		else
		jsonOutput += "{";
		jsonOutput += "\"userId\":\"" + user.getUserId() + "\"";


		jsonOutput += ",\"userName\":\"" + user.getUserName()+ "\"";
		jsonOutput += ",\"address1\":\"" + user.getAddress1()+ "\"";
		jsonOutput += ",\"address2\":\"" + user.getAddress2()+ "\"";
		jsonOutput += ",\"phone\":\"" + user.getPhone()+ "\"";
		jsonOutput += ",\"emailId\":\"" + user.getEmailId()+ "\"";

		jsonOutput += "}";



		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");


		} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
		} finally {

		}

		return null;

		}		

	public String getUserProfilePic() {
		HttpServletResponse response = ServletActionContext.getResponse();

		String imagePath = "";
		// response.setContentType("");
		try {
			User usr =linkController.findUser(user.getUserId());
			if ( usr.getImgPath() != null) {
				imagePath = getText("storage.user.logopath") + "/"
						+ usr.getUserId() + "/"
						+ usr.getImgPath();
				Image im = new Image();
				System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.user.logopath") + "/emptyprofilepic.png";
				Image im = new Image();
				//System.out.println(imagePath);
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}

	
	public String editFamilyUser() {
		//System.out.println("shiva");
		System.out.println(getUserId());
		//PmsPropertyManager propertyController = new PmsPropertyManager();
		UserLoginManager  userController = new UserLoginManager();
		
		try {
			
			User user = userController.findUser(getUserId());
			
			user.setUserId(getUserId());
			user.setUserName(getUserName());
			user.setAddress1(getAddress1());
			user.setAddress2(getAddress2());
			//user.setCreatedDate(getCreatedDate());
			//user.setIsActive(true);
			//user.setIsDeleted(false);
			
			//user.setModifiedBy(getModifiedBy());
			//user.setImgPath(getImgPath());
			user.setPhone(getPhone());
			//user.setEmailId(getEmailId());
			//user.setHashpassword(getHashPassword());
			//user.setSaltkey(getSaltKey());	
			
			System.out.println(user);
			
			
			
			
			userController.edit(user);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String GetProjectUserAll()
	{
		
		return null;
	}
	public String userPasswordUpdate() throws Exception {
		try {
			System.out.println("profile pass word update...");
			//System.out.println(getModel().getPassword());
			//System.out.println(getOldPassword());
			// System.out.println(getModel().getPassword());
			// System.out.println(getModel().getPhone());
			// user.setUserId(getModel().getUserId());
			// user.setPhone(getModel().getPhone());
			//String oldhashpassword=passwordDecrypt.getDigestvalid(getOldPassword(),getModel().getSaltkey());
			//getModel().getHashpassword()
			/*if (getOldPassword().equalsIgnoreCase(getModel().getPassword())) {
				user.setPassword(getModel().getPassword());
			}*/
			//if( oldhashpassword.equalsIgnoreCase( getModel().getHashpassword())){
				String PUBLIC_SALT = passwordDecrypt.secrandom();
				//System.out.println(this.getNewPassword());
				//String newhashpassword=passwordDecrypt.getDigestvalid(this.getNewPassword(),PUBLIC_SALT);
				User u = linkController.findUser(user.getUserId());
				//u.setHashPassword(newhashpassword);
				u.setSaltkey(PUBLIC_SALT);
				
			//}
			// user.setUserName(getModel().getUserName());
			// user.setEmailId(getModel().getEmailId());
			linkController.edit(u);
			this.user = getModel();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String edituser() {
		System.out.println("edit..");
		// System.out.println(getUser());
		try {
			user.setUserId(getUserId());
			//user.setPhone(phone);
			//user.setPassword(password);
			user.setUserName(userName);
			user.setEmailId(emailId);
			linkController.edit(getUser());
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		this.userList = linkController.list();
		return SUCCESS;
	}
	
	public String delete() {
		linkController.delete(getUserId());
		return SUCCESS;
	}
	
	 public String getUserBookings() throws IOException {
			
			
			try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
			   
				PmsPropertyManager propertyController = new PmsPropertyManager();
				PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
				BookingDetailManager bookingDetailController = new BookingDetailManager();
				BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
				PmsGuestManager guestController = new PmsGuestManager();
				PmsStatusManager statusController = new PmsStatusManager();
				PmsBookingManager bookingController = new PmsBookingManager();
				UserLoginManager  userController = new UserLoginManager();
				BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
				
				//this.familyRegisterList = familyController.list(getUser().getUserid());
				//model = familyRegisterList;
				//response.setContentType("application/json");
				this.userBookingList = bookingGuestController.userBookingList(getEmailId());
				//System.out.println(this.userBookingList.toString());
				String bookingIds = this.userBookingList.toString().replaceAll("\\[|\\]","");
				StringBuilder accommodationType = new StringBuilder();
				System.out.println(bookingIds);
				 
				response.setContentType("application/json");
				 
				for (int i = 0; i<bookingIds.split(",").length; i++) 
			      {
				
				  
				   
				   this.bookedList = bookingDetailController.bookedList(Integer.parseInt(bookingIds.split(",")[i].trim()));
				   for (PmsBookedDetails booked : bookedList) {
					
					/*System.out.println("BookingId -->" + bookingIds.split(",")[i]);
					System.out.println("Arrival -->" + booked.getArrivalDate());
		            System.out.println("Departure -->" + booked.getDepartureDate());
		            System.out.println("guestId -->" + booked.getGuestId());*/
		            
		            
		           
	            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
	            	this.propertyName = pmsProperty.getPropertyName().replaceAll("\\s", "");
	            	this.arrivalDate = booked.getArrivalDate();
	            	this.departureDate = booked.getDepartureDate();
	            	PmsGuest guest = guestController.find(booked.getGuestId());
	            	this.firstName = guest.getFirstName();
	            	this.lastName = guest.getLastName();
	            	this.phone = guest.getPhone();
	            	this.emailId = guest.getEmailId();
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(booked.getAccommodationId());
	            	accommodationType.append(accommodation.getAccommodationType().trim());
	            	accommodationType.append(",");
	            	this.rooms = booked.getRooms();
	            	this.adults = booked.getAdults();
	            	this.child = booked.getChild();
	            	this.tax = booked.getTax();
	            	this.amount = booked.getAmount();
					   
				   }
				   
				   if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					
					else
						jsonOutput += "{";
					//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
					jsonOutput += "\"bookingId\":\"" + bookingIds.split(",")[i].replaceAll("\\s", "") + "\"";
					jsonOutput += ",\"propertyName\":\"" + this.propertyName + "\"";
					String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
					String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(getDepartureDate());
					jsonOutput += ",\"arrivalDate\":\"" + checkIn + "\"";
					jsonOutput += ",\"departureDate\":\"" + checkOut+ "\"";
					
					//jsonOutput += ",\"firstName\":\"" + this.firstName+ "\"";
					//jsonOutput += ",\"lastName\":\"" + this.lastName+ "\"";
					//jsonOutput += ",\"phone\":\"" + this.phone+ "\"";
					//jsonOutput += ",\"emailId\":\"" + this.emailId+ "\"";
					//jsonOutput += ",\"accommodationType\":\"" + accommodationType.toString()+ "\"";
					//jsonOutput += ",\"rooms\":\"" + this.rooms+ "\"";
					//jsonOutput += ",\"adultCount\":\"" + this.adults+ "\"";
					//jsonOutput += ",\"childCount\":\"" + this.child+ "\"";
					//jsonOutput += ",\"tax\":\"" + this.tax+ "\"";
					jsonOutput += ",\"amount\":\"" + this.amount+ "\"";
					
					jsonOutput += "}";
					
		            
		            

					
				   //System.out.println(bookingIds.split(",")[i]);
				
					
			      }
				 
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		}
		
	
	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> usersList) {
		this.userList = usersList;
	} 
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
    public String getUserName() {
		return userName;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getRoleId() {
		return roleId;
	}
	
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	public String getAddress1() {
		return address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress2() {
		return address2;
	}
	
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getEmailId() {
		return emailId;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getOper() {
		return oper;
	}
    
	public void setIsDeleted(String isDelete) {
		this.isDelete = isDelete;
	}
	
	public String getIsDeleted() {
		return isDelete;
	}
	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	} 
	
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	} 
	
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setImgPath(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getImgPath() {
		return profilePic;
	}
	
	public void setHashPassword(String hashPassword) {
		this.hashPassword = hashPassword;
	}

	public String getHashPassword() {
		return hashPassword;
	}
	
	public void setSaltKey(String saltKey) {
		this.saltKey = saltKey;
	}

	public String getSaltKey() {
		return saltKey;
	}
	
	public List<PmsBookedDetails> getBookedList() {
		return bookedList;
	}

	public void setBookedList(List<PmsBookedDetails> bookedList) {
		this.bookedList = bookedList;
	}
	
	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getTax() {
		return tax;
	}
	
	public void setTax(double tax) {
		this.tax = tax;
	}
	
	public double getRefund() {
		return refund;
	}

	public void setRefund(double refund) {
		this.refund = refund;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	
	public Integer getGuestId() {
		return guestId;
	}

	public void setGuestId(Integer guestId) {
		this.guestId = guestId;
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	
	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

}
