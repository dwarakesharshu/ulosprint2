package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsGuest;
import com.ulopms.model.User;
import com.ulopms.util.HibernateUtil;


public class PmsGuestManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsGuestManager.class);

	public PmsGuest add(PmsGuest pmsGuest) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsGuest);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuest;
	}

	public PmsGuest edit(PmsGuest pmsGuest) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsGuest);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuest;
	}

	public PmsGuest delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsGuest pmsGuest = (PmsGuest) session.load(PmsGuest.class, id);
		if (null != pmsGuest) {
			session.delete(pmsGuest);
		}
		session.getTransaction().commit();
		return pmsGuest;
	}

	public PmsGuest find(int id) {
		// System.out.println(id);
		PmsGuest pmsGuest = new PmsGuest();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsGuest = (PmsGuest) session.load(PmsGuest.class, id);
			// session.getTransaction().commit();
			// System.out.println(PmsGuest.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsGuest;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsGuest> findGuestByEmail(String email) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsGuest> pmsGuests = null;
		try {
			pmsGuests = session.createQuery("from PmsGuest where emailId=:email")
					.setParameter("email", email).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuests;
	}

	@SuppressWarnings("unchecked")
	public List<PmsGuest> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsGuest> pmsGuests = null;
		try {

			pmsGuests = (List<PmsGuest>) session.createQuery("from PmsGuest").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuests;
	}
	
	

	
}
