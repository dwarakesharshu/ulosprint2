package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.ConfRoleScreen;
import com.ulopms.model.Role;
import com.ulopms.util.HibernateUtil;


public class RoleManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(RoleManager.class);

	public Role add(Role role) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(role);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return role;
	}

	public ConfRoleScreen add(ConfRoleScreen confRoleScreen) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(confRoleScreen);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return confRoleScreen;
	}

	public ConfRoleScreen edit(ConfRoleScreen confRoleScreen) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(confRoleScreen);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return confRoleScreen;
	}

	public Role edit(Role role) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(role);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return role;
	}

	public Role delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Role role = (Role) session.load(Role.class, id);
		if (null != role) {
			session.delete(role);
		}
		session.getTransaction().commit();
		return role;
	}

	public Role find(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Role role = (Role) session.get(Role.class, id);
		// session.getTransaction().commit();
		// session.close();
		return role;
	}

	@SuppressWarnings("unchecked")
	public List<Role> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Role> roles = null;
		try {

			roles = (List<Role>) session.createQuery(
					"from Role order by roleName").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return roles;
	}
	@SuppressWarnings("unchecked")
	public List<Role> list(String role) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Role> roles = null;
		try {

			roles = (List<Role>) session.createQuery(
					"from Role where role=:role order by roleName")
					.setParameter("role", role)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return roles;
	}

	@SuppressWarnings("unchecked")
	public List<ConfRoleScreen> list(short roleId, short screenId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<ConfRoleScreen> confRoleScreens = null;
		try {

			confRoleScreens = (List<ConfRoleScreen>) session
					.createQuery(
							"select a from ConfRoleScreen a join a.role b join a.screen c where b.roleId=:roleId and c.screenId=:screenId")
							.setParameter("roleId", roleId)
							.setParameter("screenId", screenId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return confRoleScreens;
	}
}
