package com.ulopms.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.joda.time.DateTime;

import com.ulopms.model.PmsPromotions;
import com.ulopms.util.HibernateUtil;


public class PromotionManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PromotionManager.class);

	public PmsPromotions add(PmsPromotions pmsPromotions) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsPromotions);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPromotions;
	}

	public PmsPromotions edit(PmsPromotions pmsPromotions) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsPromotions);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPromotions;
	}

	public PmsPromotions delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsPromotions pmsPromotions = (PmsPromotions) session.load(PmsPromotions.class, id);
		if (null != pmsPromotions) {
			session.delete(pmsPromotions);
		}
		session.getTransaction().commit();
		return pmsPromotions;
	}

	public PmsPromotions find(int id) {
		// System.out.println(id);
		PmsPromotions pmsPromotions = new PmsPromotions();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsPromotions = (PmsPromotions) session.load(PmsPromotions.class, id);
			// session.getTransaction().commit();
			// System.out.println(PropertyRate.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsPromotions;
	}

	@SuppressWarnings("unchecked")
	public List<PmsPromotions> list(int propertyId) {
        System.out.println(propertyId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ").setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date) {
        System.out.println(propertyId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date,String strPromotionType) {
        System.out.println(propertyId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId and promotionType=:promotionType "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("propertyId", propertyId)
					.setParameter("promotionType", strPromotionType)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date,String strPromotionType,int accommodationId) {
        System.out.println(propertyId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId and promotionType=:promotionType "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' "
					+ " propertyAccommodation.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommodationId)
					.setParameter("promotionType", strPromotionType)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotionsPerAccommodation(int accommodationId,Date date) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where propertyAccommodation.accommodationId=:accommodationId "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("accommodationId", accommodationId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listUpdatePromotions(int promotionId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where promotionId=:promotionId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ").setParameter("promotionId", promotionId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotionsDetails(int propertyId,Date date) {
        System.out.println(propertyId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and startDate>=:startDate or endDate>=:endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("propertyId", propertyId)
					.setParameter("startDate", date)
					.setParameter("endDate", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
}
