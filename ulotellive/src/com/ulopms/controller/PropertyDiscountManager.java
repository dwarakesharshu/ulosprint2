package com.ulopms.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyDiscount;
import com.ulopms.util.HibernateUtil;


public class PropertyDiscountManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyDiscountManager.class);

	public PropertyDiscount add(PropertyDiscount propertyDiscount) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyDiscount);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyDiscount;
	}

 	public PropertyDiscount edit(PropertyDiscount propertyDiscount) {
 		//System.out.println("shiva balannnnnnnnnn");
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyDiscount);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		
		return propertyDiscount;
	}

 	/* public PropertyItem delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyItem propertyItem = (PropertyItem) session.load(PropertyItem.class, id);
		if (null != propertyItem) {
			session.delete(propertyItem);
		}
		session.getTransaction().commit();
		return propertyItem;
	}   */

	public PropertyDiscount find(int id) {
		 //System.out.println(+id);
		PropertyDiscount propertyDiscount = new PropertyDiscount();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyDiscount = (PropertyDiscount) session.load(PropertyDiscount.class, id);
			// session.getTransaction().commit();
			// System.out.println(PropertyItem.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyDiscount;
	}   
	//from PropertyDiscount where discountName=:discountName and isActive = 'true' and isDeleted = 'false'
	public List<PropertyDiscount> findCount(String discountName) {
		 //System.out.println(+id);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDiscount = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDiscount = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where discountName=:discountName and isActive = 'true' and isDeleted = 'false'")
			.setParameter("discountName", discountName)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyDiscount;
	}   
	
	
	public List<PropertyDiscount> findActiveCount() {
		 //System.out.println(+id);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDiscount = null;
		try {
             
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDiscount = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where isSignup='true' and isActive = 'true' and isDeleted = 'false'")
			//.setParameter("isSignup", isSignup)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyDiscount;
	}   
	
	public PropertyDiscount findName(String discountName, Timestamp endDate) {
		 //System.out.println(+id);
		PropertyDiscount propertyDiscount = new PropertyDiscount();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyDiscount = (PropertyDiscount) session.createQuery("from PropertyDiscount where discountName=:discountName and endDate<=:endDate and isActive = 'true' and isDeleted = 'false'")
					.setParameter("discountName", discountName)
					.setParameter("endDate", endDate).uniqueResult();
			// session.getTransaction().commit();
			// System.out.println(PropertyItem.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyDiscount;
	}   
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> verifyDiscount(String discountName, Timestamp endDate) {
        System.out.println(endDate);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicount = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicount = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where discountName=:discountName and endDate>=:endDate and isActive = 'true' and isDeleted = 'false'")
			.setParameter("discountName", discountName)
			.setParameter("endDate", endDate)
		    .list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyDicount;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> currentDiscounts(Date currentDate) {
        //System.out.println(propertyId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where :date between "
					+ "startDate and endDate  and isActive = 'true' "
					+ "and isDeleted = 'false'")
					//.setParameter("propertyId", propertyId)
				    .setParameter("date", currentDate)
		        	.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyDicounts;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> list(int propertyId) {
        System.out.println(propertyId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where isActive = 'true' and isDeleted = 'false'")
			//.setParameter("propertyId", propertyId)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyDicounts;
	}
  
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> listresort(int propertyId,String date) throws ParseException {
        System.out.println("proper===id"+propertyId);
        
        Date today=new SimpleDateFormat("yyyy-MM-dd").parse(date); 
        System.out.println("today===id"+today);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount pd where pd.pmsProperty.propertyId =:propertyId  and pd.isActive = 'true' and pd.isDeleted = 'false' and pd.isResort = 'true' and (:today between pd.startDate and pd.endDate)")
			.setParameter("propertyId", propertyId)
			.setParameter("today", today)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyDicounts;
	}
  
	
}
