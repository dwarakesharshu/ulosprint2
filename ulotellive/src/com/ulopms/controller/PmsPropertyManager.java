package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsProperty;
import com.ulopms.util.HibernateUtil;


public class PmsPropertyManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsPropertyManager.class);

	public PmsProperty add(PmsProperty pmsProperty) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsProperty);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsProperty;
	}

	public PmsProperty edit(PmsProperty pmsProperty) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsProperty);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsProperty;
	}

	public PmsProperty delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsProperty pmsProperty = (PmsProperty) session.load(PmsProperty.class, id);
		if (null != pmsProperty) {
			session.delete(pmsProperty);
		}
		session.getTransaction().commit();
		return pmsProperty;
	}

	public PmsProperty find(int id) {
		// System.out.println(id);
		PmsProperty pmsProperty = new PmsProperty();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsProperty = (PmsProperty) session.load(PmsProperty.class, id);
			// session.getTransaction().commit();
			// System.out.println(PmsProperty.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsProperty;
	}

	@SuppressWarnings("unchecked")
	public List<PmsProperty> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("from PmsProperty").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}

	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("from PmsProperty where pmsProperty.propertyId=:propertyId")
			.setParameter("propertyId", propertyId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listPropertyByUser(int userId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("select a from PmsProperty a left join a.propertyUsers b where a.propertyId=b.pmsProperty.propertyId and b.user.userId=:userId")
			.setParameter("userId", userId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listPropertyByLocation(int locationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("select a from PmsProperty a where a.location.locationId=:locationId")
			.setParameter("locationId", locationId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
}
