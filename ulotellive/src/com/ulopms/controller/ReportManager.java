package com.ulopms.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.Revenue;
import com.ulopms.model.RevenueReport;
import com.ulopms.model.Screen;
import com.ulopms.model.UserGuestDetailReport;
import com.ulopms.util.HibernateUtil;


public class ReportManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(ReportManager.class);

	
	@SuppressWarnings("unchecked")
	public List<Screen> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Screen> screens = null;
		try {

			screens = (List<Screen>) session.createQuery("from Screen").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return screens;
	}

	@SuppressWarnings("unchecked")
	public List<BookingDetail> BookingDetails() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		try {
			bookingDetails = (List<BookingDetail>)session.createQuery("from BookingDetail").list();
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	@SuppressWarnings("unchecked")
	public List<BookingDetail> BookingDetailsByProperty(int accommodationId, Date fromDate, Date toDate, int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		try {
			if(accommodationId>0)
			{
				bookingDetails = (List<BookingDetail>)session.createQuery("select a from BookingDetail a join a.pmsBooking b where a.bookingDate between :fromDate and :toDate and "
					+ "a.propertyAccommodation.accommodationId=:accommodationId and b.pmsProperty.propertyId=:propertyId")
					.setParameter("accommodationId", accommodationId)
					.setParameter("fromDate", fromDate)
					.setParameter("toDate", toDate)
					.setParameter("propertyId", propertyId)
					.list();
			}
			else
			{
				bookingDetails = (List<BookingDetail>)session.createQuery("select a from BookingDetail a join a.pmsBooking b where a.bookingDate between :fromDate and :toDate and "
					+ "b.pmsProperty.propertyId=:propertyId")
						.setParameter("fromDate", fromDate)
						.setParameter("toDate", toDate)
						.setParameter("propertyId", propertyId)
						.list();
			}
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	@SuppressWarnings("unchecked")
	public List<Revenue> RevenueByProperty(int accommodationId, Date fromDate, Date toDate, int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Revenue> revenues = null;
		try {
			/*if(accommodationId>0)
			{
				revenues = (List<Revenue>)session.createQuery("select sum(a.amount) as amount,count(a) as nights from BookingDetail a join a.pmsBooking b where a.bookingDate between :fromDate and :toDate and "
					+ "a.propertyAccommodation.accommodationId=:accommodationId and b.pmsProperty.propertyId=:propertyId")
					.setParameter("accommodationId", accommodationId)
					.setParameter("fromDate", fromDate)
					.setParameter("toDate", toDate)
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(Revenue.class))
					.list();
			}
			else
			{
				revenues = (List<Revenue>)session.createQuery("select sum(a.amount) as amount,count(a) as nights from BookingDetail a join a.pmsBooking b where a.bookingDate between :fromDate and :toDate and "
					+ "b.pmsProperty.propertyId=:propertyId")
						.setParameter("fromDate", fromDate)
						.setParameter("toDate", toDate)
						.setParameter("propertyId", propertyId)
						.setResultTransformer(Transformers.aliasToBean(Revenue.class))
						.list();
			}*/
			if(accommodationId>0){
				revenues=(List<Revenue>)session.createQuery("select sum(amount) as amount, count(bookingDate) as nights,"
						+ " propertyAccommodation.accommodationId as accommodationId from BookingDetail"
						+ " where propertyAccommodation.accommodationId=:accommodationId and bookingDate between :fromDate and :toDate group by propertyAccommodation.accommodationId")
						.setParameter("accommodationId", accommodationId)
						.setParameter("fromDate", fromDate)
						.setParameter("toDate", toDate)
						.setResultTransformer(Transformers.aliasToBean(Revenue.class))
						.list();
			}else{
				revenues=(List<Revenue>)session.createQuery("select sum(totalAmount) as amount, count(bookingId) as nights , 0 as accommodationId from PmsBooking "
						+ " where pmsProperty.propertyId=:propertyId and arrivalDate>=:fromDate and departureDate<=:toDate ")
						.setParameter("propertyId", propertyId)
						.setParameter("fromDate", fromDate)
						.setParameter("toDate", toDate)
						.setResultTransformer(Transformers.aliasToBean(Revenue.class))
						.list();
			}
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return revenues;
	}
	@SuppressWarnings("unchecked")
	public List<BookingGuestDetail> BookingGuestDetails(int bookingId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingGuestDetail> bookingGuestDetails = null;
		try {
			bookingGuestDetails = (List<BookingGuestDetail>)session.createQuery("select a from BookingGuestDetail a join a.pmsBooking b where a.isPrimary=true and b.bookingId=:bookingId")
					.setParameter("bookingId", bookingId)
					.list();
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBookingDetail(int propertyId,Date start,Date end,int accommodationId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,pb.pmsProperty.propertyId as propertyId,"
					+ " pb.rooms as rooms, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pa.accommodationType as accommodationType,bd.pmsPromotions.promotionId as promotionId,bd.pmsSmartPrice.smartPriceId as smartPriceId,"
					+ "bd.propertyDiscount.propertyDiscountId as propertyDiscountId,pg.firstName as firstName,"
					+ "pg.lastName as lastName,bd.bookingDate as bookingDate,bd.amount as amount,bd.tax as taxAmount,pg.city as city,"
					+ "pg.phone as mobileNumber,pg.landline as phoneNumber,ps.sourceName as sourceName,pg.emailId as emailId,pp.promotionType as promotionType "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg,PmsPromotions pp,"
					+ "PmsSmartPrice psp,PropertyDiscount pd,PropertyAccommodation pa,PmsSource ps  "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and bd.propertyAccommodation.accommodationId = pa.accommodationId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId and pb.bookingId=bgd.pmsBooking.bookingId and ps.sourceId=pb.pmsSource.sourceId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and pp.promotionId=bd.pmsPromotions and psp.smartPriceId=bd.pmsSmartPrice.smartPriceId and "
					+ " bd.propertyDiscount.propertyDiscountId=pd.propertyDiscountId ";
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId "
								+ "and bd.bookingDate between :arrivalDate and :departureDate ";
					}else{
						strBookingQuery+=" and pb.arrivalDate>=:arrivalDate and pb.departureDate<=:departureDate ";
					}
					strBookingQuery+= " group by pb.bookingId,pb.pmsProperty.propertyId, pb.rooms, pb.arrivalDate,pb.departureDate,"
					+ "pa.accommodationType,bd.pmsPromotions.promotionId,bd.pmsSmartPrice.smartPriceId,bd.propertyDiscount.propertyDiscountId,"
					+ "pg.firstName,pg.lastName,bd.bookingDate,bd.amount,bd.tax,pg.city,pg.phone,pg.landline,ps.sourceName,pg.emailId,pp.promotionType  "
					+ " order by pb.bookingId DESC";
					
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
			//System.out.println(bookingDetails);
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserGuestDetailReport> UserDetails() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserGuestDetailReport> userDetails = null;
		try {
			userDetails = (List<UserGuestDetailReport>)session.createQuery("select distinct(emailId) as emailId, userName as userName,"
					+ "phone as phoneNumber,address1 as address1,address2 as address2"
					+ " from User where role.roleId = '4' group by userId")
					.setResultTransformer(Transformers.aliasToBean(UserGuestDetailReport.class)).list();
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserGuestDetailReport> GuestDetails() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserGuestDetailReport> guestDetails = null;
		try {
			guestDetails = (List<UserGuestDetailReport>)session.createQuery("select distinct(emailId) as emailId, firstName as userName,"
					+ "phone as phoneNumber,address1 as address1,address2 as address2 "
					+ " from PmsGuest group by guestId ")
					.setResultTransformer(Transformers.aliasToBean(UserGuestDetailReport.class)).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return guestDetails;
	}
}
