package com.ulopms.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import com.ulopms.model.BookingDetail;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.util.HibernateUtil;


public class PropertyRateManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyRateManager.class);

	public PropertyRate add(PropertyRate propertyRate) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyRate);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRate;
	}

	public PropertyRate edit(PropertyRate propertyRate) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyRate);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRate;
	}

	public PropertyRate delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyRate propertyRate = (PropertyRate) session.load(PropertyRate.class, id);
		if (null != propertyRate) {
			session.delete(propertyRate);
		}
		session.getTransaction().commit();
		return propertyRate;
	}

	public PropertyRate find(int id) {
		// System.out.println(id);
		PropertyRate propertyRate = new PropertyRate();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyRate = (PropertyRate) session.load(PropertyRate.class, id);
			// session.getTransaction().commit();
			// System.out.println(PropertyRate.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyRate;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRate> list(int propertyId) {
        System.out.println(propertyId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyRate> propertyRate = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyRate = (List<PropertyRate>) session.createQuery("from PropertyRate where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false'").setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRate;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> list1(int propertyRateId) {
        System.out.println(propertyRateId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyRate> propertyRate = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyRate = (List<PropertyRate>) session.createQuery("from PropertyRate where propertyRateId=:propertyRateId").setParameter("propertyRateId", propertyRateId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRate;
	}
	
	@SuppressWarnings("unchecked")
	   public List<PropertyRate> list1(int accommodationId,int sourceId,Date date) {
	      System.out.println("test"+accommodationId);
	      System.out.println("test"+date);
	      System.out.println("test"+sourceId);
	       Session session = HibernateUtil.getSessionFactory().openSession();
	       session.beginTransaction();
	       List<PropertyRate> propertyRate = null;
	       try {
	           
	       //propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
	           propertyRate = (List<PropertyRate>) session.createQuery("from PropertyRate where propertyAccommodation.accommodationId=:accommodationId and pmsSource.sourceId=:sourceId and  (:date between startDate and endDate) and isActive = 'true' and isDeleted = 'false'")
	                 
	                   .setParameter("accommodationId", accommodationId)
	                   .setParameter("sourceId", sourceId)
	                    .setParameter("date", date)
	                    .setMaxResults(1)
	                     .list();        } catch (HibernateException e) {
	           logger.error(e);
	           session.getTransaction().rollback();
	       } finally {
	           session.getTransaction().commit();
	           session = null;
	       }
	       return propertyRate;
	   }
	
	@SuppressWarnings("unchecked")
    public List<PropertyRate> list(int propertyId,int accommodationId) {
       System.out.println("test"+accommodationId);
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<PropertyRate> propertyRate = null;
        try {
             
            //propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
            propertyRate = (List<PropertyRate>) session.createQuery("from PropertyRate where pmsProperty.propertyId=:propertyId and"
            		+ " propertyAccommodation.accommodationId=:accommodationId and isActive = 'true' and isDeleted = 'false'")
                    .setParameter("propertyId", propertyId)
                    .setParameter("accommodationId", accommodationId).list();

        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return propertyRate;
    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> list(int accommodationId, int sourceId,Date date) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRate> propertyRates = null;
		try {
            // select a from PropertyRate a join a.propertyAccommodation b join a.pmsSource c where b.accommodationId=:accommodationId and c.sourceId=:sourceId and :date between a.startDate and a.endDate order by a.orderBy asc
			propertyRates = (List<PropertyRate>) session.createQuery("select a from PropertyRate a join a.propertyAccommodation b join a.pmsSource c "
					+ "where b.accommodationId=:accommodationId and c.sourceId=:sourceId and :date between a.startDate and a.endDate"
					+ " and a.isActive = true order by a.propertyRateId DESC")
					.setParameter("accommodationId", accommodationId)
					.setParameter("sourceId", sourceId)
					.setParameter("date", date)
					.setMaxResults(1)
					.list();
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRates;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRate> list(int propertyId,int accommodationId,int sourceId,Date start,Date end) {
	       System.out.println("test"+accommodationId);
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        List<PropertyRate> propertyDate = null;
	        try {
	             
	        	propertyDate=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.propertyAccommodation.accommodationId=:accommodationId "
		        			+ " and pmsSource.sourceId=:sourceId and propertyRate.startDate>=:startDate and propertyRate.endDate<=:endDate "
		        			+ " and propertyRate.isActive = 'true' and propertyRate.isDeleted = 'false' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("accommodationId", accommodationId)
		        			.setParameter("sourceId", sourceId)
		        			.setParameter("startDate", start)
		        			.setParameter("endDate", end).list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertyDate;
	    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodation> list(int propertyId, String datesa) throws ParseException {
		System.out.println(datesa);
		System.out.println(propertyId);
		Date dates=new SimpleDateFormat("yyyy-MM-dd").parse(datesa); 
		//System.out.println(dates);
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodation> accommodationRates = null;
		try {

			accommodationRates = (List<PropertyAccommodation>) session.createQuery("select bd.bookingDate from BookingDetail bd,PropertyAccommodation pa "
					+ "where bd.bookingDate=:dates and pa.pmsProperty.propertyId=:propertyId and bd.propertyAccommodation.accommodationId = pa.accommodationId "
					+ "group by bd.bookingDate,bd.bookingDetailsId")
					.setParameter("propertyId", propertyId)
					.setParameter("dates", dates)
					.list();
			//System.out.println(bookingDetails);
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationRates;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listDateDetails(int propertyId,int accommodationId,Date checkedDate) {
	       System.out.println("test"+accommodationId);
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        List<PropertyRate> propertyDate = null;
	        try {
	             
	        	propertyDate=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.propertyAccommodation.accommodationId=:accommodationId "
		        			+ " and :date between propertyRate.startDate and propertyRate.endDate "
		        			+ " and propertyRate.isActive = 'true' and propertyRate.isDeleted = 'false' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("accommodationId", accommodationId)
		        			.setParameter("date", checkedDate)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertyDate;
	    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listSmartPriceCheckDetails(int propertyId,int accommodationId,Date checkedDate) {
	       System.out.println("test"+accommodationId);
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.propertyAccommodation.accommodationId=:accommodationId "
		        			+ " and :date between propertyRate.startDate and propertyRate.endDate and propertyRate.isActive = 'true' and "
		        			+ " propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("accommodationId", accommodationId)
		        			.setParameter("date", checkedDate)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertySmartPriceCheck;
	    }
	
	
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listSmartPrices(int propertyId,Date curdate) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.isActive = 'true' and "
		        			+ " propertyRate.startDate>=:startDate and propertyRate.endDate>=:endDate and "
		        			+ " propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("startDate", curdate)
		        			.setParameter("endDate", curdate)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertySmartPriceCheck;
	    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listPropertyRates(int propertyId,Date curdate) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.isActive = 'true' "
		        			+ " and propertyRate.startDate>=:startDate and propertyRate.endDate>=:endDate and "
		        			+ " propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='false' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("startDate", curdate)
		        			.setParameter("endDate", curdate)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertySmartPriceCheck;
	    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listActiveSmartPricesCheck(int propertyRateId) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.propertyRateId=:propertyRateId and propertyRate.isActive = 'true' and "
		        			+ " propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyRateId", propertyRateId)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertySmartPriceCheck;
	    }
	
}
