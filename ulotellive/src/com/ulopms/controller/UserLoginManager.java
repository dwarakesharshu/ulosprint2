package com.ulopms.controller;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.User;
import com.ulopms.model.UserClientToken;
import com.ulopms.util.HibernateUtil;


public class UserLoginManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(UserLoginManager.class);

	public User add(User user) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(user);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}

	public UserClientToken add(UserClientToken userClientToken) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(userClientToken);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userClientToken;
	}

	public User edit(User user) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(user);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}

	public User delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		User user = (User) session.load(User.class, id);
		if (null != user) {
			session.delete(user);
		}
		session.getTransaction().commit();
		return user;
	}

	public User findUser(int id) {
		// System.out.println(id);
		User user = new User();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			user = (User) session.get(User.class, id);
			// session.getTransaction().commit();
			// System.out.println(user.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		// session.close();
		return user;
	}

	public User find(int id) {
		
		User user = new User();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			user = (User) session.load(User.class, id);
			// session.getTransaction().commit();
			// System.out.println(user.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		// session.close();
		return user;
	}
	@SuppressWarnings("unchecked")
	public List<User> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("from User").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	@SuppressWarnings("unchecked")
	public List<User> list(String roleName) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("select a from User a join a.role b where b.roleId=:roleId")
					.setParameter("roleName", roleName)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> list(short roleId, int companyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("select a from User a join a.role b join a.company c where b.roleId=:roleId and c.companyId =:companyId")
					.setParameter("roleId", roleId)
					.setParameter("companyId",companyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	
	

	@SuppressWarnings("unchecked")
	public List<User> listAll() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("from User").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> listAll(int companyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("from User where company.companyId=:companyId order by createdOn desc")
					.setParameter("companyId", companyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}

	
	@SuppressWarnings("unchecked")
	public List<User> verifyUser(String loginName) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> user = null;
		try {
			user = session
					.createQuery(
							"from User where emailId=:loginName ")
							.setParameter("loginName", loginName).list();
							//.setParameter("password", password).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<User> verifyLogin(String loginName, String password) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> user = null;
		try {
			user = session
					.createQuery(
							"from User where emailId=:loginName and hashpassword=:password and isActive=true")
							.setParameter("loginName", loginName)
							.setParameter("password", password).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<User> findUserByEmail(String email) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> user = null;
		try {
			user = session.createQuery("from User where emailId=:email")
					.setParameter("email", email).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}

}
