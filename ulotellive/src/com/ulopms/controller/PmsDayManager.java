package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsDays;
import com.ulopms.util.HibernateUtil;


public class PmsDayManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsDayManager.class);

	

	/*public PmsAmenity edit(PmsAmenity pmsAmenity) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsAmenity);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsAmenity;
	}

	public PmsAmenity delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsAmenity pmsAmenity = (PmsAmenity) session.load(PmsAmenity.class, id);
		if (null != pmsAmenity) {
			session.delete(pmsAmenity);
		}
		session.getTransaction().commit();
		return pmsAmenity;
	}
*/
	public PmsDays find(int id) {
		// System.out.println(id);
		PmsDays pmsDays = new PmsDays();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsDays = (PmsDays) session.get(PmsDays.class, id);
			// session.getTransaction().commit();
			// System.out.println(PmsAmenity.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsDays;
	}
	
	public PmsAmenity find(String id) {
		// System.out.println(id);
		PmsAmenity pmsAmenity = new PmsAmenity();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsAmenity = (PmsAmenity) session.load(PmsAmenity.class, id);
			// session.getTransaction().commit();
			// System.out.println(PmsAmenity.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsAmenity;
	}

	@SuppressWarnings("unchecked")
	public List<PmsDays> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsDays> pmsdays = null;
		try {

			pmsdays = (List<PmsDays>) session.createQuery("from PmsDays where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsdays;
	}

	
}
