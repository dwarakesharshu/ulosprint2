package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.PmsBooking;
import com.ulopms.util.HibernateUtil;


public class BookingGuestDetailManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(BookingGuestDetailManager.class);

	public BookingGuestDetail add(BookingGuestDetail bookingGuestDetail) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(bookingGuestDetail);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetail;
	}

	public BookingGuestDetail edit(BookingGuestDetail bookingGuestDetail) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(bookingGuestDetail);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetail;
	}

	public BookingGuestDetail delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		BookingGuestDetail bookingGuestDetail = (BookingGuestDetail) session.load(BookingGuestDetail.class, id);
		if (null != bookingGuestDetail) {
			session.delete(bookingGuestDetail);
		}
		session.getTransaction().commit();
		return bookingGuestDetail;
	}

	public BookingGuestDetail find(int id) {
		// System.out.println(id);
		BookingGuestDetail bookingGuestDetail = new BookingGuestDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			bookingGuestDetail = (BookingGuestDetail) session.load(BookingGuestDetail.class, id);
			// session.getTransaction().commit();
			// System.out.println(BookingGuestDetail.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return bookingGuestDetail;
	}

	@SuppressWarnings("unchecked")
	public List<BookingGuestDetail> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingGuestDetail> bookingGuestDetails = null;
		try {

			bookingGuestDetails = (List<BookingGuestDetail>) session.createQuery("from BookingGuestDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingGuestDetail> userBookingList(String emailId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingGuestDetail> bookingGuestDetails = null;
		try {
			bookingGuestDetails = (List<BookingGuestDetail>) session.createQuery("select bg.pmsBooking.bookingId as bookId from BookingGuestDetail bg, PmsGuest pg where bg.pmsGuest.emailId = :emailId and pg.guestId = bg.pmsGuest.guestId").setParameter("emailId", emailId).list();
			//bookingGuestDetails = (List<BookingGuestDetail>) session.createQuery("from BookingGuestDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetails;
	}

	
}
