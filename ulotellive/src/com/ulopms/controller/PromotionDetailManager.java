package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.util.HibernateUtil;


public class PromotionDetailManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PromotionDetailManager.class);

	public PmsPromotionDetails add(PmsPromotionDetails pmsPromotionDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsPromotionDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPromotionDetails;
	}

	public PmsPromotionDetails edit(PmsPromotionDetails pmsPromotionDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsPromotionDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPromotionDetails;
	}

	public PmsPromotionDetails delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsPromotionDetails pmsPromotionDetails = (PmsPromotionDetails) session.load(PmsPromotionDetails.class, id);
		if (null != pmsPromotionDetails) {
			session.delete(pmsPromotionDetails);
		}
		session.getTransaction().commit();
		return pmsPromotionDetails;
	}

	public PmsPromotionDetails find(Long id) {
		// System.out.println(id);
		PmsPromotionDetails pmsPromotionDetails = new PmsPromotionDetails();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsPromotionDetails = (PmsPromotionDetails) session.load(PmsPromotionDetails.class, id);
			// session.getTransaction().commit();
			// System.out.println(PropertyRateDetail.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsPromotionDetails;
	}
	
	
	public PmsPromotionDetails find(int promotionDetailId) {
		//System.out.println(propertyRateDetailsId);
		PmsPromotionDetails propertyRateDetail = new PmsPromotionDetails();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyRateDetail = (PmsPromotionDetails) session.load(PmsPromotionDetails.class, promotionDetailId);
			// session.getTransaction().commit();
			// System.out.println(PropertyRateDetail.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyRateDetail;
	}
	
	public PmsPromotionDetails list(int promotionsId) {
		System.out.println(promotionsId);
		PmsPromotionDetails promotionDetails = new PmsPromotionDetails();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			promotionDetails = (PmsPromotionDetails) session.createQuery(" from PmsPromotionDetails pr  where pr.pmsPromotions.promotionId=:promotionId "
					+ "and isActive = 'true' and isDeleted = 'false'").setParameter("promotionId", promotionsId)
			.setFirstResult(0)
			.setMaxResults(1)
			.uniqueResult();
			// session.getTransaction().commit();
			// System.out.println(PropertyRateDetail.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return promotionDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotionDetails> listPromotionDetails(int promotionId, String daysOfWeek) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotionDetails> promotionsDetails = null;
		try {

			promotionsDetails = (List<PmsPromotionDetails>) session.createQuery("from PmsPromotionDetails where daysOfWeek=:daysOfWeek and"
					+ " pmsPromotions.promotionId=:promotionId and  isActive = 'true' and isDeleted = 'false'")
					.setParameter("daysOfWeek", daysOfWeek)
					.setParameter("promotionId", promotionId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionsDetails;
	}
}
