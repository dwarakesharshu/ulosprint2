package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.Screen;
import com.ulopms.util.HibernateUtil;


public class ScreenManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(ScreenManager.class);

	public Screen add(Screen screen) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(screen);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return screen;
	}

	public Screen edit(Screen screen) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(screen);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return screen;
	}

	public Screen delete(short id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Screen screen = (Screen) session.load(Screen.class, id);
		if (null != screen) {
			session.delete(screen);
		}
		session.getTransaction().commit();
		return screen;
	}

	public Screen find(short id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Screen screen = (Screen) session.load(Screen.class, id);
		// session.getTransaction().commit();
		// session.close();
		return screen;
	}

	@SuppressWarnings("unchecked")
	public List<Screen> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Screen> screens = null;
		try {

			screens = (List<Screen>) session.createQuery("from Screen").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return screens;
	}

	@SuppressWarnings("unchecked")
	public List<Screen> UserScreenList(Long roleId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Screen> screens = null;
		try {
			String sql = "select sc.screen_id,sc.screen_name,sc.screen_purpose,sc.is_active,sc.screen_path from mast_screen sc where sc.screen_id not in(select screen_id from conf_role_screen where role_id="
					+ roleId + ")";
			// screens =
			// (List<Screen>)session.createQuery("select sc.screenId,sc.screenPurpose,sc.screen from Screen sc left join e rs.role.roleId=:roleId ").setParameter("roleId",
			// "roleId").list();
			screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return screens;
	}
}
