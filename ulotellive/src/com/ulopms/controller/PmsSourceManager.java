package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsSource;
import com.ulopms.util.HibernateUtil;


public class PmsSourceManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsSourceManager.class);

	public PmsSource add(PmsSource PmsSource) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(PmsSource);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSource;
	}

	public PmsSource edit(PmsSource PmsSource) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(PmsSource);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSource;
	}

	public PmsSource delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsSource PmsSource = (PmsSource) session.load(PmsSource.class, id);
		if (null != PmsSource) {
			session.delete(PmsSource);
		}
		session.getTransaction().commit();
		return PmsSource;
	}

	public PmsSource find(int id) {
		// System.out.println(id);
		PmsSource PmsSource = new PmsSource();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			PmsSource = (PmsSource) session.get(PmsSource.class, id);
			// session.getTransaction().commit();
			// System.out.println(PmsSource.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return PmsSource;
	}

	@SuppressWarnings("unchecked")
	public List<PmsSource> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsSource> PmsSources = null;
		try {

			PmsSources = (List<PmsSource>) session.createQuery("from PmsSource").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSources;
	}

	
}
