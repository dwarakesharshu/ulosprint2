package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.Location;
import com.ulopms.util.HibernateUtil;


public class LocationManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(LocationManager.class);

	public Location add(Location location) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(location);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return location;
	}

	public Location edit(Location location) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(location);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return location;
	}

	public Location delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Location location = (Location) session.load(Location.class, id);
		if (null != location) {
			session.delete(location);
		}
		session.getTransaction().commit();
		return location;
	}

	public Location find(int id) {
		// System.out.println(id);
		Location location = new Location();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			location = (Location) session.load(Location.class, id);
			// session.getTransaction().commit();
			// System.out.println(Location.getLoginName());
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return location;
	}

	@SuppressWarnings("unchecked")
	public List<Location> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Location> locations = null;
		try {

			locations = (List<Location>) session.createQuery("from Location where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locations;
	}

	
}
