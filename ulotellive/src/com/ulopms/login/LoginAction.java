package com.ulopms.login;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.catalina.startup.UserConfig;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;  

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;  

//import com.medwecare.controller.AccessRightsManager;
//import com.medwecare.controller.AwsAPIManager;
//import com.medwecare.controller.CompanyManager;
//import com.medwecare.controller.RoleManager;

//import com.medwecare.controller.UserRightsManager;







import org.hibernate.Hibernate;

import antlr.StringUtils;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.AccessRightManager;
import com.ulopms.controller.RoleAccessRightManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.model.AccessRight;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.Role;
import com.ulopms.model.RoleAccessRight;
import com.ulopms.model.User;
import com.ulopms.model.UserClientToken;
import com.ulopms.security.passwordDecrypt;

public class LoginAction extends ActionSupport implements SessionAware, ModelDriven<User> {
	private String username;
	private String password;
    private UserLoginManager linkController = new UserLoginManager();
	private List<User> get_users;
	private List<User> users;
	private List<RoleAccessRight> roleAccessList;
	private User user;
	private HttpSession session;
	
	
	
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	@Override
    public User getModel() {
        return user;
    }
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	public String execute() {

		String ret = "error";
		try
		{
			int count=0;
			if(ActionContext.getContext().getSession().get("countcap")!=null)
			{
				if(ActionContext.getContext().getSession().get("countcap")!="0")
				{
					count=(int) ActionContext.getContext().getSession().get("countcap");
					System.out.println("COUNT"+count);
				}
				
			}
			count=Integer.parseInt(String.valueOf((sessionMap.get("countcap")!=null?sessionMap.get("countcap"):"0")));
			System.out.println("countcat"+count);   
			RoleManager roleController = new RoleManager();
			AccessRightManager accessRightController = new AccessRightManager();
			get_users=linkController.verifyUser(username);
			if(get_users.size()>0)
			{
				System.out.println("step1");
				String pass=passwordDecrypt.getDigestvalid(password, get_users.get(0).getSaltkey());
				 
				users =  linkController.verifyLogin(username, pass);
				
				if(users.size()>0)
				{
					 count=0;
					user = users.get(0);
					User user1 =linkController.findUser(user.getUserId());
					String userPermissions ="";
					/*List<UserAccessRights> userAccessRightsList =  userRightsController.list(user.getUserId());
					for(UserAccessRights userAccessRights : userAccessRightsList)
					{
						AccessRights accessRights =  accessRightsController.find(userAccessRights.getAccessRights().getAccessRightsId());
						if(!userPermissions.equalsIgnoreCase(""))
							userPermissions += "*" +accessRights.getAccessRights();
						else
							userPermissions = accessRights.getAccessRights();
					}
					 System.out.println(userPermissions);
					
					
					*/
					//short r = 4;
					Role role =  roleController.find(user1.getRole().getRoleId());
					sessionMap.put("loginName",user.getUserName());
					sessionMap.put("userId",user.getUserId());
					sessionMap.put("emailId",user.getEmailId());
					sessionMap.put("role",role.getRoleName());
					sessionMap.put("userName",user.getUserName());
					sessionMap.put("userStatus","1");
					sessionMap.put("USER", user);
					sessionMap.put("countcap", count);
					//role id based access rights with true, comma separated
					RoleAccessRightManager roleAccessController = new RoleAccessRightManager();
					
					this.roleAccessList = roleAccessController.list(user1.getRole().getRoleId());
					//System.out.println(bookingList);
					StringBuilder displayNames = new StringBuilder();
					for (RoleAccessRight roleAccessRight : roleAccessList) {
					//Hibernate.initialize(roleAccessRight.getAccessRight());
					//System.out.println("roleaccessid" +roleAccessRight.getAccessRight());
					AccessRight accessRight = accessRightController.find(roleAccessRight.getAccessRight().getAccessRightsId());
					accessRight.getDisplayName();
					System.out.println("access Names" + accessRight.getDisplayName());
					
					displayNames.append(accessRight.getDisplayName());
					displayNames.append(",");
					
					}
					
					sessionMap.put("userRights", displayNames);
					//System.out.println("comma sep access Names" + displayNames);
					
					ret= "success";
				} else {
				
					addActionError(getText("error.login"));
					ActionContext.getContext().getSession().put("countcap", ++count);
					ret= "error";
				}
			}else {
			
				addActionError(getText("error.login"));
				ActionContext.getContext().getSession().put("countcap", ++count);
				ret= "error";
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return ret;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
    public List<RoleAccessRight> getRoleAccessList() {
		return roleAccessList;
	}

	public void setRoleAccessList(List<RoleAccessRight> roleAccessList) {
		this.roleAccessList = roleAccessList;
	}
	
	
	public String logout()
	{
		System.out.println("session logout");
		if(sessionMap!=null) sessionMap.invalidate();
		if(session!=null) session.invalidate();
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",0);
		return "success";
	}
}
