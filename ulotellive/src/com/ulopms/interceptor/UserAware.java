package com.ulopms.interceptor;

import com.ulopms.model.User;


	public interface UserAware {
		 
	    public void setUser(User user);
	}

