package com.ulopms.interceptor;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;

import java.util.Map;  

import com.opensymphony.xwork2.interceptor.Interceptor;
import com.ulopms.model.User;

public class MyLoggingInterceptor implements Interceptor{

	private static final long serialVersionUID = 1L;

	public String intercept(ActionInvocation invocation) throws Exception {

		
		 Map<String, Object> sessionAttributes = invocation.getInvocationContext().getSession();
         
	        User user = (User) sessionAttributes.get("USER");
	         
	        if(user == null){
	        	//System.out.println("user failed");
	            return Action.LOGIN;
	        }else{
	        	//System.out.println("user passed");
	            Action action = (Action) invocation.getAction();
	            if(action instanceof UserAware){
	                ((UserAware) action).setUser(user);
	            }
	            return invocation.invoke();
	        }
	}

	public void destroy() {
		System.out.println("Destroying MyLoggingInterceptor...");
	}

	public void init() {
		System.out.println("Initializing MyLoggingInterceptor...");
	}

}
