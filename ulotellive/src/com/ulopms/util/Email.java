package com.ulopms.util;

import com.sendgrid.*;
import java.io.*;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;




public class Email {
	
	private String _to;
	private String _from;
	private String _username;
	private String _password;
	private String _host;
	private String _port;
	private String _subject;
	private String _body;
	private Writer _bodyContent;
	private String _cc;
	private String _cc2;
	
	
	private ArrayList<String> _bcc;
	
	
	public ArrayList<String> get_bcc() {
		return _bcc;
	}
	public void set_bcc(ArrayList<String> _bcc) {
		this._bcc = _bcc;
	}
	
	
	
	public void send()
	{
		 try {
		      SendGrid sg = new SendGrid("SG.jdGJTlO0TXS3eYf-m8VnmQ.NHcjPb73VZNTs3s3P3cjdsGUShjSWjTWoQGF_Ic8lSc");
		      com.sendgrid.Email from = new com.sendgrid.Email(_from);
		      String subject = _subject;
		      //System.out.println("to address" + _to);
		      //System.out.println("cc address" +_cc);
		      
		      Personalization personalization = new Personalization();
		      
		      com.sendgrid.Email to = new com.sendgrid.Email(_to);
		      personalization.addTo(to);
		      
		      if(get_cc() != null){
		      
		    	  com.sendgrid.Email cc = new com.sendgrid.Email(_cc);
		    	  com.sendgrid.Email cc2 = new com.sendgrid.Email(_cc2);
			      personalization.addCc(cc);
			      personalization.addCc(cc2);

		    
		      }
		      
		      Content content = new Content("text/html", _bodyContent.toString());
		      Mail mail = new Mail(from, subject, to, content);
		      //Mail mail = new Mail();
		      
		      //mail.addContent(content);
		      //mail.from(from);
		      
		      mail.addPersonalization(personalization);
		      
		      Request request = new Request();
		      request.method = Method.POST;
		      request.endpoint = "mail/send";
		      request.body = mail.build();
		      //System.out.println("{\"personalizations\":[{\"to\":[{\"email\":\""+ _to +"\"}],\"subject\":\""+_subject+"\"}],\"from\":{\"email\":\""+_from+"\"},\"content\":[{\"type\":\"text/html\",\"value\": \""+_bodyContent.toString()+"\"}]}");
		      //request.body = "{\"personalizations\":[{\"to\":[{\"email\":\""+ _to +"\"}],\"subject\":\""+_subject+"\"}],\"from\":{\"email\":\""+_from+"\"},\"content\":[{\"type\":\"text/html\",\"value\": \""+_bodyContent.toString()+"\"}]}";
		      Response response = sg.api(request);
		      System.out.println(response.statusCode);
		      System.out.println(response.body);
		      System.out.println(response.headers);
		      System.out.println(request.body);
		    } catch (IOException ex) {
		      //throw ex;
		    	ex.printStackTrace();
		    }
	}
	
	public String get_to() {
		return _to;
	}

	public void set_to(String _to) {
		this._to = _to;
	}

	public String get_from() {
		return _from;
	}

	public void set_from(String _from) {
		this._from = _from;
	}

	public String get_username() {
		return _username;
	}

	public void set_username(String _username) {
		this._username = _username;
	}

	public String get_password() {
		return _password;
	}

	public void set_password(String _password) {
		this._password = _password;
	}

	public String get_host() {
		return _host;
	}

	public void set_host(String _host) {
		this._host = _host;
	}

	public String get_port() {
		return _port;
	}

	public void set_port(String _port) {
		this._port = _port;
	}

	public String get_subject() {
		return _subject;
	}

	public void set_subject(String _subject) {
		this._subject = _subject;
	}

	public String get_body() {
		return _body;
	}

	public void set_body(String _body) {
		this._body = _body;
	}
	
	public Writer get_bodyContent() {
		return _bodyContent;
	}
	public void set_bodyContent(Writer _bodyContent) {
		this._bodyContent = _bodyContent;
	}
	public String get_cc() {
		return _cc;
	}
	public void set_cc(String _cc) {
		this._cc = _cc;
	}
	
	public String get_cc2() {
		return _cc2;
	}
	public void set_cc2(String _cc2) {
		this._cc2 = _cc2;
	}
}
