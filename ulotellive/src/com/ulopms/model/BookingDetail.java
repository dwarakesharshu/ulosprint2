package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the booking_details database table.
 * 
 */
@Entity
@Table(name="booking_details")
@NamedQuery(name="BookingDetail.findAll", query="SELECT b FROM BookingDetail b")
public class BookingDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="booking_details_booking_details_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="booking_details_id", unique=true, nullable=false)
	private Integer bookingDetailsId;

	@Column(name="adult_count")
	private Integer adultCount;

	private double amount;

	@Column(name="child_count")
	private Integer childCount;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="booking_date")
	private Date bookingDate;
    
	@Column(name="random_no")
	private double randomNo;
	
	@Column(name="infant_count")
	private Integer infantCount;
	
	@Column(name="room_count")
	private Integer roomCount;

	private double refund;

	private double tax;
    
	
	
	//bi-directional many-to-one association to PmsStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="status_id")
	private PmsStatus pmsStatus;

	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="accommodation_id")
	private PropertyAccommodation propertyAccommodation;
	
    //bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="room_id")
	private PropertyAccommodationRoom propertyAccommodationRoom;
		
	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="booking_id")
	private PmsBooking pmsBooking;		
		

	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="property_discount_id")
	private PropertyDiscount propertyDiscount;
	
	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="smart_price_id")
	private PmsSmartPrice pmsSmartPrice;
		
		
	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="promotion_id")
	private PmsPromotions pmsPromotions;	

	public BookingDetail() {
	}

	public Integer getBookingDetailsId() {
		return this.bookingDetailsId;
	}

	public void setBookingDetailsId(Integer bookingDetailsId) {
		this.bookingDetailsId = bookingDetailsId;
	}

	public Integer getAdultCount() {
		return this.adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}
	
	
	public Integer getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(Integer roomCount) {
		this.roomCount = roomCount;
	}

	public Integer getChildCount() {
		return this.childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Date getBookingDate() {
		return this.bookingDate;
	}
	
	public double getRefund() {
		return this.refund;
	}

	public void setRefund(double refund) {
		this.refund = refund;
	}

	public double getTax() {
		return this.tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}
	
	public void setRandomNo(double randomNo) {
		this.randomNo = randomNo;
	}
	
	public double getRandomNo() {
		return this.randomNo;
	}

	public PmsStatus getPmsStatus() {
		return this.pmsStatus;
	}

	public void setPmsStatus(PmsStatus pmsStatus) {
		this.pmsStatus = pmsStatus;
	}

	public PropertyAccommodation getPropertyAccommodation() {
		return this.propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}
	
	public PropertyAccommodationRoom getPropertyAccommodationRoom() {
		return this.propertyAccommodationRoom;
	}

	public void setPropertyAccommodationRoom(PropertyAccommodationRoom propertyAccommodationRoom) {
		this.propertyAccommodationRoom = propertyAccommodationRoom;
	}
	
	public PmsBooking getPmsBooking() {
		return this.pmsBooking;
	}

	public void setPmsBooking(PmsBooking pmsBooking) {
		this.pmsBooking = pmsBooking;
	}
	
	public PropertyDiscount getPropertyDiscount() {
		return propertyDiscount;
	}

	public void setPropertyDiscount(PropertyDiscount propertyDiscount) {
		this.propertyDiscount = propertyDiscount;
	}
	
	public PmsSmartPrice getPmsSmartPrice(){
		return pmsSmartPrice;
	}
	
	public void setPmsSmartPrice(PmsSmartPrice pmsSmartPrice){
		this.pmsSmartPrice=pmsSmartPrice; 
	}
	
	public PmsPromotions getPmsPromotions(){
		return pmsPromotions;
	}
	
	public void setPmsPromotions(PmsPromotions pmsPromotions){
		this.pmsPromotions=pmsPromotions;
	}
}