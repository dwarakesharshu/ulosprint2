package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the access_rights database table.
 * 
 */
@Entity
public class RevenueReport implements Serializable {
	//ivate static final long serialVersionUID = 1L;

	
	private double amount;

	
	private Integer nights;


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public Integer getNights() {
		return nights;
	}


	public void setNights(Integer nights) {
		this.nights = nights;
	}

	

}