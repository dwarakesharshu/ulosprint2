package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the location_photo database table.
 * 
 */
@Entity
@Table(name="location_photo")
@NamedQuery(name="LocationPhoto.findAll", query="SELECT l FROM LocationPhoto l")
public class LocationPhoto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="location_photo_location_photo_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="location_photo_id", unique=true, nullable=false)
	private Integer locationPhotoId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="is_primary")
	private Boolean isPrimary;

	@Column(name="modified_by")
	private Timestamp modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="photo_path", length=100)
	private String photoPath;

	//bi-directional many-to-one association to Location
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="location_id")
	private Location location;

	public LocationPhoto() {
	}

	public Integer getLocationPhotoId() {
		return this.locationPhotoId;
	}

	public void setLocationPhotoId(Integer locationPhotoId) {
		this.locationPhotoId = locationPhotoId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Boolean getIsPrimary() {
		return this.isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public Timestamp getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Timestamp modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPhotoPath() {
		return this.photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

}