package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_booking database table.
 * 
 */

public class PmsBookedDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	
	
	
	private Integer propertyId;
	
	private Long roomCount;
	
	private Integer guestId;
	
	private Integer bookingId;
	
	private Integer statusId;
	
	private Integer accommodationId;
    
	private String accommodationType;
	
	private Integer maxOccupancy;
	
	private double adultsIncludedRate;
	
	private double childIncludedRate;
	
	private long adults;

	private double amount;

    private long child;
	
	private double extraAdult;
	
	private double extraChild;
	
	private double baseAmount;
	
	private Integer noOfUnits;
	
	private Long available;
	
	private Timestamp arrivalDate;

	private Timestamp departureDate;

	private Integer rooms;
	
	private double refund;

	private double tax;
	
	private Boolean isActive;

	private Boolean isDeleted;


	

	//
	
	public Integer getPropertyId() {
		return this.propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getGuestId() {
		return this.guestId;
	}

	public void setGuestId(Integer guestId) {
		this.guestId = guestId;
	}
	
	public Long getRoomCount() {
		return this.roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public String getAccommodationType() {
			return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
			this.accommodationType = accommodationType;
	}
	
	public Integer getMaxOccupancy() {
		return this.maxOccupancy;
    }

   public void setMaxOccupancy(Integer maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
    }
   
   public double getAdultsIncludedRate() {
		return this.adultsIncludedRate;
    }

   public void setAdultsIncludedRate(double adultsIncludedRate) {
		this.adultsIncludedRate = adultsIncludedRate;
    }
   
   public double getChildIncludedRate() {
		return this.childIncludedRate;
   }

   public void setChildIncludedRate(double childIncludedRate) {
		this.childIncludedRate = childIncludedRate;
   }
   
   public long getAdults() {
		return this.adults;
	}

	public void setAdults(long adults) {
		this.adults = adults;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public long getChild() {
		return this.child;
	}

	public void setChild(long child) {
		this.child = child;
	}
	
	
   public double getExtraChild() {
		return this.extraChild;
   }

   public void setExtraChild(double extraChild) {
		this.extraChild = extraChild;
   }
   
   public double getExtraAdult() {
		return this.extraAdult;
   }

   public void setExtraAdult(double extraAdult) {
		this.extraAdult = extraAdult;
   }
 	
	public double getBaseAmount() {
		return this.baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}
	
	public Integer getNoOfUnits() {
		return this.noOfUnits;
	}

	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	
	public Long getAvailable() {
		return this.available;
	}

	public void setAvailable(Long available) {
		this.available = available;
	}
	
	public Timestamp getArrivalDate() {
		return this.arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public Timestamp getDepartureDate() {
		return this.departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}
  

	public Integer getBookingId() {
		return this.bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
    
	public Integer getStatusId() {
		return this.statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	
    public Integer getRooms() {
		return this.rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}
	
	
	public double getRefund() {
		return this.refund;
	}

	public void setRefund(double refund) {
		this.refund = refund;
	}

	public double getTax() {
		return this.tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


}