package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


public class BookingDetailReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer accommodationId;
	
	private String accommodationType;
	
	private Integer propertyId;
	
	private Integer bookingId;
	
	private Integer bookingDetailId;
	
	private Integer rooms;
	
	private Timestamp arrivalDate;
	
	private Timestamp departureDate;
	
	private Integer promotionId;
	
	private Integer smartPriceId;
	
	private Integer propertDiscountId;
	
	private String firstName;
	
	private String lastName;
	
	private String emailId;
	
	private String mobileNumber;
	
	private String phoneNumber;

	private Long roomCount;
	
	private Timestamp bookingDate;
	
	private Double amount;
	
	private Integer propertyDiscountId;
	
	private String city;
	
	private String sourceName;

	private String promotionType;
	
	private Double taxAmount;

	
	public BookingDetailReport(){
		
	}
	
	public BookingDetailReport(String accommodationType,Timestamp bookingDate,String firstName,String lastName,String emailId,String mobileNumber,String phoneNumber,Double amount){
		this.accommodationType=accommodationType;
		this.bookingDate=bookingDate;
		this.firstName=firstName;
		this.lastName=lastName;
		this.emailId=emailId;
		this.mobileNumber=mobileNumber;
		this.phoneNumber=phoneNumber;
		this.amount=amount;
	}
	
	
	public Integer getPropertyDiscountId() {
		return propertyDiscountId;
	}

	public void setPropertyDiscountId(Integer propertyDiscountId) {
		this.propertyDiscountId = propertyDiscountId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getAccommodationType() {
		return accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}
	
	public Timestamp getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Timestamp bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	public Integer getBookingDetailId() {
		return bookingDetailId;
	}

	public void setBookingDetailId(Integer bookingDetailId) {
		this.bookingDetailId = bookingDetailId;
	}

	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}

	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	public Integer getSmartPriceId() {
		return smartPriceId;
	}

	public void setSmartPriceId(Integer smartPriceId) {
		this.smartPriceId = smartPriceId;
	}

	public Integer getPropertDiscountId() {
		return propertDiscountId;
	}

	public void setPropertDiscountId(Integer propertDiscountId) {
		this.propertDiscountId = propertDiscountId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getRoomCount() {
		return this.roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	
	public String getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}
	
	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

}