package com.ulopms.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the pms_smart_price database table.
 * 
 */
@Entity
@Table(name="pms_smart_price")
@NamedQuery(name="PmsSmartPrice.findAll", query="SELECT p FROM PmsSmartPrice p")


public class PmsSmartPrice implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@SequenceGenerator(name="id",sequenceName="pms_smart_price_smart_price_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="smart_price_id")
	private Integer smartPriceId;

	@Column(name="percent_from")
	private double percentFrom;
	
	@Column(name="percent_to")
	private double percentTo;
	
	@Column(name="amount_percent")
	private double amountPercent;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
		
	public Integer getSmartPriceId() {
		return smartPriceId;
	}

	public void setSmartPriceId(Integer smartPriceId) {
		this.smartPriceId = smartPriceId;
	}

	public double getPercentFrom() {
		return percentFrom;
	}

	public void setPercentFrom(double percentFrom) {
		this.percentFrom = percentFrom;
	}

	public double getPercentTo() {
		return percentTo;
	}

	public void setPercentTo(double percentTo) {
		this.percentTo = percentTo;
	}

	public double getAmountPercent() {
		return amountPercent;
	}

	public void setAmountPercent(double amountPercent) {
		this.amountPercent = amountPercent;
	}
	
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
}