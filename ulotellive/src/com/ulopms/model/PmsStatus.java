package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_status database table.
 * 
 */
@Entity
@Table(name="pms_status")
@NamedQuery(name="PmsStatus.findAll", query="SELECT p FROM PmsStatus p")
public class PmsStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="status_id", unique=true, nullable=false)
	private Integer statusId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(length=50)
	private String status;

	@Column(name="status_description", length=250)
	private String statusDescription;

	//bi-directional many-to-one association to BookingDetail
	@OneToMany(mappedBy="pmsStatus")
	private List<BookingDetail> bookingDetails;

	//bi-directional many-to-one association to PmsBooking
	@OneToMany(mappedBy="pmsStatus")
	private List<PmsBooking> pmsBookings;

	public PmsStatus() {
	}

	public Integer getStatusId() {
		return this.statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return this.statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public List<BookingDetail> getBookingDetails() {
		return this.bookingDetails;
	}

	public void setBookingDetails(List<BookingDetail> bookingDetails) {
		this.bookingDetails = bookingDetails;
	}

	public BookingDetail addBookingDetail(BookingDetail bookingDetail) {
		getBookingDetails().add(bookingDetail);
		bookingDetail.setPmsStatus(this);

		return bookingDetail;
	}

	public BookingDetail removeBookingDetail(BookingDetail bookingDetail) {
		getBookingDetails().remove(bookingDetail);
		bookingDetail.setPmsStatus(null);

		return bookingDetail;
	}

	public List<PmsBooking> getPmsBookings() {
		return this.pmsBookings;
	}

	public void setPmsBookings(List<PmsBooking> pmsBookings) {
		this.pmsBookings = pmsBookings;
	}

	public PmsBooking addPmsBooking(PmsBooking pmsBooking) {
		getPmsBookings().add(pmsBooking);
		pmsBooking.setPmsStatus(this);

		return pmsBooking;
	}

	public PmsBooking removePmsBooking(PmsBooking pmsBooking) {
		getPmsBookings().remove(pmsBooking);
		pmsBooking.setPmsStatus(null);

		return pmsBooking;
	}

}