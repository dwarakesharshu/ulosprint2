package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


public class DashBoard implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
private long totals;

private Integer accommodationId;

private String accommodationType;

private long todayArrival;

private long todayDeparture;

private long todayOccupancy;

private long totalRooms;

private double totalRevenue;

private Long roomCount;

	
	
	public long getTotals() {
		return this.totals;
	}

	public void setTotals(long totals) {
		this.totals = totals;  
	}      
	
	
	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public String getAccommodationType() {
		return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}
	
	public long getTodayArrival() {
		return this.todayArrival;
	}

	public void setTodayArrival(long todayArrival) {
		this.todayArrival = todayArrival;
	}
	
	
	public long getTodayDeparture() {
		return this.todayDeparture;
	}

	public void setTodayDeparture(long todayDeparture) {
		this.todayDeparture = todayDeparture;
	}
	
	public long getTodayOccupancy() {
		return this.todayOccupancy;
	}

	public void setTodayOccupancy(long todayOccupancy) {
		this.todayOccupancy = todayOccupancy;
	}

	public long getTotalRooms() {
		return this.totalRooms;
	}

	public void setTotalRooms(long totalRooms) {
		this.totalRooms = totalRooms;
	}
	
	public double getTotalRevenue() {
		return this.totalRevenue;
	}

	public void setTotalRevenue(double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}
	
	public Long getRoomCount() {
		return this.roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
}