package com.ulopms.model;

import java.io.Serializable;


public class UserGuestDetailReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String userName;
	
	private String emailId;
	
	private String phoneNumber;
	
	private String address1;
	
	private String address2;
	

	public UserGuestDetailReport(){
		
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	

}