package com.ulopms.model;

public class Revenue {
	
	public Revenue() {
	}
	
	private double amount;

	
	private long nights;
	
	private Integer accommodationId; 

	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	
	public long getNights() {
		return nights;
	}


	public void setNights(long nights) {
		this.nights = nights;
	}


	public Integer getAccommodationId() {
		return accommodationId;
	}


	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	
}
