package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@Table(name="role")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="mast_role_role_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="role_id", unique=true, nullable=false)
	private Integer roleId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="is_active", nullable=false)
	private Boolean isActive;

	@Column(name="is_deleted", nullable=false)
	private Boolean isDeleted;

	@Column(name="modification_counter")
	private Integer modificationCounter;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;

	@Column(name="role_description", length=255)
	private String roleDescription;

	@Column(name="role_name", length=50)
	private String roleName;

	//bi-directional many-to-one association to ConfRoleScreen
	@OneToMany(mappedBy="role")
	private List<ConfRoleScreen> confRoleScreens;

	//bi-directional many-to-one association to PropertyUser
	@OneToMany(mappedBy="role")
	private List<PropertyUser> propertyUsers;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="role")
	private List<User> users;

	public Role() {
	}

	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModificationCounter() {
		return this.modificationCounter;
	}

	public void setModificationCounter(Integer modificationCounter) {
		this.modificationCounter = modificationCounter;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getRoleDescription() {
		return this.roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<ConfRoleScreen> getConfRoleScreens() {
		return this.confRoleScreens;
	}

	public void setConfRoleScreens(List<ConfRoleScreen> confRoleScreens) {
		this.confRoleScreens = confRoleScreens;
	}

	public ConfRoleScreen addConfRoleScreen(ConfRoleScreen confRoleScreen) {
		getConfRoleScreens().add(confRoleScreen);
		confRoleScreen.setRole(this);

		return confRoleScreen;
	}

	public ConfRoleScreen removeConfRoleScreen(ConfRoleScreen confRoleScreen) {
		getConfRoleScreens().remove(confRoleScreen);
		confRoleScreen.setRole(null);

		return confRoleScreen;
	}

	public List<PropertyUser> getPropertyUsers() {
		return this.propertyUsers;
	}

	public void setPropertyUsers(List<PropertyUser> propertyUsers) {
		this.propertyUsers = propertyUsers;
	}

	public PropertyUser addPropertyUser(PropertyUser propertyUser) {
		getPropertyUsers().add(propertyUser);
		propertyUser.setRole(this);

		return propertyUser;
	}

	public PropertyUser removePropertyUser(PropertyUser propertyUser) {
		getPropertyUsers().remove(propertyUser);
		propertyUser.setRole(null);

		return propertyUser;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setRole(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setRole(null);

		return user;
	}

}