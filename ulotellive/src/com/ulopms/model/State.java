package com.ulopms.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the state database table.
 * 
 */
@Entity
@Table(name="state")
@NamedQuery(name="State.findAll", query="SELECT s FROM State s")
public class State implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="state_code", unique=true, nullable=false, length=10)
	private String stateCode;

	@Column(name="state_name", length=100)
	private String stateName;

	//bi-directional many-to-one association to PmsGuest
	@OneToMany(mappedBy="state")
	private List<PmsGuest> pmsGuests;

	//bi-directional many-to-one association to PmsProperty
	@OneToMany(mappedBy="state")
	private List<PmsProperty> pmsProperties;

	//bi-directional many-to-one association to Country
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="country_code")
	private Country country;

	public State() {
	}

	public String getStateCode() {
		return this.stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public List<PmsGuest> getPmsGuests() {
		return this.pmsGuests;
	}

	public void setPmsGuests(List<PmsGuest> pmsGuests) {
		this.pmsGuests = pmsGuests;
	}

	public PmsGuest addPmsGuest(PmsGuest pmsGuest) {
		getPmsGuests().add(pmsGuest);
		pmsGuest.setState(this);

		return pmsGuest;
	}

	public PmsGuest removePmsGuest(PmsGuest pmsGuest) {
		getPmsGuests().remove(pmsGuest);
		pmsGuest.setState(null);

		return pmsGuest;
	}

	public List<PmsProperty> getPmsProperties() {
		return this.pmsProperties;
	}

	public void setPmsProperties(List<PmsProperty> pmsProperties) {
		this.pmsProperties = pmsProperties;
	}

	public PmsProperty addPmsProperty(PmsProperty pmsProperty) {
		getPmsProperties().add(pmsProperty);
		pmsProperty.setState(this);

		return pmsProperty;
	}

	public PmsProperty removePmsProperty(PmsProperty pmsProperty) {
		getPmsProperties().remove(pmsProperty);
		pmsProperty.setState(null);

		return pmsProperty;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}