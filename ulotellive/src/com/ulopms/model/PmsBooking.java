package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_booking database table.
 * 
 */
@Entity
@Table(name="pms_booking")
@NamedQuery(name="PmsBooking.findAll", query="SELECT p FROM PmsBooking p")
public class PmsBooking implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="pms_booking_booking_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="booking_id", unique=true, nullable=false)
	private Integer bookingId;

	@Column(name="arrival_date")
	private Timestamp arrivalDate;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="departure_date")
	private Timestamp departureDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	private Integer rooms;

	@Column(name="security_deposit")
	private double securityDeposit;

	@Column(name="total_amount")
	private double totalAmount;

	@Column(name="total_refund")
	private double totalRefund;

	@Column(name="total_tax")
	private double totalTax;
	
	@Column(name="arrived_date")
	private Timestamp arrivedDate;
	
	@Column(name="departured_date")
	private Timestamp departuredDate;

	
	//bi-directional many-to-one association to BookingGuestDetail
	@OneToMany(mappedBy="pmsBooking")
	private List<BookingGuestDetail> bookingGuestDetails;

	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;

	//bi-directional many-to-one association to PmsStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="status_id")
	private PmsStatus pmsStatus;
	
	//bi-directional many-to-one association to PmsStatus
			@ManyToOne(fetch=FetchType.LAZY)
			@JoinColumn(name="source_id")
			private PmsSource pmsSource;


	public PmsBooking() {
	}

	public Integer getBookingId() {
		return this.bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	public Timestamp getArrivalDate() {
		return this.arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getDepartureDate() {
		return this.departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getRooms() {
		return this.rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}

	public double getSecurityDeposit() {
		return this.securityDeposit;
	}

	public void setSecurityDeposit(double securityDeposit) {
		this.securityDeposit = securityDeposit;
	}

	public double getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalRefund() {
		return this.totalRefund;
	}

	public void setTotalRefund(double totalRefund) {
		this.totalRefund = totalRefund;
	}

	public double getTotalTax() {
		return this.totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}
	
	public Timestamp getArrivedDate() {
		return arrivedDate;
	}

	public void setArrivedDate(Timestamp arrivedDate) {
		this.arrivedDate = arrivedDate;
	}

	public Timestamp getDeparturedDate() {
		return departuredDate;
	}

	public void setDeparturedDate(Timestamp departuredDate) {
		this.departuredDate = departuredDate;
	}


	public List<BookingGuestDetail> getBookingGuestDetails() {
		return this.bookingGuestDetails;
	}

	public void setBookingGuestDetails(List<BookingGuestDetail> bookingGuestDetails) {
		this.bookingGuestDetails = bookingGuestDetails;
	}

	public BookingGuestDetail addBookingGuestDetail(BookingGuestDetail bookingGuestDetail) {
		getBookingGuestDetails().add(bookingGuestDetail);
		bookingGuestDetail.setPmsBooking(this);

		return bookingGuestDetail;
	}

	public BookingGuestDetail removeBookingGuestDetail(BookingGuestDetail bookingGuestDetail) {
		getBookingGuestDetails().remove(bookingGuestDetail);
		bookingGuestDetail.setPmsBooking(null);

		return bookingGuestDetail;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public PmsStatus getPmsStatus() {
		return this.pmsStatus;
	}

	public void setPmsStatus(PmsStatus pmsStatus) {
		this.pmsStatus = pmsStatus;
	}
	
	public PmsSource getPmsSource() {
		return pmsSource;
	}

	public void setPmsSource(PmsSource pmsSource) {
		this.pmsSource = pmsSource;
	}



}