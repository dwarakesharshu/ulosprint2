package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the conf_role_screen database table.
 * 
 */
@Entity
@Table(name="conf_role_screen")
@NamedQuery(name="ConfRoleScreen.findAll", query="SELECT c FROM ConfRoleScreen c")
public class ConfRoleScreen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="conf_role_screen_role_screen_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="role_screen_id", unique=true, nullable=false)
	private Integer roleScreenId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="is_active", nullable=false)
	private Boolean isActive;

	@Column(name="is_deleted", nullable=false)
	private Boolean isDeleted;

	@Column(name="modification_counter")
	private Integer modificationCounter;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;

	//bi-directional many-to-one association to Role
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="role_id")
	private Role role;

	//bi-directional many-to-one association to Screen
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="screen_id")
	private Screen screen;

	public ConfRoleScreen() {
	}

	public Integer getRoleScreenId() {
		return this.roleScreenId;
	}

	public void setRoleScreenId(Integer roleScreenId) {
		this.roleScreenId = roleScreenId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModificationCounter() {
		return this.modificationCounter;
	}

	public void setModificationCounter(Integer modificationCounter) {
		this.modificationCounter = modificationCounter;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Screen getScreen() {
		return this.screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

}