package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the access_rights database table.
 * 
 */
@Entity
@Table(name="access_rights")
@NamedQuery(name="AccessRight.findAll", query="SELECT a FROM AccessRight a")
public class AccessRight implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="access_rights_access_rights_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="access_rights_id", unique=true, nullable=false)
	private Integer accessRightsId;

	@Column(name="access_rights", length=50)
	private String accessRights;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="display_name", length=50)
	private String displayName;

	@Column(name="rights_type", length=20)
	private String rightsType;

	//bi-directional many-to-one association to UserAccessRight
	@OneToMany(mappedBy="accessRight")
	private List<UserAccessRight> userAccessRights;
    
	//bi-directional many-to-one association to PropertyItem
	@OneToMany(mappedBy="accessRight")
	private List<RoleAccessRight> roleAccessRights;

		
	public AccessRight() {
	}

	public Integer getAccessRightsId() {
		return this.accessRightsId;
	}

	public void setAccessRightsId(Integer accessRightsId) {
		this.accessRightsId = accessRightsId;
	}

	public String getAccessRights() {
		return this.accessRights;
	}

	public void setAccessRights(String accessRights) {
		this.accessRights = accessRights;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getRightsType() {
		return this.rightsType;
	}

	public void setRightsType(String rightsType) {
		this.rightsType = rightsType;
	}

	public List<UserAccessRight> getUserAccessRights() {
		return this.userAccessRights;
	}

	public void setUserAccessRights(List<UserAccessRight> userAccessRights) {
		this.userAccessRights = userAccessRights;
	}

	public UserAccessRight addUserAccessRight(UserAccessRight userAccessRight) {
		getUserAccessRights().add(userAccessRight);
		userAccessRight.setAccessRight(this);

		return userAccessRight;
	}

	public UserAccessRight removeUserAccessRight(UserAccessRight userAccessRight) {
		getUserAccessRights().remove(userAccessRight);
		userAccessRight.setAccessRight(null);

		return userAccessRight;
	}
	
	public List<RoleAccessRight> getRoleAccessRights() {
		return this.roleAccessRights;
	}

	public void setRoleAccessRights(List<RoleAccessRight> roleAccessRights) {
		this.roleAccessRights = roleAccessRights;
	}

	public RoleAccessRight addRoleAccessRight(RoleAccessRight roleAccessRight) {
		getRoleAccessRights().add(roleAccessRight);
		roleAccessRight.setAccessRight(this);

		return roleAccessRight;
	}

	public RoleAccessRight removeAccessRight(RoleAccessRight roleAccessRight) {
		getRoleAccessRights().remove(roleAccessRight);
		roleAccessRight.setAccessRight(null);

		return roleAccessRight;
	}


}