package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property_rate database table.
 * 
 */
@Entity
@Table(name="pms_promotions")
@NamedQuery(name="PmsPromotions.findAll", query="SELECT p FROM PmsPromotions p")
public class PmsPromotions implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="pms_promotions_promotion_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="promotion_id")
	private Integer promotionId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="end_date")
	private Timestamp endDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="promotion_name")
	private String promotionName;

	@Column(name="start_date")
	private Timestamp startDate;
	
	@Column(name="promotion_is_active")
	private Boolean promotionIsActive;
	
	@Column(name="promotion_type")
	private String promotionType;

	//bi-directional many-to-one association to PmsProperty
	@ManyToOne
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;

	
	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne
	@JoinColumn(name="accommodation_id")
	private PropertyAccommodation propertyAccommodation;

	//bi-directional many-to-one association to PropertyRateDetail
	@OneToMany(fetch=FetchType.LAZY,mappedBy="pmsPromotions")
	private List<PmsPromotionDetails> pmsPromotionDetails;

	public PmsPromotions() {
	}

	public Integer getPromotionId() {
		return this.promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Boolean getPromotionIsActive() {
		return this.promotionIsActive;
	}

	public void setPromotionIsActive(Boolean promotionIsActive) {
		this.promotionIsActive = promotionIsActive;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPromotionName() {
		return this.promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public String getPromotionType() {
		return this.promotionType;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}
	
	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public PropertyAccommodation getPropertyAccommodation() {
		return this.propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}

	public List<PmsPromotionDetails> getPmsPromotionDetails() {
		return this.pmsPromotionDetails;
	}

	public void setPmsPromotionDetails(List<PmsPromotionDetails> pmsPromotionDetails) {
		this.pmsPromotionDetails = pmsPromotionDetails;
	}
	
	public PmsPromotionDetails addPmsPromotionDetails(PmsPromotionDetails pmsPromotionDetails) {
		getPmsPromotionDetails().add(pmsPromotionDetails);
		pmsPromotionDetails.setPmsPromotions(this);

		return pmsPromotionDetails;
	}

	public PmsPromotionDetails removePropertyRateDetail(PmsPromotionDetails pmsPromotionDetails) {
		getPmsPromotionDetails().remove(pmsPromotionDetails);
		pmsPromotionDetails.setPmsPromotions(null);

		return pmsPromotionDetails;
	}
	

}