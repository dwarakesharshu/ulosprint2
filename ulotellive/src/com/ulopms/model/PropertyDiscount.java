package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_items database table.
 * 
 */
@Entity
@Table(name="property_discount")
@NamedQuery(name="PropertyDiscount.findAll", query="SELECT p FROM PropertyDiscount p")
public class PropertyDiscount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_discount_property_discount_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_discount_id", unique=true, nullable=false)
	private Integer propertyDiscountId;
	
	@Column(name="discount_name", length=50)
	private String discountName;	
	
	@Column(name="start_date")
	private Timestamp startDate;
	
	@Column(name="end_date")
	private Timestamp endDate;
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="created_date")
	private Timestamp createdDate;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
    
    @Column(name="discount_type", length=50)
	private String discountType;
    
   
	@Column(name="discount_units")
	private Integer discountUnits;
    
    @Column(name="discount_inr")
    private double discountInr; 
    
    @Column(name="discount_percentage")
	private double discountPercentage; 
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="is_signup")
	private Boolean isSignup;
	
	@Column(name="is_resort")
	private Boolean isResort;

   

	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	//bi-directional many-to-one association to PropertyAccommodation
	/*	@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="accommodation_id")
		private PropertyAccommodation propertyAccommodation;*/




	public PropertyDiscount() {
	}

	public Integer getPropertyDiscountId() {
		return this.propertyDiscountId;
	}

	public void setPropertyDiscountId(Integer propertyDiscountId) {
		this.propertyDiscountId = propertyDiscountId;
	}

	

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getDiscountName() {
		return this.discountName;
	}

	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}


	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	
	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	
	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}
    
	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	
	public Integer getDiscountUnits() {
			return discountUnits;
		}

	public void setDiscountUnits(Integer discountUnits) {
			this.discountUnits = discountUnits;
		}

	public double getDiscountInr() {
			return discountInr;
		}

	public void setDiscountInr(double discountInr) {
			this.discountInr = discountInr;
		}

	public double getDiscountPercentage() {
			return discountPercentage;
		}

	public void setDiscountPercentage(double discountPercentage) {
			this.discountPercentage = discountPercentage;
		}

	public Boolean getIsSignup() {
		return isSignup;
	}

	public void setIsSignup(Boolean isSignup) {
		this.isSignup = isSignup;
	}
	
	public Boolean getIsResort() {
		return isResort;
	}

	public void setIsResort(Boolean isResort) {
		this.isResort = isResort;
	}
	
	
	/*public PropertyAccommodation getPropertyAccommodation() {
		return this.propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}*/

}