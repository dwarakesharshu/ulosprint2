package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_properties database table.
 * 
 */
@Entity
@Table(name="pms_properties")
@NamedQuery(name="PmsProperty.findAll", query="SELECT p FROM PmsProperty p")
public class PmsProperty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="pms_properties_property_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_id", unique=true, nullable=false)
	private Integer propertyId;

	@Column(name="\"Address1\"", length=250)
	private String address1;

	@Column(name="\"Address2\"", length=250)
	private String address2;

	@Column(length=100)
	private String city;

	@Column(name="country_code", length=5)
	private String countryCode;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(length=20)
	private String latitude;

	@Column(length=20)
	private String longitude;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="property_contact", length=50)
	private String propertyContact;

	@Column(name="property_description", length=1000)
	private String propertyDescription;

	@Column(name="property_email", length=50)
	private String propertyEmail;

	@Column(name="property_logo", length=50)
	private String propertyLogo;

	@Column(name="property_name", length=100)
	private String propertyName;

	@Column(name="property_phone", length=20)
	private String propertyPhone;

	@Column(name="zip_code", length=10)
	private String zipCode;
	
	@Column(name="property_thumbimg", length=10)
	private String propertyThumbPath;
	
	@Column(name="reservation_manager_name", length=100)
	private String reservationManagerName;
	
	@Column(name="reservation_manager_email", length=50)
	private String reservationManagerEmail;
	
	@Column(name="reservation_manager_contact", length=50)
	private String reservationManagerContact;
	
	@Column(name="resort_manager_name", length=100)
	private String resortManagerName;
	
	@Column(name="resort_manager_email", length=50)
	private String resortManagerEmail;
	
	@Column(name="resort_manager_contact", length=50)
	private String resortManagerContact;
	
	@Column(name="tax_valid_from")
	private Timestamp taxValidFrom;

	
    @Column(name="tax_valid_to")
	private Timestamp taxValidTo;
	
	@Column(name="tax_is_active")
	private Boolean taxIsActive;

	@Column(name="property_hotel_policy", length=10)
	private String propertyHotelPolicy;
	
	@Column(name="property_standard_policy", length=10)
	private String propertyStandardPolicy;
	
	@Column(name="property_cancellation_policy", length=10)
	private String propertyCancellationPolicy;

	
	//bi-directional many-to-one association to PmsBooking
	@OneToMany(mappedBy="pmsProperty")
	private List<PmsBooking> pmsBookings;

	//bi-directional many-to-one association to PropertyType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="property_type_id")
	private PropertyType propertyType;
	
	//bi-directional many-to-one association to PropertyType
		//@ManyToOne(fetch=FetchType.LAZY)
		//@JoinColumn(name="property_id")
		//private PropertyUser propertyUser;

	//bi-directional many-to-one association to State
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="state_code")
	private State state;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="property_owner_id")
	private User user;

	//bi-directional many-to-one association to PropertyAccommodation
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyAccommodation> propertyAccommodations;

	//bi-directional many-to-one association to PropertyAccommodation
		@OneToMany(mappedBy="pmsProperty")
		private List<PropertyUser> propertyUsers;

		
	public List<PropertyUser> getPropertyUsers() {
			return propertyUsers;
		}

		public void setPropertyUsers(List<PropertyUser> propertyUsers) {
			this.propertyUsers = propertyUsers;
		}

	//bi-directional many-to-one association to PropertyAmenity
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyAmenity> propertyAmenities;

	//bi-directional many-to-one association to PropertyItemCategory
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyItemCategory> propertyItemCategories;

	//bi-directional many-to-one association to PropertyItem
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyItem> propertyItems;

	//bi-directional many-to-one association to PropertyPhoto
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyPhoto> propertyPhotos;

	//bi-directional many-to-one association to PropertyTaxe
	
	/*@OneToMany(mappedBy="pmsProperty")
	private List<PropertyTaxe> propertyTaxes;
	*/
	
	//bi-directional many-to-one association to Location
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="location_id")
		private Location location;

	public PmsProperty() {
	}

	public Integer getPropertyId() {
		return this.propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPropertyContact() {
		return this.propertyContact;
	}

	public void setPropertyContact(String propertyContact) {
		this.propertyContact = propertyContact;
	}

	public String getPropertyDescription() {
		return this.propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getPropertyEmail() {
		return this.propertyEmail;
	}

	public void setPropertyEmail(String propertyEmail) {
		this.propertyEmail = propertyEmail;
	}

	public String getPropertyLogo() {
		return this.propertyLogo;
	}

	public void setPropertyLogo(String propertyLogo) {
		this.propertyLogo = propertyLogo;
	}

	public String getPropertyName() {
		return this.propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyPhone() {
		return this.propertyPhone;
	}

	public void setPropertyPhone(String propertyPhone) {
		this.propertyPhone = propertyPhone;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String getPropertyThumbPath() {
		return this.propertyThumbPath;
	}

	public void setPropertyThumbPath(String propertyThumbPath) {
		this.propertyThumbPath = propertyThumbPath;
	}
	
	public String getResortManagerName() {
		return this.resortManagerName;
	}

	public void setResortManagerName(String resortManagerName) {
		this.resortManagerName = resortManagerName;
	}
	
	public String getReservationManagerName() {
		return this.reservationManagerName;
	}

	public void setReservationManagerName(String reservationManagerName) {
		this.reservationManagerName = reservationManagerName;
	}
	
	public String getResortManagerEmail() {
		return this.resortManagerEmail;
	}

	public void setResortManagerEmail(String resortManagerEmail) {
		this.resortManagerEmail = resortManagerEmail;
	}
	
	public String getReservationManagerEmail() {
		return this.reservationManagerEmail;
	}

	public void setReservationManagerEmail(String reservationManagerEmail) {
		this.reservationManagerEmail = reservationManagerEmail;
	}
	
	public String getResortManagerContact() {
		return this.resortManagerContact;
	}

	public void setResortManagerContact(String resortManagerContact) {
		this.resortManagerContact = resortManagerContact;
	}
	
	public String getReservationManagerContact() {
		return this.reservationManagerContact;
	}

	public void setReservationManagerContact(String reservationManagerContact) {
		this.reservationManagerContact = reservationManagerContact;
	}
	

	public List<PmsBooking> getPmsBookings() {
		return this.pmsBookings;
	}

	public void setPmsBookings(List<PmsBooking> pmsBookings) {
		this.pmsBookings = pmsBookings;
	}

	public PmsBooking addPmsBooking(PmsBooking pmsBooking) {
		getPmsBookings().add(pmsBooking);
		pmsBooking.setPmsProperty(this);

		return pmsBooking;
	}

	public PmsBooking removePmsBooking(PmsBooking pmsBooking) {
		getPmsBookings().remove(pmsBooking);
		pmsBooking.setPmsProperty(null);

		return pmsBooking;
	}

	public PropertyType getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(PropertyType propertyType) {
		this.propertyType = propertyType;
	}
	
	/*
	public PropertyUser getPropertyUser() {
		return this.propertyUser;
	}

	public void setPropertyUser(PropertyUser propertyUser) {
		this.propertyUser = propertyUser;
	}
	*/

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<PropertyAccommodation> getPropertyAccommodations() {
		return this.propertyAccommodations;
	}

	public void setPropertyAccommodations(List<PropertyAccommodation> propertyAccommodations) {
		this.propertyAccommodations = propertyAccommodations;
	}

	public PropertyAccommodation addPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		getPropertyAccommodations().add(propertyAccommodation);
		propertyAccommodation.setPmsProperty(this);

		return propertyAccommodation;
	}

	public PropertyAccommodation removePropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		getPropertyAccommodations().remove(propertyAccommodation);
		propertyAccommodation.setPmsProperty(null);

		return propertyAccommodation;
	}

	public List<PropertyAmenity> getPropertyAmenities() {
		return this.propertyAmenities;
	}

	public void setPropertyAmenities(List<PropertyAmenity> propertyAmenities) {
		this.propertyAmenities = propertyAmenities;
	}

	public PropertyAmenity addPropertyAmenity(PropertyAmenity propertyAmenity) {
		getPropertyAmenities().add(propertyAmenity);
		propertyAmenity.setPmsProperty(this);

		return propertyAmenity;
	}

	public PropertyAmenity removePropertyAmenity(PropertyAmenity propertyAmenity) {
		getPropertyAmenities().remove(propertyAmenity);
		propertyAmenity.setPmsProperty(null);

		return propertyAmenity;
	}

	public List<PropertyItemCategory> getPropertyItemCategories() {
		return this.propertyItemCategories;
	}

	public void setPropertyItemCategories(List<PropertyItemCategory> propertyItemCategories) {
		this.propertyItemCategories = propertyItemCategories;
	}

	public PropertyItemCategory addPropertyItemCategory(PropertyItemCategory propertyItemCategory) {
		getPropertyItemCategories().add(propertyItemCategory);
		propertyItemCategory.setPmsProperty(this);

		return propertyItemCategory;
	}

	public PropertyItemCategory removePropertyItemCategory(PropertyItemCategory propertyItemCategory) {
		getPropertyItemCategories().remove(propertyItemCategory);
		propertyItemCategory.setPmsProperty(null);

		return propertyItemCategory;
	}

	public List<PropertyItem> getPropertyItems() {
		return this.propertyItems;
	}

	public void setPropertyItems(List<PropertyItem> propertyItems) {
		this.propertyItems = propertyItems;
	}

	public PropertyItem addPropertyItem(PropertyItem propertyItem) {
		getPropertyItems().add(propertyItem);
		propertyItem.setPmsProperty(this);

		return propertyItem;
	}

	public PropertyItem removePropertyItem(PropertyItem propertyItem) {
		getPropertyItems().remove(propertyItem);
		propertyItem.setPmsProperty(null);

		return propertyItem;
	}

	public List<PropertyPhoto> getPropertyPhotos() {
		return this.propertyPhotos;
	}

	public void setPropertyPhotos(List<PropertyPhoto> propertyPhotos) {
		this.propertyPhotos = propertyPhotos;
	}

	public PropertyPhoto addPropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().add(propertyPhoto);
		propertyPhoto.setPmsProperty(this);

		return propertyPhoto;
	}

	public PropertyPhoto removePropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().remove(propertyPhoto);
		propertyPhoto.setPmsProperty(null);

		return propertyPhoto;
	}

	/*public List<PropertyTaxe> getPropertyTaxes() {
		return this.propertyTaxes;
	}

	public void setPropertyTaxes(List<PropertyTaxe> propertyTaxes) {
		this.propertyTaxes = propertyTaxes;
	}*/

	/*public PropertyTaxe addPropertyTaxe(PropertyTaxe propertyTaxe) {
		getPropertyTaxes().add(propertyTaxe);
		propertyTaxe.setPmsProperty(this);

		return propertyTaxe;
	}

	public PropertyTaxe removePropertyTaxe(PropertyTaxe propertyTaxe) {
		getPropertyTaxes().remove(propertyTaxe);
		propertyTaxe.setPmsProperty(null);

		return propertyTaxe;
	}
    */
	
	public Timestamp getTaxValidFrom() {
		return taxValidFrom;
	}

	public void setTaxValidFrom(Timestamp taxValidFrom) {
		this.taxValidFrom = taxValidFrom;
	}

	public Timestamp getTaxValidTo() {
		return taxValidTo;
	}

	public void setTaxValidTo(Timestamp taxValidTo) {
		this.taxValidTo = taxValidTo;
	}

	public Boolean getTaxIsActive() {
		return taxIsActive;
	}

	public void setTaxIsActive(Boolean taxIsActive) {
		this.taxIsActive = taxIsActive;
	}
	
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	public String getPropertyHotelPolicy()
	{
		return propertyHotelPolicy;
	}
	
	public void setPropertyHotelPolicy(String propertyHotelPolicy){
		this.propertyHotelPolicy=propertyHotelPolicy;
	}
	
	public String getPropertyStandardPolicy()
	{
		return propertyStandardPolicy;
	}
	
	public void setPropertyStandardPolicy(String propertyStandardPolicy){
		this.propertyStandardPolicy=propertyStandardPolicy;
	}
	
	public String getPropertyCancellationPolicy()
	{
		return propertyCancellationPolicy;
	}
	
	public void setPropertyCancellationPolicy(String propertyCancellationPolicy){
		this.propertyCancellationPolicy=propertyCancellationPolicy;
	}
}