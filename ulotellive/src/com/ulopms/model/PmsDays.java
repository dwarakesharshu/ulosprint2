package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_amenities database table.
 * 
 */
@Entity
@Table(name="pms_days")
@NamedQuery(name="PmsDays.findAll", query="SELECT p FROM PmsDays p")
public class PmsDays implements Serializable {
	private static final long serialVersionUID = 1L;
 
	
	@Id  
    @SequenceGenerator(name="id",sequenceName="pms_days_day_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="day_id", unique=true, nullable=false)
	private Integer dayId;

	@Column(name="days_of_week", length=250)
	private String daysOfWeek;
	
	@Column(name="day_part")
	private Integer dayPart;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	public PmsDays() {
	}

	public Integer getDayId() {
		return this.dayId;
	}

	public void setDayId(Integer dayId) {
		this.dayId = dayId;
	}

	public String getDaysOfWeek() {
		return this.daysOfWeek;
	}

	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}

	public Integer getDayPart() {
		return this.dayPart;
	}

	public void setDayPart(Integer dayPart) {
		this.dayPart = dayPart;
	}
	

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	

	
}