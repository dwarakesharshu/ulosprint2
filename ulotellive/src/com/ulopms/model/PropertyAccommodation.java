package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property_accommodation database table.
 * 
 */
@Entity
@Table(name="property_accommodation")
@NamedQuery(name="PropertyAccommodation.findAll", query="SELECT p FROM PropertyAccommodation p")
public class PropertyAccommodation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="accommodation_id", unique=true, nullable=false)
	private Integer accommodationId;

	@Column(length=5)
	private String abbreviation;

	@Column(name="accommodation_description", length=1000)
	private String accommodationDescription;

	@Column(name="accommodation_type", length=10)
	private String accommodationType;

	@Column(name="no_of_adults")
	private Integer noOfAdults;

	@Column(name="base_amount", precision=131089)
	private double baseAmount;

	@Column(name="no_of_child")
	private Integer noOfChild;
	
	@Column(name="no_of_infant")
	private Integer noOfInfant;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="extra_adult", precision=131089)
	private double extraAdult;

	@Column(name="extra_child", precision=131089)
	private double extraChild;
	
	@Column(name="extra_infant", precision=131089)
	private double extraInfant;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	
	@Column(name="min_occupancy")
	private Integer minOccupancy;
	
	@Column(name="max_occupancy")
	private Integer maxOccupancy;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="no_of_units")
	private Integer noOfUnits;
	
	@Column(name="accommodation_path", length=250)
	private String accommodationImgPath;

	//@Column(name="property_id")
	//private Integer propertyId;

	//bi-directional many-to-one association to PropertyAccommodationRoom
	@OneToMany(mappedBy="propertyAccommodation")
	private List<PropertyAccommodationRoom> propertyAccommodationRooms;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;

	public PropertyAccommodation() {
	}

	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public String getAbbreviation() {
		return this.abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getAccommodationDescription() {
		return this.accommodationDescription;
	}

	public void setAccommodationDescription(String accommodationDescription) {
		this.accommodationDescription = accommodationDescription;
	}

	public String getAccommodationType() {
		return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}

	public Integer getNoOfAdults() {
		return this.noOfAdults;
	}

	public void setNoOfAdults(Integer noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public double getBaseAmount() {
		return this.baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Integer getNoOfChild() {
		return this.noOfChild;
	}
	
	public Integer getNoOfInfant() {
		return this.noOfInfant;
	}

	public void setNoOfInfant(Integer noOfInfant) {
		this.noOfInfant = noOfInfant;
	}

	public void setNoOfChild(Integer noOfChild) {
		this.noOfChild = noOfChild;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public double getExtraAdult() {
		return this.extraAdult;
	}

	public void setExtraAdult(double extraAdult) {
		this.extraAdult = extraAdult;
	}
	
	public double getExtraInfant() {
		return this.extraInfant;
	}

	public void setExtraInfant(double extraInfant) {
		this.extraInfant = extraInfant;
	}

	public double getExtraChild() {
		return this.extraChild;
	}

	public void setExtraChild(double extraChild) {
		this.extraChild = extraChild;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getMinOccupancy() {
		return this.minOccupancy;
	}

	public void setMinOccupancy(Integer minOccupancy) {
		this.minOccupancy = minOccupancy;
	}

	public Integer getMaxOccupancy() {
		return this.maxOccupancy;
	}

	public void setMaxOccupancy(Integer maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getNoOfUnits() {
		return this.noOfUnits;
	}

	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	
	public String getAccommodationImgPath() {
		return this. accommodationImgPath;
	}

	public void setAccommodationImgPath(String accommodationImgPath) {
		this.accommodationImgPath = accommodationImgPath;
	}
	
	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}


	/*public Integer getPropertyId() {
		return this.propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
    */
	public List<PropertyAccommodationRoom> getPropertyAccommodationRooms() {
		return this.propertyAccommodationRooms;
	}

	public void setPropertyAccommodationRooms(List<PropertyAccommodationRoom> propertyAccommodationRooms) {
		this.propertyAccommodationRooms = propertyAccommodationRooms;
	}

	public PropertyAccommodationRoom addPropertyAccommodationRoom(PropertyAccommodationRoom propertyAccommodationRoom) {
		getPropertyAccommodationRooms().add(propertyAccommodationRoom);
		propertyAccommodationRoom.setPropertyAccommodation(this);

		return propertyAccommodationRoom;
	}

	public PropertyAccommodationRoom removePropertyAccommodationRoom(PropertyAccommodationRoom propertyAccommodationRoom) {
		getPropertyAccommodationRooms().remove(propertyAccommodationRoom);
		propertyAccommodationRoom.setPropertyAccommodation(null);

		return propertyAccommodationRoom;
	}

	
}