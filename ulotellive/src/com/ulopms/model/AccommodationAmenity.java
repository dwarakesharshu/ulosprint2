package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the accommodation_amenities database table.
 * 
 */
@Entity
@Table(name="accommodation_amenities")
@NamedQuery(name="AccommodationAmenity.findAll", query="SELECT a FROM AccommodationAmenity a")
public class AccommodationAmenity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="accommodation_amenities_accommodation_amenity_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="accommodation_amenity_id", unique=true, nullable=false)
	private Integer accommodationAmenityId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	//bi-directional many-to-one association to PmsAmenity
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="amenity_id")
	private PmsAmenity pmsAmenity;

	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="accommodation_id")
	private PropertyAccommodation propertyAccommodation;

	public AccommodationAmenity() {
	}

	public Integer getAccommodationAmenityId() {
		return this.accommodationAmenityId;
	}

	public void setAccommodationAmenityId(Integer accommodationAmenityId) {
		this.accommodationAmenityId = accommodationAmenityId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public PmsAmenity getPmsAmenity() {
		return this.pmsAmenity;
	}

	public void setPmsAmenity(PmsAmenity pmsAmenity) {
		this.pmsAmenity = pmsAmenity;
	}

	public PropertyAccommodation getPropertyAccommodation() {
		return this.propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}

}