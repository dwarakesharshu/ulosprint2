<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Users  
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Property Users</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="users.length > 0">
						<div class="box">     
							<div class="box-header">
								<h3 class="box-title">Users</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover">
									<tr>										
										<th>userName</th>
										<th>Email</th>
										<th>Role</th>
										<th>Edit</th>
									</tr>
									<tr ng-repeat="u in users">										
										<td>{{u.userName}}</td>
										<td>{{u.emailId}}</td>
										<td>{{u.roleName}}</td>
										<td>
											<a href="#editUser" ng-click="getSingleUser(u.propUserId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary" href="#adduser"  data-toggle="modal" >Add User</a>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
        
      	
							
      	<div class="modal" id="adduser">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add User</h4>
					</div>
					 <form name="addUser">
					<div class="modal-body" >
					<div id="message"></div>
						<div class="form-group">
							<label>User Name</label>
							<input type="text" class="form-control"  name="userName"  id="userName"  placeholder="UserName" ng-model='userName' ng-pattern="/^[a-zA-Z0-9]+$/" ng-required="true">
						    <span ng-show="addUser.userName.$error.pattern" style="color:red">Not a valid Name!</span>
						     
						</div>
						
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control"  name="emailId"  id="emailId"  placeholder="email" ng-model='emailId'  ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true">
						    <span ng-show="addUser.emailId.$error.pattern"  style="color:red">Not a valid Email!</span>
						</div>
						
						<div class="form-group">
							<label>Roles</label>
							<select name="roleId" id="roleId" value="" class="form-control">
	                       	<option ng-repeat="r in roles" value="{{r.DT_RowId}}">{{r.roleName}}</option>
	                        </select>
							
						</div>
						
						
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addSingleUser()"  class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editUser">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit User</h4>
					</div>
					 <form name="editUser">					
					<div class="modal-body" ng-repeat ="us in user" >
					<div id="editmessage"></div>
					    <input type="hidden" value="{{us.propertyUserId}}" name="editPropertyUserId" id="editPropertyUserId">
						<div class="form-group">
							<label>User Name</label>
							<input type="text" class="form-control"  name="editUserName"  id="editUserName" value="{{us.userName}}" ng-model='us.userName' ng-pattern="/^[a-zA-Z0-9]+$/" ng-required="true">
						    <span ng-show="editUser.editUserName.$error.pattern" style="color:red">Not a valid Name!</span>
						     
						</div>
						
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control"  name="editEmailId"  id="editEmailId"  value="{{us.emailId}}" ng-model='us.emailId'  ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true">
						    <span ng-show="editUser.editEmailId.$error.pattern"  style="color:red">Not a valid Email!</span>
						</div>
						
						<div class="form-group">
							<label>Roles</label>
							<select name="editRoleId" id="editRoleId" value="" class="form-control">
	                       	<option ng-repeat="r in roles" value="{{r.DT_RowId}}" ng-selected ="r.DT_RowId == us.roleId">{{r.roleName}}</option>
	                        </select>							
						</div>
						
						<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="editSingleUser()" class="btn btn-primary">Save</button>
					</div>
						
					</div>
					</form>
					
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {


			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

           
			
			$scope.getUsers = function() {
				
				
				
				var url = "get-property-users";
				$http.get(url).success(function(response) {
					
					console.log(response);
	                
					$scope.users = response.data;
		
				});
			};
			
			
			
			  $scope.getRoles = function() {
					
					var url = "get-property-roles";
					$http.get(url).success(function(response) {
					    //console.log(response);
						$scope.roles = response.data;
			
					});
			   };
				
			$scope.addSingleUser = function(){
				
				//alert($('#taskId').val());
				var fdata = "userName=" + $('#userName').val()
				+ "&emailId=" + $('#emailId').val()
				+ "&roleId=" + $('#roleId').val();
				
				
				alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-property-user'
						}).then(function successCallback(response) {
							
							$scope.getUsers();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success User Added Succesfully.</div>';
				            $('#message').html(alert);
				            
							$timeout(function(){
					      	$('#btnclose').click();
					        }, 2000);
				}, function errorCallback(response) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			$scope.editSingleUser = function(){
				
				
				
				//alert($('#editRoleId').val());
				
				var fdata = "userName=" + $('#editUserName').val()
				+ "&emailId=" + $('#editEmailId').val()
				+ "&roleId=" + $('#editRoleId').val()
				+ "&propertyUserId=" + $('#editPropertyUserId').val();
				
				
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-property-user'
						}).then(function successCallback(response) {
							
							$scope.getUsers();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success User Updated Succesfully.</div>';
				            $('#editmessage').html(alert);
				            
							$timeout(function(){
							$('#editbtnclose').click();
					        }, 2000);
							
				}, function errorCallback(response) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
            $scope.getSingleUser = function(rowid) {
				
				//alert(rowid);
				var url = "get-user?propertyUserId="+rowid;
					$http.get(url).success(function(response) {
				    console.log(response);				
					$scope.user = response.data;
		
				});
				
			};

			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
            $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
				
		    $scope.getPropertyList();
			
			
			$scope.getUsers();
			
		    $scope.getRoles();
		    
			//$scope.unread();
			//
			
			
		});

	
		
		   
		        

		
	</script>
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
       