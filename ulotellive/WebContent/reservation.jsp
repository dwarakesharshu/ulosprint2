<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
table.table-bordered > tbody > tr > th {
//border:1px solid #3c8dbc ;
}
table.table-bordered  {
  // border:1px solid #3c8dbc ;
/*     margin-top:20px; */
 }
table.table-bordered > thead > tr > th{
   //border:1px solid #3c8dbc ;
}
table.table-bordered > tbody > tr > td{
   //border:1px solid #3c8dbc ;
}
.tablesuccess{
    background-color: #092f53 !important;
    color:#ffffff !important;
    text-align:center;
}
</style>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           
            <small>Reservations</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        
            <li class="active">Resort Direct</li>
          </ol>
        </section>
	
	
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Availability</a></li>
                  <li ><a href="#tab-guest-information" data-toggle="tab">Guest Information</a></li>
                  <li ><a href="#tab-confirmation-summary" data-toggle="tab">Confirmation Summary</a></li>
                </ul>
                <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
        <section class="content">
        <div class="row">
        <form name="bookingForm">
                 <div class="form-group col-md-3">
                <label>Check-in:</label>

                     <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckin">
                   <span class="fa fa-calendar"></span>
              </label>   
                  <input type="text" id="bookCheckin" type="text" class="form-control" name="bookCheckin"  value="">
 
                  
                </div>
                <!-- /.input group -->
              </div>
                        <div class="form-group col-md-3">
                <label>Check-out:</label>

                <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckout">
                   <span class="fa fa-calendar"></span>
              </label>   
                  <input type="text" id="bookCheckout" type="text" class="form-control" name="bookCheckout"  value="">
 
                  
                </div>
                <!-- /.input group -->
              </div>
              <div class="col-md-2">
              <button type="button" ng-click="getAvailability()" class="btn btn-block btn-primary btn-sm" style="margin-top:25px">Search</button>
             </div>
             
   
        </form>
        
            </div>
        	<div class="table-responsive no-padding"  > 
	<table class="table table-bordered" id="example1">
	<h4>Availability</h4>
	<tr class="tablesuccess">
	
	<th>Type</th>
	<th>Starting from</th>
	<th>Arrival Date</th>
	<th>Departure date </th>
	<th>Available</th>
	<th>Max Person </th>
	<!-- <th>Select Rooms</th> -->
	<th>Action</th>
	
	</tr>
	<tr ng-repeat="av in availablities track by $index">
	
	<td ng-model="accommodationType">{{av.accommodationType}}</td>
	<td ng-model="baseAmount">{{av.baseAmount}}</td>
	<td>{{av.arrivalDate}}</td>
	    <td>{{av.departureDate}}</td>
	    <td ng-model="rooms">{{av.available}}</td>
	    <!--   //<td ng-model="rooms">{{av.noOfUnits}}</td> --> 
	    <td ng-model="rooms"> Max {{av.maxOccupancy}} Person(s)</td>
	   
	    <!-- <td ng-model="rooms">{{av.rooms}}</td> -->
	    <!--<td><select ng-model="r.value" ng-change="update(r.value,$index)"><option ng-repeat="r in range(1,1)" value="{{r}}">{{r}}</option></select></td>
	    <!--  Selected Value is:{{rValue}}-->
	    <!-- shiva add addrow(av.tax) -->
	    <td>
	     <a type="button" class= "btn btn-danger btn-style btn-sm" ng-show="av.available == 0 ">sold out</a>
	    <button type="button" ng-hide="av.available == 0 " class="btn btn-success btn-style btn-sm"  class="btn btn-success btn-style" id="btn{{av.accommodationId}}"  ng-click="selectType(av.accommodationId,$index)" >SELECT ROOM</button>
                                        <input type='checkbox' name='type[]' style="visibility: hidden;"  value='{{av.accommodationId}}' id="{{av.accommodationId}}"  ng-click="manageTypes(av.accommodationId);"/>
	    </td>
	    <!-- <td><a class="btn btn-xs btn-primary" href="#addmodal" ng-disabled="av.available == 0"  ng-click="addRow(av.accommodationId,av.accommodationType,av.baseAmount,av.arrivalDate,av.departureDate,av.rooms,av.noOfAdults,av.noOfChild,av.minOccupancy,av.extraAdult,av.extraChild,av.tax,av.available,$index)"  data-toggle="modal" min="0" >Add</a></td> -->
	    
	</tr>
	<tr>
	
	<!-- <td>Sub Total <br> 100rs</td>
	<td>Grand Total <br> 100rs</td>
	    <td>Deposit <br> 100rs</td> -->
	</tr>
	</table>
	</div>
	
	
	     <div class="table-responsive no-padding"  > 
	<table class="table table-bordered " id="finalData" >
	
	<tr class="tablesuccess">
	<th>Type</th>
	<th>Rooms</th>
	<th>Adult</th>
	<th>Child</th>
	<th>Infant</th>
	<!-- shiva add <th>tax</th> -->
	<th>Total</th>
	<th>Tax</th>
	
	<th>Total</th>
	</tr>
	<tr id="tr-{{$index}}" ng-repeat="rs in roomsSelected track by $index">
	<td>{{rs.accommodationType}}</td>
	    <td width="20%">
	   <div class="input-group">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'rooms',rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount)" ng-disabled="addroom <=0">
                              <span class="glyphicon glyphicon-minus"></span>
                              </button>
                              </span>
                              <input type="text" name="rooms"  class="form-control input-number rooms{{$index}}" value="1" min="1" max="10">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'rooms',rs.available,rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount)"  data-type="plus" data-field="quant">
                              <span class="glyphicon glyphicon-plus"></span>
                              </button>
                              </span>
                              </div>
                              </td>
                              <td width="20%"> <div class="input-group">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'adult',rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount)" ng-disabled="addroom <=0">
                              <span class="glyphicon glyphicon-minus"></span>
                              </button>
                              </span>
                              <input type="text" name="adult"  class="form-control input-number adult{{$index}}" value="1" min="1" max="10">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-success btn-number"     ng-click="plus($index,'adult',20,rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount)"  data-type="plus" data-field="quant">
                              <span class="glyphicon glyphicon-plus"></span>
                              </button>
                              </span>
                              </div>
                              </td>
                              <td width="20%"> <div class="input-group">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'child',rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount)" ng-disabled="addroom <=0">
                              <span class="glyphicon glyphicon-minus"></span>
                              </button>
                              </span>
                              <input type="text" name="child"  class="form-control input-number child{{$index}}" value="1" min="1" max="10">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'child',10,rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount)"  data-type="plus" data-field="quant">
                              <span class="glyphicon glyphicon-plus"></span>
                              </button>
                              </span>
                              </div>
                              </td>
	  <td width="20%"> <div class="input-group">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'infant',rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount)" ng-disabled="addroom <=0">
                              <span class="glyphicon glyphicon-minus"></span>
                              </button>
                              </span>
                              <input type="text" name="infant"  class="form-control input-number infant{{$index}}" value="1" min="1" max="10">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'infant',10,rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount)"  data-type="plus" data-field="quant">
                              <span class="glyphicon glyphicon-plus"></span>
                              </button>
                              </span>
                              </div>
                              </td>
	<!-- shiva add <td>rs.tax</td> -->
	   <td>{{rs.total}}</td> 
	  <td>{{rs.tax}}</td>
	 
	  <td>{{ getTotal()}} </td>
	  </tr>
	<tr>
	<!-- <td></td>
	<td></td> -->
	<td style="border:none!important"></td>
	<td style="border:none!important"></td>
	<td style="border:none!important"></td>
	<td style="border:none!important"></td>
	<td style="border:none!important"></td>
	<td width="15%"> 
<%-- 	<select class="form-control" onchange='count()'>
	           <option>Discount</option>
                                          <option>5%</option>
                                          <option>10%</option>
                                         <option>15%</option>
                                        <option>20%</option>
  </select> --%></td>	
	<td class="danger" width="10%">Sub Total:</td>
	<td class="danger"  id="totalAmount" >{{ getTotal()}} </td>
	
	<!--   <td class="danger" id="totalRooms">{{ getRoomsTotal() }} </td>
	<td><input type="hidden" value="0" id="tax"></td>
	<td><input type="hidden" value="0" id="securityDeposit"></td>-->
	<input type="hidden" id="bookingId" value="<s:property value="#session.bookingId" />" name="">
	</tr>
	</table>
	<button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
	</div>
    
                        
          
          
        </section><!-- /.content -->
                
                      
                        

                </div><!-- /.tab-content -->
                <!-- guest information begins -->
              	  
              	  <div class="tab-pane" id="tab-guest-information">
              	     <form name="addguestinformation">
	<div class="modal-body" >
	<div class="row">
	<div id="message"></div>
	
	   <div class="form-group col-sm-4">
	<label for="firstName">First Name</label>
	<input type="text" class="form-control"  name="firstName"  id="firstName"  placeholder="First Name" ng-model='firstName' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
	    <span ng-show="addguestinformation.firstName.$error.pattern" style="color:red">Enter The Valid First Name</span>
	</div>
	<div class="form-group col-sm-4">
	<label for="lastName">Last Name</label>
	<input type="text" class="form-control"  name="lastName"  id="lastName"  placeholder="Last Name" ng-model='lastName' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
	    <span ng-show="addguestinformation.lastName.$error.pattern" style="color:red">Enter The Valid Second Name!</span>
	</div>
	<div class="form-group col-sm-4">
	<label for="guestEmail">E-mail</label>
	<input type="email" class="form-control"  name="guestEmail"  id="guestEmail"  placeholder="Enter Your Email" ng-model='guestEmail' ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">
	    <span ng-show="addguestinformation.guestEmail.$error.pattern" style="color:red">Enter The Valid Email ID!</span>
	     
	</div>
	<div class="form-group col-sm-4">
	<label for="mobile_phone">Mobile Phone</label>
	<input type="text" class="form-control"  name="mobilePhone"  id="mobilePhone"  placeholder="Enter The Mobile Number" ng-model='mobilePhone' ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="20">
	    <span ng-show="addguestinformation.mobilePhone.$error.pattern" style="color:red">Enter the Valid Mobile Number!</span> 
	</div>
	
	
	<div class="form-group col-sm-4">
	<label for="guestCity">City</label>
	<input type="text" class="form-control"  name="guestCity"  id="guestCity"  placeholder="City" ng-model='guestCity' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
	    <span ng-show="addguestinformation.guestCity.$error.pattern" style="color:red">Enter The City</span>     
	</div>
	    <div class="form-group col-sm-4">
	<label for="guestZipcode">Postal Zipcode</label>
	<input type="text" class="form-control"  name="guestZipcode" id="guestZipCode" placeholder="Zip Code" ng-model='guestZipcode' ng-pattern="/^[0-9]/" ng-required="true" maxlength="10">
	     <span ng-show="addguestinformation.guestZipcode.$error.pattern" style="color:red">Enter The Valid POstal Code</span>
	</div>
	<div class="form-group col-sm-4">
                            <label for="guestcountrycode">Country</label>
                         <select name="countryCode" id="countryCode" value="{{property[0].countryCode}}" class="form-control">
                                        <option value="select">select</option>
                            <option ng-repeat="c in countries" value="{{c.countryCode}}"  ng-selected ="property[0].countryCode == c.countryCode">{{c.countryName}}</option>
                                      </select>
                        </div>
                              <div class="form-group col-sm-4">
                                      <label>State</label>
                                      <select name="stateCode" id="stateCode" value="{{property[0].stateCode}}" class="form-control">
                                        <option value="select">select</option>
                                    <option ng-repeat="s in states" value="{{s.stateCode}}" ng-selected ="property[0].stateCode == s.stateCode">{{s.stateName}}</option>
                                      </select>
                   
                         </div>
                         <div class="form-group col-sm-4">
                            <label>Source</label>
                            <select  class="form-control" name="sourceId"  id="sourceId" value="" class="form-control " ng-required="true">
                            <option ng-repeat="sr in sources" value="{{sr.sourceId}}" ng-selected ="sr.sourceId == r.sourceId">{{sr.sourceName}}</option>
                            </select>

                        </div>
                        	<div class="form-group col-sm-4">
                            <label for="guestcountrycode">Sub Total</label>
                        <input type="text"value="{{ getTotal()}}" id="txtSubTotal" class="form-control" >
                        </div>
                              <div class="form-group col-sm-4">
                                      <label>Discount</label>
                                      <select name="discount" id="discount"  ng-change="discountedGrossTotal()" ng-model="discount" class="form-control">
                                        <option value="select">select</option>
                                    <option ng-repeat="rd in resortdiscount" value="{{rd.discountId}}"  >{{rd.discountName}}</option>
                                      </select>
                   
                         </div>
                         <div class="form-group col-sm-4">
                            <label>Grand Total</label>
                          <input type="text" value="" id="totalAmounts"  name ="subTotal" class="form-control" >

                        </div>
                        
             <!--   <div class="form-group col-sm-4">
	
	<input type="hidden" class="form-control"  name="paymentLink"  id="paymentLink"  placeholder="Enter The Payment Link">
	    <br>
	    <div ng-repeat="invo in invoice">
	    <input type="hidden" class="form-control"  name="Link" value="{{invo.id}}" id="linkId" >
	     </div>
	    <button class="btn btn-primary pull-left" type="button"  ng-click="sendLink()">Send Link</button> 
	     <button class="btn btn-primary pull-left" type="button"  ng-click="goLink()">get status</button> 
	    </div> -->
	    
	    <div class="col-sm-12 col-lg-12 col-md-12">
	    <div ng-repeat="invo in invoice">
	    <input type="hidden" class="form-control"  name="Link" value="{{invo.id}}" id="linkId" >
	     </div>
	        <!--  <input type="text" placeholder="Total"  class="form-control"> -->
	         <button   href="#addmodal" ng-click=""  data-toggle="modal" class="btn btn-primary  text-center">Create Payment Link</button>
      	
                                   <button class="btn btn-primary " type="button"  ng-click="goLink()">get status</button>
	         <button ng-click="confirmBooking()" ng-disabled="addguestinformation.$invalid" class="btn btn-primary nextBtn">Pay At Hotel</button>
	    
	    </div>
	    	
	   
	
	</div>
	</div>
	<div class="modal-footer">
	<button class="btn btn-danger previousBtn pull-left" type="button" >Previous</button>  
	     <button ng-click="confirmBooking()" ng-disabled="addguestinformation.$invalid" class="btn btn-primary nextBtn">Next</button> 
	</div>
	</form>
              	        	<div class="modal" id="addmodal">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" id="btnclose" class="close" data-dismiss="modal"
	aria-hidden="true">x</button>
	<h4 class="modal-title">Create Payment Link</h4>
	</div>
	<form name="addItems">
	<div class="modal-bodys" >
	
	    <div class="form-group col-md-12">
	<label>Amount: </label>
	<input type="text" value="" id="totalAmounts"   name ="subTotal" class="form-control" >
	    
	</div>
	
	   <div class="form-group col-md-12">
	<label>Summary:</label>
	<textarea  class="form-control"  name="itemDescription"  id="itemDescription" value=""  placeholder="Description" required maxlength="250"></textarea>
	</div>
	
	<div class="form-group col-md-6">
	<label>Expiry Date:</label>
	<input type="text" class="form-control"  name="expiryDate"  id="expiryDate"  value="" placeholder="Select Date" required maxlength="250">
	</div>
	
	<div class="form-group col-md-6">
	<label>Customer Phone:</label>
	<input type="text" class="form-control"  id="itemMobile"  name="itemMobile"   placeholder="Mobile Number"  value="{{mobilePhone}}"  ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="20">
	    <span ng-show="addItems.itemMobile.$error.pattern" style="color:red">Not a valid Number !</span>
	</div>
	<div class="form-group col-md-12">
	<label>Customer Email:</label>
	<input type="text" class="form-control"  name="itemEmail"  id="itemEmail"  placeholder="E-mail" value="{{guestEmail}}" ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">
	    <span ng-show="addItems.itemEmail.$error.pattern" style="color:red">Not a valid Email id !</span>
	</div>
	
	 
	
	</div>
	<div class="modal-footer">
	
	<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
	<button ng-click="createLink()" ng-disabled="addItems.$invalid" class="btn btn-primary">Send Payment Link</button>
	</div>
	</form>
	</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dalog -->
	</div>
                  <!--   <button class="btn btn-primary previousBtn pull-left" type="button" >Previous</button>
                    <button class="btn btn-primary nextBtn pull-right" type="button" >Next</button> -->
              	  </div>
              	  <!-- guest information begins -->
              	  <div class="tab-pane" id="tab-confirmation-summary">
              	 
              	  	<div class="row">
              	  	<form name="guestConfirmation">
              	  	<div class="modal-body">
              	  	
              	  
	
              	  	<div id="message"></div>
                        <div class="invoice-box"  style="background:#fff;font-size:14px;max-width:800px;margin:auto;padding:30px;border:1px solid #000;max-width:800px;margin:auto;padding:30px;border:1px solid #eee;box-shadow:0 0 10px rgba(0, 0, 0, .15);font-size:16px;line-height:24px;font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;color:#555;">
        <table cellpadding="0" cellspacing="0" style="width:100%;line-height:inherit;text-align:left;" >
            <tr class="top">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left;">
                        <tr>
                            <td class="title">
                                <img src="https://ulohotels.com/ulowebsite/images/logo.png" style="width:100%; max-width:200px;">
                            </td>
                            <td style="text-align:right;">
                               <h2 style="color:#88ba41;text-transform:uppercase;word-wrap: break-word;vertical-align: middle;">Booking Voucher</h2>
                            </td>
                          
                        </tr>
	<hr>
                    </table>
                </td>
            </tr>
            
            <tr class="informations">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left;border-bottom:1px solid #000">
                        <tr>
                            <td>
                                <b>Voucher No: {{booked.data[0].bookingId}}</b>
                            </td>
                            
                            <td style="text-align:right;">
                              <b>GST : 33AABCU9979Q2Z8<br>CIN : U55100TN2016PTC113073<br>PAN : AABCU9979Q</b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
	      <tr class="informations">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left; border-bottom:1px solid #000" >
	  <h3 align="middle" style="color:#155d90">Guest Booking Information</h3>
                        <tr>
                            <td style="text-align:center;">
                               <b><span style="color:#155d90">Name:</span> {{booked.data[0].firstName}}<br><span style="color:#155d90">Mobile:</span> {{booked.data[0].phone}}<br><span style="color:#155d90">Booking Id:</span>{{booked.data[0].bookingId}}<br><span style="color:#155d90">Check-in:</span>{{booked.data[0].arrivalDate}} |<span style="color:#155d90"> Check-out:</span>{{booked.data[0].departureDate}}</b>
                            </td>
                            
                            <td style="text-align:center;" >
                              <b><span style="color:#155d90">Property Name: </span>{{booked.data[0].propertyName}}<br><span style="color:#155d90">No Of Nights:</span>{{booked.data[0].days}}<br><span style="color:#155d90">Meal Plan:</span>Regular<br><span style="color:#155d90">Adults</span>:{{booked.data[0].adultCount}} |<span style="color:#155d90"> Child:</span>{{booked.data[0].childCount}}</b>
                            </td>
                        </tr>
	
                    </table>
                </td>
            </tr>

        </table>
	         
	          <table style="width:100%;line-height:inherit;text-align:left;padding:10px; "   >
	
            <tr style=" background:#eee;font-weight:bold;">
                <th width="35%" style="color:#155d90">
                    Particulars
                </th>
                
                <th width="25%"  style="color:#155d90">
                   Rate
                </th>
	<th width="25%"  style="color:#155d90">
                    Qty
                </th>
                
                <th width="15%"  style="color:#155d90">
              Total
                </th>
	
            </tr>
	      <tr >
	
                <th style="font-weight:normal;" >
                 {{booked.data[0].accommodationType}}
                </th>
                
                <th style="font-weight:normal;">
            {{booked.data[0].accommodationRate}}
                </th >
	<th style="font-weight:normal;" >
               {{booked.data[0].rooms}}
                </th>
                
                <th style="font-weight:normal;" >
             {{booked.data[0].amount}}
                </th>
            </tr>

    <tr >

       <td style=" color:#155d90; " >Sub-Total</td>
      <td style=" color:#155d90; "></td>
      <td style="color:#155d90;" >GST</td>
        <td style="font-weight:normal;" >{{booked.data[0].tax}}</td>
    </tr>
	
	
   <tr  style="background:#eee;font-weight:bold; ">
   <td style="color:#155d90;">Total</td>
      <td></td>
      <td></td>
      
        <td>{{booked.data[0].totalAmount}}</td>
        
	<input type ="hidden" value="{{booked[0].firstName}}" id="customerName" name="customerName">
	<input type ="hidden" value="{{booked[0].totalAmount}}" id="totalAmount" name="totalAmount">
	<input type ="hidden" value="{{booked[0].tax}}" id="totalTax" name="totalTax">
	
	
    </tr>
             </table>
	
	<table  style="width:100%;line-height:inherit;text-align:left;">
	<hr>
                        <tr> <h6 align="middle" style="color:#000">This is computer Generated Voucher.No Signature Required</h6>
	</tr>
	</table>
	<table  style="width:100%;line-height:inherit;text-align:left;">
	 
                        <tr> <h2 align="middle" style="color:#155d90">Thank You For Choosing Ulo Hotels!</h2>
	</tr>
	</table>
	<table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41">
	 
                          <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                                <b><span style="color:#fff" >Mobile:</span>  +91 9543 592-593 <span style="color:#fff"> Email:</span> <span style="color:#155d90">Support@ulohotels.com </span></b>
                            </td>
                              </tr>
	    <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                              CORPORATE OFFICE: 56,B2,Oyster Apartment, 4th Avenue,19th Street, Ashok Nagar, Chennai, Tamil Nadu 600083
                            </td>
                        </tr>
	</table>
	<!-- 	<table  style="width:100%;line-height:inherit;text-align:left;">
	<tr>
	<td align="middle" style="font-size:14px;color:#000000;">
	<a href="#" onclick="return show('Page2','Page1');">Show page 2</a>
	</td>
	</tr>
	</table> -->
	 
    </div>
              	  	</div>
              	  	  	
              	  	<div class="modal-footer">
              	  	<div class="col-lg-12"></div>
	<button class="btn btn-danger previousBtn pull-left" type="button" >Previous</button>
	<button  ng-disabled="guestConfirmation.$invalid" class="btn btn-primary nextBtn" id="printer">Print</button>
	     <button class="btn btn-primary nextBtn pull-right" type="button"  ng-click="getBookedDetails()">Book Now</button>
	</div>
              	  	</form>
              	  	</div>
              	  </div>
              
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
          </div>
        
        
          
          
        </section><!-- /.content -->
      
    

      </div><!-- /.content-wrapper -->
      
<!-- <button id="rzp-button1">Pay</button> -->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>

var link = {
	
	  "customer": {
	    "name": "Test",
	    "email": "test@test.com",
	    "contact": "9999999999"
	  },
	  
	  "type": "link",
	  "view_less": 1,
	  "amount": 2000,
	  "currency": "INR",
	  "description": "Payment link for this purpose - xyz.",
	  "key": "rzp_test_bQQGkpEPtwppNI",
	   "name": "Merchant Name",
	   
	  "image": "/your_logo.png",
	  "expire_by": 1493630556
	
	
	
};
var options = {
	"key": "rzp_test_bQQGkpEPtwppNI",
    "amount": "2000", // 2000 paise = INR 20
    "name": "Merchant Name",
    "description": "Purchase Description",
    "image": "/your_logo.png",
    "handler": function (response){
        alert(response.razorpay_payment_id);
    },
    "prefill": {
        "name": "Harshil Mathur",
        "email": "harshil@razorpay.com"
    },
    "notes": {
        "address": "Hello World"
    },
    "theme": {
        "color": "#F37254"
    }
};
var rzp1 = new Razorpay(link);

document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script src="js/jquery-2.1.1.js" type="text/javascript"></script>
 <script>
 $('.datepicker').datepicker({
	    dateFormat: 'dd-mm-yy'
	});
 </script>
	<script>
    
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

	 
	
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

        $("#bookCheckin").datepicker({
            dateFormat: 'mm/dd/yy',
          
            minDate:  0,
            onSelect: function (formattedDate) {
                var date1 = $('#bookCheckin').datepicker('getDate'); 
                var date = new Date( Date.parse( date1 ) ); 
                date.setDate( date.getDate() + 1 );        
                var newDate = date.toDateString(); 
                newDate = new Date( Date.parse( newDate ) );   
                $('#bookCheckout').datepicker("option","minDate",newDate);
                $timeout(function(){
                  //scope.checkIn = formattedDate;
                });
            }
        });

        $("#bookCheckout").datepicker({
            dateFormat: 'mm/dd/yy',
          
            minDate:  0,
            onSelect: function (formattedDate) {
                var date2 = $('#bookCheckout').datepicker('getDate'); 
                $timeout(function(){
                  //scope.checkOut = formattedDate;
                });
            }
        }); 
	
	$scope.unread = function() {
	//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
	var notifiurl = "unreadnotifications.action";
	$http.get(notifiurl).success(function(response) {
	$scope.latestnoti = response.data;
	});
	};

	
	
	$scope.getCategories = function() {
   
	var url = "get-categories";
	$http.get(url).success(function(response) {
	    //console.log(response);
	$scope.categories = response.data;
	
	});
	};
	
	
	$scope.getTaxes = function() {
	
	var url = "get-taxes";
	$http.get(url).success(function(response) {
	    //console.log(response);
	$scope.taxes = response.data;
	
	});
	};
	
	   $scope.getItems = function() {
       
	var url = "get-items";
	$http.get(url).success(function(response) {
	    console.log(response);
	$scope.items = response.data;
	
	});
	};
	
	
	
	
       $scope.getAvailability = function(){
	
	
	//alert($('#propertyItemCategoryId').val());
	
	var fdata = "&arrivalDate=" + $('#bookCheckin').val()
	+ "&departureDate=" + $('#bookCheckout').val()
             
	
	
	$http(
	{
	method : 'POST',
	data : fdata,
	headers : {
	'Content-Type' : 'application/x-www-form-urlencoded'
	},
	url : 'get-availability'
	}).success(function(response) {
	
	$scope.availablities = response.data;
	console.log($scope.availablities);
	
	});

	};
	
	
	
	$scope.getBooking = function (id) {
	
	$scope.indexval = id;
	        
	};
	
	$scope.getTableData = function () {
	
	
	   
	};
		$scope.update = function(rValue,indx) {
		
		
		$scope.rValue = rValue;
		$scope.roomsSelected[indx].rooms = rValue;
		$scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rValue;
		
		$scope.roomsSelected[indx].adultsAllow  = $scope.availablities[indx].noOfAdults * rValue;
		$scope.roomsSelected[indx].minOccu  = $scope.availablities[indx].minOccupancy * rValue;
		$scope.roomsSelected[indx].childAllow  = $scope.availablities[indx].noOfChild * rValue;
		
		   // console.log($scope.item.n, $scope.item.name)
	    }
		
	
	$scope.plus = function(indx,className,limit,arrivalDate,departureDate,minOccupancy,maxOccupancy,extraAdult,extraInfant,extraChild,baseAmount) {
		
	
	
	    
	var currentVal =  parseInt($('.'+className+indx).val());
	
	
	 
	      	   // If is not undefined
	             if (!isNaN(currentVal)) {
	            	 
	            	if(currentVal >= limit){
	            	
	            	$('.'+className+indx).val(currentVal);
	            	//alert("Sorry You Are Exceeding The Limit");
	            	 
	             }
	            	else{
	                 // Increment
	                 $('.'+className+indx).val(currentVal + 1);
	                 
	            	}
	             } 
	      	   
	      	   
	            else {
	                 // Otherwise put a 0 there
	                 $('.'+className+indx).val(0);
	             }
	      	   
	             var rooms = parseInt($('.rooms'+indx).val());
	         var adult = parseInt($('.adult'+indx).val());
	         var child = parseInt($('.child'+indx).val());
	         var infant = parseInt($('.infant'+indx).val());
	         
	         
	         
	       /*   var date1 = new Date(arrivalDate);
	         var date2 = new Date(departureDate); */
	         var a1 = arrivalDate.split(/[^0-9]/);
	         var d1= new Date (a1[0],a1[1]-1,a1[2],a1[3],a1[4],a1[5] );
	        // alert(arrival+ " "+d1);
	         
	         var a2 = departureDate.split(/[^0-9]/);
	         var d2= new Date (a2[0],a2[1]-1,a2[2],a2[3],a2[4],a2[5] );
	         //alert(departure+ " "+d2);
	       
	         var timeDiff = Math.abs(d2.getTime() - d1.getTime());
	         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
	        // alert(diffDays);
	         var minOccu = parseInt(minOccupancy*rooms);
	         
	         var maxOccu = parseInt(maxOccupancy*rooms);
	         
	         var extraAdult = parseInt(extraAdult);
	         var extraInfant = parseInt(extraInfant);
	         var extraChild = parseInt(extraChild);
	         var amount = parseFloat(baseAmount);
	         var totalOccu = parseInt(adult+child+infant);
	         var total = (diffDays*rooms*baseAmount);
	         var roomAmount = parseFloat(total);
	         var adltChd = parseInt(adult+child); 
	         
	         var adltChdIft = parseInt(adult+child+infant); 
	        // alert(adltChdIft);
	         $scope.roomsSelected[indx].rooms = rooms;
	             $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
	         $scope.roomsSelected[indx].adultsCount = adult;
	         $scope.roomsSelected[indx].childCount = child;
	         $scope.roomsSelected[indx].infantCount = infant;
	        
	         
	         
	         
	         if(totalOccu > maxOccu){
	         
	        	alert("Running out of Maximum Occupancy");
	        	 
	        	$('.adult'+indx).val(maxOccu);
	        	$('.child'+indx).val(0);
	        	$('.infant'+indx).val(0);
	        	 
	         $scope.roomsSelected[indx].adultsCount = 1;
	         $scope.roomsSelected[indx].childCount = 0;
	         $scope.roomsSelected[indx].infantCount = 0;
	         $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
	        	//finTotal = 0;
	        	// $(this).val(currentVal-1);
	        	//$scope.roomsSelected[indx].adultsCount = currentVal-1;
	        
	         }
	         
	         else{
	       
	         if(adult>minOccu){
	        	 
	        	var adultsLeft = (adult-minOccu);
	        	 
	        	$scope.roomsSelected[indx].total =  (diffDays * adultsLeft * extraAdult + roomAmount);
	        	//alert("adult" +adultsLeft);
	         }
	         
	         else if(child>minOccu){
	        	 
	        	var childLeft = (child-minOccu);
	        	$scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
	        	// alert("child" +childLeft);
	         }
	         
	         else if(infant>minOccu){
	        	 
	        	var infantLeft = (infant-minOccu);
	        	$scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
	        	//alert("infant" +infantLeft);
	         }
                     
	         else if((adult+child) > minOccu){
	        	 
	        	var childLeft = (adltChd-minOccu);
	        	$scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
	        	// alert("child" +childLeft);
	         }
                     
	         else if((adult+child+infant) > minOccu && infant >0){
	        	 
	        	var infantLeft = (adltChdIft-minOccu);
	        	$scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
	        	//alert("infant" +infantLeft);
	         }
	         
	         }
                      
	         var finTotal = $scope.roomsSelected[indx].total;
	         
	           if(finTotal < 1000){
	                     //alert("dsfsd");
	                     var percent = 0;
	                     var taxes = 0/100*finTotal* diffDays;
	                     $scope.roomsSelected[indx].tax = taxes; 
	                     //alert(taxes);
	                 }
	                 else if(finTotal >= 1000 && finTotal <= 2500){
	                     //alert("dsfsd");
	                     var percent = 12;
	                     //var taxes = 12/100*amount* diffDays;
	                     var taxes = Math.round(percent / 100 * finTotal) * diffDays;
	                     $scope.roomsSelected[indx].tax = taxes; 
	                     //alert(taxes);
	                 }
	                 else if(finTotal >= 2500 && finTotal <= 7000){
	                     //alert("dsfsd");
	                     var percent = 18;
	                     var taxes = Math.round(percent / 100 * finTotal) * diffDays;
	                     $scope.roomsSelected[indx].tax = taxes; 
	                     //alert(taxes);
	                 }
	                 else if(finTotal >= 7000 && finTotal <= 7000000){
	                     //alert("dsfsd");
	                     var percent = 28;
	                     //var taxes = Math.round(percent)/100 *amount* diffDays;
	                    var taxes = Math.round(percent / 100 * finTotal) * diffDays;
	                    $scope.roomsSelected[indx].tax = taxes; 
	                     //alert( taxes);
	                 }
	          
	          
	      /*   if( totalOccu > minOccu &&  adult > minOccu && totalOccu <= maxOccu ){
	        	
	         adultsLeft = parseInt(totalOccu-minOccu); 
	         $scope.roomsSelected[indx].total =  (diffDays * adultsLeft * extraAdult + roomAmount);
	        
	          }
	        		
	        */	 
	            
	             //var rooms = parseInt($('.'+className).val());
	             //$scope.roomsSelected[indx].rooms = rooms;
	             //$scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
	      	   
	}
		
		
              $scope.minus = function(indx,className,arrivalDate,departureDate,minOccupancy,maxOccupancy,extraAdult,extraInfant,extraChild,baseAmount) {
	
		var currentVal =  parseInt($('.'+className+indx).val());
		var limit = 0;
	      	   // If is not undefined
	             if (!isNaN(currentVal)) {
	                 // Increment
	                
	                 if(currentVal <= limit){
	            	
	            	$('.'+className+indx).val(currentVal);
	            	
	            	 
	             }
	            	else{
	                 // Increment
	            	   $('.'+className+indx).val(currentVal - 1);
	                 
	            	}
	                 
	             } else {
	                 // Otherwise put a 0 there
	                 $('.'+className+indx).val(0);
	             }
	      	   
	      	   
	             var rooms = parseInt($('.rooms'+indx).val());
	         var adult = parseInt($('.adult'+indx).val());
	         var child = parseInt($('.child'+indx).val());
	         var infant = parseInt($('.infant'+indx).val());
	        /*  var date1 = new Date(arrivalDate);
	         var date2 = new Date(departureDate); */
	         var a1 = arrivalDate.split(/[^0-9]/);
	         var d1= new Date (a1[0],a1[1]-1,a1[2],a1[3],a1[4],a1[5] );
	        // alert(arrival+ " "+d1);
	         
	         var a2 = departureDate.split(/[^0-9]/);
	         var d2= new Date (a2[0],a2[1]-1,a2[2],a2[3],a2[4],a2[5] );
	         //alert(departure+ " "+d2);
	       
	         var timeDiff = Math.abs(d2.getTime() - d1.getTime());
	         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
	         var minOccu = parseInt(minOccupancy*rooms);
	         var maxOccu = parseInt(maxOccupancy*rooms);
	         var extraAdult = parseInt(extraAdult);
	         var extraInfant = parseInt(extraInfant);
	         var extraChild = parseInt(extraChild);
	         var amount = parseFloat(baseAmount);
	         var totalOccu = parseInt(adult+child+infant);
	         var total = (diffDays*rooms*baseAmount);
	         var roomAmount = parseFloat(total);
	         var adltChd = parseInt(adult+child); 
	         
	         var adltChdIft = parseInt(adult+child+infant); 
	        // alert(adltChdIft);
	         $scope.roomsSelected[indx].rooms = rooms;
	             $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
	         $scope.roomsSelected[indx].adultsCount = adult;
	         $scope.roomsSelected[indx].childCount = child;
	         $scope.roomsSelected[indx].infantCount = infant;
	         var finTotal = $scope.roomsSelected[indx].total;
	         
	         
	         if(totalOccu > maxOccu){
	         
	        	alert("Running out of Maximum Occupancy");
	        	 
	        	$('.adult'+indx).val(1);
	        	$('.child'+indx).val(0);
	        	$('.infant'+indx).val(0);
	        	 
	        	$scope.roomsSelected[indx].adultsCount = 1;
	         $scope.roomsSelected[indx].childCount = 0;
	         $scope.roomsSelected[indx].infantCount = 0;
	         $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
	        	//finTotal = 0;
	        	// $(this).val(currentVal-1);
	        	//$scope.roomsSelected[indx].adultsCount = currentVal-1;
	        
	         }
	         
	         else{
	       
	         if(adult>minOccu){
	        	 
	        	var adultsLeft = (adult-minOccu);
	        	$scope.roomsSelected[indx].total =  (diffDays * adultsLeft * extraAdult + roomAmount);
	        	//alert("adult" +adultsLeft);
	         }
	         
                     if(child>minOccu){
	        	 
	        	var childLeft = (child-minOccu);
	        	$scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
	        	// alert("child" +childLeft);
	         }
	         
                     if(infant>minOccu){
	        	 
	        	var infantLeft = (infant-minOccu);
	        	$scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
	        	//alert("infant" +infantLeft);
	         }
                     
                     if((adult+child) > minOccu){
	        	 
	        	var childLeft = (adltChd-minOccu);
	        	$scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
	        	// alert("child" +childLeft);
	         }
                     
                     if((adult+child+infant) > minOccu && infant >0){
	        	 
	        	var infantLeft = (adltChdIft-minOccu);
	        	$scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
	        	//alert("infant" +infantLeft);
	         }
	         
	         }
                      //alert($scope.roomsSelected[indx].total);
	         
	          
	        //  var taxy = $scope.roomsSelected[indx].tax;
	          
	          if(finTotal < 1000){
	                     //alert("dsfsd");
	                     var percent = 0;
	                     var taxes = 0/100*finTotal* diffDays;
	                     $scope.roomsSelected[indx].tax = taxes; 
	                     //alert(taxes);
	                 }
	                 else if(finTotal >= 1000 && finTotal <= 2500){
	                     //alert("dsfsd");
	                     var percent = 12;
	                     //var taxes = 12/100*amount* diffDays;
	                     var taxes = Math.round(percent / 100 * finTotal) * diffDays;
	                     $scope.roomsSelected[indx].tax = taxes; 
	                     //alert(taxes);
	                 }
	                 else if(finTotal >= 2500 && finTotal <= 7000){
	                     //alert("dsfsd");
	                     var percent = 18;
	                     var taxes = Math.round(percent / 100 * finTotal) * diffDays;
	                     $scope.roomsSelected[indx].tax = taxes; 
	                     //alert(taxes);
	                 }
	                 else if(finTotal >= 7000 && finTotal <= 7000000){
	                     //alert("dsfsd");
	                     var percent = 28;
	                     //var taxes = Math.round(percent)/100 *amount* diffDays;
	                    var taxes = Math.round(percent / 100 * finTotal) * diffDays;
	                    $scope.roomsSelected[indx].tax = taxes; 
	                     //alert( taxes);
	                 }
	          
	       
	             
	      	   
	}
	
	
	  $scope.selectType = function(id,indx){
      	    
              
              //alert(btn.value);
             // var  a = document.getElementById(btn).hide();
              //alert(id);
              //var x = document.getElementById("btn"+id).style.display='none'; 
              
              var text = document.getElementById("btn"+id).firstChild;
              //text.data = text.data == "SELECT ROOM" ? "Selected" : "SELECT ROOM";
               
              if(document.getElementById(id).checked){
              
              
              document.getElementById(id).checked = false;
              
              text.data = "SELECT ROOM";
              document.getElementById("btn"+id).classList.remove("actives");
              $scope.manageTypes(id);
              
              }
              
              else{
                  
              text.data = "SELECTED";  
              document.getElementById(id).checked = true;   
              document.getElementById("btn"+id).classList.add("actives");
              $scope.manageTypes(id,indx);
              
              }
              //alert("hai");
                
              }
   
      $scope.manageTypes = function(id,indx){
 
      if(document.getElementById(id).checked){
      
      
      //var accId = parseInt($scope.accommodations[indx].accommodationId);
      //var available = parseInt($scope.accommodations[indx].available);
     // var amount = parseFloat($scope.accommodations[indx].baseAmount);
      
     // var chkval = document.getElementById(id).value;
     // var newType = {'accommodationId':accId ,'baseAmount':amount,'available':available};
      
       //$scope.typeSelected.push(newType);
      
       var dt = $scope.availablities[indx];
       $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,dt.rooms,dt.noOfAdults,dt.noOfChild,dt.minOccupancy,dt.maxOccupancy,dt.extraAdult,dt.extraInfant,dt.extraChild,dt.available,indx,dt.tax);
      
       // $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,dt.rooms,dt.noOfAdults,dt.noOfChild,dt.minOccupancy,dt.extraAdult,dt.extraInfant,dt.extraChild,dt.available,indx,dt.tax);    
       
       //avai
       console.log($scope.roomSelected);
      
    } 
      
   else{
      
     //$scope.typeSelected.splice(id);
      $scope.cleaner($scope.roomsSelected,id);
      //var chkval = document.getElementById(id).value;
      //alert(chkval);
    }
 
      
          };
          
          
          $scope.cleaner = function(arr,id){
            //console.log(id);
        	console.log(arr.length);
        	
            	for (var i = 0; i < arr.length; i++) {
            	        var cur = arr[i];
            	        
            	        if (cur.accommodationId == id) {
            	            arr.splice(i, 1);
            	            break;
            	        }
            	    }
            	 
            };
	
       $scope.confirmBooking = function(){
	
	//alert("sdghgd");
	//alert($('#propertyItemCategoryId').val());
	  
	
	/*var fdata = "arrivalDate=" + $('#bookCheckin').val()
	+ "&departureDate=" + $('#bookCheckout').val()
	+ "&rooms=" + $("#totalRooms").text().replace(/\s/g,'')
	+ "&totalTax=" + $('#tax').val()
	+ "&securityDeposit=" + $('#securityDeposit').val()*/
	
	var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
	var data = JSON.parse(text);
	var sourceid =  $('#sourceId').val();
	
	$http(
	{
	method : 'POST',
	data : data,
	//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
	dataType: 'json',
	headers : {
	'Content-Type' : 'application/json; charset=utf-8'

	},
	url : "add-booking?sourceId="+sourceid
	
	
	}).then(function successCallback(response) {
	
	
	var gdata = "firstName=" + $('#firstName').val()
	+ "&lastName=" + $('#lastName').val()
	+ "&emailId=" + $('#guestEmail').val()
	+ "&phone=" + $('#mobilePhone').val()
	+ "&landline=" + $('#landlinePhone').val()
	+ "&address1=" + $('#firstAddress').val()
	+ "&address1=" + $('#secondAddress').val()
	+ "&city=" + $('#guestCity').val()
	+ "&zipCode=" + $('#guestZipCode').val()
	+ "&countryCode=" + $('#countryCode').val()
	+ "&stateCode=" + $('#stateCode').val()
	
	$http(
	{
	method : 'POST',
	data : gdata,
	headers : {
	'Content-Type' : 'application/x-www-form-urlencoded'
	},
	url : 'add-guest'
	
	}).then(function successCallback(response) {
	
	/*var bdata = "accommodationId=" + $('#accommodationId').val()
	+ "&adultCount=" + $('#adults').val()
	+ "&childCount=" + $('#child').val()
	+ "&amount=" + $('#price').val()
	+ "&refund=0
	+ "&statusId=2"
	+ "&tax=0"*/
	    console.log($scope.roomsSelected);
	    //angular.toJson
	    //var bdata ="jsonData=" + '{"roomsbooked":' +JSON.stringify($scope.roomsSelected) + '}';
	    //var bdata = '{"data":' +$scope.roomsSelected + '}';
	    //var bdata = "jsonData=" + JSON.stringify($scope.roomsSelected);
	    // var bdata = angular.toJson($scope.roomsSelected);
	//var bdata = $scope.roomsSelected;
	
	//var bdata = '{"data":[{"accommodationId":9,"accommodationType":"luxury"}]}';
	//var data = {data: {jsonKey1 : 'one', jsonKey2: 'two',jsonKey5: 2, jsonKey6: '2'}};
	var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
	//alert(text);
	var data = JSON.parse(text);
	//var data = $scope.roomsSelected;
	var discountid =  $('#discount').val(); 
	
	//alert("discountid"+discountid);
	$http(
	    {
	method : 'POST',
	data : data,
	//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
	dataType: 'json',
	headers : {
	'Content-Type' : 'application/json; charset=utf-8'
	//'Content-Type' : 'application/x-www-form-urlencoded'
	},
	url : "add-booking-details?discountId="+discountid
	//url : 'jsonTest'
	   }).then(function successCallback(response) {
	
	
	    // alert("booking has been done sucessfully");	
	     $scope.booked = response.data;
	     //alert(JSON.stringify($scope.booked));
	     console.log($scope.booked.data[0].bookingId);
	  
	    
	   }, function errorCallback(response) {
	
	  
	
	});
	
	}, function errorCallback(response) {
	
	  
	
	});
	
	}, function errorCallback(response) {
	
	  
	// called asynchronously if an error occurs
	// or server returns response with an error status.
	});

	};
	
	
	$scope.range = function(min, max, step){
	    step = step || 1;
	    var input = [];
	    for (var i = min; i <= max; i += step) input.push(i);
	    return input;
	};

	$scope.getStates = function() {
             
             var url = "get-states";
             $http.get(url).success(function(response) {
                 //console.log(response);
                 //alert(JSON.stringify(response.data));
                 $scope.states = response.data;
             
             });
         };
         
         $scope.getCountries = function() {
             
             var url = "get-countries";
             $http.get(url).success(function(response) {
                 //console.log(response);
                 //alert(JSON.stringify(response.data));
                 $scope.countries = response.data;
             
             });
         };
         
         $scope.getResortDiscount = function() {
        	
        	 var url = "get-resort-discount";
             $http.get(url).success(function(response) {
                 //console.log(response);
                 //alert(JSON.stringify(response.data));
                 $scope.resortdiscount = response.data;
             
             });
        	 
         }
        
         $scope.roomsSelected = [];
         // shiva add addrom tax
         
         $scope.addRow = function(id,type,baseAmount,arrival, departure,rooms,adultsAllow,childAllow,minOccu,maxOccu,extraAdult,extraInfant,extraChild,available,indx,tax){	
	        	
        	 
        	 
        	if(available == 0){
             	
        	 
        	    
             	// alert("Rooms is not available");
             	  //$("#submitBtn").attr("disabled","disabled");
             	    //btn.disabled = true;
        	console.log("Disable");
                 }
                 else{
        
        	 
         var type = type.replace(/\s/g,'');
         var id = parseInt(id);
         var minOccu = parseInt(minOccu);
         var maxOccu = parseInt(maxOccu);
         var extraAdult = parseInt(extraAdult);
         var extraInfant = parseInt(extraInfant);
         var extraChild = parseInt(extraChild);
         
         var adultsAllow = parseInt(adultsAllow);
         var childAllow = parseInt(childAllow);
        
         var amount = parseFloat(baseAmount);
         var rooms = parseInt(rooms);
         
        /*  var date1 = new Date(arrival);
         var date2 = new Date(departure); */
         
        // var a = arrival.split(/[^0-9]/);
         //for (i=0;i<a.length;i++) { alert(a[i]); }
         //var d= new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
       //  alert(s+ " "+d);
       
         //alert(arrival);
         //alert(date1);
         
         var a1 = arrival.split(/[^0-9]/);
         var d1= new Date (a1[0],a1[1]-1,a1[2],a1[3],a1[4],a1[5] );
        // alert(arrival+ " "+d1);
         
         var a2 = departure.split(/[^0-9]/);
         var d2= new Date (a2[0],a2[1]-1,a2[2],a2[3],a2[4],a2[5] );
         //alert(departure+ " "+d2);
       
         var timeDiff = Math.abs(d2.getTime() - d1.getTime());
         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
         var randomNo = Math.random();
         
        // alert(diffDays);
         
         //shiva add var taxes
         if(amount < 1000){
             //alert("dsfsd");
             var percent = 0;
             var taxes = 0/100*amount* diffDays;
             //alert(taxes);
         }
         else if(amount >= 1000 && amount <= 2500){
             //alert("dsfsd");
             var percent = 12;
             //var taxes = 12/100*amount* diffDays;
             var taxes = Math.round(percent / 100 * amount) * diffDays;
             //alert(taxes);
         }
         else if(amount >= 2500 && amount <= 7000){
             //alert("dsfsd");
             var percent = 18;
             var taxes = Math.round(percent / 100 * amount) * diffDays;
             //alert(taxes);
         }
         else if(amount >= 7000 && amount <= 7000000){
             //alert("dsfsd");
             var percent = 28;
             //var taxes = Math.round(percent)/100 *amount* diffDays;
            var taxes = Math.round(percent / 100 * amount) * diffDays;
             //alert( taxes);
         }
         //alert(diffDays);
         var total = (diffDays*rooms*amount);
         //shiva update newData diffDays only
         var newData = {'arrival':arrival,'indx':indx,'available':available,'departure':departure,'accommodationId':id,'sourceId':1,'accommodationType':type, 'rooms': rooms,'diffDays':diffDays, 'baseAmount':amount, 'total':total ,'adultsAllow':adultsAllow,'childAllow':childAllow,'minOccu':minOccu,'maxOccu':maxOccu,'extraAdult':extraAdult,'extraInfant':extraInfant,'extraChild':extraChild,'adultsCount':0,'infantCount':0,'childCount':0,'refund':0,'statusId':2,'tax':taxes,'randomNo':randomNo};
        	   $scope.roomsSelected.push(newData);
        	   console.log($scope.roomsSelected);
        	   // $scope.availablities[indx].available = $scope.availablities[indx].available - 1 ;
                 }
        	};
        	
            $scope.getTotal = function(){
            	
        	    var total = 0;
        	    var tax = 0;
        	    for(var i = 0; i < $scope.roomsSelected.length; i++){
        	        var accommodation = $scope.roomsSelected[i];
        	        tax += (accommodation.tax);
        	        total += (accommodation.total );
        	    }
        
        	    var all = (total+tax);
        	    return all;
        	}  	
          
          $scope.getRoomsTotal = function(){
      	    var totalRooms = 0;
      	    for(var i = 0; i < $scope.roomsSelected.length; i++){
      	        var roomsCount = $scope.roomsSelected[i];
      	        totalRooms += parseInt(roomsCount.rooms);
      	    }
      	    return totalRooms;
      	  } 	
	
          $scope.removeRoom = function(index,indx,available){
        	  //  alert(indx);
        	  // alert(available);
        	// var Available = parseInt($scope.availablities[indx].available);
        	  //alert(Available);
        	   
        	$scope.availablities[indx].available = parseInt($scope.availablities[indx].available) + 1; 
        	 
              $scope.roomsSelected.splice(index,1); 
        	 
        	
        	//$scope.availablities[id].available = 1 ; 
        	//alert($scope.roomsSelected[indx]);
        	 
        	
        	} 	
          
	$scope.getPropertyList = function() {
	 
	        	var userId = $('#adminId').val();
		var url = "get-user-properties?userId="+userId;
		$http.get(url).success(function(response) {
		    
		$scope.props = response.data;
		
		});
		};
		
        $scope.change = function() {
 	   
	        //alert($scope.id);
	        
	        var propertyId = $scope.id;	
 	        var url = "change-user-property?propertyId="+propertyId;
 	$http.get(url).success(function(response) {
 	
 	window.location = '/ulopms/dashboard'; 
 	//$scope.change = response.data;
 	
 	});
	       
		};
		
		$scope.update = function(rValue,indx) {
		
		
		$scope.rValue = rValue;
		$scope.roomsSelected[indx].rooms = rValue;
		$scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rValue;
		
		$scope.roomsSelected[indx].adultsAllow  = $scope.availablities[indx].noOfAdults * rValue;
		$scope.roomsSelected[indx].minOccu  = $scope.availablities[indx].minOccupancy * rValue;
		$scope.roomsSelected[indx].childAllow  = $scope.availablities[indx].noOfChild * rValue;
		
		   // console.log($scope.item.n, $scope.item.name)
	    }
		
		$scope.updateExtraAdult = function(adults,child,indx,minOccu,baseAmount,diffDays,rooms) {
	 
	  //alert("adult"+adults);
	  //alert("child"+child);
	//alert("days"+diffDays);
	//	alert("room"+rooms);
	  var total = (diffDays*rooms*baseAmount);
	  adults = parseInt(adults);
	  child = parseInt(child);
	  minOccu = parseInt(minOccu);
	  var totalOccu = parseInt(adults+child);
	  var roomAmount = parseFloat(total);
	  $scope.roomsSelected[indx].adultsCount = adults;
	  $scope.roomsSelected[indx].childCount = child;
	  var adt = $scope.roomsSelected[indx].adultsCount;
	  var chd =  $scope.roomsSelected[indx].childCount;
	  //alert(chd);
	// alert("child"+$scope.roomsSelected[indx].childCount);
	  
	          if(totalOccu>minOccu){
	  
	      if(adults<=minOccu){
	  
	  var childLeft = totalOccu-minOccu;
	  
	  // alert("extraChild "+childLeft); 
	  
	  $scope.roomsSelected[indx].total =  (diffDays * childLeft * $scope.roomsSelected[indx].extraChild) + roomAmount;
	  // var bdprice1 =  (adultsLeft * $scope.roomsSelected[indx].extraAdult);
	 
	  }
	  
	  if(adults > minOccu){
	      
	      $scope.roomsSelected[indx].total = roomAmount;
	     
	      var adultsLeft = adults-minOccu; 
	      //alert("extraAdult "+adultsLeft);
	      
	      $scope.roomsSelected[indx].total = (diffDays * adultsLeft * $scope.roomsSelected[indx].extraAdult) + roomAmount;
	      
	      var left =  totalOccu-minOccu;
	      //alert(left);
	  var extraChd =   left-adultsLeft;
	  
	  $scope.roomsSelected[indx].total += (diffDays * extraChd * $scope.roomsSelected[indx].extraChild);
	  
	  //alert("extraChild "+ extraChd);
	  
	  }  
	  
	  }
	  
	/* if(adults>minOccu){
	
	  var adultsLeft = adults-minOccu;
	  
	  var bdprice1 =  (adultsLeft * $scope.roomsSelected[indx].extraAdult);
	 
	  var bdprice2 =  (child*$scope.roomsSelected[indx].extraChild);	
	  
	  var totalbedprice = bdprice1;
	  
	  $scope.roomsSelected[indx].total += totalbedprice ;
	  
	  }*/
	  
	  /* if(adults<=minOccu){
	    
	 
	  var totalbedprice = Math.abs(totalOccu-minOccu)*($scope.roomsSelected[indx].extraChild); 	
	      
	  $scope.roomsSelected[indx].total += totalbedprice ;
	  
	      }*/
	  
	  else{
	  
	  $scope.roomsSelected[indx].total = roomAmount * $scope.roomsSelected[indx].rooms ;
	  
	  }
	       
	}
	  
	    
        $scope.childAge = function(indx,chValue) {
	
        var val = chValue;
        var row = document.getElementById("tr-"+indx);
        
       
        $('#output'+indx).empty();
        var output;
        for (var i = 0, length = val; i < length; i++) {
                  
            output = "<select> <option value=''>Age</option> <option value='1'>1</option> <option value='2'>2</option> <option value='3'>3</option> </select> &nbsp &nbsp";
            $('#output'+indx).append(output);
        }
        
        
        /*if(chValue>0){
       
        var x = row.insertCell(5);
        x.innerHTML = " <select> <option value='1'>1</option> <option value='2'>2</option> <option value='3'>3</option> </select> ";
        i++;
       
        }
        else{
        
        row.deleteCell(5);
        	
        }*/
        
	}
      
        
 $scope.sendLink = function(){
	
	
	
	
	var pdata = "link=" + $('#paymentLink').val()
	                + "&email=" + $('#guestEmail').val()
	            + "&name=" + $('#firstName').val();
	            
            $http(
	{
	method : 'POST',
	data : pdata,
	headers : {
	'Content-Type' : 'application/x-www-form-urlencoded'
	},
	url : 'send-payment-link'
	}).success(function(response) {
	
	$scope.link = response.data;
	alert("link sent successfully");
	//console.log($scope.booked[0].firstName);
	
	
	});

	};
	
	
$scope.createLink = function(){
	
	/*var link = 	{
	  "customer": {
	    "name": "Test",
	    "email": "test@test.com",
	    "contact": "9999999999"
	  },
	  "type": "link",
	  "view_less": 1,
	  "amount": 2000,
	  "currency": "INR",
	  "description": "Payment link for this purpose - xyz.",
	  "expire_by": 1493630556
	};*/
	
	//alert("create link");
    
    var type = "link";
    var view_less = 1;
    var amount = ($('#gopi').val()*100);
    var currency = "INR";
    var description = "sample test link";
    var expire_by = Math.floor(new Date($('#expiryDate').val()).getTime()/1000);
   // var expire_by = 1507833000;
      //1507833000000
    var name = $('#firstName').val();
    var email =  $('#itemEmail').val();
    var phone =  $('#itemMobile').val();
   
   
    
    
    //alert(Math.floor(expire_by/1000));
    /*var name = "gopinath";
    var email = "gopinath@ulohotels.com";
    var phone = "8870088876";*/
    
    
    var pdata = "link=" + $('#guestEmail').val()
    + "&email=" + $('#guestEmail').val()
    + "&name=" + $('#firstName').val();
    
    
   /* var total = $('#gopi').val();
    var name = $('#firstName').val();
    var email =  $('#itemEmail').val();
    var phone =  $('#itemMobile').val();
    var description = $('#itemDescription').val();
    var expire_by = $('#expiryDate').val();*/
    
    

	var idata = "type=" + type
                +"&view_less=" + view_less
                + "&amount=" + amount
                + "&name=" + name
                + "&email=" + email
                + "&phone=" + phone
                + "&currency=" + currency
	            + "&description=" + description
	            + "&expire_by=" + expire_by;
	
	
	
	            
    $http(
	{
	method : 'POST',
	data : idata,
	
	headers : {
	'Content-Type' : 'application/x-www-form-urlencoded'
	},
	url : 'create-payment-link'
	
	}).success(function(response) {
	
	$scope.invoice = response.data;
	
	alert("link sent successfully");
	//console.log($scope.booked[0].firstName);
	
	
	});

	};	
	
        
	
	$scope.goLink = function(){
	
	//alert("hai");
	//alert($('#bookingId').val());
	var invoiceId = $('#linkId').val();
	//var invoiceId = 'inv_8oJzL37diCruyV';
	var fdata = "invoiceId=" + invoiceId; 
	
	            //alert(bookingId);
	//alert("payid" +invoiceId);
	
	$http(
	{
	method : 'POST',
	data : fdata,
	headers : {
	'Content-Type' : 'application/x-www-form-urlencoded'
	},
	url : 'get-invoice-status'
	}).success(function(response) {
	
	$scope.linku = response.data;
	//console.log($scope.booked[0].firstName);
	//alert($scope.linku[0].amount_paid);
	        if($scope.linku[0].amount_paid == 0){
	        	alert("Payment Pending");
	        }else
	        	{
	        	alert("Payment Success");
	        	}
	
	});
	

	};
		
	
   $scope.getBookedDetails = function(){
	
	//alert("hai");
	//alert($('#bookingId').val());
	//$('#bookingId').val();
	var bookingId = '10';
	var fdata = "bookingId=" + bookingId; 
	
             
	
	
	$http(
	{
	method : 'POST',
	data : fdata,
	headers : {
	'Content-Type' : 'application/x-www-form-urlencoded'
	},
	url : 'get-booked-details'
	}).success(function(response) {
	
	$scope.booked = response.data;
	//console.log($scope.booked[0].firstName);
	
	
	});

	};
	
	$scope.getSources = function() {

	var url = "get-sources";
	$http.get(url).success(function(response) {
	    //console.log(response);
	$scope.sources = response.data;
	
	});
	};
	
	$scope.discountedGrossTotal = function() {
		//alert("enter"+ $scope.discount);
		$scope.manageType($scope.discount,$scope.resortdiscount);
		  var discountOption = document.getElementById("discount").value;
      	 var subTotal = document.getElementById("txtSubTotal").value;
      	 
      	 //grossTotal = document.getElementById("totalAmounts").value;
      	 
		  //alert(discountOption);
		  //alert(subTotal);
		  //alert(grossTotal);
	    //document.getElementById("totalAmounts").value = Math.round(subTotal.value - (subTotal.value * dropdownVal));
     // alert(document.getElementById("totalAmounts").value);
		};
		
		   
	     $scope.manageType = function(id,arr){
	         //alert("id"+id)
	    	 for (var i = 0; i < arr.length; i++) {
	    	        var cur = arr[i];
	    	        //alert("name"+ cur.discountPercentage);
	    	        if (cur.discountId == id) {
	    	        	//alert("success");
	    	        	
	    	        	 var discountOption = document.getElementById("discount").value;
	    	         	 var subTotal = document.getElementById("txtSubTotal").value;
	    	         	document.getElementById("totalAmounts").value = Math.round(subTotal - (subTotal * cur.discountPercentage));
	    	         	//alert("final"+document.getElementById("totalAmounts").value); 
	    	         	break;
	    	        }
	    	 }
	    	    };
		 
	
	    $scope.getPropertyList();
	   
	    //$scope.getTotal();
	    
	    //$scope.getTotalRooms();
	    
	$scope.getTaxes();
	$scope.getCategories();
	
	$scope.getItems();
	$scope.getBooking();
	$scope.getSources();
	
	$scope.getStates();
        
        $scope.getCountries();
        
        $scope.getResortDiscount();
        
        
        
        //console.log($scope.roomSelected);
        //$scope.getBookedDetails();
	//$scope.unread();
	//
        
	
	});

	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
  $( function() {
  /*   $("#bookCheckin" ).datepicker({minDate:0});
    $("#bookCheckout" ).datepicker({minDate:0}); */
    $("#checkin" ).datepicker({minDate:0});
    $("#checkout" ).datepicker({minDate:0});
    $("#expiryDate" ).datepicker({minDate:1});
  });

  </script>
	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
	     <script>
  /* next and previous button */
 $('.nextBtn').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.previousBtn').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
  </script>
	<script>
 	/* function discountedGrossTotal(dropdownVal){
      	 var discountOption = document.getElementById("discount"),
      	 subTotal = document.getElementById("txtSubTotal"),
      	 grossTotal = document.getElementById("totalAmounts");
      	 grossTotal.value = Math.round(subTotal.value - (subTotal.value * dropdownVal));
        
      	} */
 	
 	/* $scope.manageTypes = function(id,indx){
        
        if(document.getElementById(id).checked){
            
            
            var accId = parseInt($scope.accommodations[indx].accommodationId);
            var available = parseInt($scope.accommodations[indx].available);
            var amount = parseFloat($scope.accommodations[indx].baseAmount);
            var extraAdult=parseFloat($scope.accommodations[indx].extraAdult);
            var extraChild=parseFloat($scope.accommodations[indx].extraChild);
            var extraInfant=parseFloat($scope.accommodations[indx].extraInfant);
           
           // var chkval = document.getElementById(id).value;
            var newType = {'accommodationId':accId ,'baseAmount':amount,'available':available,'extraAdult':extraAdult,'extraChild':extraChild,'extraInfant':extraInfant};
           
             $scope.typeSelected.push(newType);
             
             console.log($scope.typeSelected);
            
        } 
        else{
            
           //$scope.typeSelected.splice(id);
            $scope.cleaner($scope.typeSelected,id);
            //var chkval = document.getElementById(id).value;
            //alert(chkval);
        }
       
            
     };
     
     
     $scope.cleaner = function(arr,id){
         
    	 for (var i = 0; i < arr.length; i++) {
    	        var cur = arr[i];
    	        if (cur.accommodationId == id) {
    	            arr.splice(i, 1);
    	            break;
    	        }
    	    }
    	 
    }; */
	
	</script>
	<script src="js/jquery-2.1.1.js" type="text/javascript"></script>