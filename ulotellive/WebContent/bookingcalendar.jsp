<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
           <!-- /.col -->
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              
               <!--  <select  ng-options="at.accommodationId as at.accommodationType for at in accommodations" 
                      ng-model="id" ng-change="change()" ng-selected="true" style="margin-top:30px;" >
                      <option value="">Change Type</option>
                   
                </select>--> 
                           
               <div class="form-group col-md-12">
							 <h3 class="text-center">Select Accomadtion Type</h3>
							
							<select name="propertyAccommodationId" id="propertyAccommodationId" value="" class="form-control" onchange="getCalendar(this.value);">
	                       	<option value="">Choose Accommodation</option>
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
				</div>
				<input type="hidden" id="accommId" name="accommId" class="form-control" value = "" placeholder="Accommodation">
			  <br>
             <div id="calendar"></div> 
             
<div id="createEventModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                <h4>Room Inventory</h4>
            </div>
            <div id="modalBody" class="modal-body">
               <div class="form-group">
                    <input class="form-control" type="text" placeholder="Available Rooms" id="availableRoom">
                </div>

                <div class="form-group form-inline">
                    <div class="input-group ">
                        <input type="text" id="eventDueDate" class="form-control" placeholder="Date">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                    </div>
                </div>
                  
                <div class="form-group">
                    <input type="text" id="availableNights" class="form-control" placeholder="Number of Nights">
           
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button type="submit" class="btn btn-primary" id="submitButton">Update Inventory</button>
            </div>
        </div>
    </div>
</div>

            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
  <%-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- Page specific script -->
<script>
$(document).ready(function () {
    // page is now ready, initialize the calendar...
     var results = [];
     $('#calendar').fullCalendar({
        // put your options and callbacks here
        defaultView: 'month',
       // eventBorderColor: "#de1f1f",
        eventColor: '#77b300',
        eventTextColor:'#fff',
     
   
         header:
        {  
            left: 'prev,next,today',
            center: 'title',
            right: 'month'
        },

        editable: false,
        selectable: true,
        events: dowEvents,
        
                //When u select some space in the calendar do the following:
      

        //When u drop an event in the calendar do the following:
        eventDrop: function (event, delta, revertFunc) {
            //do something when event is dropped at a new location
        },

        //When u resize an event in the calendar do the following:
        eventResize: function (event, delta, revertFunc) {
            //do something when event is resized
        },

 /*        eventRender: function(event, element) {
            $(element).tooltip({title: event.title});             
        },
 */
        //Activating modal for 'when an event is clicked'
        eventClick: function (event) {
        	
        	alert(event.start);
        	var date = new Date(event.start);
        	var strDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
        	//var strDate = new Date(event.start);
        	
        	 $.ajax({
      		   
       		    url: "change-inventory"+"?strDate="+strDate,
     	 		//url: "get-booking-calendar"+ "?chartDate=" +results,
     	 		//url: "get-bookingchart"+ "?chartDate=" +results,
     	 		method: "GET",
     	 		async: true,
     	 		success: function(response) {
     	 			
     	 		window.location = '/ulopms/reservation';
     	 		
     	 		//alert("success");
     	 			
     	 			}
     	 	});
        	 
        	//$('#createEventModal').modal('show');
            $('#availableRoom').html(event.title);
            $('#availableNight').html(event.description);
            //$('#createEventModal').modal();
        },
        eventRender: function(event, element, view) {
        	 $(element).tooltip({
                 content: event.description
             });
        	$(element).tooltip({title: event.title});  
        	//$(element).tooltip({description: event.description}); 
            var cellheight = $('.fc-widget-content').height();
            $(element).css('height', '40px');
            $(element).css('width', '145px');
            //$(element).css('margin-left', '30px');
            element.find('.fc-time').hide();
            $(element).css('font-size', '1em');
            $(element).css('text-align', 'center');
            $(element).css('line-height', '40px');
           
            var modal = $("#createEventModal");
            modal.find("#availableRoom").html(event.title);
            //$('#createEventModal').modal();
            //#createEventModal.modal();
        },
     
       
    }
     )

      function doSubmit(){
        $("#createEventModal").modal('hide');
         }
     function dowEvents(start, end, tz, callback) {
    	
    	
    	 
    	  //alert(curr);
    	  var events = [];
    	  var curr = start;
    	  results = enumerateDaysBetweenDates(start, end);
    	  
    	  
    	 // alert(start);
    	  
    	  var accommodationId = $('#accommId').val();
    	  //alert(accommodationId);
    	  //var arr =  new Array;
    	  $.ajax({
    		   
    		  
    	 	
    	 		url: "get-booking-calendar"+ "?chartDate=" +results + "&accommodationId="+accommodationId,
    	 		//url: "get-booking-calendar"+ "?chartDate=" +results,
    	 		//url: "get-bookingchart"+ "?chartDate=" +results,
    	 		method: "GET",
    	 		async: false,
    	 		success: function(response) {
    	 			//alert(response.data);
    	 			//alert(chartDate);
    	 			events = response.data;
    	 			var lim = events.length;
    	 			
    	 			for (var i = 0; i < lim; i++){
    	 				 
    	 					 events[i].start = moment(results[i]);
    	 					// start = start.add(1, 'day');
    	 				
    	 				}
    	 			//events.start = moment(curr);
    	 		    
    	 			// var json = JSON.stringify(response.data);
    	 			// var json = json.replace(/\o/g,moment(curr));
    	 			 //var json = json.replace(/\"/g, "");
    	 			//var json = json.replaceAll('(?<="onpress":)"([^"]+)"','$1');
    	 			//json.replace(/\"/g, "")
    	 			// console.log(JSON.stringify(events));
    	 				
    	 			// events = JSON.parse(json);
    	 			// console.log(events);
    	 			 //alert(results);
    	 				
    	 			 /*arr = response.data.map(function(a){
    	 							   return a.size;
    	 							});
    	 						*/
    	 						 //alert(arr);	
    	 		}
    	 	});
    	  
    	  
    	  
    	/*  while (curr <= end) {
    	   // if (curr.format('dddd') === 'Monday')
    	    {
    	      events.push({
    	        title: 'Rooms 10',
    	        start: moment(curr)
    	      });
    	    }
    	    curr = curr.add(1, 'day');
    	  }
    	*/
    	
    	  console.log(events);
    	  callback(events);
    	
    	}
     
     
    });






</script>
<script>
     var enumerateDaysBetweenDates = function(startDate, endDate) {
      
      
      var now = startDate,
      dates = [];
      
      
     while (now.isBefore(endDate) || now.isSame(endDate)) {
           // dates.push({title:now.format("YYYY-MM-DD"),start:moment(startDate)});
           dates.push(now.format("YYYY-MM-DD"));
          //  moment().format();               // Jun 12th 17

            now.add('days', 1);
        }
      return dates;
      
  };

 function getCalendar(id){

 
 
 $('#accommId').val(id); 
 
 //$('#calendar').fullCalendar( 'removeEventSource', events);
 //$('#calendar').fullCalendar( 'addEventSource', events);         
 $('#calendar').fullCalendar( 'refetchEvents' );
 
 }

</script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>

    
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

		 
		
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

		
	    $scope.getPropertyList = function() {
			 
	        	var userId = $('#adminId').val();
	 			var url = "get-user-properties?userId="+userId;
	 			$http.get(url).success(function(response) {
	 			    
	 				$scope.props = response.data;
	 	
	 			});
	 		};
	 		
	    $scope.getAccommodations = function() {

				var url = "get-accommodations";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
	 		
       
	 		
	 	 $scope.change = function() {
	        	   
		        alert($scope.id);
		        
		       	       
		 		};
	 			
	 	
	 		
	    $scope.getPropertyList();
	    
	    $scope.getAccommodations();
	    
	   
		//$scope.unread();
		//
        
		
	});

		
	</script>