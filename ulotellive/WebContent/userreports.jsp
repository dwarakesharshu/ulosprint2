<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            User Details
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Report</a></li>
            <li class="active">User Report</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <!-- <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Search &amp; Filter</h3>
        </div> -->
        <div class="box-body">
          <div class="row">
              <div class="col-sm-2 col-md-2">
                <div class="form-group">
				<label>&nbsp;</label>
              	<button type="button" ng-click="getExportUserList()" class="btn btn-danger pull-right btn-block btn-sm">Download</button>
              	</div>
              </div>
              
          </div>
        </div>
      </div>
      
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Results</h3>

              
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>User Name</th>
                  <th>Email Id</th>
                  <th>Mobile Number</th>
                  <th>Line Address1</th>
                  <th>Line Address2</th>
                  
                </tr>
                <tr ng-repeat="user in userList">
                  <td>{{user.userName}}</td>
                  <td>{{user.emailId}}</td>
                  <td>{{user.phoneNumber}}</td>
                  <td>{{user.address1}}</td>
                  <td>{{user.address2}}</td>
                </tr>
                	
                <tr ng-if="userList.count==0">
                	<td  colspan="8">No Records found
                	</td>
                </tr>
             </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
    </section>
 		
 		
</div>
    <input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
 
	<script>
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		
		 		
			$scope.getExportUserList = function(){
	    		alert()
				$http(
						{
							method : 'POST',
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'export-report-users'
						}).success(function(response) {
							alert("see the download files in download folder");
							$scope.userList = response.data;
							
							
						});

			};
			
			
			$scope.getUsers = function() {

				var url = "get-user-report";
				$http.get(url).success(function(response) {
					$scope.userList = response.data;
		
				});
			};
			
			$scope.getUsers();
			
			
			
		
	});
		</script>
		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		  
		 