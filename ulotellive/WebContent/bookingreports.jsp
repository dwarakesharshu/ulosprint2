<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Booking Details
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Report</a></li>
            <li class="active">Booking Report</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Search &amp; Filter</h3>
        </div>
        <div class="box-body">
          <div class="row">
          <div class="col-sm-2 col-md-2">
           <div class="form-group">
                <label>From Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="fromDate" required>
                </div>
                <!-- /.input group -->
              </div>
              
              </div>
              <div class="col-sm-2 col-md-2">
           <div class="form-group">
                <label>To Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="toDate" required>
                </div>
                <!-- /.input group -->
              </div>
              
              </div>
              <div class="col-sm-2 col-md-2">
              <div class="form-group">
                <label>Accommodation Type</label>
                <select class="form-control select2" style="width: 100%;" id="accommodationId" name="accommodationId">
                  <option selected="selected" value="0">--All--</option>
                  <option ng-repeat="acc in accommodations" value="{{acc.accommodationId}}">{{acc.accommodationType}}</option>
                </select>
              </div>
              </div>
              <div class="col-sm-2 col-md-2">
              
              <div class="form-group">
<!--               <label>Search</label> -->
				<label>&nbsp;</label>
              	<button type="button" ng-click="getPropertyBookingList()" class="btn btn-danger pull-right btn-block btn-sm">Search</button>
              	</div>
              </div>
              
              <div class="col-sm-2 col-md-2">
              	<div class="form-group">
<!--               	<label>Download</label> -->
				<label>&nbsp;</label>
              	<button type="button" ng-click="getExportBookingList()" class="btn btn-danger pull-right btn-block btn-sm">Download</button>
              	</div>
              </div>
          </div>
           
        
          <!-- /.row -->
          
        </div>
        <!-- /.box-body -->
      </div>
      
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Results</h3>

              
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Booking ID</th>
                  <th>Booking Date</th>
                  <th>Guest Name</th>
                  <th>Check-In</th>
                  <th>Check-Out</th>
                  <th>Accommodation Type</th>
                  <th>Amount</th>
                  <th>Source</th>
                </tr>
                <tr ng-repeat="book in bookinglist">
                  <td>{{book.bookingId}}</td>
                  <td>{{book.boookingDate}}</td>
                  <td>{{book.guestName}}</td>
                  <td>{{book.checkIn}}</td>
                  <td>{{book.checkOut}}</td>
                  <td>{{book.accommodationType}}</td>
                  <td>{{book.amount}}</td>
                  <td>{{book.source}}</td>
                </tr>
                	
                <tr ng-if="bookinglist.count==0">
                	<td  colspan="8">No Records found
                	</td>
                </tr>
             </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
    </section>
 		
 		
</div>
    <input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
 <script>
 
 $('.datepicker').datepicker({
	    dateFormat: 'dd-mm-yy'
	 });
 </script>
	<script>
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		
		 $scope.getPropertyBookingList = function(){
				
				var fdata = "fromDate=" + $('#fromDate').val()+ "&toDate=" + $('#toDate').val()+ "&accommodationId="+$('#accommodationId').val();
				//var fdata = "&arrivalDate=" + ""+ "&departureDate=";
	    		
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'rpt-booking'
						}).success(function(response) {
							
							$scope.bookinglist = response.data;
							
						});

			};
			
			$scope.getExportBookingList = function(){
				
				var fdata = "fromDate=" + $('#fromDate').val()+ "&toDate=" + $('#toDate').val()+ "&accommodationId="+$('#accommodationId').val();
				//var fdata = "&arrivalDate=" + ""+ "&departureDate=";
	    		
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'export-rpt-booking'
						}).success(function(response) {
							
							$scope.bookinglist = response.data;
							alert("Report Saved Successfully in Download's Folder");
							
						});

			};
			
			
			var propertyId = $('#propertyId').val(); 	
			$scope.getAccommodations = function() {

				var url = "get-front-accommodations?propertyId="+propertyId;
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
			
			//$scope.getPropertyBookingList();
			
			$scope.getAccommodations();
			
			
			
		
	});
		</script>
		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		  
		  <script>
 
//Date picker
  $('#fromDate').datepicker({
    autoclose: true
  });
//Date picker
  $('#toDate').datepicker({
    autoclose: true
  });
  </script>
