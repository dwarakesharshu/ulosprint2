<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ULO Hotels</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
      <!-- iCheck -->
      <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
      <!-- Google recaptcha -->
      <script src="https://www.google.com/recaptcha/api.js" async defer></script>
   </head>
   <body class="hold-transition login-page">
      <div class="login-box">
         <!-- /.login-logo -->
         <div class="login-box-body">
            <div class="col-md-12">
               <div class="signin-page-logo align-center"><img src="images/ulo-logo.png" height="100px" width="200px" class="img-responsive"/ ></a>
               </div>
            </div>
            <p class="login-box-msg">Sign in to start your session</p>
            <s:form id="myForm" action="login.action" method="post" theme="simple" onsubmit ="return validate()">
               <div class="form-group has-feedback">
                  <input type="text" name="username" id="username" class="form-control" placeholder="Email">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>
               <div class="form-group has-feedback">
                  <input type="password" name="password"  id="password" class="form-control" placeholder="Password">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
               </div>
               <s:if test="hasActionErrors()">
                  <div id="password_error_message" style="color:red;font-weight:normal;">
                     <s:iterator value="actionErrors">
                        <s:property/>
                     </s:iterator>
                  </div>
               </s:if>
               <s:if test="%{#session['countcap']>3}">
                  <div id="captchashow">
                     <table>
                        <tr>
                           <td>
                              <div class="g-recaptcha" data-sitekey="6LdrlSAUAAAAANGKJe_wASqoEv3fjw-94WdlPAPs"></div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <!-- <input type="text" id="txtInput" value=""/>  -->   
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <s:property value="#sessionMap.count" />
                           </td>
                        </tr>
                     </table>
                  </div>
               </s:if>
               <div class="row">
                  <div class="col-xs-8">
                     <div class="checkbox icheck">
                        <label>
                        <input type="checkbox"> Remember Me
                        </label>
                     </div>
                  </div>
                  <!-- /.col -->
                  <div class="col-xs-4">
                     <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                  </div>
                  <!-- /.col -->
               </div>
            </s:form>
            <a href="forgetpassword.jsp">I forgot my password</a><br>
         </div>
         <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
      <!-- jQuery 2.1.4 -->
      <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
      <!-- Bootstrap 3.3.5 -->
      <script src="bootstrap/js/bootstrap.min.js"></script>
      <!-- iCheck -->
      <script src="plugins/iCheck/icheck.min.js"></script>
      <script>
         $(function () {
           $('input').iCheck({
             checkboxClass: 'icheckbox_square-blue',
             radioClass: 'iradio_square-blue',
             increaseArea: '20%' // optional
           });
         });
      </script>
      <script>
         function validate(){
         	
         	
         	var username = document.getElementById("username").value;
         	var password = document.getElementById("password").value;
         	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
         	
         	
         	if(username == "")
             {
                alert( "Please enter username!" );
                document.getElementById("username").focus(); 
                return false;
             }
         	
         	if(!username.match(mailformat))  
         	{  
         		alert("You have entered an invalid email address!");
         		return false; 
         		document.getElementById("username").focus();  
         	    
         	}  
         	
            if(password == "")
             {
                alert( "Please enter passowrd!" );
                document.getElementById("password").focus(); 
                return false;
             }
         	
         	
           if($('#captchashow').is(":visible"))
         	{
         	    var captcha = document.getElementById("g-recaptcha-response").value;
         		if(captcha === null || captcha.length === 0 ) {
         		alert("please check the captcha");
         			return false;
         			} 
         	
         	}
         	
         	else{
         		
         		return true;  
         	}
         	
         	}
         
         
         
         
         
          function Captcha(){
           
           
          canvas = document.getElementById("myCanvas");
          ctx=canvas.getContext("2d");
          ctx.font="30px Comic Sans MS";
          ctx.fillStyle = "";
         
         ctx.textAlign = "center";
         ctx.clearRect(0, 0, canvas.width, canvas.height);
         
                              var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
                              var i;
                              for (i=0;i<4;i++){
                                var a = alpha[Math.floor(Math.random() * alpha.length)];
                                var b = alpha[Math.floor(Math.random() * alpha.length)];
                                var c = alpha[Math.floor(Math.random() * alpha.length)];
                                var d = alpha[Math.floor(Math.random() * alpha.length)];
                                var e = alpha[Math.floor(Math.random() * alpha.length)];
                                var f = alpha[Math.floor(Math.random() * alpha.length)];
                                var g = alpha[Math.floor(Math.random() * alpha.length)];
                               }
                             //var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
                             
         code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e;
             
         ctx.fillText(code, canvas.width/2, canvas.height/2);
                     }
                           function ValidCaptcha(){
         
                               var string1 =code.replace(/ /g,'');
                               var string2 = (document.getElementById('txtInput').value).replace(/ /g,'');
         
                               if (string1 == string2){
                                 
                                 return true;
                               }
                               else{        
                                 return false;
                               }
                           }
                           function removeSpaces(string){
                             return string.split(' ').join('');
                           }
         
         
         
                           $(document).ready(function () {
                         	  Captcha();
                          });
         
      </script>  
   </body>
</html>
