<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
    My Profile
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Profile</li>
           <!--  <li class="active">New</li>  -->
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        	
        	<div class="row">
        	 
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                 <li class="active"><a href="#fa-icons" data-toggle="tab">My Profile</a></li>
                 
                </ul>
                <div class="tab-content">
                 
                 
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
                  	<section id="secimage">
                  		<div class="row fontawesome-icon-list">
                      	<div class="col-xs-12">
                      	 <div class="box box-widget widget-user-2">
                  		<div class="widget-user-header bg-yellow">
                  			<div class="widget-user-image">
                    			<img class="img-circle"  ngf-select="upload($file)" ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" src="profile-picture.action"  alt="User Avatar">
                    		<!-- 	<input type="File" name="myFile"  id="myFile" accept=".png,.jpg"  style="display:none;"  />  -->
                    			
                  			</div><!-- /.widget-user-image -->
                 			 <h3 class="widget-user-username">{{singleuser[0].o_username}}</h3>
                  			<h5 class="widget-user-desc">My Profile</h5>
                		</div>
                		</div>
                		</div>
                		</div>
                  	</section>
                  	<form name="myuser" ng-repeat="us in user">
            
                      <div class="row fontawesome-icon-list" >
                     
                        <div class="col-md-4 col-sm-4">
                        	<div class="form-group">
                      			<label for="firstname">User Name</label>
                      			<input type="text" class="form-control" name="userName" ng-required="true"  id="userName" value="{{us.userName}}"  placeholder="firstname">
                    		</div>
						</div>
						
                         <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<!-- <label for="lastname">User Id</label> -->
                      			<input type="hidden" disabled class="form-control" name="userId" id="userId" ng-required="true" value="{{us.userId}}" placeholder="">
                    		</div>
                        </div>
                    <!--     <div class="col-md-3 col-sm-4">
                        	 <div class="form-group">                          		
                     			 <input type="text" class="form-control" id="roleId" name="roleId" value="<s:property value="user.roleId"/>" placeholder="" >
                    			</div>
                 			 </div>      --> 
                 			    
                      </div>
              

                  
                      <h4 class="page-header">Contact Details</h4>
	                      <div class="row fontawesome-icon-list" >
	                        <div class="col-md-4 col-sm-4">
                        	<div class="form-group">
                      			<label for="address1">Address 1</label>
                      			<input type="text" class="form-control" name="address1"  id="address1" ng-required="true" value="{{us.address1}}" placeholder="address">
                    		</div>
						</div>
	                        <div class="col-md-4 col-sm-4">
	                        	<div class="form-group">
	                      			<label for="address2">Address 2</label>
	                      			
	                      			<input type="text" class="form-control" name="address2"  id="address2" ng-required="true" value="{{us.address2}}" placeholder="address">
	                    		</div>
	                        </div>
	                        
	                    
	                          
						 <div class="col-md-4 col-sm-4">
                        	<div class="form-group">
                      			<label for="phone">Phone</label>
                      			<input type="text" class="form-control" name="phone"  id="phone" ng-required="true" value="{{us.phone}}" placeholder="phone">
                    		</div>
						</div>
						
				<!-- 	 <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      		 <label for="email">enail</label>  
                      			<input type="hidden" class="form-control" name="emailId"  id="emailId" value="<s:property value="user.emailId"/>" placeholder="email">
                    		</div>
						</div>    -->
					</div>
				
                  	           
                  	           
                  	   
                  	   <section id="video-player">
                      <div class="row fontawesome-icon-list">
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4">
                             <a class="btn btn-warning" href="#addmodal" ng-click="enableTask(x.DT_RowId)"  data-toggle="modal" >change password</a>
                         	<button type="submit"  ng-click="editFamilyUser()" class="btn btn-primary ">Save</button> 
                      
                        </div>
                        
                    
                       
                      </div>
                    </section>
                  	</form>
					
							
                    <div class="modal" id="addmodal">
                <form method="post" theme="simple">   
			<div class="modal-dialog">
			
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">change password</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
						<input type="hidden" name="userId" id="userId" value="<s:property value="user.userId"/>" /> </input>
						</div>
						<div class="form-group">
							<label>New password</label>
							<input type="password" class="form-control" ng-required="true" name="newPassword"  id="newPassword"  placeholder="Newpassword">
						</div>
					  	<div class="form-group">
							<label>Confirm password</label>
							<input type="password" class="form-control" ng-required="true"  name="confirmPassword"  id="confirmPassword"  placeholder="ConfirmPassword">
						</div> 
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
					 	<button type="submit" ng-click="changepassword()" class="btn btn-primary">Save</button>  
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			</form>
			
			<!-- /.modal-dalog -->
		</div>
			
         

                    
              

                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            
              	  
              	
              	  
              	  	
			   
			   	</div>

              	 
              
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        
       
          
          
        </section><!-- /.content -->
      
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
		var app = angular.module('myApp',['ngFileUpload']);
		app.controller('customersCtrl',['$scope',
		             					'Upload',
		            					'$timeout',
		            					'$http', function($scope,Upload,$timeout,$http) {

			
			
			$("#alert-message").hide();
			
			
			
			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
		    //$scope.progressbar.start();
			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);
	       	
	       	$scope.unread = function() {
				var notifiurl = "unreadnotifications.action";
				$http.get(notifiurl).success(function(response) {
					$scope.latestnoti = response.data;
				});
				};

				
				
				
				$scope.getUserPhotos = function() {
					//var userid = document.getElementById("userId").value;
					var url = "user-picture";
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.photos = response.data;
					
					});
				};
				
				 $scope.getUserProfile = function() {
	                    //var userid = document.getElementById("userId").value;	                    
	                    var url = "get-user-profile";
	                    $http.get(url).success(function(response) {
	                    	  
	                    	//console.log(response);
	                        //alert(JSON.stringify(response.data));
	                        $scope.user = response.data;
	                   
	                    });
	                };    
								
				
				$scope.editFamilyUser=function(){
					
					
					//alert ("userId="+$('#userid').val());
					var fdata ="userId="+$('#userId').val()
					+"&userName="+$('#userName').val()
					+"&phone="+$('#phone').val()
					+"&address1="+$('#address1').val()
					+"&address2="+$('#address2').val();
					
					//+"&roleId="+$('#roleId').val()
					//+"&emailId="+$('#emailId').val();
					
					//alert(userName);
					//alert(fdata);
					
					$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'edit-family-user'
					}).then(function successCallback(response) {
							//alert("update successfully");
						

							//console.log($scope.singleuser);
					}, function errorCallback(response) {
						
						alert("not updated");
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				};
				
				
				$scope.changepassword=function(){
					
					var newpass = document.getElementById("newPassword").value;
			    	var confirmpass = document.getElementById("confirmPassword").value;
			    	var passpattern = /(?=.*\d)(?=.*[@#$%])(?=.*[a-z])(?=.*[A-Z]).{8,}/;
			    	
			    	if(newpass=="") {
				     //   alert("New password is required");
				        return false;
				      }
			    	
			    	 if(!passpattern.test(newpass)) {
			    	 //       alert("Password should have atleast 8 characters length,with 1 uppercase,1 special character and 1 number");
			    	      
			    	        return false;
			    	  }
			        
			    	 if(newpass != confirmpass) {
			 	        alert("password doesnot match!");
			 	        return false;
			 	      }
			    	 
					//alert ("userId="+$('#userid').val());
					var fdata = "userId="+$('#userId').val()
					+"&newPassword="+$('#newPassword').val()					
					+"&confirmPassword="+$('#confirmPassword').val();
					
					
					alert(fdata);
					
					$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'changeuserpassword'
				
						
					});  
					//alert("password update successfully");
				};
				
				
	
		       	// upload on file select or drop
		        $scope.upload = function (file) {
		        	var userid = document.getElementById("userId").value;
		        	
		            Upload.upload({
		                url: 'userpicupdate',
		                enableProgress: true, 
		                data: {myFile: file, 'userId': userid} 
		            }).then(function (resp) {
		            	
		            	//$scope.getUserPhotos();
		                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
		                //$scope.getPropertyPhotos();
		            }, function (resp) {
		                console.log('Error status: ' + resp.status);
		            }, function (evt) {
		            	
		                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		                
		                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		            });
		           
		            
		        };
		        
                 
		        $scope.getUserProfile();
			//$scope.unread();
		        //$scope.editFamilyUser(); 

		       
		}]);
		
	
	</script>
	
	
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
	
  
    

	 
       