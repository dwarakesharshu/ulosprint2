                       <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
table.table-bordered > tbody > tr > th {
//border:1px solid #3c8dbc ;
}
table.table-bordered  {
  // border:1px solid #3c8dbc ;
/*     margin-top:20px; */
 }
table.table-bordered > thead > tr > th{
   //border:1px solid #3c8dbc ;
}
table.table-bordered > tbody > tr > td{
   //border:1px solid #3c8dbc ;
}
.tablesuccess{
    background-color: #092f53 !important;
    color:#ffffff !important;
    text-align:center;
}
</style>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           
            <small>Room management</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        
            <li class="active">Room Management</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	 <div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Availability</a></li>
                  
                  <!-- <li ><a href="#tab-confirmation-summary" data-toggle="tab">Confirmation Summary</a></li> -->
                </ul>
                <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
        <section class="content">
        <div class="row">
          <div class="row">
        <form name="bookingForm">
                 <div class="form-group col-md-3">
                <label>Check-in:</label>

                     <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckin">
                   <span class="fa fa-calendar"></span>
              </label>   
                  <input type="text" id="bookCheckin" type="text" class="form-control" name="bookCheckin"  value="">
 
                  
                </div>
                <!-- /.input group -->
              </div>
                        <div class="form-group col-md-3">
                <label>Check-out:</label>

                <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckout">
                   <span class="fa fa-calendar"></span>
              </label>   
                  <input type="text" id="bookCheckout" type="text" class="form-control" name="bookCheckout"  value="">
 
                  
                </div>
                <!-- /.input group -->
              </div>
               <div class="form-group col-md-3">
							<label> Select Accommodation</label>
							<select name="propertyAccommodationId" ng-model="propertyAccommodationId" id="propertyAccommodationId" value="" class="form-control select2" multiple="multiple">
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
						  </div>
              <div class="col-md-2">
              <button type="button" ng-click="getRoomAvailability()" class="btn btn-block btn-primary btn-sm" style="margin-top:25px">Search</button>
             </div>
             
   
        </form>
        
            </div>
            
            	<div class="table-responsive no-padding"  > 
								<table class="table table-bordered" id="example1">
									<h4>Availability</h4>
									<tr class="tablesuccess">
										
										<th>Type</th>										
										<th>Available</th>
										<th>Adult</th>
										<th>Child</th>
									
										<th>Price</th>
										<th>Tax</th>
										<th>Select Room</th>
										<th>Action</th>
										
									</tr>
									<tr ng-repeat="av in availablities track by $index">
									
										<td ng-model="accommodationType">{{av.accommodationType}}</td>
										<td>{{av.available}}</td>
 										<td><input type="text" class="form-control"  name="noofAdults" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index)"  id="noofAdults"  placeholder="adult" ng-model='noofAdults' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="text" class="form-control"  name="noofChilds" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index)"  id="noofChilds"  placeholder="child" ng-model='noofChilds' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
                                     <!--  <td><input type="text" class="form-control"  name="noofInfant" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index)" id="noofInfant"  placeholder="Infant" ng-model='noofInfant' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td> --> 
									    <td><input type="text" class="form-control"  name="priceAmount" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index)" id="priceAmount"  placeholder="price" ng-model='priceAmount' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>									    
									    <td><input type="text" class="form-control"  name="taxAmount" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index)" id="taxAmount"  placeholder="tax" ng-model='taxAmount' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><select  ng-model="ad.value" id="rooms" class="form-control" ng-change="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index)"><option ng-repeat="ad in range(1,av.available)" value="{{ad}}">{{ad}}</option></select></td>
									   <td><button class="btn btn-xs btn-primary" ng-click ="removeRoom($index)">Remove</button></td>
									    <!--<td><select ng-model="r.value" ng-change="update(r.value,$index)"><option ng-repeat="r in range(1,1)" value="{{r}}">{{r}}</option></select></td>
									    <!--  Selected Value is:{{rValue}}-->
									   
									    <!-- <td><a class="btn btn-xs btn-primary" href="#addmodal" ng-disabled="av.available == 0"  ng-click="addRow(av.accommodationId,av.accommodationType,av.baseAmount,av.arrivalDate,av.departureDate,av.rooms,av.noOfAdults,av.noOfChild,av.minOccupancy,av.extraAdult,av.extraChild,av.tax,av.available,$index)"  data-toggle="modal" min="0" >Add</a></td> -->
									    
									</tr>
									<tr>
								
										<!-- <td>Sub Total <br> 100rs</td>
										<td>Grand Total <br> 100rs</td>
									    <td>Deposit <br> 100rs</td> -->
									</tr>
								</table>
							</div>
								
					<!-- 		    <div class="table-responsive no-padding" style="display:none;" > 
								<table class="table table-bordered " id="finalData" >
									
									<tr class="tablesuccess">
										<th>Type</th>
										<th>Rooms</th>
										<th>Adult</th>
										<th>Child</th>
										<th>Infant</th>	
										<th>Tax</th>						
										<th>Price</th>
										
									</tr>
									<tr id="tr-{{$index}}" ng-repeat="rs in roomsSelected track by $index">
										<td>{{rs.accommodationType}}</td>
									   <td>{{rs.rooms}}</td>
									   <td>{{rs.adultsCount}}</td>
									   <td>{{rs.childCount}}</td>
									   <td>{{rs.infantCount}}</td>	
									   <td>{{rs.tax}}</td>
									   <td>{{rs.total}}</td>
									
									
									</tr>
								
								</table>
								
							</div>  -->
							 
						
              	     <form name="addguestinformation">
              	     <h4>Guest Info</h4>
					<div class="modal-body" >
					<div class="row">
					<div id="message"></div>
					<div class="form-group col-sm-3">
							<label>Source</label>
							<select   name="sourceId"  ng-model="sourceId" id="sourceId" placeholder="Select Source" class="form-control " ng-required="true">
							<option>Select Source</option>
							<option ng-repeat="sr in sources" value="{{sr.sourceId}}" ng-selected ="sr.sourceId == r.sourceId">{{sr.sourceName}}</option>
							</select>
						</div>	
					
					   <div class="form-group col-sm-3">
							<label for="firstName">Name</label>
							<input type="text" class="form-control"  name="firstName"  id="firstName"  placeholder="Name" ng-model='firstName' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
						    <span ng-show="addguestinformation.firstName.$error.pattern" style="color:red">Enter The Valid First Name</span>
						</div> 
						
								<div class="form-group col-sm-3">
							<label for="guestEmail">E-mail</label>
							<input type="email" class="form-control"  name="guestEmail"  id="guestEmail"  placeholder="Enter Your Email" ng-model='guestEmail' ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">
						    <span ng-show="addguestinformation.guestEmail.$error.pattern" style="color:red">Enter The Valid Email ID!</span>
						     
						</div>
								
								<div class="form-group col-sm-3">
							<label for="landlinePhone">Mobile</label>
							<input type="text" class="form-control"  name="mobileNumber"  id="mobileNumber"  placeholder="Enter The Mobile Number " ng-model='mobileNumber' ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="20">
						    <span ng-show="addguestinformation.landlinePhone.$error.pattern" style="color:red">Enter the Valid Phone Number!</span> 
						</div>
								
						</div>
                             
						</div>
					</div>
					<div class="modal-footer">
					  
				     <button ng-click="confirmBooking()" ng-disabled="addguestinformation.$invalid"  class="btn btn-primary nextBtn">Block</button> <!-- ng-disabled="addguestinformation.$invalid" -->
					</div>
					</form>
        
            </div>
        				
				
    
                        
          
          
        </section><!-- /.content -->
                
                      
                        

                </div><!-- /.tab-content -->
                <!-- guest information begins -->
              	  

              	
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
          </div>
        
        
          
          
        </section><!-- /.content -->
      
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
 <script>
 $('.datepicker').datepicker({
	    dateFormat: 'dd-mm-yy'
	 });
 </script>
	<script>
    
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		$("#bookCheckin").datepicker({
            dateFormat: 'mm/dd/yy',
          
            minDate:  0,
            onSelect: function (formattedDate) {
                var date1 = $('#bookCheckin').datepicker('getDate'); 
                var date = new Date( Date.parse( date1 ) ); 
                date.setDate( date.getDate() + 1 );        
                var newDate = date.toDateString(); 
                newDate = new Date( Date.parse( newDate ) );   
                $('#bookCheckout').datepicker("option","minDate",newDate);
                $timeout(function(){
                  //scope.checkIn = formattedDate;
                });
            }
        });

        $("#bookCheckout").datepicker({
            dateFormat: 'mm/dd/yy',
          
            minDate:  0,
            onSelect: function (formattedDate) {
                var date2 = $('#bookCheckout').datepicker('getDate'); 
                $timeout(function(){
                  //scope.checkOut = formattedDate;
                });
            }
        }); 
		 $('.select2').select2()
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

		
		
		$scope.getCategories = function() {
   
			var url = "get-categories";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.categories = response.data;
	
			});
		};
		
		
		$scope.getTaxes = function() {
			
			var url = "get-taxes";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.taxes = response.data;
	
			});
		};
		
	   $scope.getItems = function() {
       
			var url = "get-items";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.items = response.data;
	
			});
		};
		
	
		
		
       $scope.getRoomAvailability = function(){
			
			
			//alert($('#propertyItemCategoryId').val());
			
			var fdata = "&arrivalDate=" + $('#bookCheckin').val()
			+ "&departureDate=" + $('#bookCheckout').val()
			+ "&accommodationTypeId=" + $('#propertyAccommodationId').val()
             
			//alert(fdata);
			
			$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'get-room-availability'
					}).success(function(response) {
						
						$scope.availablities = response.data;
						
						  var indx = 0;
                          //$scope.roomsSelected = [];
                          //addRow(av.accommodationId,av.accommodationType,av.baseAmount,av.arrivalDate,av.departureDate,av.rooms,av.noOfAdults,av.noOfChild,av.minOccupancy,av.extraAdult,av.extraChild,av.available,$index,av.tax)      
                       
                          for (var i = 0; i < $scope.availablities.length; i++) 
                             {
                             var dt = $scope.availablities[i];
                           // alert(dt);
                             $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,dt.rooms,dt.noOfAdults,dt.noOfChild,dt.noOfInfant,dt.minOccupancy,dt.extraAdult,dt.extraInfant,dt.extraChild,dt.available,i,dt.tax);    
                             //alert($scope.addRow);
                            
                             }  
                          //console.log($scope.booked[0].firstName);
					
					});

		};
		
		
		
		$scope.getBooking = function (id) {
		
			$scope.indexval = id;
	        
		};
		
		$scope.getTableData = function () {
		
			
	   
		};
		
		
       $scope.confirmBooking = function(){
    	   
    	  // alert($scope.roomsSelected1);
    	   var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
    	  
			var data = JSON.parse(text);
			//alert(data);
			 var sourceId =  $('#sourceId').val()
			 //var sourceId = parseInt(sourceid);
			 //alert(sourceId);
			$http(
					{
						method : 'POST',
						data : data,
						//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
						dataType: 'json',
						headers : {
							 'Content-Type' : 'application/json; charset=utf-8'

						},
						//var url = "get-user-properties?userId="+userId;
						url : "add-booking?sourceId="+sourceId
					}).then(function successCallback(response) {
						
						
						var gdata = "firstName=" + $('#firstName').val() 
						+ "&emailId=" + $('#guestEmail').val()
						+ "&phone=" + $('#mobileNumber').val()
						
						
						$http(
					{
						method : 'POST',
						data : gdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'add-guest'
						
					}).then(function successCallback(response){
						
						var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
						//alert(text);
						var data = JSON.parse(text);
						$http(
							    {
								method : 'POST',
								data : data,
								//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
								dataType: 'json',
								headers : {
									 'Content-Type' : 'application/json; charset=utf-8'
									//'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'add-booking-details'
								//url : 'jsonTest'
							   }).then(function successCallback(response) {
								  // $scope.booked = response.data;
								   alert("added successfully");
								   window.location.reload(); 
							   
							   })
						
					})
					})
					
       };
		
		$scope.range = function(min, max, step){
		    step = step || 1;
		    var input = [];
		    for (var i = min; i <= max; i += step) input.push(i);
		    return input;
		 };
		 
		
		 
		 $scope.addRoom = function(id,type,available,adult,child,infant,price,tax,rooms,indx){
			//(id);
			var type = type.replace(/\s/g,'');
	         var id = parseInt(id);
	         var available = parseInt(available);
	         var adult = parseInt(adult);
	         var child = parseInt(child);	         
	         var price = parseInt(price);
	         var tax = parseInt(tax);
	         //var sourceId = parseInt(source);
	         var rooms = parseInt(rooms);
	        //alert(sourceId);
	         $scope.roomsSelected[indx].rooms = rooms; 
	         $scope.roomsSelected[indx].adultsCount = adult; 
	         $scope.roomsSelected[indx].childCount = child;
	         $scope.roomsSelected[indx].infantCount = infant;
	         $scope.roomsSelected[indx].total = price; 
	         $scope.roomsSelected[indx].tax = tax; 
	         //$scope.roomsSelected[indx].sourceId = sourceId; 
			 
			 
		 }

		
        
         $scope.roomsSelected = [];
         // shiva add addrom tax
         $scope.addRow = function(id,type,baseAmount,arrival, departure,rooms,adultsAllow,childAllow,infantAllow,minOccu,extraAdult,extraChild,tax,available,indx){	
        //alert(indx);
        // var Availableee = parseInt(available);
         //alert(Availableee);
        // alert(available);
          //var available = 0;
         if(available == 0){
        	
     	// alert("Rooms is not available");
     	  //$("#submitBtn").attr("disabled","disabled");
     	    //btn.disabled = true;
			 console.log("Disable");
         }
         else{
        	 
        
         var type = type.replace(/\s/g,'');
         var id = parseInt(id);
         var minOccu = parseInt(minOccu);
         var extraAdult = parseInt(extraAdult);
         var extraChild = parseInt(extraChild);         
         var adultsAllow = parseInt(adultsAllow);
         var childAllow = parseInt(childAllow);        
         var amount = parseFloat(baseAmount);
         var rooms = parseInt(rooms);
         var date1 = new Date(arrival);
         var date2 = new Date(departure);
         var timeDiff = Math.abs(date2.getTime() - date1.getTime());
         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
         var randomNo = Math.random();         
         var taxes = tax * diffDays;
         //alert(tax);
         var total = (diffDays*rooms*amount);
        //shiva add newData tax:taxes
         var newData = {'arrival':arrival,'indx':indx,'departure':departure,'accommodationId':id,'accommodationType':type,'available':available, 'rooms': rooms,'diffDays':diffDays, 'baseAmount':amount, 'total':total ,'adultsAllow':adultsAllow,'childAllow':childAllow,'minOccu':minOccu,'extraAdult':extraAdult,'extraChild':extraChild,'adultsCount':0,'childCount':0,'infantCount':0,'refund':0,'statusId':2,'tax':taxes,'randomNo':randomNo};
         //alert(newData);
               $scope.roomsSelected.push(newData);
        	   console.log($scope.roomsSelected);
        	   $scope.availablities[indx].available = $scope.availablities[indx].available;
        	
        	   return false;
        	   
       
         }	
         };
        	
          $scope.getTotal = function(){
        	    var total = 0;
        	    for(var i = 0; i < $scope.roomsSelected.length; i++){
        	        var accommodation = $scope.roomsSelected[i];
        	        total += (accommodation.total);
        	    }
        	    return total;
        	} 	
          
          $scope.getRoomsTotal = function(){
      	    var totalRooms = 0;
      	    for(var i = 0; i < $scope.roomsSelected.length; i++){
      	        var roomsCount = $scope.roomsSelected[i];
      	        totalRooms += parseInt(roomsCount.rooms);
      	    }
      	    return totalRooms;
      	  } 	
		
          $scope.removeRoom = function(index){
        	 
        	   
        	 //$scope.availablities[indx].available = parseInt($scope.availablities[indx].available) + 1; 
        	
              $scope.availablities.splice(index,1); 
              $scope.roomsSelected.splice(index,1); 
              //alert($scope.availablities[indx]);
        	 //$scope.availablities[id].available = 1 ; 
        	 //alert($scope.roomsSelected[indx]);
        	 
        	
        	} 	
          
		 $scope.getPropertyList = function() {
			 
	        	var userId = $('#adminId').val();
	 			var url = "get-user-properties?userId="+userId;
	 			$http.get(url).success(function(response) {
	 			    
	 				$scope.props = response.data;
	 	
	 			});
	 		};
	 		
        $scope.change = function() {
 	   
	        //alert($scope.id);
	        
	        var propertyId = $scope.id;	
 	        var url = "change-user-property?propertyId="+propertyId;
 			$http.get(url).success(function(response) {
 				
 				 window.location = '/ulopms/dashboard'; 
 				//$scope.change = response.data;
 	
 			});
		       
	 		}; 
      
        
/*    $scope.getBookedDetails = function(){
			
			//alert("hai");
			//alert($('#bookingId').val());
			//$('#bookingId').val();
			var bookingId = '10';
			var fdata = "bookingId=" + bookingId; 
			
             
			
			
			$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'get-booked-details'
					}).success(function(response) {
						
						$scope.booked = response.data;
						//console.log($scope.booked[0].firstName);
						
						
					});

		}; */
		
		$scope.getSources = function() {

			var url = "get-sources";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.sources = response.data;
	
			});
		};
		
		$scope.getAccommodations = function() {

			var url = "get-accommodations";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.accommodations = response.data;
	
			});
		};
			
	    $scope.getPropertyList();
	   
	    //$scope.getTotal();
	    
	    //$scope.getTotalRooms();
	    
		$scope.getTaxes();
		$scope.getCategories();
		
		$scope.getItems();
		$scope.getBooking();
		
        $scope.getSources();
        $scope.getAccommodations();
        //$scope.getBookedDetails();
		//$scope.unread();
		//
        
		
	});

	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
>
	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
	     <script>
  /* next and previous button */
 $('.nextBtn').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.previousBtn').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
  </script>
   <style>
 .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: green;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: default;
    float: left;
    margin-right: 5px;
    margin-top: 5px;
    padding: 0 5px;
}
 </style>
