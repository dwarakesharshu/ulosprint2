<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

 <%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"

    pageEncoding="ISO-8859-1"%>

      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    
    <style>
    
  
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }
        
        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
  

        .tcolumn

        {

            text-align:left;

        }

    </style>

    

<div id="wrapper">

    <section id="page-header" class="section background">

        <div class="container">

            <div class="row">

                <div class="col-sm-12">

                    <ul class="c1 breadcrumb text-left">

                        <li><a href="index">Home</a></li>

                        <li>Successful Payment</li>

                    </ul>

                    <input type="hidden" value ="<%=session.getAttribute("bookingId") %>" id="bookingId" name="bookingId" >

                    <h3>SUCCESSFUL PAYMENT</h3>

                     <div class="tab-pane" id="tab-confirmation-summary">
                   <div class="row">
                   <div class="invoice-box"  style="background:#fff;font-size:14px;max-width:800px;margin:auto;padding:30px;border:1px solid #000;max-width:800px;margin:auto;padding:30px;border:1px solid #eee;box-shadow:0 0 10px rgba(0, 0, 0, .15);font-size:16px;line-height:24px;font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;color:#555;">
        <table cellpadding="0" cellspacing="0" style="width:100%;line-height:inherit;text-align:left;" >
            <tr class="top">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left;">
                        <tr>
                            <td class="title">
                                <img src="https://ulohotels.com/ulowebsite/images/logo.png" style="width:100%; max-width:200px;">
                            </td>
                            <td style="text-align:right;">
                               <h2 style="color:#88ba41;text-transform:uppercase;word-wrap: break-word;vertical-align: middle;">Booking Invoice</h2>
                            </td>
                          
                        </tr>
						<hr>
                    </table>
                </td>
            </tr>
            
            <tr class="informations">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left;border-bottom:1px solid #000">
                        <tr>
                            <td>
                                <b>Voucher No: {{booked[0].bookingId}}</b>
                            </td>
                            
                            <td style="text-align:right;">
                              <b>GST : 33AABCU9979Q2Z8<br>CIN : U55100TN2016PTC113073<br>PAN : AABCU9979Q</b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
			      <tr class="informations">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left; border-bottom:1px solid #000" >
						  <h3 align="middle" style="color:#155d90">Guest Booking Information</h3>
                        <tr>
                            <td style="text-align:center;">
                               <b><span style="color:#155d90">Name:</span> {{booked[0].firstName}}<br><span style="color:#155d90">Mobile:</span> {{booked[0].phone}}<br><span style="color:#155d90">Booking Id:</span>{{booked[0].bookingId}}<br><span style="color:#155d90">Check-in:</span>{{booked[0].arrivalDate}} |<span style="color:#155d90"> Check-out:</span>{{booked[0].departureDate}}</b>
                            </td>
                            
                            <td style="text-align:center;" >
                              <b><span style="color:#155d90">Property Name: </span>{{booked[0].propertyName}}<br><span style="color:#155d90">No Of Nights:</span>{{booked[0].days}}<br><span style="color:#155d90">Meal Plan:</span>Regular<br><span style="color:#155d90">Adults</span>:{{booked[0].adultCount}} |<span style="color:#155d90"> Child:</span>{{booked[0].childCount}}</b>
                            </td>
                        </tr>
							
                    </table>
                </td>
            </tr>

        </table>
	         
		          <table style="width:100%;line-height:inherit;text-align:left;padding:10px; "   >
			
            <tr style=" background:#eee;font-weight:bold;">
                <th width="35%" style="color:#155d90">
                    Particulars
                </th>
                
                <th width="25%"  style="color:#155d90">
                   Rate
                </th>
				 <th width="25%"  style="color:#155d90">
                    Qty
                </th>
                
                <th width="15%"  style="color:#155d90">
              Total
                </th>
				
            </tr>
			      <tr >
				
                <th style="font-weight:normal;" >
                 {{booked[0].accommodationType}}
                </th>
                
                <th style="font-weight:normal;">
            {{booked[0].accommodationRate}}
                </th >
				 <th style="font-weight:normal;" >
               {{booked[0].rooms}}
                </th>
                
                <th style="font-weight:normal;" >
             {{booked[0].amount}}
                </th>
            </tr>

    <tr >

       <td style=" color:#155d90; " >Sub-Total</td>
      <td style=" color:#155d90; "></td>
      <td style="color:#155d90;" >GST</td>
        <td style="font-weight:normal;" >{{booked[0].tax}}</td>
    </tr>
	
	
   <tr  style="background:#eee;font-weight:bold; ">
   <td style="color:#155d90;">Total</td>
      <td></td>
      <td></td>
      
        <td>{{booked[0].totalAmount}}</td>
        
		<input type ="hidden" value="{{booked[0].firstName}}" id="customerName" name="customerName">
		<input type ="hidden" value="{{booked[0].totalAmount}}" id="totalAmount" name="totalAmount">
		<input type ="hidden" value="{{booked[0].tax}}" id="totalTax" name="totalTax">
		
		
    </tr>
             </table>
		
			 <table  style="width:100%;line-height:inherit;text-align:left;">
						 <hr>
                        <tr> <h6 align="middle" style="color:#000">This is computer Generated Voucher.No Signature Required</h6>
						</tr>
						</table>
									 <table  style="width:100%;line-height:inherit;text-align:left;">
						 
                        <tr> <h2 align="middle" style="color:#155d90">Thank You For Choosing Ulo Hotels!</h2>
						</tr>
						</table>
									 <table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41">
						 
                          <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                                <b><span style="color:#fff" >Mobile:</span>  +91 9543 592-593 <span style="color:#fff"> Email:</span> <span style="color:#155d90">Support@ulohotels.com </span></b>
                            </td>
                              </tr>
							    <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                              CORPORATE OFFICE: 56,B2,Oyster Apartment, 4th Avenue,19th Street, Ashok Nagar, Chennai, Tamil Nadu 600083
                            </td>
                        </tr>
						</table>
					<!-- 	 <table  style="width:100%;line-height:inherit;text-align:left;">
						<tr>
						<td align="middle" style="font-size:14px;color:#000000;">
						 <a href="#" onclick="return show('Page2','Page1');">Show page 2</a>
						</td>
						</tr>
						</table> -->
						 
    </div>
                   </div>
                 

               

                            

                    <div class="modal-footer">

                    <div class="col-lg-12">

                    

                     <button  onclick="javascript:window.print();" class="btn btn-primary nextBtn" id="printer">Print</button>

                    </div>

                    

                    </form>

                    </div>

                  </div>

                </div>

            </div><!-- end row -->

        </div><!-- end container -->

    </section><!-- end section -->

    <section class="section clearfix pad-bot">

        <div class="container">

            <div class="row">

                <div id="fullwidth" class="col-sm-12">

                    <!-- START CONTENT -->

                    <div class="row">

                        <div id="content" class="col-md-12 col-sm-12 col-xs-12">

                            <div class="post-wrapper text-center clearfix">

                                <div class="successful">

                                   <!--   <img src="ulowebsite/images/successful.png" alt="">-->

                                </div>

                                <h6>Dear customer,</h6>

                                <p>Thank you very much for ordering our product. You will be receiving an e-mail within next 72 hours, with the attachment or instructions to download.<br>

                                For any problems please <a href="mailto:reservations@ulohotels.com ">reservations@ulohotels.com </a> </p>

                                <a href="index" class="btn btn-primary btn-normal btn-lg">CONTINUE</a>

                            </div><!-- end post-wrapper -->

                        </div><!-- end col -->

                    </div><!-- end row -->

                    <!-- END CONTENT -->

                </div><!-- end fullwidth -->

            </div><!-- end row -->

        </div><!-- end container -->

    </section>

</div>

 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>

<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

<script>

        var app = angular.module('myApp',['ngProgress']);

        app.controller('customersCtrl',function($scope, $http,$timeout,ngProgressFactory,$location) {

            

            //$scope.progressbar = ngProgressFactory.createInstance();

            //$scope.progressbar.setColor("green");

            //$scope.progressbar.start();

            

           //   $timeout(function(){

                // $scope.progressbar.complete();

           //     $scope.show = true;

           //     $("#pre-loader").css("display","none");

           // }, 2000);

            

           var bookingId = $('#bookingId').val();

                

                  

           $scope.getBookedDetails = function(){

                

                //alert("hai");

                //alert($('#bookingId').val());

                //$('#bookingId').val();

                

                var fdata = "bookingId=" + bookingId; 

                

                 

                

                

                $http(

                        {

                            method : 'POST',

                            data : fdata,

                            headers : {

                                'Content-Type' : 'application/x-www-form-urlencoded'

                            },

                            url : 'get-booked-details'

                        }).success(function(response) {

                            

                            $scope.booked = response.data;

                            console.log($scope.booked[0].firstName);

                            

                            

                        });

            };

                

                

                

            $scope.getBookedDetails();

            

                    

                        

                       

                        

             

                        

        });

        

    </script>

     <!--<script src="js/jquery-2.1.1.js" type="text/javascript"></script>-->

      <script src="ulowebsite/js/jquery.min.js"></script>

    


<!-- Google Code for Payments Conversion Page -->
<script>
  window.addEventListener('load',function(){
    if(window.location.pathname.indexOf('paymentsuccess')!=-1){
      var value = document.querySelector('#totalAmount').getAttribute('value');alert(value);
      (new Image()).src='//www.googleadservices.com/pagead/conversion/844448474/?value='+value+'&currency_code=INR&label=arp1CKvdzHQQ2oXVkgM&guid=ON&script=0';
    }
  })
</script>

<script>
window.addEventListener('load',function(){
    if(window.location.pathname.indexOf('paymentsuccess')!=-1){
var id = document.querySelector('#bookingId').getAttribute('value');
var name = document.querySelector('#customerName').getAttribute('value');
var tax = document.querySelector('#totalTax').getAttribute('value');
var total = document.querySelector('#totalAmount').getAttribute('value');
//alert(id);
//alert(name);
//alert(tax);
//alert(total);

dataLayer.push({
		'event':'ecomm_event',
    'transactionId': id, // Transaction ID - this is normally generated by your system.
    'transactionAffiliation': name, // Affiliation or store name
    'transactionTotal': total, // Grand Total
    'transactionTax': tax , // Tax.
    'transactionShipping':'0' // Shipping cost
    
});
    }
})
</script>










    
 
