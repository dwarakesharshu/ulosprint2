<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Ulotel</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
   </head>
   <body class="hold-transition lockscreen">
      <!-- Automatic element centering -->
      <div class="lockscreen-wrapper">
         <div class="lockscreen-logo">
            <a href="https://www.ulohotels.com"><b>Ulo</b> Hotels</a>
         </div>
         <!-- User name -->
         <div class="lockscreen-name">
            <s:property value="%{#user.userName}"/>
         </div>
         <!-- START LOCK SCREEN ITEM -->
         <div class="lockscreen-item">
            <div class="box box-default">
               <div class="box-footer no-padding">
                  <ul class="nav nav-pills nav-stacked">
                     <s:iterator value="listPmsProperty" var="p" >
                        <li>
                           <s:a action="change-user-property?propertyId=%{#p.propertyId}">
                              <s:property value="%{#p.propertyName}"/>
                           </s:a>
                        </li>
                     </s:iterator>
                  </ul>
               </div>
               <!-- /.footer -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.lockscreen-item -->
         <div class="text-center">
            <a href="login">Or sign in as a different user</a>
         </div>
         <div class="lockscreen-footer text-center">
            Copyright &copy; 2017 <b><a href="#" class="text-black">Ulohotels</a></b><br>
            All rights reserved
         </div>
      </div>
      <!-- /.center -->
      <!-- jQuery 2.1.4 -->
   </body>
</html>