<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="<s:property value="%{metaDescription}"/>">
    <meta name="author" content="">

    <title><s:property value="%{metaTag}"/></title>

    <!-- Favicons -->
    <link rel="shortcut icon" href="ulowebsite/images/favicon.png" type="image/png" />
    <link rel="apple-touch-icon" href="ulowebsite/images/favicon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="ulowebsite/images/favicon.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="ulowebsite/images/favicon.png" />

    <!-- Bootstrap -->
    <link href="ulowebsite/css/bootstrap.css" rel="stylesheet">
    <!-- Default Styles -->
    <link href="ulowebsite/css/style.css" rel="stylesheet">
    <link href="ulowebsite/css/newstyle.css" rel="stylesheet">
    <!-- Custom Styles -->
    <link href="ulowebsite/css/custom.css" rel="stylesheet">
    <link href="ulowebsite/css/jquery-ui.css" rel="stylesheet">
    <link href="ulowebsite/css/lightslider.css" rel="stylesheet">   

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<!--     <link href="ulowebsite/rs-plugin/css/settings.css" rel="stylesheet">
       <link href="ulowebsite/rs-plugin/css/settings.css" rel="stylesheet">
    -->
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M3RH7N5');</script>
<!-- End Google Tag Manager -->
    
</head>
<body ng-app="myApp" ng-controller="customersCtrl">
<div id="loader">

   <div class="loader-container">

       <h3 class="loader-back-text"><img src="ulowebsite/images/loader.gif" alt="" class="loader"></h3>

   </div>

</div>
<div id="wrapper">
           <div class="topbar">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7 logoindex">
                     <a href="index" style="padding:10px;">
                     <img src="ulowebsite/images/logo.png" alt="ulo-logo"></a>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2 headerbtn" style="border-right:1px dotted #DCDCDC;" >
                     <s:if test="#session.userId == null">
                        <a href="customer-login" class="btn btn-primary btn-xs  top-btn" style="padding:10px;margin-top:19px;margin-right:15px;margin-bottom:10px">login</a>
                        <a href="customer-signup" class="btn btn-primary btn-xs  top-btn" style="padding:10px;margin-top:19px;margin-bottom:10px">Signup</a>
                     </s:if>
                     <s:if test="#session.userId != null">
                        <div class="dropdown">
                           <button class="dropbtn" ><i class="fa fa-user"></i>&nbsp; <i class="icon-down-open-mini"></i></button>
                           <div class="dropdown-content">
                              <p>Hi! <%=session.getAttribute("userName") %></p>
                              <br>
                              <a href="ulouserprofile">Profile</a>
                              <a href="ulologout">Logout</a>
                           </div>
                        </div>
                     </s:if>
                  </div>
                  <div class="col-xs-12 col-sm-12 hidden-lg hidden-md">
                   <div class="col-xs-12 col-sm-12">
                        <span style="color:#114b70;font-size:14px;"><i class="fa fa-envelope" style="color:#88ba41;"></i>&nbsp;support@ulohotels.com  </span>
                     </div>
                     <div class="col-xs-12 col-sm-12">
                        <span style="color:#114b70;font-size:14px;"><i class="fa fa-phone" style="color:#88ba41;"></i>&nbsp; 9543 592 593 </span>
                     </div>
                  </div>
                  <div class="hidden-xs hidden-sm col-md-3 col-lg-3 ulocontact">
                     <span><i class="fa fa-envelope" style="color:#88ba41;"></i>&nbsp;support@ulohotels.com </span> <br>
                     <span><i class="fa fa-phone" style="color:#88ba41;"></i> &nbsp;+91-9543 592 593</span>
                  </div>
               </div>
            </div>
            <!-- end container -->
         </div>
         <!-- end topbar -->



    

    <section class=" clearfix background-grey">
        <div class="container">
              <div class="row">
                 <div class="col-md-12 col-xs-12">
                    <div class="form">
                       <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab_01">
                              
                                <form name="searchForm" class="bookform form-inline row"  action="get-search-form" method="post">
                                    <div class="form-group col-md-4 col-sm-6 col-xs-12"  >
                                           <select class="form-control" data-style="btn-white" id="location" name="locationId" style="padding:0px;" ng-required="true">
                                             <option style="display:none;" value="">Select Your Location</option>
                                          <option  style="padding:20px !important;background-color:#222222;color:#ffffff;"  list="location" value="{{l.DT_RowId}}" ng-repeat="l in location"  ng-selected ="<%=request.getParameter("locationId") %> == {{l.DT_RowId}}">{{l.locationName}}</option>
                                           </select>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                  
                                                 <div class="input-group date">
                   <label class="input-group-addon btn" for="datepicker">
                   <span class="fa fa-calendar"></span>
              </label>   
                   <input type="text" class="form-control" value="<%= ((session.getAttribute("checkIn1")==null)?"":session.getAttribute("checkIn1")) %>" placeholder="Check in" id="datepicker" name="arrivalDate1"  ng-required="true"  >
                   <input type="hidden" class="form-control" value="<%= ((session.getAttribute("checkIn")==null)?"":session.getAttribute("checkIn")) %>" placeholder="Check in" id="alternate" name="arrivalDate"  ng-required="true"  >  
 
                </div>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                  
                                                                   <div class="input-group date">
                   <label class="input-group-addon btn" for="datepicker1">
                   <span class="fa fa-calendar"></span>
              </label>   
                <input type="text" class="form-control"  value="<%= ((session.getAttribute("checkOut1")==null)?"":session.getAttribute("checkOut1")) %>" placeholder="Check out" id="datepicker1" name="departureDate1"  ng-required="true" >
               <input type="hidden" class="form-control"  value="<%= ((session.getAttribute("checkOut")==null)?"":session.getAttribute("checkOut")) %>" placeholder="Check out" id="alternate1" name="departureDate"  ng-required="true" >  
 
                </div>
                                    </div>
                     
                                    <div class="form-group col-md-2 col-sm-6 col-xs-12">
                                       <!--  <a href="properties" value="submit" type="submit" class="btn btn-primary btn-block" ng-click="searchForm.$valid" ng-disabled="searchForm.$invalid"><i class="icon-search"></i></a> -->
                                          <button type="submit" class="btn btn-primary btn-block"  >Modify Search</button>
                                    </div>
                                </form>
                            </div><!-- end tab-pane -->


                        </div><!-- end tab-content -->
                    </div><!-- end homeform -->
                </div><!-- end col -->
         
            </div><!-- end row -->
            <div class="row">    
            <div class="col-md-12">
            <form>
                <div class="row well" ng-repeat="p in properties">
                  <div class="mini-destit row">
                        <div class="col-md-4">
                       <a href="details?propertyId={{p.DT_RowId}}"><img src="get-property-thumb?propertyId={{p.DT_RowId}}" alt="" class="img-responsive img-thumbnail"> </a>
                        </div><!-- end col -->
                        <div class="col-md-6">
                        <div class="col-lg-12">
                        <a href="details?propertyId={{p.DT_RowId}}"><h4 style="color:#88ba41">{{p.propertyName}}</h4></a>
                           
                        </div>
                        <div class="col-lg-12">
                            <div >
                                         <i style="color:#cc6601;" class="fa fa-star" title="3 Star Property"></i>
                                        <i style="color:#cc6601;" class="fa fa-star" title="3 Star Property"></i>
                                        <i  style="color:#cc6601;"class="fa fa-star" title="3 Star Property"></i>
                                    </div> 
                        <h6 ><i class="fa fa-map-marker"></i> <span style="text-transform: capitalize !important;">{{p.city}}</span></h6>
                         
                        </div>
                    
                            <div class="col-lg-12 col-md-12 col-sm-12">
      
                                    <p> <span style="color:#114b70;font-weight:bold;">AMENTIES :</span><span ng-repeat="amm in p.amenities" style="text-transform: capitalize;font-weight:bold;" >
                                   		<i class="{{amm.icon}}" style="colr:#"> {{amm.amenityName}},</i>
                                   		&nbsp;
                                     </span>
                                     </p>
                                      <p><span style="color:#114b70;font-weight:bold;"> ROOM CATEGORIES :</span> <span ng-repeat="accom in p.accommodation" style="text-transform:capitalize!important;font-weight:bold;  ">
                                         {{accom.accommodationName}} {{$last ? '' : ($index==p.accommodation.length-2) ? ' , ' : ', '}}
                                      </span></p>
                                  
                                </div>
                       
                         
                        </div>
                        <div class="col-md-2">
                                     
                                <h3> &#2352; {{p.accommodation[0].baseAmount | currency:"":0}}  </h3>
                                <a href="details?propertyId={{p.DT_RowId}}" class="btn btn-success btn-style">Book Now</a>
      
                        </div>
                        
                        <!-- end col -->
                    </div>
                   </form>
                </div>
                
               <!--  <input type="text" value = "<%= session.getAttribute("departureDate") %>" id="departureDate" name="departureDate" >
               <input type="text" value = "<%=session.getAttribute("arrivalDate") %>" id="arrivalDate" name="arrivalDate" > -->
              <!--  <input type="hidden" value ="<%=request.getParameter("locationId") %>" id="locationId" name="locationId" >  -->
               <s:hidden type="hidden" value ="%{locationId}" id="locationId" name="locationId" />
               <!--<s:textfield key="metaTag" value="%{metaTag}" />--> 
                <!--<div class="row well">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                  <img class="pull-left" src="http://placehold.it/200x200">
                    <h4>&nbsp;&nbsp;Treebo Srico</h4><br>
                      <h6>&nbsp;&nbsp;Banjara Hills, Hyderabad</h6>
                      <a href="#" class="btn btn-success pull-right"> Quick Book</a>
                   </div>
                </div>-->

            </div>
            </div><!-- end fullwidth -->
         
            <div class="content_scoller " >
               <p><s:property value="%{webContent}"/></p>
               </div>
            </div><!-- end row -->
     
    </section><!-- end section -->



    
   
    
    <!-- start of footer -->
    <footer class="footer clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-3 col-xs-12">
                    <div class="widget">
                        <div class="widget-title">
                            <h3>GET IN TOUCH</h3>
                        </div><!-- end title -->
                        <div class="textwidget">
                            <p>
                                56,B2,Oyster Apartment, <br>
                                4th Avenue,19th Street,<br>
                                 Ashok Nagar,Chennai,<br>
                                Tamil Nadu 600083<br>
                                India<br>
                                <i class="fa fa-envelope"></i> reservations@ulohotels.com<br>
                                 <i class="fa fa-phone"></i> +91-9543 592 593</p>
                        </div><!-- end textwidget -->
                    </div><!-- end widget -->
                </div><!-- end col -->
                <div class="col-md-6 col-sm-5 col-xs-12">
                    <div class="widget ">
                        <div class="widget-title">
                            <h3>OUR USEFUL LINKS</h3>
                        </div><!-- end title -->
                        <div class="row">
                            <div class="col-md-6">
                        <div class="newsletterwidget">
                            <ul style="list-style-type:none;">
                               <!-- <li><a href="index.php">Home</a></li>-->
                                 
                             <li><a href="aboutulo">About</a></li>
                             
                                <li><a href="privacy">Privacy policy</a></li>
                               <li><a href="termsandconditions">Terms & conditions</a></li>
                                <li><a href="refundterms">Cancellation And Refund Policy</a></li>
                                <li><a href="team">Our Team</a></li>
                               
                            </ul>
                        </div>
                                </div>
                            <div class="col-md-6">
                        <div class="newsletterwidget">
                          <ul style="list-style-type:none;">
                             
                              <li><a href="partner-with-us">Partner With us</a></li>
                              <li><a href="ulo-faq">Frequently Asked Questions</a></li>
                              <li><a href="careers-ulo">Careers</a></li>
                              <li><a href="contactus">Contact</a></li>
                              
                             
                          </ul>
                        </div><!-- end newsletter widget -->
                            </div>
                    </div><!-- end widget -->
                        </div>
                </div><!-- end col -->
                  <div class="col-md-2 col-sm-3 col-xs-12 text-center">
                    <div class="">
                        <img src="ulowebsite/images/logo-ft.png" alt="">
                    </div><!-- end logo -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </footer><!-- end copyrights -->
    <div class="copyrights clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <p>	&copy; 2017 Ulo Hotels Private Limited, All Rights Reserved.</p>
                </div><!-- end col -->

                <div class="col-md-6 text-right">
                    <div class="social-footer">
                        <a href="https://www.facebook.com/Ulohotels/" title="Facebook"><i class="icon-facebook"></i></a>
                        <a href="https://twitter.com/ulohotels" title="Twitter"><i class="icon-twitter"></i></a>
                        <a href="https://www.pinterest.com/ulohotels/" title="Pinterest"><i class="icon-pinterest"></i></a>
                        <a href="https://goo.gl/ucr1n8" title="google-plus"><i class="icon-gplus"></i></a>
                    </div><!-- end social-footer -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end copyrights -->
</div><!-- end wrapper -->
</body>
	    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	    <!-- jQuery UI 1.11.4 -->
	    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
 <script src="js1/ngprogress.js" type="text/javascript"></script>
    <!--<script src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
    <script async defer    
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg9dqRsKwRWXGdTKsUNHTHhfQqdSyip9k&callback=initMap">
    </script>
    <script src="ulowebsite/js/bootstrap.min.js"></script>
    <script src="ulowebsite/js/retina.js"></script>
    <script src="ulowebsite/js/sidebar.js"></script>
    <script src="ulowebsite/js/circle.js"></script>
    <script src="ulowebsite/js/progress.js"></script>
    <script src="ulowebsite/js/jquery.prettyPhoto.js"></script>
    <script src="ulowebsite/js/contact.js"></script>
    <script src="ulowebsite/js/parallax.js"></script>
    <script src="ulowebsite/js/owl.carousel.js"></script>
    <script src="ulowebsite/js/bootstrap-select.js"></script>
    <script src="ulowebsite/js/custom.js"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
        <script src="ulowebsite/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
    <script src="ulowebsite/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="ulowebsite/js/revslider.js"></script>
<script src="ulowebsite/js/lightslider.js"></script>

 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>


<script>

		var app = angular.module('myApp',['ngProgress']);
		app.controller('customersCtrl',function($scope, $http,$timeout,ngProgressFactory,$location) {

			
			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
		    //$scope.progressbar.start();
			
	       //	$timeout(function(){
	      	    // $scope.progressbar.complete();
	       //     $scope.show = true;
	       //     $("#pre-loader").css("display","none");
	       // }, 2000);
			var from = document.getElementById("datepicker");
		    var to = document.getElementById("datepicker1");
		    
		   if(document.getElementById("datepicker").value == ""){
		       var today = new Date();
		      
		       var dd = today.getDate();
		       var MM = today.getMonth()+1;
		       var yy = today.getFullYear();

		       today = dd+'/'+MM+'/'+yy;
		       // alert(today);    
		       from.value = (today);
		       var today = new Date();
		       var dd = today.getDate()+1;
		       var MM = today.getMonth()+1;
		       var yy = today.getFullYear();
		        today1 = dd+'/'+MM+'/'+yy;
		      // alert(today1);
		       to.value = (today1);
		       
		   }    
		   
		   
		   
		   var from = document.getElementById("alternate");
		    var to = document.getElementById("alternate1");
		    
		     if(document.getElementById("alternate").value == ""){
		         
		    var today = new Date();
		    
		   var dd = today.getDate();
		   var mm = today.getMonth()+1;
		   var yy = today.getFullYear();

		   today = mm+'/'+dd+'/'+yy;
		   // alert(today);    
		    from.value = (today);
		    var today = new Date();
		    var dd = today.getDate()+1;
		   var mm = today.getMonth()+1;
		   var yy = today.getFullYear();
		    today1 = mm+'/'+dd+'/'+yy;
		  // alert(today1);
		   to.value = (today1); 
		    
		   }
		    
		     $("#datepicker").datepicker({
	                dateFormat: 'dd/mm/yy',
	                 altField: "#alternate",
	                altFormat: "mm/dd/yy", 
	                minDate:  0,
	                onSelect: function (formattedDate) {
	                    var date1 = $('#datepicker').datepicker('getDate'); 
	                    var date = new Date( Date.parse( date1 ) ); 
	                    date.setDate( date.getDate() + 1 );        
	                    var newDate = date.toDateString(); 
	                    newDate = new Date( Date.parse( newDate ) );   
	                    $('#datepicker1').datepicker("option","minDate",newDate);
	                    $timeout(function(){
	                      //scope.checkIn = formattedDate;
	                    });
	                }
	            });

	            $("#datepicker1").datepicker({
	                dateFormat: 'dd/mm/yy',
	                 altField: "#alternate1",
	                altFormat: "mm/dd/yy",
	                minDate:  0,
	                onSelect: function (formattedDate) {
	                    var date2 = $('#datepicker1').datepicker('getDate'); 
	                    $timeout(function(){
	                      //scope.checkOut = formattedDate;
	                    });
	                }
	            });
			      // var locationId = $('#locationId').val();
			       

			      var fdata = "&arrivalDate="+ $('#alternate').val()
          + "&departureDate=" + $('#alternate1').val()
          + "&locationId=" + $('#locationId').val()
         
          //alert(fdata);
        
                   
                  
                $scope.getProperties = function() {
                    
                     $http(
                             {
                                method : 'POST',
                                data : fdata,
                                headers : {
                                    'Content-Type' : 'application/x-www-form-urlencoded'
                                },
                               url : "get-location-properties"
                             }).success(function(response) {
                        //console.log(response);
                        //alert(JSON.stringify(response.data));
                        
                        $scope.properties = response.data;
                        
                        
                    });
                };
				
				
				
				
	         /* $scope.getSearchForm = function() {
					
					var url = "get-search-form?locationId="+locationId;
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.searchform = response.data;
					
					});
				};
				
				$scope.getSearchForm();	 		
			       */
			   
			   	$scope.getLocation = function() {
						
						var url = "get-location";
						$http.get(url).success(function(response) {
							//console.log(response);
							//alert(JSON.stringify(response.data));
							$scope.location = response.data;
						  
							
						});
					};
					
				 		
				       
				    	
			    $scope.getLocation();   
			       
		        $scope.getProperties();
						

		});

		
	</script>

    <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
      <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/586f59d212631a10690bdfa8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 

      <style>
         .mar-b10 i{
    margin-bottom: 10px;
}
        .bold{
            font-weight:bold;
        }  


 p{
font-size:15px;
}
     .background-grey{background:#f1f1f1 ;padding-top:10px;}
     .background-white{#fff}
   .well{
       background-color: #fff;
   border-radius: .2rem;
   box-shadow: 0 0.2rem 0.4rem 0 rgba(0,0,0,.16);
   }
   .margin-bot{margin-bottom:10px;}
   .btn-style{
           font-family: roboto,Helvetica Neue,Helvetica,Arial,sans-serif;
           background-color: transparent;
           color: #fafafa ;
       
           box-shadow: none;
 
           
           text-align: center;
           text-decoration: none;
           text-transform: uppercase;
           white-space: nowrap;
   }
   .btn-style:hover{
                   color: #fff!important;
                   background-color: #88ba41 ;
                   border-color: #88ba41 ;
   }
@media (max-width:347px){
.res h6{
font-size:13px;
}
}
   .mini-desti-title p{
       margin-bottom: 0;
   }
   .mini-desti-title{
   padding:5px;
   }
   .content_scoller {
   height: 45px;
   width: 100%;
   border: 1px solid #f1f1f1!important  ;
    /* border-bottom: 1px solid #C0C0C0 !important; */
   overflow: auto;
   color: #f1f1f1;
   font-size: 11px;
   margin: 0 0 15px;
 overflow: hidden;
   }


        
  </style>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M3RH7N5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

</html>