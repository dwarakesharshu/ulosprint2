<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<style>
    
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
  display: none !important;
}
    
    .banner{
        height: 700px;
    }
/*- - - - - - - - - - - - - - - - - - - - 
	room
- - - - - - - - - - - - - - - - - - - - */
    .inner h4{
        font-size: 15px;
    }
    .room {
    margin-bottom:30px;
    background:#edb83a;
}
.selectpicker{

width:100%!important;
}
.room h3 {
    text-align:center;
    margin-bottom:20px;
}

.room img {
}

.room .text {
    color:#333;
    padding:20px 30px 30px;
}

.room-specs {
    padding:20px;
    border-bottom:solid 5px #ddd;
}

.room-specs ul {
    list-style:none;
    margin:0;
    padding:0;
}

.room-specs ul li {
    border-bottom:solid 1px #ddd;
    padding-bottom:5px;
    margin-bottom:5px;
}

.room-specs ul li:before {
    list-style:none;
    color:#EDB83A;
    content:open-quote;
    font-size:14px;
    line-height:.1em;
    margin-right:10px;
    font-family:FontAwesome;
    content:"\f046";
}

.room-info h4 {
    margin-top:0;
}

.room .price,.room .btn-book-now {
    width:50%;
    float:left;
    text-align:center;
    color:#fff;
    padding:20px 0;
}

.room .price {
    background:rgba(0,0,0,.1);
}

.room .btn-book-now {
    background:rgba(0,0,0,.15);
    text-decoration:none;
}

.room .btn-book-now:hover {
    background:#655233;
    text-decoration:none;
}

.room-single .room-info,.room-single .room-specs {
    padding:0;
    border-bottom:none;
    background:none;
}

.room-single .room-specs {
    margin-top:20px;
    margin-bottom:20px;
}

.room-single .inner {
    padding:10px 40px;
    background:#eee;
    width:100%;
    margin-top:20px;
    margin-right: 20px;
}

.room-single .room-price {
    color:#fff;
    background:#EDB83A;
    padding:5px 10px 4px;
    display:inline-block;
}

.room-single .btn-book {
    padding:7px 10px 6px;
}

.room-single .room-price span {
    font-size:22px;
    font-weight:700;
}

.room-item img {
    width:100%;
    margin:0;
    padding:0;
    height:100%;
}

.room-item h1 {
   
    text-transform:uppercase;
    padding:0;
    margin:0;
    font-size:16px;
    letter-spacing:3px;
}

.room-item .overlay {
    position:absolute;
    bottom:0;
    background:#edb83a;
    padding:35px 30px 20px;
    width:100%;
    height:84px;
    text-align:center;
}

.room-item {
    position:relative;
    overflow:hidden!important;
}

.room-list .room-item {
    margin-bottom:30px!important;
}

.room-item .desc {
    height:0;
    color:#333;
    padding-left:30px;
    padding-right:30px;
}

.price {
    font-size:32px;
    color:#333;
   text-align: left;
    margin:30px 0;
}

.price span {
    font-size:14px;
}

.room-item-alt {
	border-bottom:solid 1px #eee;
	padding-bottom:30px;
	margin-bottom:30px;
}
.room-item-alt img{
	width:100%;
}

.room-item-alt h1{
	font-size:32px;
	margin-top:0;
}

.room-item-alt .room-specs{
border:none;
margin:0; padding-left:0;
}
.btn-border {
    border: solid 1px rgba(0,0,0,.3);
    background: none;
    padding: 5px 20px;
    
    text-decoration: none!important;
    }
    

  
    	
        .demo .item{
            margin-bottom: 60px;
        }
		.content-slider li{
		    background-color: #ed3020;
		    text-align: center;
		    color: #FFF;
		}
		.content-slider h3 {
		    margin: 0;
		    padding: 70px 0;
		}
		.demo{
			width:80%;
            margin: 0 auto;
		}
    
    a:hover, a > *:hover, a:focus{
        color: none;
    }
    
    
    
    .hotel-right-info h5{
        margin-bottom: 0;
        font-size:25px;
    text-transform: uppercase;
    }
        
    .hotel-right-info p{
        margin-bottom: 5px;
    }
    .hotel-info-top{
        margin-bottom: 10px;
    }
    .cl-gr{
        color: #808080;
        font-size: 13px;
    }
    .clr-grn{
        color: #88ba41!important;
    }
    /*.btn-width{
        width: 20%; 
       max-width: 20%;
        min-width: 20%;
        padding: 10px;
    }
   */
    .filter{
        display: inline-block;
        margin-right: 8px;
    }
    .filter p{
        margin: 0;
       color: #b1b1b1;
    font-size: 13px;
}
    .filter h6{
        text-transform: capitalize; 
        font-size:15px;
    }
    .filter{
        margin-top:10px;
    }
    .search-mod{
        margin-top: 10px;
        text-align:center;
    }
    
    .why-choose .col{
        background: #fff;
    box-shadow: 0 0.2rem 0.4rem 0 rgba(0,0,0,.16);
        padding: 30px;
            margin-bottom: 40px;
}
    .why-choose .col h6{
     font-size: 14px; 
        text-align: center;
        
    }
    .why-choose img{
     margin-left:30%;   
    }
    .box{
        border: 1px solid #88ba41;
        padding: 10px;
    }
    .box span{margin-right: 20px;}
    .icons{padding: 5px 10px;display: inline-block;text-align: center;}
    
    .room-price{
        margin-top: 35px;
    }
    .room-price h5{
        margin-bottom: 0;
    }
    .room-price em{
        font-size: 10px;
        margin: 0;
        padding: 0;
    }
    .review ul li{list-style-type: none;}
    .margin-top20px{margin-top:20px;}
    @media (max-width:991px){
        .box{
            text-align: center;
        }
    }
@media (max-width:338px){
  .filter h6 {
    text-transform: capitalize;
    font-size: 13px;
}
  .filter p{
        margin: 0;
       color: #b1b1b1;
    font-size: 11px;
}

}
.mar-top{margin-top:25px;}
    .left-mar{
        margin-left:10%;
    }
    .btn-style:hover{
        color: #fff!important;
    }
    

/* carosal */
   #carousel-custom {
   padding:10px;
   }
 #carousel-custom .carousel-indicators {
   margin: 10px 0px 0px!important;
   overflow: auto!important;
   position: static!important;
   text-align: left!important;
   white-space: nowrap!important;
   width: 100%!important;
   
}
#carousel-custom .carousel-indicators li {
   background-color: transparent!important;
   -webkit-border-radius: 0!important;
   border-radius: 0!important;
   display: inline-block!important;
   height: auto!important;
   margin: 0 !important;
   width: auto!important;
}#carousel-custom .carousel-indicators li img {
   display: block!important;
   /* opacity: 0.5!important; */
   
}#carousel-custom .carousel-indicators li.active img {
  /*  opacity: 1!important; */
}#carousel-custom .carousel-indicators li:hover img {
   opacity: 0.75!important;
}#carousel-custom .carousel-outer {
   position: relative!important;
}.thumb-preview {
   width: 100%!important;
}.thumbs {
   width: 100% !important;
   margin: 12px auto !important;
   background: #dde3e4
!important;
   padding: 10px 0px 6px 10px!important;
}.thumbs img {
   width: 81px!important;
   height: 50px!important;
}.carousel-inner .item > img{
height:340px !important;
}
.carousel-control.left {
   background: none;
}.carousel-control.right {
   background: none;
}.slider-mover-left {
   width: 40px;
   height: 40px;
   line-height: 32px;
   position: absolute;
   top: 45%;
   z-index: 5;
   display: inline-block;
   border-radius: 50%;
   left: 5px;
   border: solid 2px #1B1410
;
}.slider-mover-left img {
   width: 30px;
}.slider-mover-right {
   width: 40px;
   height: 40px;
   line-height: 32px;
   border-radius: 50%;
   position: absolute;
   top: 45%;
   z-index: 5;
   display: inline-block;
   right: 5px;
   border: solid 2px #1B1410
;
}.slider-mover-right img {
   width: 30px;
}
.actives{
background-color:#092f53!important;
}
</style>
 
<div id="wrapper">
<section id="page-header" class="">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <ul class="c1 breadcrumb text-left">
               <li><a href="index">Home</a></li>
               <li>Hotel Detail</li>
            </ul>
            <h3>{{property[0].propertyName}}</h3>
         </div>
      </div>
   </div>
</section> 
<section class="section clearfix ">
   <div class="container">
       <div class="row">
                 <div class="col-lg-12 col-md-12 col-xs-12">
                    <form name="myform">
                       <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab_01">
        
                                          <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                  
                                                 <div class="input-group date">
            
                                            <input type="text" class="form-control" value="<%= ((session.getAttribute("checkIn1")==null)?"":session.getAttribute("checkIn1")) %>" placeholder="Check in" id="datepicker" name="arrivalDate1"  ng-required="true">
                                            <input type="hidden" class="form-control" value="<%= ((session.getAttribute("checkIn")==null)?"":session.getAttribute("checkIn")) %>" placeholder="Check in" id="alternate" name="arrivalDate"  ng-required="true"> 
                            <label class="input-group-addon btn" for="datepicker">
                   <span class="fa fa-calendar"></span>
              </label>  
                </div>
                                    </div>
                                   <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                  
                                                                   <div class="input-group date">
                 
                                          <input type="text" class="form-control"  value="<%= ((session.getAttribute("checkOut1")==null)?"":session.getAttribute("checkOut1")) %>" placeholder="Check out" id="datepicker1" name="departureDate1"   ng-required="true" >
                                           <input type="hidden" class="form-control"  value="<%= ((session.getAttribute("checkOut")==null)?"":session.getAttribute("checkOut")) %>" placeholder="Check out" id="alternate1" name="departureDate"   ng-required="true" >
     <label class="input-group-addon btn" for="datepicker1">
                   <span class="fa fa-calendar"></span>
              </label> 
                                     </div>
                                    </div>
                     
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                       <!--  <a href="properties" value="submit" type="submit" class="btn btn-primary btn-block" ng-click="searchForm.$valid" ng-disabled="searchForm.$invalid"><i class="icon-search"></i></a> -->
                                          <button type="submit"  class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Modifying" ng-click="getFrontAvailability()">MODIFY SEARCH</button>
                                    </div>
                                
                            </div><!-- end tab-pane -->
                        </div><!-- end tab-content -->
                    </form><!-- end homeform -->
                </div><!-- end col -->   
            </div><!-- end row -->
   <div class="row pad-bot-30">
<div class="col-md-7">            
        <div class="simple-slider box">
                       <div id="carousel-custom" class="carousel slide" data-ride="carousel">
                           <div class="carousel-outer">
                               <!-- Wrapper for slides -->
                               <div class="carousel-inner">
              <div class="item" ng-class="{active:!$index}" ng-repeat="p in photos track by $index">  <img ng-src="get-property-image-stream?propertyPhotoId={{p.DT_RowId}}"  width="100%"  class="img-responsive">                       </div>
                               </div>
                               <!-- Controls -->
                               <a class="left carousel-control" href="#carousel-custom" role="button" data-slide="prev">
                                   <span class="slider-mover-left no-bg" aria-hidden="true">
                                      <img src="ulowebsite/images/chevron-left.png" alt="chevron-left">
                                   </span>
                                   <span class="sr-only">Previous</span>
                               </a>
                               <a class="right carousel-control" href="#carousel-custom" role="button" data-slide="next">
                                   <span class="slider-mover-right no-bg" aria-hidden="true">
                                      <img src="ulowebsite/images/chevron-right.png" alt="chevron-right">
                                   <span class="sr-only">Next</span>
                               </a>
                           </div>
                           <!-- Indicators -->
                           <ol class="carousel-indicators thumbs visible-lg visible-md"   >
                               <li data-target="#carousel-custom" ng-repeat="p in photos track by $index"  data-slide-to="{{$index}}" > <img ng-src="get-property-image-stream?propertyPhotoId={{p.DT_RowId}}" alt="Chevrolet Impala"></li>                            </ol>
                       </div>
                   </div>
    </div>
      <input type="hidden" value ="<%=session.getAttribute("arrivalDate") %>" id="arrivalDate" name="arrivalDate" >
      <input type="hidden" value ="<%=session.getAttribute("departureDate") %>" id="departureDate" name="departureDate" >
      <input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
      <div class="col-md-5 room-single pad-bot hotel-right-info">
       <div class="container">
            <div class="row hotel-info-top" ng-repeat="pr in property">
               <h5>{{pr.propertyName}}</h5>
               <p class="cl-gr">{{pr.city}}</p>
               <span class="rating ">
               <i class="fa fa-star"></i>
               <i class="fa fa-star"></i>
               <i class="fa fa-star"></i>
               <i class="fa fa-star"></i>
               <i class="fa fa-star"></i>
               </span>&nbsp;<span class="cl-gr clr-grn"> 5 Reviews</span><!-- end rating -->
               <a href="javascript:;" onclick="goToByScroll('mapview');" class="btn btn-primary btn-style margin-bot left-mar">View Map</a>
            </div>
 <!--            <div class="row" style="margin-bottom:4px;">
               <h5>&#2352;   {{ getTotal() }} </h5>
               {{accommodations[0].baseAmount}} 
                </div>
             <div class="row">
             <a href="bookingdetails" class="btn btn-primary btn-normal btn-lg">Book Now</a>
             <button type="button" class="btn btn-success btn-style" ng-click="getTypes()" >Book Now</button>
           
            </div> -->
            
            <div class="row">
            <div class="hotel-icon-list margin-top20px">
               
                  <div class="col-md-6"> 
                  
                     <div class="col-md-6 col-sm-6 col-xs-12" ng-repeat="amm in amenities" ng-if="amm.status=='true'">
                     	<span class="icon-container"><i style="position:relative; top: calc(50% - 10px);" class="{{amm.icon}}"></i></span> {{amm.amenityName}}
                    </div>
                     
                 </div>
                 
                  
               </div>
               <!-- end hotel-icon-list -->
            </div>
            
         </div>
               </div>
      <!----row end-->
   </div>

   <div class="row">
      <div class="col-md-6 hotel-title">
         <div class="container">
            <div class="container">
            <hr class="left">
            <h4>ROOM TYPES & FACILITIES</h4>
               <div class="row hotel-title" ng-repeat ="av in accommodations  track by $index"><!-- | orderBy:'baseAmount' -->
                  
                  
                  <div class="box">
					<div class="row">

                       <div class="col-md-3">

                          <img ng-src="accommodation-picture?propertyAccommodationId={{av.accommodationId}}" alt="" class="img-responsive img-thumbnail">

                       </div>
                      
                       <div class="col-md-8">
                       <div class="col-md-8">
                              <h5>{{av.accommodationType}} </h5>

                          <p>

                             <span><i class="fa fa-arrows-alt"> {{av.size}} Sqft</i></span>

                             <span><i class="fa fa-users"> {{av.minOccupancy}} Guests (Max {{av.maxOccupancy}} Adults)</i></span>

                          </p>
                       </div>
                     <div class="col-md-4  ">

                       <div class="col-md-6">

                            <h6>&#2352; {{av.baseAmount | currency:"":0}}</h6>

                          

                       </div><div class="col-md-6">

                        
                       <a type="button" class= "btn btn-danger btn-style" ng-show="av.available == 0 ">sold out</a>
                    <button type="button" ng-hide="av.available == 0 " class="btn btn-success btn-style"  class="btn btn-success btn-style" id="btn{{av.accommodationId}}"  ng-click="selectType(av.accommodationId,$index)" >Select Room </button>
						<h6>{{av.promotions}} </h6>
                       <!--  <a type="button" class= "btn btn-danger btn-style" ng-show="av.available == 0 ">sold out</a> -->
                           <input type='checkbox' name='type[]' style="visibility: hidden;"  value='{{av.accommodationId}}' id="{{av.accommodationId}}"  ng-click="manageTypes(av.accommodationId);"/>
                        </div>

                       </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                           <div class="icons">
                             
                              <p></p>
                           </div>
                            <div class="icons" ng-repeat="amm in av.amenities">
                              <img ng-src="{{amm.icon}}" width="30px" height="30px">
                              <p>{{amm.amenityName}}</p>
                            </div>
                        </div>
                     

                       </div>

                    </div>
                  
                  </div>
                
                  
               </div>
            </div>
             <div class="stream-content">
              <!-- <a href=""><i class="fa fa-close pull-right close"></i></a> -->
              <div class="row">
             <form>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 chk-details" >
       
               <img src="ulowebsite/images/details/roomlock.png" class="img-responsive" />
             <p>{{typeSelected.length}}</p>
              <img src="ulowebsite/images/details/rupees.png" class="img-responsive" />
             <p>{{getTotal()}}</p>
             
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <button type="button" class="btn btn-success chk-btn" ng-click="getTypes()" ng-disabDled="{{getTotal() == 0}}" >BookNow</button>
              </div>
              </form>
              </div>
             </div>
            <input type="hidden" id ="lat" name="lat" value="<s:property value="latitude" />">
            <input type="hidden" id ="lng" name="lng" value="<s:property value="longitude" />" >
            <div class="row">
               <div class="col-md-10">
                  <div class="row hotel-desc mar-top">
                     <div class="col-md-12">
                        <h5>ABOUT THE HOTEL</h5>
                        <hr class="left">
                        <p class="text-justify">{{property[0].propertyDescription}} </p>
                     </div>
                     <!-- end col -->
                  </div>
                  <!-- end hote-desc -->
              
                  <!-- end hotel-icon-list -->
                  <div id="mapview" name="mapview">
                  <div class="map-section"  style="margin-top: 20px;">
                     <h5>Location Map</h5>
                     <hr class="left">
                      <div id="map" style="height:250px;width:100%"></div>
                  </div>
                  </div>
                  <div class="hotel-title why-choose mar-top">
                    <h4>OUR 5 B's Services</h4>
                     <hr class="left">
                        </div>
                        <div class="row">
                <div class="col-md-12 ">
                    <div class="service-style">
                        <div class="border-radius col-md-4 col-sm-4 col-xs-12">
                            <img src="ulowebsite/images/fivebs/bestguest.png" height="70px" width="70px">
                              <h6>Best in Guest Service & Care</h6>
                        </div>
                  
                    </div><!-- end service -->

                    <div class="service-style">
                        <div class=" border-radius col-md-4 col-sm-4 col-xs-12">
                           <img src="ulowebsite/images/fivebs/bedclean.png" height="70px" width="70px">
                            <h6>Bed Clean & Comfortable</h6>
                        </div>
                     
                    </div><!-- end service -->
                     <div class="service-style">
                        <div class="border-radius col-md-4 col-sm-4 col-xs-12">
                            <img src="ulowebsite/images/fivebs/budgetfriendly.png" height="70px" width="70px">
                            <h6>Budget Friendly & Value</h6>
                        </div>
                     
                    </div>
                    <div class="col-md-2"></div>
                          <div class="service-style">
                        <div class=" border-radius col-md-4 col-sm-4 col-xs-12">
                          <img src="ulowebsite/images/fivebs/breakfast.png" height="70px" width="70px">
                             <h6>Breakfast Healthy & Regional</h6>
                        </div>
                 
                    </div>

                    <div class="service-style">
                        <div class=" border-radius col-md-4 col-sm-4 col-xs-12">
                         <img src="ulowebsite/images/fivebs/bathroom.png" height="70px" width="70px">
                         <h6>Bathroom hygienic & Functional</h6>
                        </div>
                     
                    </div>
                    <div class="col-md-2"></div>
                </div><!-- end col -->
                  </div>
                  <div class="review-widget clearfix">
                     <div class="hotel-title">
                        <h5>REVIEWS</h5>
                        <hr class="left">
                     </div>
                     <div class="row hotel-review">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                           <div class="review-list">
                              <h5>SERVICE:</h5>
                              <span class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                           </div>
                           <div class="review-list">
                              <h5>SLEEP QUALITY:</h5>
                              <span class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                           </div>
                           <div class="review-list">
                              <h5>LOCATION:</h5>
                              <span class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                           </div>
                           <div class="review-list">
                              <h5>SWIMMING POOL:</h5>
                              <span class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                           </div>
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                           <div class="review-list">
                              <h5>VALUE:</h5>
                              <span class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                           </div>
                           <div class="review-list">
                              <h5>CLEANLINESS:</h5>
                              <span class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                           </div>
                           <div class="review-list">
                              <h5>ROOMS:</h5>
                              <span class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                           </div>
                           <div class="review-list">
                              <h5>FITNESS FACILITY:</h5>
                              <span class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                           </div>
                        </div>
                        <!-- end col -->
                        <div class="col-md-4 col-sm-4 col-xs-12">
                           <div class="review-total text-center">
                              <h1>10/10</h1>
                              <span class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                           </div>
                           <!-- end review-total -->
                        </div>
                        <!-- end col -->
                     </div>
                     <!-- end hotel-review -->
                  </div>
                  <!-- end review widget -->
                  <div class="comments">
                     <ul class="media-list clearfix">
                        <li class="media clearfix">
                           <a class="pull-left" href="#">
                           <img class="media-object img-thumbnail" src="ulowebsite/images/clients/1.jpg" alt="Generic placeholder image" height="100px" width="100px">
                           </a>
                           <div class="media-body">
                              <h4 class="media-heading">Sabrees Kg</h4>
                              <p class="comment-meta pull-left">
                                 January 10, 2017 . 11:05 PM
                              </p>
                              <span class="rating pull-right">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                              <div class="clearfix"></div>
                              <p>"ULO Sms Residency Is a Peaceful Hotel in Kodaikanal. Hotel View Is Very Superb. Food Is Very Good. I Am Enjoyed That Weekend In Kodaikanal.."</p>
                           </div>
                        </li>
                        <li class="media clearfix">
                           <a class="pull-left" href="#">
                           <img class="media-object img-thumbnail" src="ulowebsite/images/clients/3.jpg" alt="Generic placeholder image" height="100px" width="100px">
                           </a>
                           <div class="media-body">
                              <h4 class="media-heading">Arun</h4>
                              <p class="comment-meta pull-left">
                                 April 30, 2017 . 10:05 AM
                              </p>
                              <span class="rating pull-right">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              </span><!-- end rating -->
                              <div class="clearfix"></div>
                              <p>"I went with my family to coorg for my vacation. We booked and stayed at ULO Alpinis Estate Resort. We enjoyed everything - well decorative rooms, excellent location and fresh food."</p>
                           </div>
                        </li>
                     </ul>
                  </div>
                  <!-- end comments -->
                 <!-- <div class="leave-a-feedback text-center clearfix">
                     <h6>PLEASE <a href="signin">LOGIN / REGISTER</a> TO LEAVE A FEEDBACK</h6>
                  </div>-->
                  <!-- end leave-a-feedback -->
                  <br>
                  <div class="related-hotels clearfix">
                     <div class="hotel-title">
                        <h5>Most Visited Destination</h5>
                        <hr class="left">
                     </div>
                     <div class="row">
                       <div class="col-md-4 col-sm-6 col-xs-12" ng-repeat="l in location | limitTo:3" >
                           <div class="post-wrapper clearfix ">
                              <div class="hotel-wrapper">
                                 <div class="post-media" class="ng-cloak" ng-cloak>
                                     <a href="properties?locationId={{l.DT_RowId}}">
                                     <img ng-src="get-location-picture?locationId={{l.DT_RowId}}" alt="" class="img-responsive">
                                     </a>
                                 </div>
                                 <!-- end media -->
                                 <div class="post-title clearfix">
                                    <div class="pull-left">
                                       <a href="properties?locationId={{l.DT_RowId}}"> <h5>{{l.locationName}} </h5></a>
                                    </div>
                                    <!-- end left -->
                                    <div class="pull-right">
                                       
                                    </div>
                                    <!-- end left -->
                                 </div>
                                 <!-- end title -->
                                 <span class="rating">
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                                 <i class="fa fa-star"></i>
                                 </span><!-- end rating -->
                                 <p>{{l.description}}</p>
                              </div>
                              <!-- end hotel-wrapper -->                            
                           </div>
                           <!-- end post-wrapper -->
                        </div>
                        <!-- end col -->
                        <!-- end col -->
                        
                        
                     </div>
                     <!-- end row -->
                  </div>
                  <!-- end related-hotels -->
               </div>
            </div>
         </div>
      </div>
</section>
</div>


  
<!-- end wrapper -->

     <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>
        var app = angular.module('myApp',['ngProgress']);
        app.controller('customersCtrl',function($scope, $http,$timeout,ngProgressFactory,$location,$interval) {
        	
        	$('.btn').on('click', function() {
        	    var $this = $(this);
        	  $this.button('loading');
        	    setTimeout(function() {
        	       $this.button('reset');
        	   }, 1000);
        	});
        	
        	$("#datepicker").datepicker
            
            ({
         
                 dateFormat: 'dd/mm/yy',
                 minDate:  0,
                 altField: "#alternate",
                 altFormat: "mm/dd/yy",
                 onSelect: function (formattedDate) {
                     var date1 = $('#datepicker').datepicker('getDate');
                     var date = new Date( Date.parse( date1 ) );
                     date.setDate( date.getDate() + 1 );        
                     var newDate = date.toDateString();
                     newDate = new Date( Date.parse( newDate ) );  
                     $('#datepicker1').datepicker("option","minDate",newDate);
                     $timeout(function(){
                       //scope.checkIn = formattedDate;
                     });
                 }
             });
           
             $("#datepicker1").datepicker({
                 dateFormat: 'dd/mm/yy',
                 minDate:  0,
                 altField: "#alternate1",
                 altFormat: "mm/dd/yy",
                 onSelect: function (formattedDate) {
                     var date2 = $('#datepicker1').datepicker('getDate');
                     $timeout(function(){
                       //scope.checkOut = formattedDate;
                     });
                 }
             });
          
             var from = document.getElementById("datepicker");
             var to = document.getElementById("datepicker1");
             
             if(document.getElementById("datepicker").value == ""){
                 var today = new Date();
                
                 var dd = today.getDate();
                 var MM = today.getMonth()+1;
                 var yy = today.getFullYear();

                 today = dd+'/'+MM+'/'+yy;
                 // alert(today);    
                 from.value = (today);
                 var today = new Date();
                 var dd = today.getDate()+1;
                 var MM = today.getMonth()+1;
                 var yy = today.getFullYear();
                  today1 = dd+'/'+MM+'/'+yy;
                // alert(today1);
                 to.value = (today1);
                 
             }    
             
             var from = document.getElementById("alternate");
             var to = document.getElementById("alternate1");
             
              if(document.getElementById("alternate").value == ""){

             var today = new Date();
             
             var dd = today.getDate();
             var mm = today.getMonth()+1;
             var yy = today.getFullYear();

             today = mm+'/'+dd+'/'+yy;
             // alert(today);    
              from.value = (today);
              var today = new Date();
              var dd = today.getDate()+1;
             var mm = today.getMonth()+1;
             var yy = today.getFullYear();
              today1 = mm+'/'+dd+'/'+yy;
            // alert(today1);
             to.value = (today1);
             
              }
              
              
           var propertyId = $('#propertyId').val();   
           var checkIn =  $('#datepicker').val();
           var checkOut =  $('#datepicker1').val();
            //$scope.progressbar = ngProgressFactory.createInstance();
            //$scope.progressbar.setColor("green");
            //$scope.progressbar.start();
            
           //   $timeout(function(){
                // $scope.progressbar.complete();
           //     $scope.show = true;
           //     $("#pre-loader").css("display","none");
           // }, 2000);
           
             $scope.getFrontAvailability = function(){
                 
                 
                 
                 
                 $scope.typeSelected = [];
                 $scope.accommodations = [];
                 var typeSeleted = new Array();
                 
       /*             if($('#datepicker').val() == ""){
                      $('#datepicker').focus();
                   }
                   
                   if($('#datepicker1').val() == ""){
                       
                       //$('#datepicker1').focus();
                       
                   } */
                   
       
                   //alert($('#propertyItemCategoryId').val());
                   //$scope.accommodations.remove;
                   
                 var fdata = "&arrivalDate=" + $('#alternate').val()
                 + "&departureDate=" + $('#alternate1').val()
                 + "&propertyId=" + $('#propertyId').val()
                
                // alert(fdata);
                
                 $http(
                         {
                             method : 'POST',
                             data : fdata,
                             headers : {
                                 'Content-Type' : 'application/x-www-form-urlencoded'
                             },
                             url : 'get-front-availability'
                         }).success(function(response) {
                            
                             $scope.accommodations = response.data;
                          
                            
                          
                         });             
                 };
             
                           
                      $scope.update = function(){
                              alert("dasd");
                       };
                           
                       $scope.selectType = function(id,indx){
                    	    
                           
                           //alert(btn.value);
                          // var  a = document.getElementById(btn).hide();
                           //alert(id);
                           //var x = document.getElementById("btn"+id).style.display='none'; 
                           
                           var text = document.getElementById("btn"+id).firstChild;
                           //text.data = text.data == "SELECT ROOM" ? "Selected" : "SELECT ROOM";
                            
                           if(document.getElementById(id).checked){
                           
                           
                           document.getElementById(id).checked = false;
                           
                           text.data = "SELECT ROOM";
                           document.getElementById("btn"+id).classList.remove("actives");
                           $scope.manageTypes(id);
                           
                           }
                           
                           else{
                               
                           text.data = "SELECTED";  
                           document.getElementById(id).checked = true;   
                           document.getElementById("btn"+id).classList.add("actives");
                           $scope.manageTypes(id,indx);
                           
                           }
                           //alert("hai");
                             
                           }
                
           $scope.manageTypes = function(id,indx){
              
               if(document.getElementById(id).checked){
                   
                   
                   var accId = parseInt($scope.accommodations[indx].accommodationId);
                   var available = parseInt($scope.accommodations[indx].available);
                   var amount = parseFloat($scope.accommodations[indx].baseAmount);
                   var extraAdult=parseFloat($scope.accommodations[indx].extraAdult);
                   var extraChild=parseFloat($scope.accommodations[indx].extraChild);
                   var extraInfant=parseFloat($scope.accommodations[indx].extraInfant);
                  
                  // var chkval = document.getElementById(id).value;
                   var newType = {'accommodationId':accId ,'baseAmount':amount,'available':available,'extraAdult':extraAdult,'extraChild':extraChild,'extraInfant':extraInfant};
                  
                    $scope.typeSelected.push(newType);
                    
                    console.log($scope.typeSelected);
                   
               } 
               else{
                   
                  //$scope.typeSelected.splice(id);
                   $scope.cleaner($scope.typeSelected,id);
                   //var chkval = document.getElementById(id).value;
                   //alert(chkval);
               }
              
                   
            };
            
            
            $scope.cleaner = function(arr,id){
                
           	 for (var i = 0; i < arr.length; i++) {
           	        var cur = arr[i];
           	        if (cur.accommodationId == id) {
           	            arr.splice(i, 1);
           	            break;
           	        }
           	    }
           	 
           };
                
           $scope.range = function(min, max, step){
                    step = step || 1;
                    var input = [];
                    for (var i = min; i <= max; i += step) input.push(i);
                    return input;
            };
            
          $scope.removeRoom = function(indx){
                   
                 $scope.roomsSelected.splice(indx);
                  
                  
                 }  
            
          
         
          
        
          
          $scope.getType = function(){
                
              var text = '{"types":' + JSON.stringify($scope.typeSelected) + '}';
              var data = JSON.parse(text);
                
                 $http(
                        {
                            method : 'POST',
                            data : data,
                            //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
                            dataType: 'json',
                            headers : {
                                 'Content-Type' : 'application/json; charset=utf-8'
                            },
                            
                            //url : 'bookingdetails'
                            url : 'get-type-selected'
                            
                        }).success(function(response) {
                            
                            //window.location = '/bookingdetails'; 
                            alert(response);
                            //$scope.type = response.data;
                            
                            //console.log($scope.booked[0].firstName);
                            
                            
                        });
              
            };
            
            
           $scope.getTypes = function() {
               
                var accommodationTypes = "&accommodationTypes="+'{"types":' + JSON.stringify($scope.typeSelected) + '}';
               
               //alert(accommodationTypes);
               
               $http(
                        {
                            method : 'POST',
                            data : accommodationTypes,
                            headers : {
                                'Content-Type' : 'application/x-www-form-urlencoded'
                            },
                            url : 'set-type-selected'
                        }).success(function(response) {
                            
                        	//alert(JSON.stringify(response));
                            //alert("dsfsdf");
                            window.location = '/ulopms/bookingdetails'; 
    
                        });
            };
          
         
          
         $scope.getTotal = function(){
            var total = 0;
            for(var i = 0; i < $scope.typeSelected.length; i++){
                var accommodation = $scope.typeSelected[i];
                total += (accommodation.baseAmount);
            }
            return total;
         }  
          
          $scope.getPropertyPhotos = function() {
                
                var url = "get-property-photos?propertyId="+propertyId;
                $http.get(url).success(function(response) {
                    //console.log(response);
                    //alert(JSON.stringify(response.data));
                    $scope.photos = response.data;
                
                });
            };
        
    /*      $scope.getAccommodationAmenities = function(rowId) {
                
                //$('#adminId').val()   
                 
                document.getElementById("accommodationId").value = rowId;
                var url = "get-accommodation-amenities?accommodationId="+rowId;
                $http.get(url).success(function(response) {
                    //console.log(response);
                    $scope.accamenities = response.data;
            
                });
            };     */
           
            
            $scope.getAccommodations = function() {
                var url = "get-front-accommodations?propertyId="+propertyId;
                $http.get(url).success(function(response) {
                    //console.log(response);
                    $scope.accommodations = response.data;
        
                });
            };
            
            $scope.getProperty = function() {
                
                var url = "get-front-property?propertyId="+propertyId;
                $http.get(url).success(function(response) {
                    //console.log(response);
                    //alert(JSON.stringify(response.data));
                    $scope.property = response.data;
                
                });
            };
            
            $scope.getLocation = function() {
                
                var url = "get-location";
                $http.get(url).success(function(response) {
                    //console.log(response);
                    //alert(JSON.stringify(response.data));
                    $scope.location = response.data;
                
                });
            };
            
                $scope.getPropertyAmenities = function() {
                var url = "get-property-amenities-wo";
                $http.get(url).success(function(response) {
                    //console.log(response);
                    $scope.amenities = response.data;
            
                });
            };
            $scope.getPropertyAmenities();  
            
            $interval( function(){ $scope.getFrontAvailability(); }, 60000);
                
            $scope.getLocation();
            
            //$scope.getAccommodationAmenities();
                if(checkIn!== ""){
                
                $scope.getFrontAvailability();
                
            }
            //$scope.getAccommodations();
            $scope.getPropertyPhotos(); 
            $scope.getProperty();        
                
        });
        
    </script>
<script>
 function latlong() {
        return new google.maps.LatLng( $("#lat").val(), $("#lng").val() );
    }
 function initMap() {
         var lat = document.getElementById("lat").value;
         var lng = document.getElementById("lng").value;
        
         
         
        //var lati = $('#lat').val();
        //var lngi = $('#lng').val();
        //console.log("sdfsdf");
    
        //var uluru = {lat: 12.593053, lng: 78.625839};
        //alert($('#lat').val()+'--'+$('#lng').val());
       // var uluru =  JSON.parse("{\"lat\":\""+$('#lat').val()+"\",\"lng\":\""+$('#lng').val()+"\"}");
        //alert(uluru.lat);
        
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 6,
          center: latlong()
        });
        var marker = new google.maps.Marker({
          position: latlong(),
          map: map
        });
      }
      
    
    </script>
    <script>
    function goToByScroll(id){
        
        // Reove "link" from the ID
      id = id.replace("link", "");
        // Scroll
      $('html,body').animate({
          scrollTop: $("#"+id).offset().top},
          'slow');
    }
    
    function check(id){
    alert(id);
    	
    }
    
    function check(){
    	alert("sdasd");
    	
    }
 </script>   

   