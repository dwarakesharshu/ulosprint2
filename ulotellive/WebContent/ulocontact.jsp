<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div id="wrapper">
   <section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index">Home</a></li>
                        <li>Contact Us</li>
                    </ul>
                    <h3>Contact Us</h3>
                </div>
            </div> 
        </div>
    </section>

    

    <section class="section clearfix">
        <div class="container">
            <div class="row pad-bot ">
             <h5>Contact Form</h5>
                            <div id="contact_form" class="contact_form">
                                <div id="message"><s:actionmessage theme="simple" /></div>
                                <form id="contactform" class="row" action="ulocontact" name="contactform" method="post">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name *"> 
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="email" id="email" class="form-control" placeholder="Email *"> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" id="subject" name="subject" class="form-control" placeholder="Subject"> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="form-control" name="message" id="message" rows="8" placeholder="Messages goes here.."></textarea>
                                    <button type="submit" value="SEND"  class="pull-right btn btn-primary">SUBMIT</button>
                                    </div>
                                </form>    
                                
                            </div><!-- end contact-form -->
        </div>
            <div class="row">
                <div id="fullwidth" class="col-sm-12 pad-bot">

                    <!-- START CONTENT -->
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <h5>Contact Info</h5>
                            <div class="contact-info">
                            <ul>
                                <li><i class="icon-map"></i> Ulo Hotels Private Limited<br>
56,B2,Oyster Apartment, <br>
4th Avenue,19th Street,<br>
Ashok Nagar,Chennai,<br>
                                      Tamil Nadu 600093</li>
                                <li><i class="icon-phone"></i> <strong>Phone:</strong>  +91-9543 592 593</li>
                                <li><i class="icon-print"></i> <strong>Landline:</strong> +044 4208 1908</li>
                                <li><i class="icon-mail"></i> <strong>Email:</strong> <a href="mailto:reservations@ulohotels.com ">reservations@ulohotels.com</a></li>
                                <li><i class="icon-link"></i> <strong>Web:</strong> <a href="https://ulohotels.com">www.ulohotels.com</a></li>
                            </ul>
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="map">
       
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7773.555376461509!2d80.203924!3d13.049818!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6121c6e19c9a0375!2sULO+HOTELS!5e0!3m2!1sen!2sin!4v1495444260492" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
   
    </div>
                           
                        </div><!-- end col -->
                    </div><!-- end row -->
                    <!-- END CONTENT -->

                </div><!-- end fullwidth -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 

 <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>