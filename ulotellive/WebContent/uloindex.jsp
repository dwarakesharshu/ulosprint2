<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<div id="wrapper">
    <section class="section fullscreen background parallax" style="background-image:url('ulowebsite/upload/hotel-bgnew.jpg');" data-img-width="1920" data-img-height="1133" data-diff="100">
        <div class="container">
            <div class="row homeform">
                 <div class="col-md-12 col-xs-12">
                    <div class="home-form">
                       <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab_01">
                                <h6 style="color:#fff;margin-bottom:5px;">new generation quality budget hotel chain</h6>
                                <form name="searchForm" class="bookform form-inline row"  action="get-search-form" method="post">
                                    <div class="form-group col-md-4 col-sm-6 col-xs-12"  >
                                           
                                           <select class="form-control selectpicker" data-style="btn-white" id="locationId" name="locationId" style="padding:0px;" ng-required="true">
                                             <option style="display:none" value="">Select Your Location</option>
                                          		<option value="{{j.DT_RowId}}" ng-repeat="j in locationnew" >{{j.locationName}}</option>
                                           </select>
                                          
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Check in" id="datepicker" name="arrivalDate"  ng-required="true"  onkeypress="return false;"> 
                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Check out" id="datepicker1" name="departureDate"   ng-required="true" onkeypress="return false;" >
                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                     
                                    <div class="form-group col-md-2 col-sm-6 col-xs-12">
                                       <!--  <a href="properties" value="submit" type="submit" class="btn btn-primary btn-block" ng-click="searchForm.$valid" ng-disabled="searchForm.$invalid"><i class="icon-search"></i></a> -->
                                          <button type="submit" class="btn btn-primary btn-block" ng-click="searchForm()" >Search</button>
                                    </div>
                                </form>
                            </div><!-- end tab-pane -->
                            
                            


                        </div><!-- end tab-content -->
                    </div><!-- end homeform -->
                </div><!-- end col -->

                <div class="col-md-7 col-xs-12">
   
                </div><!-- end col -->            
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->

    <section class="section clearfix">
        <div class="container">
            <div class="hotel-title text-center">
                <h3>LOCATION</h3>
                <hr>
            </div><!-- end hotel-title -->

            <div class="row">
                <div class="col-md-6" ng-repeat="l in location">
                    <div class="mini-desti row">
                        <div class="col-md-4">
                            <img ng-src="get-location-picture?locationId={{l.DT_RowId}}" alt="" class="img-responsive">
                        </div><!-- end col -->
                        <div class="col-md-8">
                            <div class="mini-desti-title">
                                <div class="pull-left">
                                    <h6><a href="properties?locationId={{l.DT_RowId}}"> {{l.locationName}}</a> </h6>
                                    <span class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </span><!-- end rating -->
                                </div>
                                <div class="pull-right">
<!--                                    <h6>$500</h6>-->
                                </div>  
                                <div class="clearfix"></div>   
                                <div class="mini-desti-desc">
                                    <p>{{l.description}}</p>
                                </div>
                            </div><!-- end title -->
                        </div><!-- end col -->
                    </div><!-- end mini-desti -->

                    
                    
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->  
<section class="section clearfix section-bottom">
        <div class="container">
            <div class="hotel-title text-center">
                <h3>OUR 5 B's Services</h3>
                <hr>
            </div><!-- end hotel-title -->
            <div class="row">
                <div class="col-md-12 ">
                    <div class="service-style">
                        <div class="border-radius col-md-4 col-sm-4 col-xs-12">
                            <img src="ulowebsite/images/fivebs/bestguest.png" height="70px" width="70px">
                              <h6>Best in Guest Service & Care</h6>
                        </div>
                  
                    </div><!-- end service -->

                    <div class="service-style">
                        <div class=" border-radius col-md-4 col-sm-4 col-xs-12">
                           <img src="ulowebsite/images/fivebs/bedclean.png" height="70px" width="70px">
                            <h6>Bed Clean & Comfortable</h6>
                        </div>
                     
                    </div><!-- end service -->
                     <div class="service-style">
                        <div class="border-radius col-md-4 col-sm-4 col-xs-12">
                            <img src="ulowebsite/images/fivebs/budgetfriendly.png" height="70px" width="70px">
                            <h6>Budget Friendly & Value</h6>
                        </div>
                     
                    </div>
                    <div class="col-md-2"></div>
                          <div class="service-style">
                        <div class=" border-radius col-md-4 col-sm-4 col-xs-12">
                          <img src="ulowebsite/images/fivebs/breakfast.png" height="70px" width="70px">
                             <h6>Breakfast Healthy & Regional</h6>
                        </div>
                 
                    </div>

                    <div class="service-style">
                        <div class=" border-radius col-md-4 col-sm-4 col-xs-12">
                         <img src="ulowebsite/images/fivebs/bathroom.png" height="70px" width="70px">
                         <h6>Bathroom hygienic & Functional</h6>
                        </div>
                     
                    </div>
                    <div class="col-md-2"></div>
                </div><!-- end col -->

            </div><!-- end row -->
        </div><!-- end container -->
    </section>
<section class="section fullscreen background parallax" style="background-image:url('ulowebsite/upload/parallax_03.jpg');" data-img-width="1920" data-img-height="586" data-diff="10">
        <div class="container">
             <div class="hotel-title text-center">
                <h3>OUR CUSTOMERS REVIEWS</h3>
                 <hr>
            </div>
            <div id="testimonials">
                <div class="testi-item">
<!--                     <div class="testi"><img class="image img-responsive size-image" src="images/clients/1.jpg"></div>-->
                    <div class="hotel-title text-center">
                     <img class="wid-img" src="ulowebsite/images/clients/aadhira.jpg" >
                      
                        <p>"The place was much better than what I thought it would be.We had dinner and breakfast there, and it was yum. Our whole trip from booking this place till we reached bangalore was just PERFECT!! My friend from Delhi loved the whole experience.But I Would definitely go back to this place and not just for one night."</p>
                        <h6>- Aadhira -</h6>
                    </div>
                </div><!-- end testi-item -->

               <div class="testi-item">
<!--                    <div class="testi"><img class="image img-responsive size-image" src="images/clients/2.jpg"></div>-->
                    <div class="hotel-title text-center">
                         <img class="wid-img" src="ulowebsite/images/clients/anturkarpratik.jpg">
                        
                       <p>"Location was awesome with outdoor sports.Comparing to expenses the rooms were quite good. Road for homestay is quite amazing with a great experience of offroad driving."</p>
                        <h6>- Anturkar pratik -</h6>
                    </div>
                </div><!-- end testi-item -->
                <div class="testi-item">
                    <div class="hotel-title text-center">
                   <img class="wid-img" src="ulowebsite/images/clients/jayanth.jpg" >
                      
                        <p>"Few months me and my friends rode to this hotel, we wanted to go to the ghat of Kolli Hills, while searching for properties we came across this new hotel. Ambience - Very Good, Staff - Very Courteous, Cleanliness - Excellent Food - Great Stuff..loved it Value for Money - 100 %
                        "</p>
                        <h6>- Jayant Mehta -</h6>
                    </div>
                </div><!-- end testi-item -->
            </div><!-- end testimonials -->
        </div><!-- end container -->
    </section><!-- end section -->
</div><!-- end wrapper -->
  <script>
 
  </script>

<script>

		var app = angular.module('myApp',['ngProgress']);
		app.controller('customersCtrl',function($scope, $http,$timeout,ngProgressFactory) {
	
			
			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
		    //$scope.progressbar.start();
			
	       //	$timeout(function(){
	      	    // $scope.progressbar.complete();
	       //     $scope.show = true;
	       //     $("#pre-loader").css("display","none");
	       // }, 2000);

$("#datepicker").datepicker({
    dateFormat: 'mm/dd/yy',
    minDate:  0,
    onSelect: function (formattedDate) {
        var date1 = $('#datepicker').datepicker('getDate'); 
        var date = new Date( Date.parse( date1 ) ); 
        date.setDate( date.getDate() + 1 );        
        var newDate = date.toDateString(); 
        newDate = new Date( Date.parse( newDate ) );   
        $('#datepicker1').datepicker("option","minDate",newDate);
        $timeout(function(){
          //scope.checkIn = formattedDate;
        });
    }
});

$("#datepicker1").datepicker({
	dateFormat: 'mm/dd/yy',
    minDate:  0,
    onSelect: function (formattedDate) {
        var date2 = $('#datepicker1').datepicker('getDate'); 
        $timeout(function(){
          //scope.checkOut = formattedDate;
        });
    }
});
		
				$scope.getLocation = function() {
					
					var url = "get-location";
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.location = response.data;
						$scope.locationnew= $scope.location;
					
					});
				};
	
	
 		
			       
			    	
		        $scope.getLocation();
						

		});

		
	</script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
	
	 <!--<script src="js/jquery-2.1.1.js" type="text/javascript"></script>-->
   <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
