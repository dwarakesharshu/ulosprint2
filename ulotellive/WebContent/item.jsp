<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          FOOD ITEM 
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
       
            <li class="active">Food Items</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Menus</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								
								<table id="example2" class="table table-bordered table-hover">
									 <thead>
									 <tr>
									
										<th>Item Name</th>
										<th>Item Code</th>
										<th>Edit</th>
										<th>Delete</th>
										</tr>
									</thead>
									<tbody>
									<tr ng-repeat="it in items">
									
										<td>{{it.itemName}}</td>
										<td>{{it.itemCode}}</td>
											<td>
											<a href="#editmodal" ng-click="getItem(it.itemId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
											
										</td>
											<td>
							
											<a href="#" ng-click="deleteSingleItem(it.itemId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
										</td>
									</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary" href="#addmodal" ng-click=""  data-toggle="modal" >Add Category</a>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Food Item</h4>
					</div>
					<form name="addItem">
					<div class="modal-body" >
					<div id="message"></div>
						
						<!--<div class="form-group">
	                      			<label>Item Catgeory</label>
	                      			
	                      			<select name="propertyItemCatgeoryId" id="propertyItemCatgeoryId" value="{{property[0].propertyTypeId}}" class="form-control">
	                       			 <option value="select">select</option>
	                        		<option ng-repeat="pt in propertytypes" value="{{pt.propertyTypeId}}">{{pt.propertyTypeName}}</option>
	                      			</select>
	                    </div>-->
	                    
	                     <div class="form-group col-md-12">
							<label>Select Catgeory</label>
							<select name="propertyItemCategoryId" id="propertyItemCategoryId" value="" class="form-control">
	                       	<option ng-repeat="c in categories" value="{{c.DT_RowId}}">{{c.categoryName}}</option>
	                        </select>
						</div>
						
						 <!--  <input type="text" value="1" name="propertyItemCatgeoryId" id="propertyItemCatgeoryId" >-->
	                    		
	                    		
						<div class="form-group col-md-6">
							<label>Item Name</label>
							<input type="text" class="form-control"  name="itemName"  id="itemName"  placeholder="itemName" ng-model='itemName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="100">
						     <span ng-show="addItem.itemName.$error.pattern">Not a valid Name!</span>
						</div>
						
						
						<div class="form-group col-md-6">
							<label>SKU</label>
							<input type="text" class="form-control"  name="itemSku"  id="itemSku"  placeholder="SKU" ng-model='itemSku' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="20">
							<span ng-show="addItem.itemSku.$error.pattern">Not a valid SKU!</span>
						</div>
						
						<div class="form-group col-md-6">
							<label>Item Code</label>
							<input type="text" class="form-control"  name="itemCode"  id="itemCode"  placeholder="itemCode" ng-model='itemCode' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addItem.itemCode.$error.pattern" style="color:red">Not a valid Item Code!</span>
						</div>
						
						<div class="form-group col-md-6">
							<label>Item Description</label>
							<input type="text" class="form-control"  name="itemDescription"  id="itemDescription"  placeholder="itemDescription" required maxlength="250">
						</div>
						
						<div class="form-group col-md-6">
							<label>Amount</label>
							<input type="text" class="form-control"  name="itemAmount"  id="itemAmount"  placeholder="amount" ng-model='itemAmount' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addItem.itemAmount.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
						<!--  <input type="text" value="1" name="propertyTaxId" id="propertyTaxId" >-->
						
						 <div class="form-group col-md-6">
							<label>Property Tax Id</label>
							<select name="propertyTaxId" id="propertyTaxId" value="" class="form-control">
	                       	<option ng-repeat="t in taxes" value="{{t.taxId}}">{{t.taxName}}</option>
	                        </select>
							
						</div>
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addItem.$valid && addSingleItem()" ng-disabled="addItem.$invalid" class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Category</h4>
					</div>
					<form name="editItem">
					<div class="modal-body" ng-repeat="itm in item">
					<div id="editmessage"></div>
					    <input type="hidden" name ="editPropertyItemId" id ="editPropertyItemId" value="{{itm.itemId}}">
					   
					    
					    
					    
						 <div class="form-group col-md-12">
							<label>Select Category</label>
							<select name="editPropertyItemCategoryId" id="editPropertyItemCategoryId"  class="form-control">
	                       	<option ng-repeat="c in categories" value="{{c.DT_RowId}}" ng-selected="{{ c.DT_RowId == itm.propertyItemCategoryId }}">{{c.categoryName}}</option>
	                        </select>
						</div>
						
	                    		
	                    		
						<div class="form-group col-md-6">
							<label>Item Name</label>
							<input type="text" class="form-control"  name="editItemName"  id="editItemName" value="{{itm.itemName}}"   ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="100">
						     <span ng-show="editItem.eidtItemName.$error.pattern">Not a valid Name!</span>
						</div>
						
						
						<div class="form-group col-md-6">
							<label>SKU</label>
							<input type="text" class="form-control"  name="editItemSku"  id="editItemSku" value="{{itm.itemSku}}"   ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="10">
							<span ng-show="editItem.itemSku.$error.pattern">Not a valid SKU!</span>
						</div>
						
						<div class="form-group col-md-6">
							<label>Item Code</label>
							<input type="text" class="form-control"  name="editItemCode"  id="editItemCode"  value="{{itm.itemCode}}" ng-model='itm.itemCode' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editItem.editItemCode.$error.pattern" style="color:red">Not a valid Item Code!</span>
						</div>
						
						<div class="form-group col-md-6">
							<label>Item Description</label>
							<input type="text" class="form-control"  name="editItemDescription" value="{{itm.itemDescription}}"  id="editItemDescription"   required maxlength="100">
						</div>
						
						<div class="form-group col-md-6">
							<label>Amount</label>
							<input type="text" class="form-control"  name="editItemAmount"  id="editItemAmount" value="{{itm.itemAmount}}" ng-model='itm.itemAmount' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editItem.editItemAmount.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
						
						<div class="form-group col-md-6">
							<label>Property Tax Id</label>
							<select name="editPropertyTaxId" id="editPropertyTaxId" value="{{propertyTaxeId}}" class="form-control">
	                       	<option ng-repeat="t in taxes" value="{{t.taxId}}" ng-selected ="t.taxId == itm.propertyTaxeId">{{t.taxName}}</option>
	                        </select>
							
						</div>
						<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="editItem.$valid && editSingleItem()" ng-disabled="editSome.$invalid" class="btn btn-primary">Save</button>
					</div>
					</div>
					</form>
					
					
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		<div class="modal" id="deletemodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="deletebtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Delete Message</h4>
					</div>
					<div class="modal-body">
					 <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i>Your Item  has been deleted Sucessfully </h4>
                  
                 </div>
					    
						
					</div>
					<div class="modal-footer">
	                    
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

 
	<script>

       
 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

			 
			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			$scope.getCategories = function() {
       
				var url = "get-categories";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.categories = response.data;
		
				});
			};
			
			
			$scope.getTaxes = function() {
				
				var url = "get-taxes";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.taxes = response.data;
		
				});
			};
			
		   $scope.getItems = function() {
           
				var url = "get-items";
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.items = response.data;
		
				});
			};
			
		
			
			
			$scope.addSingleItem = function(){
				
				
				//alert($('#propertyItemCategoryId').val());
				
				var fdata = "propertyItemCategoryId=" + $('#propertyItemCategoryId').val()
				+ "&itemName=" + $('#itemName').val()
				+ "&itemSku=" + $('#itemSku').val()
				+ "&itemCode=" + $('#itemCode').val()
				+ "&itemDescription=" + $('#itemDescription').val()
				+ "&itemAmount=" + $('#itemAmount').val()
				+ "&propertyTaxId=" + $('#propertyTaxId').val();
				
				
				
				
				
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-item'
						}).then(function successCallback(response) {
							
							$scope.getItems();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
				            $('#message').html(alert);
				            
							$timeout(function(){
					      	$('#btnclose').click();
					        }, 2000);
							
							//$('#btnclose').click();
				}, function errorCallback(response) {
					
				  
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			$scope.editSingleItem = function(){
				
				
				
				var fdata = "propertyItemCategoryId=" + $('#editPropertyItemCategoryId').val()
				+ "&itemName=" + $('#editItemName').val()
				+ "&itemSku=" + $('#editItemSku').val()
				+ "&itemCode=" + $('#editItemCode').val()
				+ "&itemDescription=" + $('#editItemDescription').val()
				+ "&itemAmount=" + $('#editItemAmount').val()
				+ "&propertyItemId=" + $('#editPropertyItemId').val()
				+ "&propertyTaxId=" + $('#editPropertyTaxId').val();
				
				
				
				
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-item'
						}).then(function successCallback(response) {
							
							$scope.getItems();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
				            $('#editmessage').html(alert);
							$timeout(function(){
					      	$('#editbtnclose').click();
					        }, 2000);
				}, function errorCallback(response) {
					
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

				
				
			};
			
            
            
			
			
            
			$scope.getItem = function(rowid) {
				
				
				var url = "get-item?propertyItemId="+rowid;
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.item = response.data;
		
				});
				
			};
			
            $scope.deleteSingleItem = function(rowid) {
				
            	
				
				var url = "delete-item?propertyItemId="+rowid;
				$http.get(url).success(function(response) {
				    //console.log(response);
				    $scope.getItems();
				    $('#deletemodal').modal('toggle');
				    $timeout(function(){
				      	$('#deletebtnclose').click();
				    }, 2000);
		
				});
				
			};
			
			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
            $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
				
		    $scope.getPropertyList();
			
			
			$scope.getTaxes();
			$scope.getCategories();
			
			$scope.getItems();
			
			//$scope.unread();
			//
			
			
		});


		
		   
		        

		
	</script>
			
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       