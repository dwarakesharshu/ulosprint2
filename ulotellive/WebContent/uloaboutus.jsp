<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div id="wrapper">

<section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index">Home</a></li>
                        <li>ABOUT ULO HOTELS</li>
                    </ul>
                    <h3>ABOUT ULO HOTELS</h3>
                </div>
            </div> 
        </div>
    </section>

    

    <section class="section clearfix">
        <div class="container">
            <div class="row pad-bot ">
          <p> We believe the quality shouldn’t be a luxury. We wish to create a chain of budget friendly with excellent quality, service, and standards that have customers coming back
         <br>We function via the Joint Business Venture system. We will coordinate day-to-day operations, give your property a better presence on the internet, improve quality and employ advanced technology to offer the best experience for guests.
        <br>We are pocket-friendly, while still providing all the features that the smaller hotels need such as an increase in occupancy rate and impeccable standards that create  repetitive customers.</p>
        </div>
            
            <div class="row">
                <div id="fullwidth" class="col-sm-12 pad-bot">

                    <!-- START CONTENT -->
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <h4>why us</h4>
                         
                            <ul class="list-style">
                            <li>We offer a complete makeover of your property while overseeing the administration</li>
<li>Our teams have the requisite technical and domain understanding to work in sync with technology that allows us to optimize and  secure your transactions.</li>
<li>We serve a diverse set of clients by utilizing a support system that seamlessly syncs with the  customer needs.</li>
<li>We are available around-the-clock to take care of your needs.</li>
<li>We guarantee excellent quality</li>
<li>We ensure complete security of your data right from registration to payment.</li>
                           </ul>
                        </div><!-- end col -->

                        <div class="col-md-6 col-sm-12 col-xs-12">
                          <h4>What is in it for you?</h4>
                           <ul class="list-style">
                        <li>Increased Occupancy Rate</li>
<li>Increased Revenue</li>
<li>On time payments</li>
<li>Stronger internet presence</li>
<li>Tangible Cost Effectiveness</li>
<li>Scalability </li>
<li>Increased Efficiency</li>
<li>Cost Optimization</li>
                                
                            </ul>
                        </div><!-- end col -->
                        <div class="col-lg-12 ">
                        <h4 class="text-center">How do we work?</h4>
                        <ul class="list-style">
                         <p>We make sure your property has a strong presence on the internet and promote it through our partners. The day-to-day administration of the property will be taken care by our property manager. We ensure that the USP of the property is maintained while striving to achieve international standards in amenities, hygiene, and services. We also handle review platforms to increase rankings and quality through  </p>
                        </ul>
                        </div>
                        <div class="col-lg-12 text-center">
                         <h4>Our services Like:</h4>
                                    <ul class="list-style" style="list-style: none;">
                        <li>Digital Marketing Team.</li>
<li>Complete Property Management.</li>
<li>On time payments.</li>
<li>Property Development Tool.</li>
<li>Development Services.</li>
<li>Operations, Sales & Marketing.</li>
<li>Centralized Revenue Team.</li>
<li>Dedicated Quality Control Team.</li>
<li>Review Management.</li>
                                
                            </ul>
                            <p>
                          We have multiple properties spread across 5 locations: Coorg, Kodaikanal, Udagamandalam, Yelagiri, and Kolli Hills. The following section shows a growth of over 60% in revenue through this business model.
                           
                            </p>
                        </div>
                    </div><!-- end row -->
                    <!-- END CONTENT -->

                </div><!-- end fullwidth -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->
</div>
 <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
    