<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
   -->
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ULO Hotels</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
      <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
      <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
      <!-- iCheck -->
      <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
      <!-- Morris chart -->
      <link rel="stylesheet" href="plugins/morris/morris.css">
      <!-- jvectormap -->
      <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
      <!-- Date Picker -->
      <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
      <link rel="stylesheet" href="plugins/select2/select2.css">
      <!-- Daterange picker -->
      <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
      <!-- bootstrap wysihtml5 - text editor -->
      <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
      <!-- ngprogress-lite - progressbar -->
      <!-- ngprogress-lite.css -->
   </head>
   <body ng-app="myApp" ng-controller="customersCtrl" class="hold-transition sidebar-mini wysihtml5-supported skin-green-light" >
      <!-- <div class="preloader-center" id="pre-loader">
         <div class="loading-indicator" style="padding: 16px;background: #fff;border-radius: 50%;">
             <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 130 130" overflow="visible" enable-background="new 0 0 140 140" id="progress">
                 <circle fill="none" stroke="#000000" stroke-width="6" stroke-miterlimit="10" cx="64.8" cy="64.8" r="59.8"></circle>
                 <circle class="styled" fill="none" stroke="#000000" stroke-width="6" stroke-miterlimit="10" cx="64.8" cy="64.8" r="59.8"></circle>
             </svg>
         </div>
         <p><b class="percentage"></b></p>
         </div>  --> 
      <div class="wrapper">
         <header class="main-header">
            <!-- Logo -->
            <a href="dashboard" class="logo ">
               <!-- mini logo for sidebar mini 50x50 pixels -->
               <span class="logo-mini"><b>ULO</b></span>
               <!-- logo for regular state and mobile devices -->
               <span class="logo-lg"><b>ULO Hotels</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
               <!-- Sidebar toggle button-->
               <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
               <span class="sr-only">Toggle navigation</span>
               </a>
               <div class="navbar-custom-menu">
                  <ul class="nav navbar-nav">
                     <li class="dropdown notifications-menu">
                        <a href="landing">
                           <i class="fa fa-exchange"></i>
                           <span class="hidden-xs">
                              <s:property value="#session.propertyName"/>
                           </span>
                        </a>
                     </li>
                     <!-- Notifications: style can be found in dropdown.less -->
                     <%--       <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <i class="fa fa-bell-o"></i>
                          <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                          <li class="header">You have 10 notifications </li>
                          <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                              <li>
                                <a href="#">
                                  <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i class="fa fa-users text-red"></i> 5 new members joined
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i class="fa fa-user text-red"></i> You changed your username
                                </a>
                              </li>
                            </ul>
                          </li>
                          <li class="footer"><a href="#">View all</a></li>
                        </ul>
                        </li> --%>
                     <!-- User Account: style can be found in dropdown.less -->
                     <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                           <img src="<s:url action='profile-picture.action'/>" class="user-image" alt="User Image">
                           <span class="hidden-xs">
                              <s:property value="user.userName"/>
                           </span>
                        </a>
                        <ul class="dropdown-menu">
                           <!-- User image -->
                           <li class="user-header">
                              <img src="<s:url action='profile-picture.action'/>" class="img-circle" alt="User Image">
                              <p>
                                 <s:property value="user.userName"/>
                                 - 
                                 <s:property value="#session.role"/>
                                 <s:property value="#session.propertyId"/>
                              </p>
                           </li>
                   
                           <li class="user-footer">
                              <div class="pull-left">
                                 <a href="userprofile" class="btn btn-primary btn-flat">Profile</a>
                              </div>
                              <div class="pull-right">
                                 <a href="logout" class="btn btn-danger btn-flat">Sign out</a>
                              </div>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!-- Left side column. contains the logo and sidebar -->
         <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
               <!-- sidebar menu: : style can be found in sidebar.less -->
               <ul class="sidebar-menu">
                  <li class="header">NAVIGATION</li>
                  <li class="active treeview">
                     <a href="dashboard">
                        <input type="hidden" id="adminId" value="<s:property value="user.userId" />" name="">
                        <!--  <input type ="text" value="<s:property value="#session.userRights"/>" > -->
                        <i class="fa fa-dashboard" style="color:#88ba41;"></i> <span>DASHBOARD</span></i>
                     </a>
                     <!-- <ul class="treeview-menu">
                        <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                        <li><a href="dashboard"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                        </ul> -->
                  </li>
                  <s:if test="%{#session.userRights.indexOf('PROPERTY DETAILS')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <i class="fa fa-bank" style="color:#88ba41;"></i> <span>PROPERTY DETAILS</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="property-profile"><i class="fa  fa-building-o" style="color:#88ba41;"></i> PROPERTY PROFILE</a></li>
                           <li><a href="property-amenities"><i class="fa fa-bell"style="color:#88ba41;"></i> PROPERTY AMENITIES</a></li>
                           <%--  <li><a href="add-amenities"><i class="fa fa-circle-o"></i> Add Amenities</a></li>  --%>
                           <li><a href="accommodation"><i class="fa fa-hotel" style="color:#88ba41;"></i> ROOM CATEGORY</a></li>
                           <li><a href="accommodation-rooms"><i class="fa fa-home" style="color:#88ba41;"></i>ROOM INVENTORY</a></li>
                        </ul>
                     </li>
                  </s:if>
                  <li class="treeview">
                     <a href="#">
                     <i class="fa  fa-list" style="color:#88ba41;"></i> <span>TAXES</span>
                     <i class="fa fa-angle-left pull-right"></i>
                     </a>
                     <ul class="treeview-menu">
                        <li><a href="taxes"><i class="fa fa-calculator" style="color:#88ba41;"></i>ADD TAX</a></li>
                        <!--   <li><a href="item-category"><i class="fa fa-sticky-note" style="color:#88ba41;"></i>MEAL PLAN</a></li>
                           <li><a href="item"><i class="fa fa-cart-arrow-down" style="color:#88ba41;"></i>FOOD ITEMS</a></li> -->
                     </ul>
                  </li>
  
                  <s:if test="%{#session.userRights.indexOf('RATE MANAGEMENT')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <i class="fa fa-fw fa-money" style="color:#88ba41;"></i> <span>RATE MANAGEMENT</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <!-- <li><a href="booking"><i class="fa fa-circle-o"></i>Booking</a></li> -->
                           <li><a href="ratemanager"><i class="fa fa-inr" style="color:#88ba41;"></i> <span>MANAGE PRICE</span></a></li>
                           <li><a href="activeratemanager"><i class="fa fa-inr" style="color:#88ba41;"></i> <span>ACTIVE RATES</span></a></li>
                           <li><a href="discountmanager"><i class="fa fa-tags"  style="color:#88ba41;"></i> <span> MANAGE DISCOUNT</span></a></li>
                        </ul>
                     </li>
                  </s:if>
                  <li class="treeview">
                     <a href="#">
                     <i class="fa fa-fw fa-money" style="color:#88ba41;"></i> <span>OFFER AND DEALS</span>
                     <i class="fa fa-angle-left pull-right"></i>
                     </a>
                     <ul class="treeview-menu">
                        <li><a href="promotion-manager"><i class="fa fa-inr"  style="color:#88ba41;"></i> <span>MANAGE PROMOTION</span></a></li>
                     </ul>
                  </li>
                  <s:if test="%{#session.userRights.indexOf('RATE MANAGEMENT')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <i class="fa fa-fw fa fa-key" style="color:#88ba41;"></i> <span>ROOM MANAGEMENT</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <!-- <li><a href="booking"><i class="fa fa-circle-o"></i>Booking</a></li> -->
                           <li><a href="room-management"><i class="fa fa-lock" style="color:#88ba41;"></i> <span>MANAGE ROOM</span></a></li>
                           <li><a href="roomviewcalendar"><i class="fa fa-street-view"  style="color:#88ba41;"></i> <span>VIEW ROOM</span></a></li>
                        </ul>
                     </li>
                  </s:if>
                  <s:if test="%{#session.userRights.indexOf('BOOKING REPORTS')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <i class="fa fa-fw fa-file-text-o" style="color:#88ba41;"></i> <span>REPORTS</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <!-- <li><a href="booking"><i class="fa fa-circle-o"></i>Booking</a></li> -->
                           <li><a href="booking-reports"><i class="fa  fa-file-excel-o" style="color:#88ba41;"></i> BOOKING REPORTS</a></li>
                           <li><a href="revenue-reports"><i class="fa  fa-file-excel-o" style="color:#88ba41;"></i> REVENUE REPORTS</a></li>
                           <li><a href="user-guest-reports"><i class="fa  fa-file-excel-o" style="color:#88ba41;"></i>USER GUEST REPORTS</a></li>
                        </ul>
                     </li>
                  </s:if>
                  <s:if test="%{#session.userRights.indexOf('USER MANAGEMENT')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <i class="fa fa-cog" style="color:#88ba41;"></i> <span>SETTING</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <!-- <li><a href="booking"><i class="fa fa-circle-o"></i>Booking</a></li> -->
                           <li><a href="property-users"><i class="fa fa-user" style="color:#88ba41;"></i> ADD USER</a></li>
                           <li><a href="manage-access"><i class="fa fa-user-plus" style="color:#88ba41;"></i> MANAGE ACCESS</a></li>
                           <li><a href="user-guest-reports"><i class="fa  fa-file-excel-o" style="color:#88ba41;"></i>USER GUEST REPORTS</a></li>
                        </ul>
                     </li>
                  </s:if>
                  <s:if test="%{#session.userRights.indexOf('RESORT DIRECT')!=-1}">
                     <li class="header">Direct-Walkin</li>
                     <li><a href="reservation"><i class="fa fa-credit-card" style="color:#88ba41;"></i> <span>RESORT DIRECT</span></a></li>
                     <%--             		<li><a href="room-management"><i class="fa fa-credit-card" style="color:#88ba41;"></i> <span>ROOM MANAGEMENT</span></a></li>	
                        <li><a href="roomviewcalendar"><i class="fa fa-credit-card" style="color:#88ba41;"></i> <span>ROOM VIEW</span></a></li>
                        <li><a href="bookingcalendar"><i class="fa fa-calendar text-chartreuse " style="color:#88ba41;"></i> <span>BOOKING CALENDAR</span></a></li> --%>
                  </s:if>
               </ul>
            </section>
            <!-- /.sidebar -->
         </aside>
         <tiles:insertAttribute name="body" />
         <footer class="main-footer">
            <div class="pull-right hidden-xs">
               <b>Version</b> 1.0
            </div>
            <strong>
               Copyright &copy; 2017 
               <a href='<s:property value="https://www.ulohotels.com"/>'>
                  <s:property value="%{companyName}"/>
               </a>
               .
            </strong>
            All rights reserved.
         </footer>
         <!-- Control Sidebar -->
    
         <!-- /.control-sidebar -->
         <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
         <div class="control-sidebar-bg"></div>
      </div>
      <!-- ./wrapper -->
      <!-- jQuery 2.1.4 -->
      <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script> 
      <script src="plugins/select2/select2.js"></script> 
      <script src="plugins/select2/select2.full.js"></script> 
      <!-- jQuery UI 1.11.4 -->
      <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
      <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
      <!-- AdminLTE App -->
      <script src="dist/js/app.min.js"></script>
      <!-- AdminLTE for demo purposes -->
      <script src="dist/js/demo.js"></script>
      <script>
         $.widget.bridge('uibutton', $.ui.button);
      </script>
      <!-- Bootstrap 3.3.5 -->
      <script src="bootstrap/js/bootstrap.min.js"></script>
      <!-- Slimscroll -->
      <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
      <!-- FastClick -->
      <script src="plugins/fastclick/fastclick.min.js"></script>

      <!-- progress bar -->
      <script src="js1/ngprogress.js" type="text/javascript"></script>
      <!--<script src="js1/ngprogress-lite.mins.js" type="text/javascript"></script>-->
      <!-- DataTables -->
      <script src="plugins/datatables/jquery.dataTables.min.js"></script>
      <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
      <script src="dist/js/pages/dashboard.js"></script>
    
   </body>

</html>