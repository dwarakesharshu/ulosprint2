<%--  <% response.sendRedirect("https://www.ulohotels.com"); %> --%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <!-- Meta Tag starts -->
      <meta charset="utf-8">
      
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta name="author" content="">
      <title>ULO Hotels | A New Generation Quality Budget Hotel Chain.</title>
      <meta name="description" content="If you planning for a staycation & in a budget cut, reserve cheap of cozy hotels with special discounts & deals by online hotel booking from our Ulohotels."/>
      <meta name="keywords" content="Ulo Hotels, online hotel booking, budget hotel booking, hotels near me, online budget hotels">
      <meta name="robots" content="index, follow"/>
      <!-- Meta Tag Ends -->
      <!-- Schema.org markup for Google+ -->
      <meta itemprop="name" content="ULO Hotels | A New Generation Quality Budget Hotel Chain">
      <meta itemprop="description" content="If you planning for a staycation and in a budget cut, reserve cheap of cozy hotels with special discounts & deals by online hotel booking from our Ulohotels.">
      <meta itemprop="image" content="https://ulohotels.com/ulowebsite/images/logo.png">
      <!-- Twitter Card data -->
      <meta name="twitter:card" content="summary_large_image">
      <meta name="twitter:site" content="@Ulohotels">
      <meta name="twitter:title" content="ULO Hotels | A New Generation Quality Budget Hotel Chain">
      <meta name="twitter:description" content="If you planning for a staycation and in a budget cut, reserve cheap of cozy hotels with special discounts & deals by online hotel booking from our Ulohotels.">
      <meta name="twitter:creator" content="@Ulohotels">
      <meta name="twitter:image:src" content="https://ulohotels.com/ulowebsite/images/logo.png">
      <meta property="og:url" content="https://www.ulohotels.com/" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="ULO Hotels | A New Generation Quality Budget Hotel Chain" />
      <meta property="og:description" content="If you planning for a staycation and in a budget cut, reserve cheap of cozy hotels with special discounts & deals by online hotel booking from our Ulohotels." />
      <meta property="og:image" content="https://ulohotels.com/ulowebsite/images/logo.png" />
      <!-- Favicons -->
      <link rel="shortcut icon" href="ulowebsite/images/favicon.png" type="image/png" />
      <link rel="apple-touch-icon" href="ulowebsite/images/favicon.png" />
      <link rel="apple-touch-icon" sizes="72x72" href="ulowebsite/images/favicon.png" />
      <link rel="apple-touch-icon" sizes="114x114" href="ulowebsite/images/favicon.png" />
      <!-- Bootstrap -->
      <link href="ulowebsite/css/bootstrap.css" rel="stylesheet">
      <!-- Default Styles -->
      <link href="ulowebsite/css/style.css" rel="stylesheet">
      <!-- Custom Styles -->
      <link href="ulowebsite/css/newstyle.css" rel="stylesheet">
      <link href="ulowebsite/css/custom.css" rel="stylesheet">
      <link href="ulowebsite/css/jquery-ui.css" rel="stylesheet">
      <link href="ulowebsite/css/lightslider.css" rel="stylesheet">
      <link href="ulowebsite/css/dwarakesh.css" rel="stylesheet">
      <link href="ulowebsite/css/newstyle.css" rel="stylesheet">
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M3RH7N5');</script>
<!-- End Google Tag Manager -->
  
   </head>
   <body ng-app="myApp" ng-controller="customersCtrl">
      <div id="wrapper">
        <div class="topbar">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7 logoindex">
                     <a href="index" style="padding:10px;">
                     <img src="ulowebsite/images/logo.png" alt="ulo-logo"></a>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2 headerbtn" style="border-right:1px dotted #DCDCDC;" >
                     <s:if test="#session.userId == null">
                        <a href="customer-login" class="btn btn-primary btn-xs  top-btn" style="padding:10px;margin-top:19px;margin-right:10px;margin-bottom:10px">login</a>
                        <a href="customer-signup" class="btn btn-primary btn-xs  top-btn" style="padding:10px;margin-top:19px;margin-bottom:10px">Signup</a>
                     </s:if>
                     <s:if test="#session.userId != null">
                        <div class="dropdown">
                           <button class="dropbtn" ><i class="fa fa-user"></i>&nbsp; <i class="icon-down-open-mini"></i></button>
                           <div class="dropdown-content">
                              <p>Hi! <%=session.getAttribute("userName") %></p>
                              <br>
                              <a href="ulouserprofile">Profile</a>
                              <a href="ulologout">Logout</a>
                           </div>
                        </div>
                     </s:if>
                  </div>
                  <div class="col-xs-12 col-sm-12 hidden-lg hidden-md">
                       <div class="col-xs-12 col-sm-12">
                        <span style="color:#114b70;font-size:14px;"><i class="fa fa-envelope" style="color:#88ba41;"></i>&nbsp;support@ulohotels.com  </span>
                     </div>
                     <div class="col-xs-12 col-sm-12">
                        <span style="color:#114b70;font-size:14px;"><i class="fa fa-phone" style="color:#88ba41;"></i>&nbsp; 9543 592 593 </span>
                     </div>
                  </div>
                  <div class="hidden-xs hidden-sm col-md-3 col-lg-3 ulocontact">
                     <span><i class="fa fa-envelope" style="color:#88ba41;"></i>&nbsp;support@ulohotels.com </span> <br>
                     <span><i class="fa fa-phone" style="color:#88ba41;"></i> &nbsp;+91-9543 592 593</span>
                  </div>
               </div>
            </div>
            <!-- end container -->
         </div>
         <!-- end topbar -->
         <div id="wrapper">
            <section class="section fullscreen background parallax" style="background-image:url('ulowebsite/upload/hotel-bgnew.jpg');" data-img-width="1920" data-img-height="1133" data-diff="100">
               <div class="container">
                  <div class="row homeform">
                     <div class="col-md-12 col-xs-12">
                        <div class="home-form">
                           <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="tab_01">
                                 <h6 style="color:#fff;margin-bottom:5px;">new generation quality budget hotel chain</h6>
                                 <form name="searchForm" class="bookform form-inline row"  action="get-search-form" method="post">
                                    <div class="form-group col-md-4 col-sm-6 col-xs-12"  >
                                       <select class="form-control " data-style="btn-white" id="location" name="locationId" style="padding:0px;" ng-required="true">
                                          
                                          <option list="location" value="{{l.DT_RowId}}" ng-repeat="l in location"  ng-selected ="properties?locationId={{l.DT_RowId}}">{{l.locationName}}</option>
                                       </select>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                   
                                       
                     <div class="input-group date">
                   <label class="input-group-addon btn" for="datepicker">
                   <span class="fa fa-calendar"></span>
              </label>   
                  <input type="text" class="form-control" placeholder="Check in" id="datepicker" name="arrivalDate1"  ng-required="true"  onkeypress="return false;">  
                  <input type="hidden" class="form-control" id="alternate" name="arrivalDate"  ng-required="true"  onkeypress="return false;">  
 
                </div>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                   
                                             <div class="input-group date">
                   <label class="input-group-addon btn" for="datepicker1">
                   <span class="fa fa-calendar"></span>
              </label>   
                   <input type="text" class="form-control" placeholder="Check out" id="datepicker1" name="departureDate1"   ng-required="true" onkeypress="return false;" >
                     <input type="hidden" class="form-control" id="alternate1" name="departureDate"  ng-required="true"  onkeypress="return false;">
 
                </div>
                                    </div>
                                    <div class="form-group col-md-2 col-sm-6 col-xs-12">
                                       <!--  <a href="properties" value="submit" type="submit" class="btn btn-primary btn-block" ng-click="searchForm.$valid" ng-disabled="searchForm.$invalid"><i class="icon-search"></i></a> -->
                                       <button type="submit" class="btn btn-primary btn-block" ng-click="searchForm()" >Search</button>
                                    </div>
                                 </form>
                              </div>
                              <!-- end tab-pane -->
                           </div>
                           <!-- end tab-content -->
                        </div>
                        <!-- end homeform -->
                     </div>
                     <!-- end col -->
                     <div class="col-md-7 col-xs-12">
                     </div>
                     <!-- end col -->            
                  </div>
                  <!-- end row -->
               </div>
               <!-- end container -->
            </section>
            <!-- end section -->
            <section class="section clearfix">
               <div class="container">
                  <div class="hotel-title text-center">
                     <h3>LOCATION</h3>
                     <hr>
                  </div>
                  <!-- end hotel-title -->
                  <div class="row">
                     <div class="col-md-6" ng-repeat="l in location">
                        <div class="mini-desti row">
                           <div class="col-md-4">
                              <img src="get-location-picture?locationId={{l.DT_RowId}}" alt="" class="img-responsive">
                           </div>
                           <!-- end col -->
                           <div class="col-md-8">
                              <div class="mini-desti-title">
                                 <div class="pull-left">
                                    <h6><a href="properties?locationId={{l.DT_RowId}}"> {{l.locationName}}</a> </h6>
                                    <span class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    </span><!-- end rating -->
                                 </div>
                                 <div class="pull-right">
                                    <!--                                    <h6>$500</h6>-->
                                 </div>
                                 <div class="clearfix"></div>
                                 <div class="mini-desti-desc">
                                    <p>{{l.description}}</p>
                                 </div>
                              </div>
                              <!-- end title -->
                           </div>
                           <!-- end col -->
                        </div>
                        <!-- end mini-desti -->
                     </div>
                     <!-- end col -->
                  </div>
                  <!-- end row -->
               </div>
               <!-- end container -->
            </section>
            <!-- end section -->  
            <section class="section clearfix section-bottom">
               <div class="container">
                  <div class="hotel-title text-center">
                     <h3>OUR 5 B's Services</h3>
                     <hr>
                  </div>
                  <!-- end hotel-title -->
                  <div class="row">
                     <div class="col-md-12 ">
                        <div class="service-style">
                           <div class="border-radius col-md-4 col-sm-4 col-xs-12">
                              <img src="ulowebsite/images/fivebs/bestguest.png" height="70px" width="70px">
                              <h6>Best in Guest Service & Care</h6>
                           </div>
                        </div>
                        <!-- end service -->
                        <div class="service-style">
                           <div class=" border-radius col-md-4 col-sm-4 col-xs-12">
                              <img src="ulowebsite/images/fivebs/bedclean.png" height="70px" width="70px">
                              <h6>Bed Clean & Comfortable</h6>
                           </div>
                        </div>
                        <!-- end service -->
                        <div class="service-style">
                           <div class="border-radius col-md-4 col-sm-4 col-xs-12">
                              <img src="ulowebsite/images/fivebs/budgetfriendly.png" height="70px" width="70px">
                              <h6>Budget Friendly & Value</h6>
                           </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="service-style">
                           <div class=" border-radius col-md-4 col-sm-4 col-xs-12">
                              <img src="ulowebsite/images/fivebs/breakfast.png" height="70px" width="70px">
                              <h6>Breakfast Healthy & Regional</h6>
                           </div>
                        </div>
                        <div class="service-style">
                           <div class=" border-radius col-md-4 col-sm-4 col-xs-12">
                              <img src="ulowebsite/images/fivebs/bathroom.png" height="70px" width="70px">
                              <h6>Bathroom hygienic & Functional</h6>
                           </div>
                        </div>
                        <div class="col-md-2"></div>
                     </div>
                     <!-- end col -->
                  </div>
                  <!-- end row -->
               </div>
               <!-- end container -->
            </section>
            <section class="section fullscreen background parallax" style="background-image:url('ulowebsite/upload/parallax_03.jpg');" data-img-width="1920" data-img-height="586" data-diff="10">
               <div class="container">
                  <div class="hotel-title text-center">
                     <h3>OUR CUSTOMERS REVIEWS</h3>
                     <hr>
                  </div>
                  <div id="testimonials">
                     <div class="testi-item">
                        <!--                     <div class="testi"><img class="image img-responsive size-image" src="images/clients/1.jpg"></div>-->
                        <div class="hotel-title text-center">
                           <img class="wid-img" src="ulowebsite/images/clients/aadhira.jpg" >
                           <p>"The place was much better than what I thought it would be.We had dinner and breakfast there, and it was yum. Our whole trip from booking this place till we reached bangalore was just PERFECT!! My friend from Delhi loved the whole experience.But I Would definitely go back to this place and not just for one night."</p>
                           <h6>- Aadhira -</h6>
                        </div>
                     </div>
                     <!-- end testi-item -->
                     <div class="testi-item">
                        <!--                    <div class="testi"><img class="image img-responsive size-image" src="images/clients/2.jpg"></div>-->
                        <div class="hotel-title text-center">
                           <img class="wid-img" src="ulowebsite/images/clients/anturkarpratik.jpg">
                           <p>"Location was awesome with outdoor sports.Comparing to expenses the rooms were quite good. Road for homestay is quite amazing with a great experience of offroad driving."</p>
                           <h6>- Anturkar pratik -</h6>
                        </div>
                     </div>
                     <!-- end testi-item -->
                     <div class="testi-item">
                        <div class="hotel-title text-center">
                           <img class="wid-img" src="ulowebsite/images/clients/jayanth.jpg" >
                           <p>"Few months me and my friends rode to this hotel, we wanted to go to the ghat of Kolli Hills, while searching for properties we came across this new hotel. Ambience - Very Good, Staff - Very Courteous, Cleanliness - Excellent Food - Great Stuff..loved it Value for Money - 100 %
                              "
                           </p>
                           <h6>- Jayant Mehta -</h6>
                        </div>
                     </div>
                     <!-- end testi-item -->
                  </div>
                  <!-- end testimonials -->
               </div>
               <!-- end container -->
            </section>
            <!-- end section -->
         </div>
         <!-- end wrapper -->
         <!-- start of footer -->
         <footer class="footer clearfix">
            <div class="container">
               <div class="row">
                  <div class="col-md-4 col-sm-3 col-xs-12">
                     <div class="widget">
                        <div class="widget-title">
                           <h3>GET IN TOUCH</h3>
                        </div>
                        <!-- end title -->
                        <div class="textwidget">
                           <p>
                              56,B2,Oyster Apartment, <br>
                              4th Avenue,19th Street,<br>
                              Ashok Nagar,Chennai,<br>
                              Tamil Nadu 600083<br>
                              India<br>
                              <i class="fa fa-envelope"></i> reservations@ulohotels.com<br>
                              <i class="fa fa-phone"></i> +91-9543 592 593
                           </p>
                        </div>
                        <!-- end textwidget -->
                     </div>
                     <!-- end widget -->
                  </div>
                  <!-- end col -->
                  <div class="col-md-6 col-sm-5 col-xs-12">
                     <div class="widget ">
                        <div class="widget-title">
                           <h3>OUR USEFUL LINKS</h3>
                        </div>
                        <!-- end title -->
                        <div class="row">
                           <div class="col-md-6">
                              <div class="newsletterwidget">
                                 <ul style="list-style-type:none;">
                                    <!-- <li><a href="index.php">Home</a></li>-->
                                    <li><a href="aboutulo">About</a></li>
                                    <li><a href="privacy">Privacy policy</a></li>
                                    <li><a href="termsandconditions">Terms & conditions</a></li>
                                    <li><a href="refundterms">Cancellation And Refund Policy</a></li>
                                    <li><a href="team">Our Team</a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="newsletterwidget">
                                 <ul style="list-style-type:none;">
                                    <li><a href="partner-with-us">Partner With us</a></li>
                                    <li><a href="ulo-faq">Frequently Asked Questions</a></li>
                                    <li><a href="careers-ulo">Careers</a></li>
                                    <li><a href="contactus">Contact</a></li>
                                 </ul>
                              </div>
                              <!-- end newsletter widget -->
                           </div>
                        </div>
                        <!-- end widget -->
                     </div>
                  </div>
                  <!-- end col -->
                  <div class="col-md-2 col-sm-3 col-xs-12 text-center">
                     <div class="">
                        <img src="ulowebsite/images/logo-ft.png" alt="">
                     </div>
                     <!-- end logo -->
                  </div>
                  <!-- end col -->
               </div>
               <!-- end row -->
            </div>
            <!-- end container -->
         </footer>
         <!-- end copyrights -->
         <div class="copyrights clearfix">
            <div class="container">
               <div class="row">
                  <div class="col-md-6 text-left">
                     <p>	&copy; 2017 Ulo Hotels Private Limited, All Rights Reserved.</p>
                  </div>
                  <!-- end col -->
                  <div class="col-md-6 text-right">
                     <div class="social-footer">
                        <a href="https://www.facebook.com/Ulohotels/" title="Facebook"><i class="icon-facebook"></i></a>
                        <a href="https://twitter.com/ulohotels" title="Twitter"><i class="icon-twitter"></i></a>
                        <a href="https://www.pinterest.com/ulohotels/" title="Pinterest"><i class="icon-pinterest"></i></a>
                        <a href="https://goo.gl/ucr1n8" title="google-plus"><i class="icon-gplus"></i></a>
                     </div>
                     <!-- end social-footer -->
                  </div>
                  <!-- end col -->
               </div>
               <!-- end row -->
            </div>
            <!-- end container -->
         </div>
         <!-- end copyrights -->
      </div>
      <!-- end wrapper -->
   </body>
   <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
   <!-- jQuery UI 1.11.4 -->
   <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
   <script src="js1/ngprogress.js" type="text/javascript"></script>
   <!--<script src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
   <script async defer    
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg9dqRsKwRWXGdTKsUNHTHhfQqdSyip9k&callback=initMap"></script>
   <script src="ulowebsite/js/bootstrap.min.js"></script>
   <script src="ulowebsite/js/retina.js"></script>
   <script src="ulowebsite/js/sidebar.js"></script>
   <script src="ulowebsite/js/circle.js"></script>
   <script src="ulowebsite/js/progress.js"></script>
   <script src="ulowebsite/js/jquery.prettyPhoto.js"></script>
   <script src="ulowebsite/js/contact.js"></script>
   <script src="ulowebsite/js/parallax.js"></script>
   <script src="ulowebsite/js/owl.carousel.js"></script>
   <script src="ulowebsite/js/bootstrap-select.js"></script>
   <script src="ulowebsite/js/custom.js"></script>
   <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
   <script src="ulowebsite/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
   <script src="ulowebsite/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
   <script src="ulowebsite/js/revslider.js"></script>
   <script src="ulowebsite/js/lightslider.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
   <script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
   <script>
      var app = angular.module('myApp',['ngProgress']);
      app.controller('customersCtrl',function($scope, $http,$timeout,ngProgressFactory) {
      
      	
      	//$scope.progressbar = ngProgressFactory.createInstance();
      	//$scope.progressbar.setColor("green");
          //$scope.progressbar.start();
      	
            //	$timeout(function(){
           	    // $scope.progressbar.complete();
            //     $scope.show = true;
            //     $("#pre-loader").css("display","none");
            // }, 2000);
     	 var from = document.getElementById("datepicker");
    var to = document.getElementById("datepicker1");

    var today = new Date();
    
   var dd = today.getDate();
   var mm = today.getMonth()+1;
   var yy = today.getFullYear();

   today = dd+'/'+mm+'/'+yy;
   // alert(today);    
    from.value = (today);
    var today = new Date();
    var dd = today.getDate()+1;
   var mm = today.getMonth()+1;
   var yy = today.getFullYear();
    today1 = dd+'/'+mm+'/'+yy;
  // alert(today1);
   to.value = (today1);
   
   
   var from = document.getElementById("alternate");
    var to = document.getElementById("alternate1");

    var today = new Date();
    
   var dd = today.getDate();
   var mm = today.getMonth()+1;
   var yy = today.getFullYear();

   today = mm+'/'+dd+'/'+yy;
   // alert(today);    
    from.value = (today);
    var today = new Date();
    var dd = today.getDate()+1;
   var mm = today.getMonth()+1;
   var yy = today.getFullYear();
    today1 = mm+'/'+dd+'/'+yy;
  // alert(today1);
   to.value = (today1);
    
   $("#datepicker").datepicker({
	   dateFormat: 'dd/mm/yy',
	    altField: "#alternate",
	   altFormat: "mm/dd/yy",
	   minDate:  0,
	   onSelect: function (formattedDate) {
	       var date1 = $('#datepicker').datepicker('getDate');
	       var date = new Date( Date.parse( date1 ) );
	       date.setDate( date.getDate() + 1 );        
	       var newDate = date.toDateString();
	       newDate = new Date( Date.parse( newDate ) );  
	       $('#datepicker1').datepicker("option","minDate",newDate);
	       $timeout(function(){
	         //scope.checkIn = formattedDate;
	       });
	   }
	});

	$("#datepicker1").datepicker({
	    dateFormat: 'dd/mm/yy',
	     altField: "#alternate1",
	     altFormat: "mm/dd/yy",
	   minDate:  0,
	   onSelect: function (formattedDate) {
	       var date2 = $('#datepicker1').datepicker('getDate');
	       $timeout(function(){
	         //scope.checkOut = formattedDate;
	       });
	   }
	});
	
      		$scope.getLocation = function() {
      			
      			var url = "get-location";
      			$http.get(url).success(function(response) {
      				//console.log(response);
      				//alert(JSON.stringify(response.data));
      				$scope.location = response.data;
      			
      			});
      		};
      
      
      	
      	       
      	    	
              $scope.getLocation();
      				
      
      });
      
      
   </script>
   <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
   <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
   <!--Start of Tawk.to Script-->
   <script type="text/javascript">
      var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
      (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/586f59d212631a10690bdfa8/default';
      s1.charset='UTF-8';
      s1.setAttribute('crossorigin','*');
      s0.parentNode.insertBefore(s1,s0);
      })();
   </script>
  
   <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M3RH7N5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
   <!--End of Tawk.to Script-->
</html>