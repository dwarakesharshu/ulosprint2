<!-- jvectormap -->
<link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         {{date | date:'MMM dd, yyyy'}}
         <small></small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Dashboard</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-aqua"><i class="ion ion-person-add"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Arrivals</span>
                  <span class="info-box-number" ng-repeat="ar in arrival">{{ar.arrivals}}<small></small></span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-red"><i class="fa fa-bus"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Departures</span>
                  <span class="info-box-number" ng-repeat="dp in departure">{{dp.departures}}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
         <!-- fix for small devices only -->
         <div class="clearfix visible-sm-block"></div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-green"><i class="ion ion-pie-graph"></i></span>
               <div class="info-box-content" >
                  <span class="info-box-text">Rooms Occupied</span>
                  <span class="info-box-number" ng-repeat="oc in occupancy">{{oc.occupancy}}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
         <!-- Left col -->
         <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="box box-success">
               <div class="box-header with-border">
                  <h3 class="box-title">Today Bookings</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <div class="table-responsive">
                     <table class="table no-margin">
                        <thead>
                           <tr>
                              <th>Booking ID</th>
                              <!--  <th>Guest Name</th>
                                 <th>Room Category</th> -->
                              <th>Chech-In Date</th>
                              <th>Check-Out Date</th>
                              <!-- <th>Source</th> -->
                              <th>Status</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr ng-repeat="bc in bookdetail track by $index">
                              <td>{{bc.DT_RowId}}</td>
                              <!--  <td></td>
                                 <td></td> -->
                              <td>{{bc.arrivalDate}}</td>
                              <td>{{bc.departureDate}}</td>
                              <!-- <td>{{bc.sourceId}}</td> -->                          
                              <!-- <td><span class="label label-success">Checked-In</span></td> -->
                              <td>
                                 <select name="statusId" id="statusId" ng-model="statusId" ng-change="editBookingStatus(bc.DT_RowId,statusId,bc.arrivalDate,bc.departureDate,$index)" value="" ng-required="true">
                                    <option ng-repeat="st in status" value="{{st.statusId}}"  ng-selected ="st.statusId == bc.statusId">{{st.status}}</option>
                                 </select>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!-- /.table-responsive -->
               </div>
      
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
         <div class="col-md-12">
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Sales Projection</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <div class="row">
                     <div class="col-md-8">
                        <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                           <div class="input-group date">
                              <input type="text" class="form-control" value="" placeholder="Check in" id="checkIn" name="checkIn"  ng-required="true"> 
                              <%-- <%= ((session.getAttribute("checkIn1")==null)?"":session.getAttribute("checkIn1")) %> --%>
                              <label class="input-group-addon btn" for="checkIn">
                              <span class="fa fa-calendar"></span>
                              </label>  
                           </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                           <div class="input-group date">
                              <input type="text" class="form-control"  value="" placeholder="Check out" id="checkOut" name="checkOut" ng-required="true" >
                              <%--  <%= ((session.getAttribute("checkOut1")==null)?"":session.getAttribute("checkOut1")) %> --%>
                              <label class="input-group-addon btn" for="checkOut">
                              <span class="fa fa-calendar"></span>
                              </label> 
                           </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                           <button class="btn btn-primary" name ="searchGraphChart" id="searchGraphChart" >Search</button>
                        </div>
                        <p class="text-center">
                           <strong>
                              <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
                              <script>
                                 var startdate = moment();
                                 var new_date = moment(startdate).format('LL');
                                 document.write(new_date,'&nbsp &nbsp');
                                 document.write("To",'&nbsp &nbsp');
                                 
                                  var start = moment();
                                  var thing = start.add(30, 'days').format('LL');
                                  document.write(thing);
                              </script>
                           </strong>
                        </p>
                        <div class="chart" style="pointer-events: none;">
                           <!-- Sales Chart Canvas -->
                           <canvas id="salesChart" style="height: 180px;"></canvas>
                        </div>
                        <!-- /.chart-responsive -->
                     </div>
                     <!-- /.col -->
                     <div class="col-md-4">
                        <p class="text-center">
                           <strong>Accommodation</strong>
                        </p>
                        <div class="progress-group" ng-repeat="ad in accdetail">
                           <span class="progress-text">{{ad.accommodationType}}</span>
                           <span class="progress-number"><b>{{ad.roomCounts}}</b>/{{ad.total}}</span>
                           <div class="progress sm">
                              <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                           </div>
                        </div>
                     </div>
                     <!-- /.col -->
                  </div>
                  <!-- /.row -->
               </div>
               <!-- ./box-body -->
               <div class="box-footer">
                  <div class="row" >
                     <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right" >
                           <!-- <span class="description-percentage text-green"  ng-repeat="rv in revenue"><i class="fa fa-caret-up"></i>{{rv.totalAmount/1000}}%</span> -->
                           <h5 class="description-header" ng-repeat="rv in revenue"><strong>Rs &nbsp</strong>{{rv.totalAmount}}</h5>
                           <span class="description-text">TOTAL REVENUE</span>
                        </div>
                        <!-- /.description-block -->
                     </div>
                     <!-- /.col -->
                     <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                           <!-- <span class="description-percentage text-yellow" ng-repeat="rv in revenue"><i class="fa fa-caret-left"></i>  {{rv.totalRooms/100}}%</span> -->
                           <h5 class="description-header"  ng-repeat="rv in revenue" >{{rv.totalRooms}}</h5>
                           <span class="description-text">ROOM NIGHTS</span>
                        </div>
                        <!-- /.description-block -->
                     </div>
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.box-footer -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
   
   	$scope.date = new Date();
   	
         	$timeout(function(){
        	    // $scope.progressbar.complete();
              $scope.show = true;
              $("#pre-loader").css("display","none");
          }, 2000);
   
          $("#checkIn").datepicker({
              dateFormat: 'mm/dd/yy',
            
              minDate:  0,
              onSelect: function (formattedDate) {
                  var date1 = $('#checkIn').datepicker('getDate'); 
                  var date = new Date( Date.parse( date1 ) ); 
                  date.setDate( date.getDate() + 1 );        
                  var newDate = date.toDateString(); 
                  newDate = new Date( Date.parse( newDate ) );   
                  $('#checkOut').datepicker("option","minDate",newDate);
                  $timeout(function(){
                    //scope.checkIn = formattedDate;
                  });
              }
          });
   
          $("#checkOut").datepicker({
              dateFormat: 'mm/dd/yy',
            
              minDate:  0,
             
              onSelect: function (formattedDate) {
                  var date2 = $('#checkOut').datepicker('getDate'); 
                  $timeout(function(){
                    //scope.checkOut = formattedDate;
                  });
              }
          });
   	
   	$scope.unread = function() {

   	var notifiurl = "unreadnotifications.action";
   	$http.get(notifiurl).success(function(response) {
   		$scope.latestnoti = response.data;
   	});
   	};

             
             $scope.getArrival = function() {
     			
     			var url = 'get-arrival';
     			$http.get(url).success(function(response) {
     			   //console.log(response);
     				$scope.arrival= response.data;
   
     			});
     		};    
     		
     		 $scope.getDeparture = function() {
     				
     				var url = 'get-departure';
     				$http.get(url).success(function(response) {
     				   //console.log(response);
     					$scope.departure= response.data;
   
     				});
     			};
     			
             $scope.getStatus = function() {
     				
     				var url = 'get-status';
     				$http.get(url).success(function(response) {
     				   //console.log(response);
     					$scope.status= response.data;
   
     				});
     			};
     			
   	   $scope.editBookingStatus = function(bookingId,statusId,arrival,departure,indx) {

   		   
   		   var gdata = "bookingId=" + bookingId
   			+ "&statusId=" + statusId
 
   		   $http(
   					{
   						method : 'POST',
   						data : gdata,
   						headers : {
   							'Content-Type' : 'application/x-www-form-urlencoded'
   						},
   						url : 'edit-booking-status'
   					}).then(function successCallback(response) {
   						
   						 var gdata = "bookingId=" + bookingId
   							+ "&statusId=" + statusId
   					
   						
   						$http(
   							    {
   								method : 'POST',
   								data : gdata,										
   								dataType: 'json',
   								headers : {
   								
   									'Content-Type' : 'application/x-www-form-urlencoded'
   								},
   								url : 'edit-booking-details-status'										
   							   }).success(function(response) {
   						
   								}); 
   					
   					});
     				
     			};
     			
     			$scope.getTodayOccupancy = function() {
     				
     				var url = 'get-todayoccupancy';
     				$http.get(url).success(function(response) {
     				   //console.log(response);
     					$scope.occupancy= response.data;
   
     				});
     			};   
     			
     			
   		$scope.getAllBookings = function() {
   			
   			var url = "get-allbookings";
   			$http.get(url).success(function(response) {
   			    //console.log(response);
   				$scope.bookdetail= response.data;
   				
   	
   			});
   		};
   		
   			$scope.getPropertyAccommodation = function() {
   			
   			var url = "get-propertyaccommodation";
   			$http.get(url).success(function(response) {
   			    //console.log(response);
   				$scope.accdetail= response.data;
   				
   	
   			});
   		};  
   		
   			
   			$scope.getPropertyRevenue = function() {
   				
   				var url = 'get-propertyrevenue';
   				$http.get(url).success(function(response) {
   					
   				$scope.revenue= response.data;
        
   					
   						});
   				//return total;
   			};
   			
                   
             
             
            // $scope.getBookings();
   	
   	$scope.getArrival();
             
             $scope.getDeparture();
             
             $scope.getTodayOccupancy();
             
             $scope.getAllBookings();
             
   	$scope.getPropertyAccommodation();
             
             $scope.getPropertyRevenue();
             
             $scope.getStatus();
       
   	//$scope.unread();
   	//
   	
   	
   });    
   
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script>
   var enumerateDaysBetweenDates = function(startDate, endDate) {
   var now = startDate,
   dates = [];
  
   while (now.isBefore(endDate) || now.isSame(endDate)) {
         dates.push(now.format("YYYY-MM-DD"));
  
         now.add('days',1 );
     }
   return dates;
   };
   
   var fromDate = moment();

   var toDate   = moment().add('days',30);

  
   results = enumerateDaysBetweenDates(fromDate, toDate); 

   var enumerateDaysBetweenDates = function(startDate, endDate) {
   var now = startDate,
   dates = [];
   

   while (now.isBefore(endDate) || now.isSame(endDate)) {
        dates.push(now.format("MMM D"));
 
   
        now.add('days',1);
    }
   return dates;
   };
   
   var fromDate = moment();

   var toDate   = moment().add('days', 30);
  
   result = enumerateDaysBetweenDates(fromDate, toDate); 

   
</script>   
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script></script>
<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard2.js"></script>