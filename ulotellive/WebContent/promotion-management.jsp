<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Promotions
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Property</a></li>
            <li class="active">Profile</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
       
        	
        	<div class="row">
       
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
              	<ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Last Minute Promotion</a></li>
                  <li ><a href="#tab-rooms" data-toggle="tab">Rooms Promotion</a></li>
                  <li ><a href="#tab-nights" data-toggle="tab">Nights Promotion</a></li>
                  <li ><a href="#tab-flat" data-toggle="tab">Flat Promotion</a></li>
                  <li ><a href="#tab-active" data-toggle="tab">Active Promotion</a></li>
                </ul>
             <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
                  <form method="post" theme="simple" name="lastMinutePromotionForm" >
                  <section class="content">
                  	<div class=" form-group col-md-4">
						 <label>Start Date :</label>
						 <div class="input-group date">
		                     <label class="input-group-addon btn" for="lastStartDate">
		                     <span class="fa fa-calendar"></span>
		                     </label>   
		                     <input type="text" id="lastStartDate" type="text" class="form-control" name="lastStartDate"  value="" onkeypress="return false;" ng-required="true">
	                	</div>
					</div>
					<div class="form-group col-md-4">
						<label >End Date :</label>
					    <div class="input-group date">
	                	  	<label class="input-group-addon btn" for="lastEndDate">
	                   		<span class="fa fa-calendar"></span>
	              			</label>   
	                  		<input type="text" id="lastEndDate" type="text" class="form-control" name="lastEndDate"  value="" onkeypress="return false;" ng-required="true"> 
	    	            </div>
					</div>
					<div class="form-group col-md-4">
						<label>Promotion Name :</label>
						<input type="text" class="form-control"  name="lastPromotionName"  value=""  ng-model='lastPromotionName' ng-pattern= "/^[a-zA-Z0-9]*$/" id="lastPromotionName" ng-required="true">
						<span ng-show="lastMinutePromotionForm.lastPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
					</div>
					<div class="form-group col-md-6">
						<label>Percentage :</label>
						<input type="text" class="form-control"  name="lastPercentAmount"  id="lastPercentAmount" ng-model="lastPercentAmount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Percentage">
					    <span ng-show="lastMinutePromotionForm.lastPercentAmount.$error.pattern" style="color:red">Please Enter Numeric Value</span>
					</div>
					<div class="form-group col-md-6">
						<label>Hours :</label>
						<input type="text" class="form-control"  name="lastHours"  id="lastHours" ng-model="lastHours" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Hours">
					       <span ng-show="lastMinutePromotionForm.lastHours.$error.pattern" style="color:red">Please Enter Hours</span>
					</div>
					<%-- <div class="form-group col-md-3">
						  <label>Select Accommodation Type :</label>
						  <select  name="lastPropertyAccommodationId" id="lastPropertyAccommodationId" value="" class="form-control" >
						  <option style="display:none" value="">Select Your  Accommodation</option>
						  <option style="display:none" value="">All</option>
		                  <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
		                  </select>
					</div> --%>
					<div class="form-group col-md-6" id="lastcheckedaccommodations">
							<label>Select Accommodation Type :</label>
						   <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="lastCheckAccommodationAll" value="0">All</label>
                          <div ng-repeat="at in accommodations">
                         
                      					<input type="checkbox" name="checkedAccommodation[]"  id="checkedAccommodation" value="{{at.accommodationId}}"  class="flat-red" >&nbsp {{at.accommodationType}}
                    				
                    				</div>
						   </div>

					</div>
					<div class="form-group col-md-6" id="lastcheckeddays">
							<label>Days of week :</label>
						   <div class="input-group" >

						   <label class="checkbox-inline"><input type="checkbox"  id="lastCheckAll" value="0">All Days</label>
                          <div ng-repeat="d in days">
                              <label class="checkbox-inline">
                      					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
                      					</div>
                    			</label>
                    				
						   </div>

					</div>
				
	             
	                      <div class="row fontawesome-icon-list">
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4">
	                        	<button type="submit" ng-click="lastMinutePromotionForm.$valid && addGetLastMinutePromotions()" ng-disabled="lastMinutePromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">Save</button>
	                        </div>
	                     </div>
	               </section>
                  </form>
                  </div>
                  <div class="tab-pane" id="tab-rooms">
                  <form method="post" theme="simple" name="roomPromotionForm">
                  	<div class=" form-group col-md-4">
						 <label>Start Date :</label>
						 <div class="input-group date">
		                     <label class="input-group-addon btn" for="roomStartDate">
		                     <span class="fa fa-calendar"></span>
		                     </label>   
		                     <input type="text" id="roomStartDate" type="text" class="form-control" name="roomStartDate"  value="" onkeypress="return false;" ng-required="true">
	                	</div>
					</div>
					<div class="form-group col-md-4">
						<label >End Date :</label>
					    <div class="input-group date">
	                	  	<label class="input-group-addon btn" for="roomEndDate">
	                   		<span class="fa fa-calendar"></span>
	              			</label>   
	                  		<input type="text" id="roomEndDate" type="text" class="form-control" name="roomEndDate"  value="" onkeypress="return false;" ng-required="true"> 
	    	            </div>
					</div>
					<div class="form-group col-md-4">
						<label>Promotion Name :</label>
						<input type="text" class="form-control"  name="roomPromotionName"  value=""  ng-model='roomPromotionName' ng-pattern= "/^[a-zA-Z0-9]*$/" id="roomPromotionName" ng-required="true">
						<span ng-show="roomPromotionForm.roomPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
					</div>
					<!-- <div class="form-group col-md-3">
						<label>Percentage</label>
						<input type="text" class="form-control"  name="roomPercentAmount"  id="roomPercentAmount" ng-model="roomPercentAmount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Percentage">
					</div> -->
					<%-- <div class="form-group col-md-3">
						  <label>Select Accommodation Type :</label>
						  <select  name="roomPropertyAccommodationId" id="roomPropertyAccommodationId" value="" class="form-control" >
						  <option style="display:none" value="">Select Your  Accommodation</option>
						  <option style="display:none" value="">All</option>
		                  <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
		                  </select>
					</div> --%>
					          <h4 class="page-header">Room Promotion Details </h4>
	                      <div class="col-md-2 form-group ">
	                      	<label>Book Rooms :</label>
	                        <input type="text" class="form-control"  name="bookRooms"  id="bookRooms" ng-model="bookRooms" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms">
	                        <span ng-show="roomPromotionForm.bookRooms.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Get Rooms :</label>
	                        <input type="text" class="form-control"  name="getRooms"  id="getRooms" ng-model="getRooms" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Get Rooms">
	                        <span ng-show="roomPromotionForm.getRooms.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Tariff :</label>
	                        <input type="text" class="form-control"  name="roomBaseAmount"  id="roomBaseAmount" ng-model="roomBaseAmount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms">
	                        <span ng-show="roomPromotionForm.roomBaseAmount.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Extra Adult :</label>
	                        <input type="text" class="form-control"  name="roomExtraAdult"  id="roomExtraAdult" ng-model="roomExtraAdult" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms">
	                        <span ng-show="roomPromotionForm.roomExtraAdult.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Extra Child :</label>
	                        <input type="text" class="form-control"  name="roomExtraChild"  id="roomExtraChild" ng-model="roomExtraChild" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms">
	                        <span ng-show="roomPromotionForm.roomExtraChild.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
					<div class="form-group col-md-6" id="roomcheckedaccommodations">
							<label >Select Accommodation Type :</label>
						 <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="roomCheckAccommodationAll" value="0">All</label>
                          <div ng-repeat="at in accommodations">
                          <label class="checkbox-inline">
                      					<input type="checkbox" name="checkedAccommodation[]"  id="checkedAccommodation" value="{{at.accommodationId}}"  class="flat-red" >&nbsp {{at.accommodationType}}
                    				</label>
                    				</div>
						  </div>

					</div>
					<div class="form-group col-md-12" id="roomcheckeddays">
							<label>Days of week :</label>
						  <div class="input-group" >
						   	<label class="checkbox-inline"><input type="checkbox"  id="roomCheckAll" value="0">All Days</label>
                       
                           <label class="checkbox-inline" ng-repeat="d in days" >
                      					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
                    				</label>
                    				
                    				</div>
						  
					</div>
 
               
	                      <div class="row fontawesome-icon-list">
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4">
	                        	<button type="submit" ng-click="roomPromotionForm.$valid && addGetRoomPromotions()" ng-disabled="roomPromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">Save</button>
	                        </div>
	                     </div>
	            
                  </form>
                  </div>
                  <!-- NIGHT PROMOITION -->
                  <div class="tab-pane" id="tab-nights">
                  <form method="post" theme="simple" name="nightsPromotionForm">
                  	<div class=" form-group col-md-4">
						 <label>Start Date :</label>
						 <div class="input-group date">
		                     <label class="input-group-addon btn" for="nightStartDate">
		                     <span class="fa fa-calendar"></span>
		                     </label>   
		                     <input type="text" id="nightStartDate" type="text" class="form-control" name="nightStartDate"  value="" onkeypress="return false;" ng-required="true">
	                	</div>
					</div>
					<div class="form-group col-md-4">
						<label >End Date :</label>
					    <div class="input-group date">
	                	  	<label class="input-group-addon btn" for="nightEndDate">
	                   		<span class="fa fa-calendar"></span>
	              			</label>   
	                  		<input type="text" id="nightEndDate" type="text" class="form-control" name="nightEndDate"  value="" onkeypress="return false;" ng-required="true"> 
	    	            </div>
					</div>
					<div class="form-group col-md-4">
						<label>Promotion Name :</label>
						<input type="text" class="form-control"  name="nightPromotionName"  value=""  ng-model='nightPromotionName' ng-pattern= "/^[a-zA-Z0-9]*$/" id="nightPromotionName" ng-required="true">
						<span ng-show="nightsPromotionForm.nightPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
					</div>
					<!-- <div class="form-group col-md-3">
						<label>Percentage</label>
						<input type="text" class="form-control"  name="nightPercentAmount"  id="nightPercentAmount" ng-model="nightPercentAmount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Percentage">
					</div> -->
					<%-- <div class="form-group col-md-3">
						  <label>Select Accommodation Type :</label>
						  <select  name="nightPropertyAccommodationId" id="nightPropertyAccommodationId" value="" class="form-control" >
						  <option style="display:none" value="">Select Your  Accommodation</option>
		                  <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
		                  </select>
					</div> --%>
					
                      <h4 class="page-header">Night Promotion Details </h4>
	                      <div class="col-md-2 form-group " >
	                      	<label>Book Nights :</label>
	                        <input type="text" class="form-control"  name="bookNights"  id="bookNights" ng-model="bookNights" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms">
	                        <span ng-show="nightsPromotionForm.bookNights.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Get Nights :</label>
	                        <input type="text" class="form-control"  name="getNights"  id="getNights" ng-model="getNights" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Get Rooms">
	                        <span ng-show="nightsPromotionForm.getNights.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Tariff :</label>
	                        <input type="text" class="form-control"  name="nightsBaseAmount"  id="nightsBaseAmount" ng-model="nightsBaseAmount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms">
	                        <span ng-show="nightsPromotionForm.nightsBaseAmount.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Extra Adult :</label>
	                        <input type="text" class="form-control"  name="nightsExtraAdult"  id="nightsExtraAdult" ng-model="nightsExtraAdult" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms">
	                        <span ng-show="nightsPromotionForm.nightsExtraAdult.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Extra Child :</label>
	                        <input type="text" class="form-control"  name="nightsExtraChild"  id="nightsExtraChild" ng-model="nightsExtraChild" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms">
	                        <span ng-show="nightsPromotionForm.nightsExtraChild.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div> 
	                    <div class="form-group col-md-12" id="nightscheckedaccommodations">
							<label>Select Accommodation Type :</label>
						   <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="nightsCheckAccommodationAll" value="0">All</label>
                          
                          <label class="checkbox-inline" ng-repeat="at in accommodations">
                      					<input type="checkbox" name="checkedAccommodation[]"  id="checkedAccommodation" value="{{at.accommodationId}}"  class="flat-red" >&nbsp {{at.accommodationType}}
                    				</label>
                    				
						   </div>

					</div>
					<div class="form-group col-md-12" id="nightscheckeddays">
							<label>Days of week :</label>
						   <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="nightsCheckAll" value="0">All Days</label>
                          
                         <label class="checkbox-inline" ng-repeat="d in days">
                      					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
                    				</label>
                    			
						   </div>

						</div>                      
                   
	                      <div class="row fontawesome-icon-list">
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4">
	                        	<button type="submit" ng-click="nightsPromotionForm.$valid && addGetNightPromotions()" ng-disabled="nightsPromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">Save</button>
	                        </div>
	                     </div>
	              
                  </form>
                  </div>
                  <div class="tab-pane" id="tab-flat">
                  <form method="post" theme="simple" name="flatPromotionForm">
              
                  	<div class=" form-group col-md-6">
						 <label>Start Date :</label>
						 <div class="input-group date">
		                     <label class="input-group-addon btn" for="flatStartDate">
		                     <span class="fa fa-calendar"></span>
		                     </label>   
		                     <input type="text" id="flatStartDate" type="text" class="form-control" name="flatStartDate"  value="" onkeypress="return false;" ng-required="true">
	                	</div>
					</div>
					<div class="form-group col-md-6">
						<label >End Date :</label>
					    <div class="input-group date">
	                	  	<label class="input-group-addon btn" for="endDate">
	                   		<span class="fa fa-calendar"></span>
	              			</label>   
	                  		<input type="text" id="flatEndDate" type="text" class="form-control" name="flatEndDate"  value="" onkeypress="return false;" ng-required="true"> 
	    	            </div>
					</div>
					<div class="form-group col-md-6">
						<label>Promotion Name :</label>
						<input type="text" class="form-control"  name="flatPromotionName"  value=""  ng-model='flatPromotionName' ng-pattern= "/^[a-zA-Z0-9]*$/" id="flatPromotionName" ng-required="true">
						<span ng-show="flatPromotionForm.flatPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
					</div>
					<div class="form-group col-md-6">
						<label>Percentage :</label>
						<input type="text" class="form-control"  name="flatPercentAmount"  id="flatPercentAmount" ng-model="flatPercentAmount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Percentage">
					  <span ng-show="flatPromotionForm.flatPercentAmount.$error.pattern" style="color:red">Not a valid Number!</span>
					</div>
					<div class="form-group col-md-12" id="flatcheckeddays">
							<label>Days of week :</label>
						   <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="flatCheckAll" value="0">All Days</label>
                         
                           <label class="checkbox-inline" ng-repeat="d in days">
                 					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
               				</label>
               			
						   </div>

					</div>
			
				
	                      <div class="row fontawesome-icon-list">
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4">
	                        	<button type="submit" ng-click="flatPromotionForm.$valid && addGetFlatPromotions()" ng-disabled="flatPromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">Save</button>
	                        </div>
	                     </div>
	             
                  </form>
                  </div>
                  <div class="tab-pane" id="tab-active">
                  <form method="post" theme="simple" name="activepromotions">
                  <div class="box-body table-responsive no-padding" > 
						<table class="table table-hover" id="example1">
						<thead>
						<tr>
							<th>Order</th>
						    <th>Start</th>
						   <th>End</th>
						   <th>Promotion Name</th>
						   <th>Accommodation Type</th>
						   
						    
							<th>View</th> 
							<th>Delete</th>
							</tr>
						</thead>
						<tbody class="sortable">
								<tr ng-repeat="p in promotions track by $index"  >
								<td>{{$index}}</td>
							    <td>{{p.startDate}}</td>
								<td>{{p.endDate}}</td>
								<td>{{p.promotionName}}</td>										
								<td>{{p.accommodationType}}</td>
								 <td>
									<a href="#viewmodal" ng-click="getActive(p.promotionId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
								
								</td> 
									<td>
									
<!-- 									<a href="#deletemodal" ng-click="getInactive(p.promotionId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a> -->
									<a href="#" ng-click="deletedPromotions(p.promotionId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
								</td> 
							</tr>
						
						</tbody>
					
						</table>
					</div>
                  </form>
                  </div>
                  <div class="modal" id="viewmodal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
									aria-hidden="true">x</button>
								<h4 class="modal-title">View Promotion</h4>
							</div>
							<form name="deletePromotions" id="deletePromotions" >
							<div class="modal-body" ng-repeat="p in viewPromotions">
							
							<div class="form-group col-md-12">
								  <h4><b>{{p.promotionType}}</b></h4>
							 </div>
							  <div class="form-group col-md-4">
							      <label>Accommodation Type :</label>
							      <h5>{{p.accommodationType}}</h5>
							 </div>
								
							<div class=" form-group col-md-4">
								 <label>Start Date :</label>
								 <h5>{{p.startDate}}</h5>
							</div>
								 
							<div class="form-group col-md-4">
								<label >Percentage :</label>
								<h5>{{p.promotionPercent}}</h5>
							</div>	
							
							<div class="form-group col-md-4">
								<label >Promotion Hours :</label>
								<h5>{{p.promotionHours}}</h5>
							</div>	
							
							<div class="form-group col-md-4">
								<label >Tariff :</label>
								<h5>{{p.promotionBaseAmount}}</h5>
							</div>	 
							
							<div class="form-group col-md-4">
								<label >Book Rooms :</label>
								<h5>{{p.promotionBookRooms}}</h5>
							</div>	
							
							<div class="form-group col-md-4">
								<label >Get Rooms :</label>
								<h5>{{p.promotionGetRooms}}</h5>
							</div>		
							
							<div class="form-group col-md-4">
								<label >Book Nights :</label>
								<h5>{{p.promotionBookNights}}</h5>
							</div>	
							
							<div class="form-group col-md-4">
								<label >Get Nights :</label>
								<h5>{{p.promotionGetNights}}</h5>
							</div> 
					
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn">Close</a>
							</div>
							</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dalog -->
				</div>
                  <div class="modal" id="deletemodal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
									aria-hidden="true">x</button>
								<h4 class="modal-title">Delete Promotions</h4>
							</div>
							<form name="deletePromotions" id="deletePromotions" >
							<div class="modal-body" ng-repeat="p in updatePromotions">
							<input type="hidden" class="form-control" value="{{p.promotionId}}" name="updatePromotionId"  id="updatePromotionId" ng-required="true">
							<div class="form-group col-md-12">
								  <h4><b>{{p.promotionType}}</b></h4>
							 </div>
							  <div class="form-group col-md-4">
							      <label>Accommodation Type :</label>
								  <input type="text" class="form-control"  name="updateAccommodationId"  id="updateAccommodationId" value="{{p.accommodationType}}" ng-required="true" placeholder="accommodation">
							 </div>
								
							<div class=" form-group col-md-4">
								 <label>Start Date :</label>
								 <div class="input-group date">
				                     <label class="input-group-addon btn">
				                     <span class="fa fa-calendar"></span>
				                     </label>   
				                     <input type="text" id="updateStartDate" type="text" class="form-control" name="updateStartDate"  value="{{p.startDate}}" onkeypress="return false;" ng-required="true">
			                	</div>
							</div>
							<div class="form-group col-md-4">
								<label >End Date :</label>
							    <div class="input-group date">
			                	  	<label class="input-group-addon btn">
			                   		<span class="fa fa-calendar"></span>
			              			</label>   
			                  		<input type="text" id="updateEndDate" type="text" class="form-control" name="updateEndDate"  value="{{p.endDate}}" onkeypress="return false;" ng-required="true"> 
			    	            </div>
							</div>	 
								 
					
							<div class="modal-footer">
			
								<a href="#" data-dismiss="modal" class="btn">Close</a>
								<button type="submit" ng-click="deletePromotions()" class="btn btn-primary">Delete</button>
							</div>
							</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dalog -->
				</div>
                  </div>
           
              </div>
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
            </section><!-- /.content -->
          </div><!-- /.row -->

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }
</style>
	<script>
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
     
		
          
          var sortableEle;
              sortableEle = $('.sortable').sortable({
              start: $scope.dragStart,
              update: $scope.dragEnd
              
          });
              $("#lastCheckAll").click(function () {
 			     $("#lastcheckeddays :checkbox").not(this).prop('checked', this.checked);
 			    
 			 });
              $('#lastCheckAccommodationAll').click(function(){
            	  $("#lastcheckedaccommodations :checkbox").not(this).prop('checked', this.checked);
              });
              $("#roomCheckAll").click(function () {
  			     $("#roomcheckeddays :checkbox").not(this).prop('checked', this.checked);
  			 });
              $("#roomCheckAccommodationAll").click(function () {
   			     $("#roomcheckedaccommodations :checkbox").not(this).prop('checked', this.checked);
   			 });
              $("#nightsCheckAll").click(function () {
  			     $("#nightscheckeddays :checkbox").not(this).prop('checked', this.checked);
  			 });
              $("#nightsCheckAccommodationAll").click(function () {
   			     $("#nightscheckedaccommodations :checkbox").not(this).prop('checked', this.checked);
   			 });
              $("#flatCheckAll").click(function () {
  			     $("#flatcheckeddays :checkbox").not(this).prop('checked', this.checked);
  			 });

		
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

		 
	       $("#lastStartDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#lastStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() + 1 );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#lastEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                  //scope.checkIn = formattedDate;
	                });
	            }
	        });
	      
	      $("#lastEndDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#lastEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                  //scope.checkOut = formattedDate;
	                });
	            }
	        });
	      $("#roomStartDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#roomStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() + 1 );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#roomEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                  //scope.checkIn = formattedDate;
	                });
	            }
	        });
	      
	      $("#roomEndDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#roomEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                  //scope.checkOut = formattedDate;
	                });
	            }
	        });
	      
	      $("#nightStartDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#nightStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() + 1 );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#nightEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                  //scope.checkIn = formattedDate;
	                });
	            }
	        });
	      
	      $("#nightEndDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#nightEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                  //scope.checkOut = formattedDate;
	                });
	            }
	        });
	      
	      $("#flatStartDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#flatStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() + 1 );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#flatEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                  //scope.checkIn = formattedDate;
	                });
	            }
	        });
	      
	      $("#flatEndDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#flatEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                  //scope.checkOut = formattedDate;
	                });
	            }
	        });
	      
	     
	      
	      $scope.addGetLastMinutePromotions = function(){
			    
			    var checkbox_days_value = "";
				    $("#lastcheckeddays :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				        	checkbox_days_value += $(this).val() + ",";
				        }
				    });
				    
			    var checkbox_accommodation_value = "";
			    $("#lastcheckedaccommodations :checkbox").each(function () {
			        var ischecked = $(this).is(":checked");
			        if (ischecked) {
			        	checkbox_accommodation_value += $(this).val() + ",";
			        }
			    });    
		     	
		     	var fdata = "checkedDays=" + checkbox_days_value
		     	+ "&checkedAccommodations=" + checkbox_accommodation_value
				+ "&startDate=" + $('#lastStartDate').val()
				+ "&endDate=" + $('#lastEndDate').val()
				+ "&promotionName=" + $('#lastPromotionName').val()
				+ "&percentage="+ $('#lastPercentAmount').val()
				+ "&promotionHours="+ $('#lastHours').val()
				+ "&promotionType=L"
				
				
				
				
				
				alert('fdaaaa'+fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'add-promotions'
							
						}).then(function successCallback(response) {
							
							var promotionchk=response.data.data[0].check;
							
							if(promotionchk=="true"){
								alert('Already have an Promotions');
							}
							else if(promotionchk=="false"){
							
							var gdata = "checkedDays=" + checkbox_days_value
					     	+ "&startDate=" + $('#lastStartDate').val()
							+ "&endDate=" + $('#lastEndDate').val()
							+ "&promotionName=" + $('#lastPromotionName').val()
							+ "&percentage="+ $('#lastPercentAmount').val()
							+ "&promotionHours="+ $('#lastHours').val()
							+ "&promotionType=L"
							
							alert('gdata')
							alert(gdata);
							$http(
						    {
							method : 'POST',
							data : gdata,
							//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
							
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
								//'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'add-promotion-details'
							//url : 'add-last-minute-promotion-details'
						   }).then(function successCallback(response) {
							   window.location.reload();
							
						    alert("promotions added successfully");
						  
						    
						   }, function errorCallback(response) {
							
							  
							
						});
							
					}		
							
				}, function errorCallback(response) {
					
				  
					
				});

			};
	     
			$scope.addGetRoomPromotions = function(){
			    
			    var checkbox_value = "";
				    $("#roomcheckeddays :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				
			    var checkbox_accommodation_value = "";
			    $("#roomcheckedaccommodations :checkbox").each(function () {
			        var ischecked = $(this).is(":checked");
			        if (ischecked) {
			        	checkbox_accommodation_value += $(this).val() + ",";
			        }
			    });    
		     	
		     	var fdata = "checkedDays=" + checkbox_value
		     	+ "&checkedAccommodations=" + checkbox_accommodation_value
				+ "&startDate=" + $('#roomStartDate').val()
				+ "&endDate=" + $('#roomEndDate').val()
				+ "&promotionName=" + $('#roomPromotionName').val()
				+ "&bookNights="+ $('#bookNights').val()
		     	+ "&getNights="+ $('#getNights').val()
		     	+ "&promotionBaseAmount="+ $('#roomBaseAmount').val()
		     	+ "&promotionExtraAdult="+ $('#roomExtraAdult').val()
		     	+ "&promotionExtraChild="+ $('#roomExtraChild').val()
		     	+ "&promotionType=R"
				
				
				
				
				
				alert('fdaaaa'+fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'add-promotions'
						//	url : 'add-rooms-promotion'
						}).then(function successCallback(response) {
							
							var promotionchk=response.data.data[0].check;
							
							if(promotionchk=="true"){
								alert('Already have an Promotions');
							}
							else if(promotionchk=="false"){
							var gdata = "checkedDays=" + checkbox_value
							+ "&startDate=" + $('#roomStartDate').val()
							+ "&endDate=" + $('#roomEndDate').val()
							+ "&promotionName=" + $('#roomPromotionName').val()
							+ "&bookNights="+ $('#bookRooms').val()
					     	+ "&getNights="+ $('#getRooms').val()
					     	+ "&promotionBaseAmount="+ $('#roomBaseAmount').val()
					     	+ "&promotionExtraAdult="+ $('#roomExtraAdult').val()
					     	+ "&promotionExtraChild="+ $('#roomExtraChild').val()
					     	+ "&promotionType=R"
							
							//alert(gdata);
							$http(
						    {
							method : 'POST',
							data : gdata,
							//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
							
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
								//'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'add-promotion-details'
							//url : 'add-rooms-promotion-details'
						   }).then(function successCallback(response) {
							   window.location.reload();
							
						    alert("promotions added successfully");
						  
						    
						   }, function errorCallback(response) {
							
							  
							
						});
							
					}		
							
				}, function errorCallback(response) {
					
				  
					
				});

			};
			
			$scope.addGetNightPromotions = function(){
			    
			    var checkbox_value = "";
				    $("#nightscheckeddays :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				    
			    var checkbox_accommodation_value = "";
			    $("#nightscheckedaccommodations :checkbox").each(function () {
			        var ischecked = $(this).is(":checked");
			        if (ischecked) {
			        	checkbox_accommodation_value += $(this).val() + ",";
			        }
			    });    
				    
		     	
		     	var fdata = "checkedDays=" + checkbox_value
		     	+ "&checkedAccommodations=" + checkbox_accommodation_value
				+ "&startDate=" + $('#nightStartDate').val()
				+ "&endDate=" + $('#nightEndDate').val()
				+ "&promotionName=" + $('#nightPromotionName').val()
				+ "&bookNights="+ $('#bookNights').val()
		     	+ "&getNights="+ $('#getNights').val()
		     	+ "&promotionBaseAmount="+ $('#nightsBaseAmount').val()
		     	+ "&promotionExtraAdult="+ $('#nightsExtraAdult').val()
		     	+ "&promotionExtraChild="+ $('#nightsExtraChild').val()
		     	+ "&promotionType=N"
				
				
				
				
				
				alert('fdaaaa'+fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'add-promotions'
							//url : 'add-nights-promotion'
						}).then(function successCallback(response) {
							var promotionchk=response.data.data[0].check;
							
							if(promotionchk=="true"){
								alert('Already have an Promotions');
							}
							else if(promotionchk=="false"){
							
							var gdata = "checkedDays=" + checkbox_value
							+ "&startDate=" + $('#nightStartDate').val()
							+ "&endDate=" + $('#nightEndDate').val()
							+ "&promotionName=" + $('#nightPromotionName').val()
							+ "&bookNights="+ $('#bookNights').val()
					     	+ "&getNights="+ $('#getNights').val()
					     	+ "&promotionBaseAmount="+ $('#nightsBaseAmount').val()
					     	+ "&promotionExtraAdult="+ $('#nightsExtraAdult').val()
					     	+ "&promotionExtraChild="+ $('#nightsExtraChild').val()
					     	+ "&promotionType=N"
							
							//alert(gdata);
							$http(
						    {
							method : 'POST',
							data : gdata,
							//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
							
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
								//'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'add-promotion-details'
							//url : 'add-nights-promotion-details'
						   }).then(function successCallback(response) {
							   window.location.reload();
							
						    alert("promotions added successfully");
						  
						    
						   }, function errorCallback(response) {
							
							  
							
						});
							
						}	
							
				}, function errorCallback(response) {
					
				  
					
				});

			};
			
			$scope.addGetFlatPromotions = function(){
			    
			    var checkbox_value = "";
				    $("#flatcheckeddays :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				    
		     	
		     	var fdata = "checkedDays=" + checkbox_value
				+ "&startDate=" + $('#flatStartDate').val()
				+ "&endDate=" + $('#flatEndDate').val()
				+ "&promotionName=" + $('#flatPromotionName').val()
				+ "&percentage="+ $('#flatPercentAmount').val()
				+ "&promotionType=F"
				
				
				
				
				alert('fdaaaa'+fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'add-promotions'
						//	url : 'add-flat-promotion'
						}).then(function successCallback(response) {
							var promotionchk=response.data.data[0].check;
							
							if(promotionchk=="true"){
								alert('Already have an Promotions');
							}
							else if(promotionchk=="false"){
								var gdata = "checkedDays=" + checkbox_value
								+ "&StartDate=" + $('#flatStartDate').val()
								+ "&EndDate=" + $('#flatEndDate').val()
								+ "&PromotionName=" + $('#flatPromotionName').val()
								+ "&Percentage="+ $('#flatPercentAmount').val()
								+ "&promotionType=F"
								
								alert('gdata..'+gdata);
								$http(
							    {
								method : 'POST',
								data : gdata,
								//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
								
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
									//'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url: 'add-promotion-details'
								//url : 'add-flat-promotion-details'
							   }).then(function successCallback(response) {
								   window.location.reload();
								
							    alert("promotions added successfully");
							  
							    
							   }, function errorCallback(response) {
								
								  
								
							});
						}
							
				}, function errorCallback(response) {
					
				  
					
				});

			};
			
			$scope.deletedPromotions=function(rowid){
				var fdata = "&updatePromotionId="+ rowid
				var deletePromotionsCheck=confirm("Do u want to disable the promotion?");
				if(deletePromotionsCheck){
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url: 'get-promotions-disable'
							}).then(function successCallback(response) {
								window.location.reload();
							   }, function errorCallback(response) {
								
								
							});
				}
				else{
					
				}
				
			};
			
		$scope.getDay = function() {

			var url = "get-day";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.days = response.data;
	
			});
		};	
			
			
		$scope.getAccommodations = function() {

			var url = "get-accommodations";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.accommodations = response.data;
	
			});
		};
		
		$scope.getPromotions = function() {

			var url = "get-promotions";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.promotions = response.data;
	
			});
		};
		
		$scope.getInactive = function(rowid){
			var url="get-inactive-promotions?updatePromotionId="+rowid;
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.updatePromotions = response.data;
	
			});
		};
		
		$scope.getActive = function(rowid){
			var url="get-active-promotions?updatePromotionId="+rowid;
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.viewPromotions = response.data;
	
			});
		};
			
		$scope.getAccommodations();
		$scope.getDay();
		$scope.getPromotions();
		
	});





	</script>
   