<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <style>
/* The container <div> - needed to position the dropdown content */

        .mar-b10 i{
    margin-bottom: 10px;
}
        .bold{
            font-weight:bold;
        }  
    
/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: #ffffff;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  /*margin-left: 50px;*/
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: #024068;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: #024068;
  background-image: #024068;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #024068;
}

div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
  padding-top: 10px;
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
        
        .box{
            background: #fff;
    -webkit-box-shadow:0 2px 5px rgba(0,0,0,.18);
    -moz-box-shadow: 0 2px 5px rgba(0,0,0,.18);
    box-shadow: 0 2px 5px rgba(0,0,0,.18);
    box-sizing: border-box;
        margin-top:10px;
            padding: 5px;
            margin-bottom: 20px;
        }
        .box ul li{
            list-style-type: none;
        }
        .right-mar{
            margin-right: 5px;
        }
        .marb5{margin-bottom: 5px;}
        .mar-b5 p{
            margin-bottom: 5px;
        }
        .mar-t20{
            margin-top: 20px;
        }
        .dis-block{
            display: inline-block;
        }
        .mar{
            margin: 11px 23px;
        }
    </style>
    
    <div id="wrapper">
        <section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index.php">Home</a></li>
                        <li>Profile</li>
                    </ul>
                    <h3>PROFILE</h3>
                </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->

    <section class=" clearfix pad-bot">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center">
                  <h4 class="fa fa-suitcase"></h4><br/>Booking
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-user"></h4><br/>Profile
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-key"></h4><br/>change Password
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-question-circle"></h4><br/>Help
            
                </a>
              </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    
                    <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Booking History</a></li>
    <!--  <li><a data-toggle="tab" href="#menu1">Upcoming Booking</a></li>-->
    <!--<li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
    <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>-->
  </ul>
<input type="hidden" class="form-control" name="emailId" id="emailId" value="<s:property value="#session.emailId"/>" placeholder="usrid">
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
         <div class="box">
                    <div class="row" ng-repeat="ub in userBookings track by $index">
                        <div class="col-md-4 text-center">
                        <img src="ulowesite/upload/single_payment.png" alt="" class="img-responsive">
                            <!--  <button class="btn btn-primary right-mar marb5 mar-t20">Modify Booking</button>-->
                            <button class="btn btn-primary right-mar marb5 mar-t20" ng-click=cancelBooking(ub.bookingId);>Cancel Booking</button>
                            
                            
                        </div>  
                        <div class="col-md-8" >
                            
                                         <strong class="pull-right mar">&#2352;{{ub.amount}}</strong>
                                                <h6 class="mar0">{{ub.propertyName}}</h6>
                                     
                                                <span class="rating  clearfix">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span><!-- end rating -->

                                                <ul class="clearfix pad-bot">
                                                   <!-- <li><span class="bold">LOCATION:</span>  42 Princes Square, Chennai.</li>-->
                                                    <li><span class="bold">CHECKIN:</span>  {{ub.arrivalDate}}</li>
                                                    <li><span class="bold">CHECKOUT:</span>  {{ub.departureDate}}</li>
                                                   <!--   <li><span class="bold">STAY:</span>  2 Nights, 1 Room, Max 2 Adult(s)</li>-->
                                                    <li><span class="bold">Booking ID: </span>  {{ub.bookingId}}</li>
                                                </ul>
                          </div>                  
                    </div>
                    </div>
        
      
       
        
    </div>
    <div id="menu1" class="tab-pane fade">
      <div class="box">
                    <div class="row">
                        <div class="col-md-4 text-center">
                        <img src="upload/single_payment.png" alt="" class="img-responsive">
                            <button class="btn btn-primary right-mar marb5 mar-t20">Modify Booking</button>
                            <button class="btn btn-primary right-mar marb5 mar-t20">Cancel Booking</button>
                            
                            
                        </div>  
                        <div class="col-md-8">
                            
                                         <strong class="pull-right mar">&#2352;3500</strong>
                                                <h6 class="mar0">ULO MUTHU RESIDENCY</h6>
                                     
                                                <span class="rating  clearfix">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span><!-- end rating -->

                                                <ul class="clearfix pad-bot">
                                                   <!-- <li><span class="bold">LOCATION:</span>  42 Princes Square, Chennai.</li>-->
                                                    <li><span class="bold">CHECKIN:</span>  Thu 30 May, 2017</li>
                                                    <li><span class="bold">CHECKOUT:</span>  Sat 01 Jun, 2017</li>
                                                    <li><span class="bold">STAY:</span>  2 Nights, 1 Room, Max 2 Adult(s)</li>
                                                    <li><span class="bold">Booking ID: </span>  887845</li>
                                                </ul>
                          </div>                  
                    </div>
                    </div>
        
             
       
        
    </div>

  </div>
                    
                </div>
                <!-- train section -->
                <div class="bhoechie-tab-content">
                    
                  <h4>Your Personal Information</h4>
                    <div class="box">
                    <div class="row ">
                       <div class="col-md-3 col-sm-12  col-xs-12">
                       <img class="img-circle"  ngf-select="upload($file)" ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" src="profile-picture.action" style="width:100px;" alt="User Avatar">
                        
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                        <input type="text" class="form-control" name="userName"  id="userName" value="{{user[0].o_username == 'null' ? 'username' : user[0].o_username }}" placeholder="firstname">
                        <input type="text" class="form-control" name="emailId"  id="emailId" value="{{user[0].o_email }}" placeholder="email">
                        <input type="hidden" class="form-control" name="userId" id="userId" value="<s:property value="#session.userId"/>" placeholder="usrid">
                        <input type="hidden" class="form-control" name="roleId" id="roleId" value="<s:property value="4"/>" placeholder="usrid">
                        <input type="text" class="form-control" name="address1"  id="address1" value="{{user[0].o_address1 == 'null' ? 'address1' : user[0].o_address1 }}" placeholder="address">
                        <input type="text" class="form-control" name="address2"  id="address2" value="{{user[0].o_address2 == 'null' ? 'address2' : user[0].o_address2 }}" placeholder="address">
                        <input type="text" class="form-control" name="phone"  id="phone" value="{{user[0].o_phone == 'null' ? 'phone' : user[0].o_phone }}" placeholder="phone">
                       <button  ng-click="editfamilyuser()" class="btn btn-info pull-right">Save</button> 
                            <div class="mar-b5">
                           
                                </div>
                        </div>
                    </div>
                        </div>
             

                </div>
                
                 <div class="bhoechie-tab-content box">
                    <h3>password</h3>
                   
                   <div class="form-group">
						<input type="hidden" name="userId" id="userId" value="<s:property value="user.userId"/>" /> </input>
						</div>
						<div class="form-group">
							<label>New password</label>
							<input type="password" class="form-control" name="newPassword"  id="newPassword"  placeholder="Newpassword">
						</div>
					  	<div class="form-group">
							<label>Confirm password</label>
							<input type="password" class="form-control" name="confirmPassword"  id="confirmPassword"  placeholder="ConfirmPassword">
						</div> 
                    
                    <button type="submit" ng-click="changepassword()" class="btn btn-primary">Change</button>  
                </div>

                <div class="bhoechie-tab-content box">
                    <h3>Help & Support</h3>
                    <h4>Contact us now</h4>
                    
                    <p><i class="fa fa-mobile"></i>  +91-9543 592 593</p>
                    <p><i class="fa fa-globe"></i>  reservations@ulohotels.com</p>
                </div>
                
                
            </div>
        </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

    
    <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>
    <script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
     
   
     <script>

$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>
	<script>


 
	
		var app = angular.module('myApp',['ngFileUpload']);
		app.controller('customersCtrl',['$scope',
		             					'Upload',
		            					'$timeout',
		            					'$http', function($scope,Upload,$timeout,$http) {

			$("#alert-message").hide();
			
			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
		    //$scope.progressbar.start();
			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);
	       	
	       	$scope.unread = function() {
				var notifiurl = "unreadnotifications.action";
				$http.get(notifiurl).success(function(response) {
					$scope.latestnoti = response.data;
				});
				};

				
				
				
				$scope.getUserPhotos = function() {
					
				   var userid = document.getElementById("userId").value;
					 
					//alert($('#userId').val());
					//var userid = document.getElementById("userId").value;
					var url = "user-picture?userId="+$('#userId').val();
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.photos = response.data;
					
					});
				};
				
				
                $scope.getUser = function() {
					
					//alert($('#userId').val());
					
				    var userid = document.getElementById("userId").value;
					var url = "get-customer?auserId="+$('#userId').val();
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.user = response.data;
					
					});
				};
				
				
				
                $scope.getUserBookings = function() {
					
					
					
				    var emailId = document.getElementById("emailId").value;
				   // alert($('#gopi').val());
				    //var emailId = "gopinath@ulohotels.com";
					var url = "get-user-bookings?emailId="+emailId;
					$http.get(url).success(function(response) {
						//alert(response.data);
						
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.userBookings = response.data;
					
					});
				};
				
				
                $scope.cancelBooking = function(bookingId) {
					
					//alert($('#userId').val());
					alert(bookingId);
				    //var booingkId = document.getElementById("userId").value;
					var url = "cancel-booking?bookingId="+bookingId;
					$http.get(url).success(function(response) {
						
						alert("Booking Cancelled");
						//console.log(response);
						//alert(JSON.stringify(response.data));
						//$scope.user = response.data;
					
					});
				};
				
				
				$scope.editfamilyuser=function(){
					
					
					//alert ("userId="+$('#userid').val());
					var fdata = "userId="+$('#userId').val()
					+"&userName="+$('#userName').val()
					+"&phone="+$('#phone').val()
					+"&address1="+$('#address1').val()
					+"&address2="+$('#address2').val()
					+"&roleId="+$('#roleId').val()
					+"&emailId="+$('#emailId').val();
					
					//alert(fdata);
					
					$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'edit-family-user'
					}).then(function successCallback(response) {
							//alert(response.data);
							$scope.singleuser = response.data.data;
                            alert("profile updated sucessfully");
							console.log($scope.singleuser);
					}, function errorCallback(response) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				};
				
				
				$scope.changepassword=function(){
					
					var newpass = document.getElementById("newPassword").value;
			    	var confirmpass = document.getElementById("confirmPassword").value;
			    	var passpattern = /(?=.*\d)(?=.*[@#$%])(?=.*[a-z])(?=.*[A-Z]).{8,}/;
			    	
			    	if(newpass=="") {
				        alert("New password is required");
				        return false;
				      }
			    	
			    	 if(!passpattern.test(newpass)) {
			    	        alert("Password should have atleast 8 characters length,with 1 uppercase,1 special character and 1 number");
			    	      
			    	        return false;
			    	  }
			        
			    	 if(newpass != confirmpass) {
			 	        alert("password doesnot match!");
			 	        return false;
			 	      }
			    	 
					//alert ("userId="+$('#userid').val());
					var fdata = "userId="+$('#userId').val()
					+"&newPassword="+$('#newPassword').val()
					
					+"&confirmPassword="+$('#confirmPassword').val();
					
					
					//alert(fdata);
					
					$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'changeuserpassword'
					});
				};

				
				
			
				
				
				
				
	
		       	// upload on file select or drop
		        $scope.upload = function (file) {
		        	var userid = document.getElementById("userId").value;
		        	 //alert(userid);
		            Upload.upload({
		                url: 'userpicupdate',
		                enableProgress: true, 
		                data: {myFile: file, 'userId': userid} 
		            }).then(function (resp) {
		            	//alert(resp);
		            	//$scope.getUserPhotos();
		                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
		                window.location.href='/ulouserprofile';
		                //$scope.getPropertyPhotos();
		            }, function (resp) {
		                console.log('Error status: ' + resp.status);
		            }, function (evt) {
		            	
		                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		                
		                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		            });
		           
		            
		        };
			
			//$scope.unread();
		     $scope.getUser();
		     $scope.getUserBookings();

		}]);

		
	</script>
       <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
       
