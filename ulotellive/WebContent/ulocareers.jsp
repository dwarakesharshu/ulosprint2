<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div id="wrapper">
<section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index">Home</a></li>
                        <li>Careers</li>
                    </ul>
                    <h3>Careers</h3>
                </div>
            </div> 
        </div>
    </section>

    

    <section class="section clearfix">
        <div class="container">           
            <div class="row">
                <div id="fullwidth" class="pad-bot">

                    <!-- START CONTENT -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <ul class="list-style">
                                <li>Our office is an open environment. We explore creativity from all ends and encourage spontaneity in our employees. If you have a brainwave, talk to us and go ahead. </li>
                                <li>We take individual responsibility in the work that we are entrusted to do for our customers, investors and partners</li>
                                <li> We respect and adhere to high-ethical standards that we have established as an organization and expect you to follow the same.</li>
                                <li>All this aside, we promise you a family that makes work enjoyable. Celebrating festivals, team outings, and simply unwinding provide a platform to express yourself are part of the package at Ulo Hotels. </li>
                                <li>Send us a copy of your resume to <span style="color:#88ba41;font-weight:bold;"><a href="mailto:info@ulohotels.com">info@ulohotels.com</a></span> <!-- . Our current openings will be listed at _____. --> </li>
                            </ul>
                        </div><!-- end col -->

                    </div><!-- end row -->
                    <!-- END CONTENT -->

                </div><!-- end fullwidth -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->
</div>
 <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
    