<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<div id="wrapper">
   <section id="page-header" class="section background">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <ul class="c1 breadcrumb text-left">
                  <li><a href="index">Home</a></li>
                  <li>Payment</li>
               </ul>
               <h3>PAYMENT</h3>
            </div>
         </div>
         <!-- end row -->
      </div>
      <!-- end container -->
   </section>
   <!-- end section --><ul class="nav nav-tabs hidden-lg hidden-md hidden-xs hidden-sm">
    <li class="active"><a href="#tab1" data-toggle="tab">details</a></li>
    <li><a href="#tab2" data-toggle="tab">payment</a></li>
    <li><a href="#tab3" data-toggle="tab">Summary</a></li>
</ul>
<div class="tab-content">
        <div class="tab-pane active" id="tab1">
           <section class="clearfix pad-bot">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 sol-sm-12">
            <h3></h3>
             
            </div>
         </div>
          <input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
               <textarea name="accommodationTypes" style="display:none" id ="accommodationTypes"><%= session.getAttribute("accommodationTypes") %></textarea>
        
         <div class="row breakbase box" ng-repeat="av in availablities track by $index">
            <div class="col-lg-12 col-md-12 col-sm-12">
            
               
               <div class="col-lg-3 col-md-3 col-sm-12 roomimage" >
                  <img src="accommodation-picture?propertyAccommodationId={{av.accommodationId}}" alt="" class="img-responsive img-thumbnail">   
               </div>
               <div class="col-lg-9 col-md-3 col-sm-12">
               
                  <div class="col-lg-12 col-md-12 col-sm-12 roombreakdetails">
                  <div class="col-lg-8 col-md-8 col-sm-12">
                    <ul class="breakdetails"style="list-style:none">
                       <li><span class="bold">HOTEL : </span>  {{property[0].propertyName}}</li>
                        <li><span class="bold">ROOM : </span>  {{av.accommodationType}}</li>
                        <li><span class="bold">CHECKIN :</span>  <%=session.getAttribute("checkIn") %> <span class="bold">&nbsp; CHECKOUT :</span>  <%=session.getAttribute("checkOut") %></li>
                        <li><span class="bold">NUMBER OF NIGHTS : </span> {{roomsSelected[$index].diffDays}}  <span class="bold">&nbsp; STAY:</span>  Max {{av.maxOccupancy}} Person(s)</li>
                        <li></li>
                     </ul>
         
                  
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                             
                  <div class=" col-lg-12 col-md-12 col-sm-12 catprice ">
                     <h4><i class="fa fa-inr" aria-hidden="true"></i> {{roomsSelected[$index].total}}</h4>
                   <div class="taxbreakup" id="accordion" role="tablist" aria-multiselectable="true">
                     <a  data-toggle="collapse" data-parent="#accordion" href="#collapse{{$index}}" aria-expanded="true" aria-controls="collapseOne">
                     Break up 
                    </a>
                 <div id="collapse{{$index}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    
                      GST <b id="taxpercent" ></b>%  :{{roomsSelected[$index].tax}}
                
                    <div id="xtrabed"></div>
                </div>
              </div>
                 </div>
                  </div>
                 </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="col-lg-12 col-md-12 col-sm-12 breakcount ">.
                        <form>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                           <label>Add Room :</label>  
                           <div class="input-group">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'rooms',av.arrivalDate,av.departureDate,av.minOccupancy,av.maxOccupancy,av.extraAdult,av.extraInfant,av.extraChild,av.baseAmount,av.taxIsActive)" ng-disabled="addroom <=0">
                              <span class="glyphicon glyphicon-minus"></span>
                              </button>
                              </span>
                              <input type="text" name="rooms"  class="form-control input-number rooms{{$index}}" value="1" min="1" max="10" readonly >
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'rooms',av.available,av.arrivalDate,av.departureDate,av.minOccupancy,av.maxOccupancy,av.extraAdult,av.extraInfant,av.extraChild,av.baseAmount,av.taxIsActive)"  data-type="plus" data-field="quant">
                              <span class="glyphicon glyphicon-plus"></span>
                              </button>
                              </span>
                           </div>
                        </div>
                           <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                           <label>Adult :</label>  
                           <div class="input-group">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'adult',av.arrivalDate,av.departureDate,av.minOccupancy,av.maxOccupancy,av.extraAdult,av.extraInfant,av.extraChild,av.baseAmount,av.taxIsActive)" ng-disabled="addadult <=0" data-type="minus" data-field="quant">
                              <span class="glyphicon glyphicon-minus"></span>
                              </button>
                              </span>
                              <input type="text" name="adult"  class="form-control input-number adult{{$index}}" value="1" min="1" max="100" readonly >
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'adult',20,av.arrivalDate,av.departureDate,av.minOccupancy,av.maxOccupancy,av.extraAdult,av.extraInfant,av.extraChild,av.baseAmount,av.taxIsActive)"  data-type="plus" data-field="quant">
                              <span class="glyphicon glyphicon-plus"></span>
                              </button>
                              </span>
                           </div>
                        </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                           <label>child :(5- 11 years)</label>  
                           <div class="input-group">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'child',av.arrivalDate,av.departureDate,av.minOccupancy,av.maxOccupancy,av.extraAdult,av.extraInfant,av.extraChild,av.baseAmount,av.taxIsActive)" ng-disabled="addchild <=0" data-type="minuss" data-field="quant">
                              <span class="glyphicon glyphicon-minus"></span>
                              </button>
                              </span>
                              <input type="text" name="child"  class="form-control input-number child{{$index}}" value="0" min="1" max="100" readonly >
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'child',10,av.arrivalDate,av.departureDate,av.minOccupancy,av.maxOccupancy,av.extraAdult,av.extraInfant,av.extraChild,av.baseAmount,av.taxIsActive)"  data-type="pluss" data-field="quant">
                              <span class="glyphicon glyphicon-plus"></span>
                              </button>
                              </span>
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                           <label>Infant :(0-5 years)</label>  
                           <div class="input-group">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'infant',av.arrivalDate,av.departureDate,av.minOccupancy,av.maxOccupancy,av.extraAdult,av.extraInfant,av.extraChild,av.baseAmount,av.taxIsActive)" ng-disabled="infant <=0" data-type="minus" data-field="infant">
                              <span class="glyphicon glyphicon-minus"></span>
                              </button>
                              </span>
                              <input type="text" name="infant"  class="form-control input-number infant{{$index}}" value="0" min="1" max="100" readonly >
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'infant',10,av.arrivalDate,av.departureDate,av.minOccupancy,av.maxOccupancy,av.extraAdult,av.extraInfant,av.extraChild,av.baseAmount,av.taxIsActive)"  data-type="plus" data-field="infant">
                              <span class="glyphicon glyphicon-plus"></span>
                              </button>
                              </span>
                           </div>
                        </div>
                     </form>
                  </div>
                  </div>
                  </div>
             
               </div>
  
            </div>
            </div> <!-- ng row ends -->
             
             <!-- <div class="row radio" >
             <ul ng-repeat="cd in discounts track by $index">
             <li> <label><input type="radio" name="optradio">{{cd.discountName}}</label></li>
             </ul>
             </div> -->
                  <div class="col-lg-12"><div class="breakhrtag"></div></div>   
          
           
            <!-- Another cbi -->
 
           <!--  Another cbi ends -->
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12 grandtotal ">
                <h4 >Grand Total= </h4>
             </div>
               <div class="col-lg-2 col-md-4 col-sm-12 grandtotalrs">
                  <h4 id="totalAmount"> {{ getTotal() }}</h4>
               </div>
            </div>
            <div class="row">
             <div class="col-lg-8 col-md-8 col-sm-12  ">
             </div>
               <div class="col-lg-4 col-md-4 col-sm-12 breakbtn ">
                 
                 <a href="#"><button type="button" id="submit" class="btn btn-primary btnNext" onclick ="goToByScroll('page-header');">Make Payment</button></a>
               </div>
            </div>
         </div>
      </div>
     <div class="tab-pane" id="tab2">
     <div class="container">
     <div class="row">
    
      <div class="col-lg-12 col-md-12 col-sm-12">
        <s:if test="#session.userId == null">
					   <h6 class="text-center" style="padding:5px;">Already Have An Ulo Account ? Click Here To  <button class="btn btn-primary "  ng-click="showLogin();">Login</button> </h6>
					 
					  </s:if>
					   <div class="col-lg-12 col-md-12 col-sm-12 ">

					  		<div class="row">
						<div class="form">
   <div id="login">
   <div class="row">
					  <button class="btn btn-primary"  ng-click="showGuest();">Book As A Guest</button> 
					</div><br>
      <h3>Welcome Back!</h3>
      <s:form id="myForm" action="ulodetailslogin" method="post" theme="simple" >
      
      
							
	 <div class="form-group col-sm-6 col-lg-6">
							<label for="guestEmail">E-mail</label>
							<input type="email" class="form-control"  name="username" id="username"  placeholder="Enter Your Email">
						    
						     
	 </div>
	 
	 <div class="form-group col-sm-6 col-lg-6">
							<label for="firstName">Password</label>
							<input type="password" class="form-control" name="password"  id="password"  placeholder="Password"   maxlength="50">
						   
	  </div>
	  
         <s:if test="hasActionErrors()">
            <div id="password_error_message" style="color:red;font-weight:normal;">
               <s:iterator value="actionErrors">
                  <s:property/>
               </s:iterator>
            </div>
         </s:if>
         <s:if test="%{#session['countcap']>3}">
            <div id="captchashow">
               <table>
                  <tr>
                     <td>
                        <div class="g-recaptcha" data-sitekey="6LdrlSAUAAAAANGKJe_wASqoEv3fjw-94WdlPAPs"></div>
                     </td>
                  </tr>
                  <tr>
                     <td>
                       
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <s:property value="#sessionMap.count" />
                     </td>
                  </tr>
               </table>
            </div>
         </s:if>
         <p class="forgot" style=""><a href="forget">Forgot Password?</a></p>
         <input type="submit" class="btn btn-primary" value="LogIn"/>
      </s:form>
   </div>
</div>
<!-- /form -->
						</div>
     </div>
      <h6 class="text-center">GUEST INFORMATION </h6>
      <div class="breakguestinfo"  >
   <form id="infoform" name="myformval">
                      <input type="hidden" class="form-control" name="userId" id="userId" value="<s:property value="#session.userId"/>" placeholder="usrid">
                       <div class="form-group col-sm-6 col-lg-6 breakguest">
                            <label for="firstName">Name <span style="color:red">*</span></label>
                            <input type="text" class="form-control" name="firstName" id="firstName"  value="{{user[0].o_username}}" ng-model="user[0].o_username" placeholder="Name" ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
                          <span ng-show="myformval.firstName.$error.pattern" style="color:red">Enter Your Valid Name </span>
                        </div>
                            <div class="form-group col-sm-6 col-lg-6 breakguest">
                            <label for="guestEmail">E-mail <span style="color:red">*</span></label>
                            <input type="email" class="form-control"  name="guestEmail"  id="guestEmail"  value="{{user[0].o_email}}" placeholder="Enter Your Email" ng-model="user[0].o_email"  ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">                     
                             <span ng-show="myformval.guestEmail.$error.pattern" style="color:red">Enter The Valid Email ID!</span>
                        </div>                        
                        <div class="form-group col-sm-6 col-lg-6 breakguest">
                            <label for="mobile_phone">Mobile Phone <span style="color:red">*</span></label>
                            <input type="text" class="form-control"  name="mobilePhone"  id="mobilePhone"  ng-model="user[0].o_phone"  value="{{user[0].o_phone }}" placeholder="Enter The Mobile Number"  ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="10" ng-minlength="10">
                           <span ng-show="myformval.mobilePhone.$error.pattern" style="color:red">Enter the Valid Mobile Number!</span> 
                        </div>
                        <div class="form-group col-sm-6 col-lg-6 breakguest">
                            <label for="child_included_rate">Special Request</label>
                            <textarea  class="form-control"  name="specialrequest" id="specialrequest" value="" placeholder="Special Request" maxlength="1000"></textarea>
                        </div>
                
                            <div class="form-group col-md-6 col-lg-6 breakcoupon">
                    
                            <label for="coupon" class="col-lg-5"><h6>Do you have Coupon code?</h6> </label>
                    <input type="text"  class="col-lg-4"  name="coupon" id ="coupon" value="" placeholder="Enter Coupon"></input>
                    <button class="btn btn-primary btn-coupon col-lg-2 pull-right" ng-click="applyDiscount()">Apply</button>
                        </div>
                            <div class="form-group col-md-6 col-lg-6 breakguest">
                        <label for="coupon" ><h4>Total Pay For Your Booking  <span style="color:red;padding:10px;margin:5px;"><i class="fa fa-inr" aria-hidden="true"></i> {{ getTotal() }} </span></h4> </label>
            
                        </div>
                        
                            <div class="col-md-12 text-center">
                                        <!-- <p>By selecting to complete this booking I acknowledge that I have read and accept <a href="termsandconditions">terms & conditions</a>, and <a href="privacy">privacy policy</a>.</p> -->
                                 <!-- <p>All Fields Are Mandatory</p> -->
                                        
                                           <button type="button" class="btn btn-warning btnPrevious guestbtn btn-sm">Back</button>
                                        <button type="submit" id="myDiv" ng-disabled="myformval.$invalid" ng-submit="myformval.$valid && submit()" ng-click="booking()"  class="btn btn-primary guestbtn">Proceed TO Payment</button>
                                    
                                    </div>    
                                      
                        </form>
						</div>
     </div>
     </div>
     </div>
    </div>

</div>
</section>
    </div>

</div>

</div><!-- End of width -->
 <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

<script>

		var app = angular.module('myApp',['ngProgress']);
		
		app.controller('customersCtrl',function($scope, $http,$timeout,ngProgressFactory,$location) {
			$('.btnNext').click(function(){
				  $('.nav-tabs > .active').next('li').find('a').trigger('click');
				});

				  $('.btnPrevious').click(function(){
				  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
				});

				  $(document).ready(function(){
					   function myFunction(div) {
						   $("#loader").toggle();
						   $(div).toggle();

						   }
			

				  });
			 var propertyId = $('#propertyId').val(); 	
			 
			 $('#login').hide();
			 
			 $scope.showLogin = function(){
				
				 $('#guest').hide();
				 $('#login').show();
				 //alert("dsfd");
				 
				 };
			 
			 $scope.showGuest = function(){
						
					 $('#login').hide();
					 $('#guest').show();
					 //alert("dsfd");
					 
					 };
				 
			
			  $scope.getFrontAvailability = function(){
					
				  $scope.typeSelected = [];
				  $scope.accommodations = [];
				  var typeSeleted = new Array();
				  
					if($('#datepicker').val() == ""){
					   $('#datepicker').focus();
					}
					
					if($('#datepicker1').val() == ""){
						
						$('#datepicker1').focus();
						
					}
					//alert($('#propertyItemCategoryId').val());
					//$scope.accommodations.remove;
					
					var fdata = "&arrivalDate=" + $('#datepicker').val()
					+ "&departureDate=" + $('#datepicker1').val()
					+ "&propertyId=" + $('#propertyId').val()
		             
					
					
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'get-availability'
							}).success(function(response) {
								
								$scope.accommodations = response.data;
							    //console.log($scope.accommodations[0].accommodationId);
							    var id = parseInt($scope.accommodations[0].accommodationId);
							    var available = parseFloat($scope.accommodations[0].available);
							    console.log(id);
							    var newType = {'accommodationId':id ,'available':available};
							    $scope.typeSelected.push(newType);
					        	console.log();
							});

				};
	       	
	       	   $scope.manageTypes = function(id){
	    	  
	    	   if(document.getElementById(id).checked){
	    		   
	    		   var accId = parseInt($scope.accommodations[id].accommodationId);
				   var available = parseInt($scope.accommodations[id].available);
				   
	    		  // var chkval = document.getElementById(id).value;
	    		   var newType = {'accommodationId':accId ,'available':available};
				    $scope.typeSelected.push(newType);
	    	       
	    	   } 
	    	   else{
	    		   
	    		   $scope.typeSelected.splice(id);
	    		   //var chkval = document.getElementById(id).value;
		    	   //alert(chkval);
	    	   }
	    	 
	    		   
	    	};
	        	
	    	$scope.getType = function(){
                
	             var text = $('#accommodationTypes').val(); 
	             
	             // var text = '{"types":' + JSON.stringify($scope.typeSelected) + '}';
	       	        var data = JSON.parse(text);
	                
	                 
	                
	                
	                $http(
	                        {
	                            method : 'POST',
	                            data : data,
	                            //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
	                            dataType: 'json',
	                            headers : {
	                                 'Content-Type' : 'application/json; charset=utf-8'

	                            },
	                            
	                            //url : 'bookingdetails'
	                            url : 'get-type-selected'
	                            
	                        }).success(function(response) {
	                            
	                             
	                            
	                            $scope.availablities = response.data;
	                            
	                            //alert(dt.baseAmount);
	                            var indx = 0;
	                            //$scope.roomsSelected = [];
	                            //addRow(av.accommodationId,av.accommodationType,av.baseAmount,av.arrivalDate,av.departureDate,av.rooms,av.noOfAdults,av.noOfChild,av.minOccupancy,av.extraAdult,av.extraChild,av.available,$index,av.tax)      
	                            
	                            for (var i = 0; i < $scope.availablities.length; i++) 
	                               {
	                               var dt = $scope.availablities[i];
	                              
	                               $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,dt.rooms,dt.noOfAdults,dt.noOfChild,dt.minOccupancy,dt.extraAdult,dt.extraInfant,dt.extraChild,dt.available,i,dt.tax,dt.taxIsActive);    
	                                	
	                               }  
	                            //console.log($scope.booked[0].firstName);
	                            
	                            
	                        });
	              

	            };
	            
			$scope.booking = function()
			{
			
				var options = {
					     "key": "rzp_test_bQQGkpEPtwppNI",
					    //"key": "rzp_live_vuYF9peaDXMoQF",
					    "amount": parseInt(document.getElementById ("totalAmount").innerText) * 100, // 2000 paise = INR 20
					    "name": "Ulohotels.com",
					    "description": "Welcom to Ulohotels",
					    //"image": "/your_logo.png",
					    "handler": function (response){
					        //alert(response.razorpay_payment_id);
					         $scope.confirmBooking();
					    },
					    "prefill": {
					        "name": $('#firstName').val(),
					        "email": $('#guestEmail').val(),
					        "contact":  $('#mobilePhone').val()
					    },
					    "notes": {
					        "address": $('#mobilePhone').val()
					    },
					    "theme": {
					        "color": "#89b717"
					    }
					};
					var rzp1 = new Razorpay(options);
					rzp1.open();
					e.preventDefault();
			};
			
			 $scope.confirmBooking = function(){
					
				    //document.getElementById('rzp-button1').click(); 
					
					var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
					var data = JSON.parse(text);
					//alert(text);
					var sourceid = 1;
					$http(
							{
								method : 'POST',
								data : data,
								//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
								dataType: 'json',
								headers : {
									 'Content-Type' : 'application/json; charset=utf-8'

								},
								url : "add-booking?sourceId="+sourceid
										
								
								
							}).then(function successCallback(response) {
								
								
								var gdata = "firstName=" + $('#firstName').val()
								+ "&lastName=" + $('#lastName').val()
								+ "&emailId=" + $('#guestEmail').val()
								+ "&phone=" + $('#mobilePhone').val()
								+ "&landline=" + $('#landlinePhone').val()
								+ "&address1=" + $('#firstAddress').val()
								+ "&address1=" + $('#secondAddress').val()
								+ "&city=" + $('#guestCity').val()
								+ "&zipCode=" + $('#guestZipCode').val()
								+ "&countryCode=" + $('#countryCode').val()
								+ "&stateCode=" + $('#stateCode').val()
								
							//alert(gdata);
								$http(
							{
								method : 'POST',
								data : gdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'add-guest'
								
							}).then(function successCallback(response) {
								
							
							    console.log($scope.roomsSelected);
							   
								var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
								//alert(text);
								var data = JSON.parse(text);
								//var data = $scope.roomsSelected;
								
								
								
								$http(
							    {
								method : 'POST',
								data : data,
								//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
								dataType: 'json',
								headers : {
									 'Content-Type' : 'application/json; charset=utf-8'
									//'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'add-booking-details'
								//url : 'jsonTest'
							   }).then(function successCallback(response) {
								
								
							 // alert("booking has been done sucessfully");	
							     $scope.booked = response.data;
							     //alert(JSON.stringify($scope.booked));
							     console.log($scope.booked.data[0].bookingId);
							     window.location = '/paymentsuccess'; 
							    
							   }, function errorCallback(response) {
								
								  
								
							});
								
							}, function errorCallback(response) {
								
								  
								
							});
							
							}, function errorCallback(response) {
						
					  
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});

				};
				
				
				$scope.range = function(min, max, step){
				    step = step || 1;
				    var input = [];
				    for (var i = min; i <= max; i += step) input.push(i);
				    return input;
				 };

				 $scope.getStates = function() {
		             
		             var url = "get-states";
		             $http.get(url).success(function(response) {
		                 //console.log(response);
		                 //alert(JSON.stringify(response.data));
		                 $scope.states = response.data;
		             
		             });
		         };
		         
		         $scope.getCountries = function() {
		             
		             var url = "get-countries";
		             $http.get(url).success(function(response) {
		                 //console.log(response);
		                 //alert(JSON.stringify(response.data));
		                 $scope.countries = response.data;
		             
		             });
		         };
		        
		         $scope.roomsSelected = [];
		         
		         $scope.addRow = function(id,type,baseAmount,arrival, departure,rooms,adultsAllow,childAllow,minOccu,extraAdult,extraInfant,extraChild,available,indx,tax,taxIsActive){	
		        	
		        	 
		        	 
		        	 if(available == 0){
		             	
		        		 
		        		    
		             	// alert("Rooms is not available");
		             	  //$("#submitBtn").attr("disabled","disabled");
		             	    //btn.disabled = true;
		        			 console.log("Disable");
		                 }
		                 else{
		        
		        	 
		         var type = type.replace(/\s/g,'');
		         var id = parseInt(id);
		         var minOccu = parseInt(minOccu);
		         var extraAdult = parseInt(extraAdult);
		         var extraInfant = parseInt(extraInfant);
		         var extraChild = parseInt(extraChild);
		         
		         var adultsAllow = parseInt(adultsAllow);
		         var childAllow = parseInt(childAllow);
		        
		         var amount = parseFloat(baseAmount);
		         var rooms = parseInt(rooms);
		         
		        /*  var date1 = new Date(arrival);
		         var date2 = new Date(departure); */
		         
		        // var a = arrival.split(/[^0-9]/);
		         //for (i=0;i<a.length;i++) { alert(a[i]); }
		         //var d= new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
		       //  alert(s+ " "+d);
		       
		         //alert(arrival);
		         //alert(date1);
		         
		         var a1 = arrival.split(/[^0-9]/);
		         var d1= new Date (a1[0],a1[1]-1,a1[2],a1[3],a1[4],a1[5] );
		        // alert(arrival+ " "+d1);
		         
		         var a2 = departure.split(/[^0-9]/);
		         var d2= new Date (a2[0],a2[1]-1,a2[2],a2[3],a2[4],a2[5] );
		         //alert(departure+ " "+d2);
		       
		         var timeDiff = Math.abs(d2.getTime() - d1.getTime());
		         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
		         var randomNo = Math.random();
		         
		         // alert(diffDays);
		         if(taxIsActive == 'true'){
		         var taxPercentage = $scope.getTaxPercentage(amount);
		         var taxes = Math.round(taxPercentage / 100 * amount);
		         }
		         
		         else{
		        
		        	 var taxes = 0;	 
		        	 
		         }
		         
		         
		         //alert(diffDays);
		         // 
		         var total = (rooms*amount);
		         //shiva update newData diffDays only
		         var newData = {'arrival':arrival,'indx':indx,'available':available,'departure':departure,'accommodationId':id,'sourceId':1,'accommodationType':type, 'rooms': rooms,'diffDays':diffDays, 'baseAmount':amount, 'total':total ,'adultsAllow':adultsAllow,'childAllow':childAllow,'minOccu':minOccu,'extraAdult':extraAdult,'extraInfant':extraInfant,'extraChild':extraChild,'adultsCount':0,'infantCount':0,'childCount':0,'refund':0,'statusId':2,'tax':taxes,'randomNo':randomNo};
		        	   $scope.roomsSelected.push(newData);
		        	   console.log($scope.roomsSelected);
		        	   // $scope.availablities[indx].available = $scope.availablities[indx].available - 1 ;
		                 }
		        	};
		        	
		          $scope.getTotal = function(){
		        	    var total = 0;
		        	    var tax = 0;
		        	    for(var i = 0; i < $scope.roomsSelected.length; i++){
		        	        var accommodation = $scope.roomsSelected[i];
		        	        tax += (accommodation.tax);
		        	        total += (accommodation.total);
		        	    }
		        	    var all = (total+tax);
		        	    return all;
		        	} 	
		          
		          $scope.getRoomsTotal = function(){
		      	    var totalRooms = 0;
		      	    for(var i = 0; i < $scope.roomsSelected.length; i++){
		      	        var roomsCount = $scope.roomsSelected[i];
		      	        totalRooms += parseInt(roomsCount.rooms);
		      	    }
		      	    return totalRooms;
		      	  } 	
				
		          $scope.removeRoom = function(index,available){
		        	  // alert(indx);
		        	  
		        	  var currentVal =  parseInt($('.rooms').val());
	                  // If is not undefined
                      if (!isNaN(currentVal) && currentVal > 0) {
                      // Decrement one
                       $('.rooms').val(currentVal - 1);
                       } else {
                       // Otherwise put a 0 there
                        $('.rooms').val(0);
                      }
	                  
		        	  $scope.availablities[index].available = parseInt($scope.availablities[index].available) + 1; 
		         	 
		              $scope.roomsSelected.splice(index,1); 
		              	 
		        	  
		        	  
		            } 	
		          
		          $scope.getTaxPercentage = function(amount){
		        	 
		        	  for(var i = 0; i < $scope.taxes.length; i++){
		        		    
		               if(amount >= $scope.taxes[i].taxAmountFrom && amount <= $scope.taxes[i].taxAmountTo){
		        		    	
		        		   var per = $scope.taxes[i].taxPercantage;
		        		   
		        		   return per;
		        		    
		        	   }
			      	       
		        	  }
		        	  
		          } 	
		          
				 $scope.getPropertyList = function() {
					 
			        	var userId = $('#adminId').val();
			 			var url = "get-user-properties?userId="+userId;
			 			$http.get(url).success(function(response) {
			 			    
			 				$scope.props = response.data;
			 	
			 			});
			 		};
			 		
			 		 
			 		 
			 	     $scope.getCurrentDiscounts = function() {
						 
			 	    	   var propertyId = $('#propertyId').val();
			 	    	   
				 			var url = "get-current-discounts?propertyId="+propertyId;
				 			$http.get(url).success(function(response) {
				 			    
				 				$scope.discounts = response.data;
				 	
				 			});
				 		};
				 		
			 		
			 	   $scope.applyDiscount = function() {
						 
				        	var discountName = $('#coupon').val();
				 			var url = "apply-discount?discountName="+discountName;
				 			$http.get(url).success(function(response) {
				 			    
				 				$scope.discount = response.data;
				 				if($scope.discount == ""){
				 				alert("coupon code invalid");
				 				}
				 				
				 				else{
				 					
				 					
				 					var percent = $scope.discount[0].discountPercentage;
				 					//alert(percent);
				 					var discountable = Math.round( percent )/100*$scope.getTotal();
				 					var  a = $scope.getTotal();
				 			        a= ($scope.getTotal()-discountable); 
				 					//console.log($scope.getTotal()-discountable);
				 					document.getElementById ("totalAmount").innerText = $scope.getTotal()-discountable;
				 					//alert(discounty);
				 							
				 					//alert($scope.getTotal());
				 				
				 				}
				 	
				 			});
				 		};
			 		
		        $scope.change = function() {
		 	   
			        //alert($scope.id);
			        
			        var propertyId = $scope.id;	
		 	        var url = "change-user-property?propertyId="+propertyId;
		 			$http.get(url).success(function(response) {
		 				
		 				 window.location = '/dashboard'; 
		 				//$scope.change = response.data;
		 	
		 			});
				       
			 		};
			 			
			 	$scope.update = function(rValue,indx) {
			 		
			 		
			 		$scope.rValue = rValue;
			 		$scope.roomsSelected[indx].rooms = rValue;
			 		$scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rValue;
			 		
			 		$scope.roomsSelected[indx].adultsAllow  = $scope.availablities[indx].noOfAdults * rValue;
			 		$scope.roomsSelected[indx].minOccu  = $scope.availablities[indx].minOccupancy * rValue;
			 		$scope.roomsSelected[indx].childAllow  = $scope.availablities[indx].noOfChild * rValue;
			 		
			 		   // console.log($scope.item.n, $scope.item.name)
			    }
			 	
			 	
			 	$scope.plus = function(indx,className,limit,arrivalDate,departureDate,minOccupancy,maxOccupancy,extraAdult,extraInfant,extraChild,baseAmount,taxIsActive) {
			 	
			 
			 	
			    //alert(indx);
				var currentVal =  parseInt($('.'+className+indx).val());
				 
				 
			      	   // If is not undefined
			             if (!isNaN(currentVal)) {
			            	 
			            	 if(currentVal >= limit){
					            	
				            	 $('.'+className+indx).val(currentVal);
				            	 //alert("Sorry You Are Exceeding The Limit");
				            	 
				             }
			            	 else{
			                 // Increment
			                 $('.'+className+indx).val(currentVal + 1);
			                 
			            	 }
			             } 
			      	   
			      	   
			            else {
			                 // Otherwise put a 0 there
			                 $('.'+className+indx).val(0);
			             }
			      	   
			             var rooms = parseInt($('.rooms'+indx).val());
				         var adult = parseInt($('.adult'+indx).val());
				         var child = parseInt($('.child'+indx).val());
				         var infant = parseInt($('.infant'+indx).val());
				       /*   var date1 = new Date(arrivalDate);
				         var date2 = new Date(departureDate); */
				         var a1 = arrivalDate.split(/[^0-9]/);
				         var d1= new Date (a1[0],a1[1]-1,a1[2],a1[3],a1[4],a1[5] );
				        // alert(arrival+ " "+d1);
				         
				         var a2 = departureDate.split(/[^0-9]/);
				         var d2= new Date (a2[0],a2[1]-1,a2[2],a2[3],a2[4],a2[5] );
				         //alert(departure+ " "+d2);
				       
				         var timeDiff = Math.abs(d2.getTime() - d1.getTime());
				         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
				        // alert(diffDays);
				         var minOccu = parseInt(minOccupancy*rooms);
				         
				         var maxOccu = parseInt(maxOccupancy*rooms);
				         
				         var extraAdult = parseInt(extraAdult);
				         var extraInfant = parseInt(extraInfant);
				         var extraChild = parseInt(extraChild);
				         var amount = parseFloat(baseAmount);
				         var totalOccu = parseInt(adult+child+infant);
				         var total = (rooms*baseAmount);
				         var roomAmount = parseFloat(total);
				         var adltChd = parseInt(adult+child); 
				         
				         var adltChdIft = parseInt(adult+child+infant); 
				        // alert(adltChdIft);
				         $scope.roomsSelected[indx].rooms = rooms;
			             $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
				         $scope.roomsSelected[indx].adultsCount = adult;
				         $scope.roomsSelected[indx].childCount = child;
				         $scope.roomsSelected[indx].infantCount = infant;
				        
				         
				         
				         
				         if(totalOccu > maxOccu){
					         
				        	 alert("Runnig out of Maximum Occupancy");
				        	 
				        	 $('.adult'+indx).val(1);
				        	 $('.child'+indx).val(0);
				        	 $('.infant'+indx).val(0);
				        	 
				        	 $scope.roomsSelected[indx].adultsCount = 1;
					         $scope.roomsSelected[indx].childCount = 0;
					         $scope.roomsSelected[indx].infantCount = 0;
					         $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
				        	 //finTotal = 0;
				        	// $(this).val(currentVal-1);
				        	 //$scope.roomsSelected[indx].adultsCount = currentVal-1;
				        
				         }
				         
				         else{
				       
				         if(adult>minOccu){
				        	 
				        	 var adultsLeft = (adult-minOccu);
				        	 
				        	 $scope.roomsSelected[indx].total =  (diffDays * adultsLeft * extraAdult + roomAmount);
				        //alert(adultsLeft);
				        	 //$(".xtraadult").show();
				        
				        	 //alert(extraAdult);
				        	
				         }
				         
				         else if(child>minOccu){
				        	 
				        	 var childLeft = (child-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
				        	//alert("child" +childLeft);
				        	 //$(".post").show();
				        	 //alert(childLeft);
				        		
				         }
				         
				         else if(infant>minOccu){
				        	 
				        	 var infantLeft = (infant-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
				        	 //alert("infant" +infantLeft);
				         }
                         
				         else if((adult+child) > minOccu){
				        	 
				        	 var childLeft = (adltChd-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
				        	//alert("child" +childLeft);
				        	
				        	
				         }
                         
				         else if((adult+child+infant) > minOccu && infant >0){
				        	 
				        	 var infantLeft = (adltChdIft-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
				        	 //alert("infant" +infantLeft);
				        
				         }
				         
				         }
				         
				         var amount = parseFloat(baseAmount);
				         if(taxIsActive == 'true'){
				         
				        	 var taxPercentage = $scope.getTaxPercentage(amount);
				        	 
				         }
				         else{
				        	 var taxPercentage = 0;
				         }
				         
				         var taxes = taxPercentage/100*amount* diffDays;
	                     var extraPrice = ($scope.roomsSelected[indx].total - (baseAmount * rooms * diffDays));
	                     var extraTax = taxPercentage/100*extraPrice;
	                     
	                     //alert(extraTax);
	                     var taxes = Math.round(taxPercentage / 100 * amount) * diffDays * rooms;
	                     taxes+=extraTax;
	                     $scope.roomsSelected[indx].tax = taxes;
	                     document.getElementById("xtrabed").innerHTML = "Extra Bed = " + extraPrice;
	                     document.getElementById("taxpercent").innerHTML = + taxPercentage;
                          
				       
			      	   
					 }
			 	
			 	
                  $scope.minus = function(indx,className,arrivalDate,departureDate,minOccupancy,maxOccupancy,extraAdult,extraInfant,extraChild,baseAmount,taxIsActive) {
					
                	
                	
			 		var currentVal =  parseInt($('.'+className+indx).val());
			 		var limit = 0;
			      	   // If is not undefined
			             if (!isNaN(currentVal)) {
			                 // Increment
			                
			                 if(currentVal <= limit){
					            	
				            	 $('.'+className+indx).val(currentVal);
				            	
				            	 
				             }
			            	 else{
			                 // Increment
			            	   $('.'+className+indx).val(currentVal - 1);
			                 
			            	 }
			                 
			             } else {
			                 // Otherwise put a 0 there
			                 $('.'+className+indx).val(0);
			             }
			      	   
			      	   
			             var rooms = parseInt($('.rooms'+indx).val());
				         var adult = parseInt($('.adult'+indx).val());
				         var child = parseInt($('.child'+indx).val());
				         var infant = parseInt($('.infant'+indx).val());
				        /*  var date1 = new Date(arrivalDate);
				         var date2 = new Date(departureDate); */
				         var a1 = arrivalDate.split(/[^0-9]/);
				         var d1= new Date (a1[0],a1[1]-1,a1[2],a1[3],a1[4],a1[5] );
				        // alert(arrival+ " "+d1);
				         
				         var a2 = departureDate.split(/[^0-9]/);
				         var d2= new Date (a2[0],a2[1]-1,a2[2],a2[3],a2[4],a2[5] );
				         //alert(departure+ " "+d2);
				       
				         var timeDiff = Math.abs(d2.getTime() - d1.getTime());
				         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
				         var minOccu = parseInt(minOccupancy*rooms);
				         var maxOccu = parseInt(maxOccupancy*rooms);
				         var extraAdult = parseInt(extraAdult);
				         var extraInfant = parseInt(extraInfant);
				         var extraChild = parseInt(extraChild);
				         var amount = parseFloat(baseAmount);
				         var totalOccu = parseInt(adult+child+infant);
				         var total = (diffDays*rooms*baseAmount);
				         var roomAmount = parseFloat(total);
				         var adltChd = parseInt(adult+child); 
				         
				         var adltChdIft = parseInt(adult+child+infant); 
				        // alert(adltChdIft);
				         $scope.roomsSelected[indx].rooms = rooms;
			             $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
				         $scope.roomsSelected[indx].adultsCount = adult;
				         $scope.roomsSelected[indx].childCount = child;
				         $scope.roomsSelected[indx].infantCount = infant;
				         var finTotal = $scope.roomsSelected[indx].total;
				         
				         
				         if(totalOccu > maxOccu){
					         
				        	 alert("Runnig out of Maximum Occupancy");
				        	 
				        	 $('.adult'+indx).val(1);
				        	 $('.child'+indx).val(0);
				        	 $('.infant'+indx).val(0);
				        	 
				        	 $scope.roomsSelected[indx].adultsCount = 1;
					         $scope.roomsSelected[indx].childCount = 0;
					         $scope.roomsSelected[indx].infantCount = 0;
					         $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
				        	 //finTotal = 0;
				        	// $(this).val(currentVal-1);
				        	 //$scope.roomsSelected[indx].adultsCount = currentVal-1;
				        
				         }
				         
				         else{
				       
				         if(adult>minOccu){
				        	 
				        	 var adultsLeft = (adult-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * adultsLeft * extraAdult + roomAmount);
				        	 //alert("adult" +adultsLeft);
				         }
				         
                         if(child>minOccu){
				        	 
				        	 var childLeft = (child-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
				        	// alert("child" +childLeft);
				         }
				         
                         if(infant>minOccu){
				        	 
				        	 var infantLeft = (infant-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
				        	 //alert("infant" +infantLeft);
				         }
                         
                         if((adult+child) > minOccu){
				        	 
				        	 var childLeft = (adltChd-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
				        	// alert("child" +childLeft);
				         }
                         
                         if((adult+child+infant) > minOccu && infant >0){
				        	 
				        	 var infantLeft = (adltChdIft-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
				        	 //alert("infant" +infantLeft);
				         }
				         
				         }
                          //alert($scope.roomsSelected[indx].total);
				         
				          
				         var amount = parseFloat(baseAmount);
				         
				         if(taxIsActive == 'true'){
					         
				        	 var taxPercentage = $scope.getTaxPercentage(amount);
				        	 
				         }
				         else{
				        	 var taxPercentage = 0;
				         }
				         
				        
				         var taxes = taxPercentage/100*amount* diffDays;
	                     var extraPrice = ($scope.roomsSelected[indx].total - (baseAmount * rooms * diffDays));
	                     var extraTax = taxPercentage/100*extraPrice;
	                     
	                     //alert(extraTax);
	                     var taxes = Math.round(taxPercentage / 100 * amount) * diffDays * rooms;
	                     taxes+=extraTax;
	                     $scope.roomsSelected[indx].tax = taxes;
	                     document.getElementById("xtrabed").innerHTML = "Extra Bed = " + extraPrice;
	                     document.getElementById("taxpercent").innerHTML = + taxPercentage;
			       
			             
			      	   
					 }
			 	
			 	$scope.roomPlus = function(indx) {
					
			         
			         
			 		 var currentVal =  parseInt($('.rooms').val());
			      	   // If is not undefined
			             if (!isNaN(currentVal)) {
			                 // Increment
			                 $('.rooms').val(currentVal + 1);
			             } else {
			                 // Otherwise put a 0 there
			                 $('.rooms').val(0);
			             }
			       
			             var rooms = parseInt($('.rooms').val());
			             $scope.roomsSelected[indx].rooms = rooms;
			             $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
			      	   
					 }
			 	
			 	
			 	$scope.roomMinus = function(indx) {
					
			         
			 		var currentVal =  parseInt($('.rooms').val());
	                  // If is not undefined
                   if (!isNaN(currentVal) && currentVal > 0) {
                   // Decrement one
                    $('.rooms').val(currentVal - 1);
                    } else {
                    // Otherwise put a 0 there
                     $('.rooms').val(0);
                   }
				         
                   var rooms = parseInt($('.rooms').val());
		           $scope.roomsSelected[indx].rooms = rooms;
		           $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
		           
					 }
			 	
			 	$scope.adultPlus = function(indx,arrivalDate,departureDate,minOccupancy,maxOccupancy,extraAdult,extraInfant,extraChild,baseAmount) {
					
			 		 /*var currentVal =  parseInt($('.adult').val());
			      	  
			             if (!isNaN(currentVal)) {
			                
			                 $('.adult').val(currentVal + 1);
			             } else {
			                 $('.adult').val(0);
			             }
			 		   
			 		 */
			             
			             var rooms = parseInt($('.rooms').val());
				         var adult = parseInt($('.adult').val());
				         var child = parseInt($('.child').val());
				         var infant = parseInt($('.infant').val());
				         var date1 = new Date(arrivalDate);
				         var date2 = new Date(departureDate);
				         var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24))); 
				         var minOccu = parseInt(minOccupancy*rooms);
				         var maxOccu = parseInt(maxOccupancy*rooms);
				         var extraAdult = parseInt(extraAdult);
				         var extraInfant = parseInt(extraInfant);
				         var extraChild = parseInt(extraChild);
				         var amount = parseFloat(baseAmount);
				         var totalOccu = parseInt(adult+child+infant);
				         var total = (diffDays*rooms*baseAmount);
				         var roomAmount = parseFloat(total);
				         var adltChd = parseInt(adult+child); 
				         
				         var adltChdIft = parseInt(adult+child+infant); 
				        // alert(adltChdIft);
				         $scope.roomsSelected[indx].rooms = rooms;
			             $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
				         $scope.roomsSelected[indx].adultsCount = adult;
				         $scope.roomsSelected[indx].childCount = child;
				         $scope.roomsSelected[indx].infantCount = infant;
				         var finTotal = $scope.roomsSelected[indx].total;
				        
				         
				         if(totalOccu > maxOccu){
					         
				        	 alert("Runnig out of Maximum Occupancy");
				        	 
				        	 $('.adult').val(1);
				        	 $('.child').val(0);
				        	 $('.infant').val(0);
				        	 
				        	 $scope.roomsSelected[indx].adultsCount = 1;
					         $scope.roomsSelected[indx].childCount = 0;
					         $scope.roomsSelected[indx].infantCount = 0;
					         $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
				        	 //finTotal = 0;
				        	// $(this).val(currentVal-1);
				        	 //$scope.roomsSelected[indx].adultsCount = currentVal-1;
				        
				         }
				         
				         else{
				       
				         if(adult>minOccu){
				        	 
				        	 var adultsLeft = (adult-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * adultsLeft * extraAdult + roomAmount);
				        	 //alert("adult" +adultsLeft);
				         }
				         
                         if(child>minOccu){
				        	 
				        	 var childLeft = (child-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
				        	// alert("child" +childLeft);
				        
				         }
				         
                         if(infant>minOccu){
				        	 
				        	 var infantLeft = (infant-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
				        	 //alert("infant" +infantLeft);
				         }
                         
                         if((adult+child) > minOccu){
				        	 
				        	 var childLeft = (adltChd-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * childLeft * extraChild + roomAmount);
				        	// alert("child" +childLeft);
				         }
                         
                         if((adult+child+infant) > minOccu && infant >0){
				        	 
				        	 var infantLeft = (adltChdIft-minOccu);
				        	 $scope.roomsSelected[indx].total =  (diffDays * infantLeft * extraInfant + roomAmount);
				        	 //alert("infant" +infantLeft);
				         }
				         
				         }
                          //alert($scope.roomsSelected[indx].total);
				         
				          
				        //  var taxy = $scope.roomsSelected[indx].tax;
				          
				          if(finTotal < 1000){
			                     //alert("dsfsd");
			                     var percent = 0;
			                     var taxes = 0/100*finTotal* diffDays;
			                     $scope.roomsSelected[indx].tax = taxes; 
			                     //alert(taxes);
			                 }
			                 else if(finTotal >= 1000 && finTotal <= 2500){
			                     //alert("dsfsd");
			                     var percent = 12;
			                     //var taxes = 12/100*amount* diffDays;
			                     var taxes = Math.round(percent / 100 * finTotal) * diffDays;
			                     $scope.roomsSelected[indx].tax = taxes; 
			                     //alert(taxes);
			                 }
			                 else if(finTotal >= 2500 && finTotal <= 7000){
			                     //alert("dsfsd");
			                     var percent = 18;
			                     var taxes = Math.round(percent / 100 * finTotal) * diffDays;
			                     $scope.roomsSelected[indx].tax = taxes; 
			                     //alert(taxes);
			                 }
			                 else if(finTotal >= 7000 && finTotal <= 7000000){
			                     //alert("dsfsd");
			                     var percent = 28;
			                     //var taxes = Math.round(percent)/100 *amount* diffDays;
			                    var taxes = Math.round(percent / 100 * finTotal) * diffDays;
			                    $scope.roomsSelected[indx].tax = taxes; 
			                     //alert( taxes);
			                 }
				          
				          
				      /*   if( totalOccu > minOccu &&  adult > minOccu && totalOccu <= maxOccu ){
				        	
				         adultsLeft = parseInt(totalOccu-minOccu); 
				         $scope.roomsSelected[indx].total =  (diffDays * adultsLeft * extraAdult + roomAmount);
				        
				          }
				        	 
				        */	 
				        	 
				         
				         
				          
				         
					 }
			 	
			 	
			 	
			 	// shiva update start	
			  $scope.updateExtraAdult = function(adults,child,indx,minOccu,baseAmount,diffDays,rooms) {
				  
				  
				 // alert("min"+minOccu);
				 // alert("base"+baseAmount);
				//	alert("days"+diffDays);
				 //alert("room"+rooms);
				 
				  var total = (diffDays*rooms*baseAmount);
				  adults = parseInt(adults);
				  child = parseInt(child);
				  minOccu = parseInt(minOccu);
				  var totalOccu = parseInt(adults+child);
				  var roomAmount = parseFloat(total);
				  $scope.roomsSelected[indx].adultsCount = adults;
				  $scope.roomsSelected[indx].childCount = child;
				  
				          if(totalOccu>minOccu){
					  
					      if(adults<=minOccu){
						  
						  var childLeft = totalOccu-minOccu;
						  
						  // alert("extraChild "+childLeft); 
						  
						  $scope.roomsSelected[indx].total =  (diffDays * childLeft * $scope.roomsSelected[indx].extraChild) + roomAmount;
						  // var bdprice1 =  (adultsLeft * $scope.roomsSelected[indx].extraAdult);
						 
						  }
						  
						  if(adults > minOccu){
					      
					      $scope.roomsSelected[indx].total = roomAmount;
					     
					      var adultsLeft = adults-minOccu; 
					      //alert("extraAdult "+adultsLeft);
					     
					      $scope.roomsSelected[indx].total = (diffDays * adultsLeft * $scope.roomsSelected[indx].extraAdult) + roomAmount;
					      
					      var left =  totalOccu-minOccu;
					      //alert(left);
						  var extraChd =   left-adultsLeft;
						  
						  $scope.roomsSelected[indx].total += (diffDays * extraChd * $scope.roomsSelected[indx].extraChild);
						  
						  //alert("extraChild "+ extraChd);
						  
						  }  
					  
				  }
				  
			
				  
				  else{
				  
				  $scope.roomsSelected[indx].total = roomAmount * $scope.roomsSelected[indx].rooms ;
					  
				  }
				       
				 }
			 	
			 	
			 	
			  
			    //shiva update end
		        $scope.childAge = function(indx,chValue) {
				
		        var val = chValue;
		        var row = document.getElementById("tr-"+indx);
		        
		       
		        $('#output'+indx).empty();
		        var output;
		        for (var i = 0, length = val; i < length; i++) {
		                  
		            output = "<select> <option value=''>Age</option> <option value='1'>1</option> <option value='2'>2</option> <option value='3'>3</option> </select> &nbsp &nbsp";
		            $('#output'+indx).append(output);
		        }
		        
		        
		       
		        
				 }
		      
		        
		   $scope.getBookedDetails = function(){
					
					//alert("hai");
					//alert($('#bookingId').val());
					//$('#bookingId').val();
					var bookingId = '297';
					var fdata = "bookingId=" + bookingId; 
					
		             
					
					
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'get-booked-details'
							}).success(function(response) {
								
								$scope.booked = response.data;
								//console.log($scope.booked[0].firstName);
								
								
							});

				};
				
				$scope.getProperty = function() {
					
					var url = "get-front-property?propertyId="+propertyId;
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.property = response.data;
					
					});
				};	
				
				$scope.getUser = function() {
					
					//alert($('#userId').val());
					
					
				//var userid = document.getElementById("userId").value;
					
					var url = "get-customer?auserId="+$('#userId').val();
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.user = response.data;
					
					});
				};
				
		    $scope.getTaxes = function() {

					var url = "get-taxes";
					$http.get(url).success(function(response) {
					    //console.log(response);
						$scope.taxes = response.data;
			
					});
				};
				
	       
		    $scope.getTaxes();
			$scope.getUser();
			$scope.getType();
			$scope.getProperty();
			$scope.getCurrentDiscounts();
		/*	$scope.getAccommodations();
			$scope.getPropertyPhotos();	
			$scope.getProperty();	     
			
		*/		

		});

		
	</script>
		

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
var options = {
    "key": "rzp_live_XK8Wemj85ETgaW",
    "amount": 20000, // 2000 paise = INR 20
    "name": "Merchant Name",
    "description": "Purchase Description",
    "image": "/your_logo.png",
    "handler": function (response){
        //alert(response.razorpay_payment_id);
         $scope.confirmBooking();
    },
    "prefill": {
        "name": "Harshil Mathur",
        "email": "harshil@razorpay.com"
    },
    "notes": {
        "address": "Hello World"
    },
    "theme": {
        "color": "#F37254 "
    }
};
var rzp1 = new Razorpay(options);

document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}
</script>

	
	 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
<script>
function goToByScroll(id){
   
    // Reove "link" from the ID
  id = id.replace("link", "");
    // Scroll
  $('html,body').animate({
      scrollTop: $("#"+id).offset().top},
      'slow');
}
</script>


  