<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div id="wrapper">
      <section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index">Home</a></li>
                        <li>Privacy Policy</li>
                    </ul>
                    <h3>PRIVACY POLICY</h3>
                </div>
            </div> 
        </div>
    </section>

    

    <section class="section clearfix">
        <div class="container">
            <div class="row">
                <div id="fullwidth" class="col-sm-12 pad-bot">

                    <!-- START CONTENT -->
                  
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           
                        <p>We, ULO HOTELS PRIVATE LIMITED (hereinafter referred to as (“COMPANY”), a private limited company incorporated under the companies act, 2017,  registered at 22, ARUNACHALAM SALAI, SALIGRAMAM, CHENNAI -600093, TAMILNADU, INDIA. the creators of this Privacy Policy.</p> 
                         <p>The ULO HOTELS esteems your right to privacy, and any personal information that you share with us, (your name, age, address, marital status, mobile number, credit card/Debit card particulars and other details) shall be entitled to privacy and kept confidential and shall not be used/disclosed , save for the purpose of doing the intended business with you, or if required to be disclosed under the oath of law.</p>
                        <p>We hereby agree/undertake that we do not sell, trade or otherwise transfer  your personal information to outside parties. This does not include trusted third parties who assist us in operating our website, conducting business, or providing service, as long as those parties agree to keep this information confidential.We may also release your information to comply with the law, to protect ours or other rights, property or safety. The Ulo Hotels private limited reserves its rights to collect,analyze and propagate aggregate site usage patterns of all its visitors with a view of enhancing services to its visitors. This includes sharing the information with its subsidiaries, and business associates as a general business practice.</p>
                          <p>The Ulo Hotels Private Limited assures you that it will do its best to ensure the privacy and security of your personal information, it shall not be responsible in any manner whatsoever for any violation or misuse of your personal information by unauthorized persons consequent to misuse of the internet.</p>
                         <h5>Cookies</h5>
                         <p>To personalize your experience on our website or to support one of our promotions, we may assign your computer browser a unique random number called a cookie. Cookies enhance the website performance in important ways like personalizing your experience or making your visit more convenient.</p>
                         <p>A cookie is a small text file that is stored on your computer that enables us to recognize you (for example, as a registered user) when you visit our website, The ULO HOTELS PRIVATE LIMITED does not allow/use cookies to collect your personal information. Cookies cannot read data from your computer's hard disk or read cookie files from other websites</p>
                         <p>The Ulo Hotels private limited has reserve the right to change the Terms and Privacy Policy from time to time as it deem fit, with a view to making the policy more user friendly</p>
                         <p>If you have any questions or concerns regarding this privacy policy, you shall contact us by sending an e-mail <a href="mailto:support@ulohotels.com">support@ulohotels.com</a></p>
                        </div><!-- end col -->
                    <!-- END CONTENT -->

                </div><!-- end fullwidth -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->
</div>
 <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
    