<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Property Profile
         <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="#">Property</a></li>
         <li class="active">Profile</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <div class="box-body" id= "page-header" >
               <div class="alert alert-success alert-dismissable" id="alert-message">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-check"></i> Alert!</h4>
                  Successfully updated
               </div>
            </div>
         </div>
         <div class="col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Property Info</a></li>
                  <!-- <li ><a href="#tab-subscription" data-toggle="tab">Property Images</a></li> -->
                  <li ng-repeat="pc in photocategory"><a href="#fa-icons3" ng-click="getCategoryPhotos(pc.photoCategoryId)" data-toggle="tab">{{pc.photoCategoryName}}</a></li>
                  <li ><a href="#tab-policy" data-toggle="tab">Property Policy</a></li>
               </ul>
               <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
                     <form method="post" theme="simple" name="propertyprofile">
                        <s:hidden name="propertyId" value="%{propertyId}"></s:hidden>
                        <section id="new">
                           <!-- <h4 class="page-header">General Settings</h4> -->
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Property Type</label>
                                    <select name="propertyTypeId" id="propertyTypeId"  ng-model="propertyTypeselect" class="form-control" ng-required="true">
                                       <option value="">select</option>
                                       <option ng-repeat="pt in propertytypes" value="{{pt.DT_RowId}}" ng-selected ="property[0].propertyTypeId == pt.DT_RowId">{{pt.propertyTypeName}}</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <label for="firstname">Description</label>
                                    <textarea id="propertyDescription" name="propertyDescription" class="form-control"  value="{{property[0].propertyDescription}}"  placeholder="Description"   ng-required="true">
                                    {{property[0].propertyDescription}}
                                    </textarea>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section id="web-application">
                           <h4 class="page-header">Property Contact Info</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label for="address">Property Name</label>
                                    <input type="text" class="form-control" name="propertyName"  id="propertyName" value="{{property[0].propertyName}}" placeholder="Name" ng-model='property[0].propertyName' ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.propertyName.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <%-- <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                 <label>Property Phone</label>
                                 <input type="text" class="form-control" name="propertyPhone"  id="propertyPhone" value="{{property[0].propertyPhone}}" placeholder="Phone" ng-model='property[0].propertyPhone' ng-pattern="/^[0-9]/" ng-required="true">
                                 <span ng-show="propertyprofile.propertyPhone.$error.pattern" style="color:red">Not a Valid Property Phone!</span>
                                 </div>
                                 </div> --%>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Property Email</label>	                      			
                                    <input type="text" class="form-control" name="propertyEmail"  id="propertyEmail" value="{{property[0].propertyEmail}}" placeholder="Email" ng-model='property[0].propertyEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.propertyEmail.$error.pattern" style="color:red">Not a Valid Property Email!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Property Contact</label>	                      			
                                    <input type="text" class="form-control" name="propertyContact"  id=propertyContact value="{{property[0].propertyContact}}" placeholder="Contact" ng-model='property[0].propertyContact' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.propertyContact.$error.pattern" style="color:red">Not a Valid Property Contact!</span>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section id="web-application">
                           <h4 class="page-header">Reservation Manager Contact Info</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label for="address">Reservation Manager Name</label>
                                    <input type="text" class="form-control" name="reservationManagerName" id="reservationManagerName" value="{{property[0].reservationManagerName}}" placeholder="Reservation Manager Name" ng-model='property[0].reservationManagerName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.reservationManagerName.$error.pattern" style="color:red">Not a Valid Reservation Manager Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Reservation Manager Email</label>
                                    <input type="text" class="form-control" name="reservationManagerEmail" id="reservationManagerEmail" value="{{property[0].reservationManagerEmail}}" placeholder="Reservation Manager Email" ng-model='property[0].reservationManagerEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true">
                                    <span ng-show="propertyprofile.reservationManagerEmail.$error.pattern" style="color:red">Not a Valid Reservation Manager Email!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Reservation Manager Contact</label>
                                    <input type="text" class="form-control" name="reservationManagerContact" id="reservationManagerContact" value="{{property[0].reservationManagerContact}}" placeholder="Reservation Manager Contact" ng-model='property[0].reservationManagerContact' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.reservationManagerContact.$error.pattern" style="color:red">Not a Valid Reservation Manager Contact!</span>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section id="web-application">
                           <h4 class="page-header">Resort Manager Contact Info</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label for="address">Resort Manager Name</label>
                                    <input type="text" class="form-control" name="resortManagerName" id="resortManagerName" value="{{property[0].resortManagerName}}" placeholder="Resort Manager Name" ng-model='property[0].resortManagerName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerName.$error.pattern" style="color:red">Not a Valid Resort Manager Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Resort Manager Email</label>
                                    <input type="text" class="form-control" name="resortManagerEmail" id="resortManagerEmail" value="{{property[0].resortManagerEmail}}" placeholder="Resort Manager Email" ng-model='property[0].resortManagerEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerEmail.$error.pattern" style="color:red">Not a Valid Resort Manager Email!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Resort Manager Contact</label>
                                    <input type="text" class="form-control" name="resortManagerContact" id="resortManagerContact" value="{{property[0].resortManagerContact}}" placeholder="Resort Manager Contact" ng-model='property[0].resortManagerContact' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerContact.$error.pattern" style="color:red">Not a Valid Resort Manager Contact!</span>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section id="property-address">
                           <h4 class="page-header">Property Address</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label for="countryCode">Country</label>
                                    <select name="countryCode" id="countryCode" value="{{property[0].countryCode}}" class="form-control" ng-required="true">
                                       <option value="select">select</option>
                                       <option ng-repeat="c in countries" value="{{c.countryCode}}"  ng-selected ="property[0].countryCode == c.countryCode">{{c.countryName}}</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Address 1</label>	                      			
                                    <input type="text" class="form-control" name="address1"  id="address1" value="{{property[0].address1}}" placeholder="Address 1" ng-model='property[0].address1' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/" ng-required="true">
                                    <span ng-show="propertyprofile.address1.$error.pattern" style="color:red">Not a Valid Alphabet!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Address 2</label>                   			
                                    <input type="text" class="form-control" name="address2"  id="address2" value="{{property[0].address2}}" placeholder="Address 2" ng-model='property[0].address2' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/" ng-required="true">
                                    <span ng-show="propertyprofile.address2.$error.pattern" style="color:red">Not a Valid Alphabet!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>City</label>	                      			
                                    <input type="text" class="form-control" name="city"  id="city" value="{{property[0].city}}" placeholder="City" ng-model='property[0].city' ng-pattern="/^[a-zA-Z]/" ng-required="true">
                                    <span ng-show="propertyprofile.city.$error.pattern" style="color:red">Not a Valid Alphabet!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>State</label>
                                    <select name="stateCode" id="stateCode" value="{{property[0].stateCode}}" class="form-control" ng-required="true">
                                       <option value="select">select</option>
                                       <option ng-repeat="s in states" value="{{s.stateCode}}" ng-selected ="property[0].stateCode == s.stateCode">{{s.stateName}}</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Zip Code</label>	                      			
                                    <input type="text" class="form-control" name="zipCode"  id="zipCode" value="{{property[0].zipCode}}" placeholder="Zip Code" ng-model='property[0].zipCode' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.zipCode.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Latitude</label>	                      			
                                    <input type="text" class="form-control" name="latitude"  id="latitude" value="{{property[0].latitude}}" placeholder="Latitude" ng-model='property[0].latitude' ng-pattern="/^[0-9]/" ng-required="true" >
                                    <span ng-show="propertyprofile.latitude.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Longitude</label>	                      			
                                    <input type="text" class="form-control" name="longitude"  id="longitude" value="{{property[0].longitude}}" placeholder="Longitude" ng-model='property[0].longitude' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.longitude.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section id="property-map">
                           <h4 class="page-header">Property Tax</h4>
                           <div class="row">
                              <div class="col-md-12">
                     <form name="tax">
                     <div class="form-group col-md-3">
                     <label>Tax Valid From:</label>
                     <div class="input-group date">
                     <label class="input-group-addon btn" for="taxValidFrom">
                     <span class="fa fa-calendar"></span>
                     </label>   
                     <input type="text" id="taxValidFrom" type="text" class="form-control" name="taxValidFrom"  value="{{property[0].taxValidFrom}}">
                     </div>
                     <!-- /.input group -->
                     </div>
                     <div class="form-group col-md-3">
                     <label>Tax Valid To:</label>
                     <div class="input-group date">
                     <label class="input-group-addon btn" for="taxValidTo">
                     <span class="fa fa-calendar"></span>
                     </label>   
                     <input type="text" id="taxValidTo" type="text" class="form-control" name="taxValidTo"  value="{{property[0].taxValidTo}}">
                     </div>
                     <!-- /.input group -->
                     </div>
                     <!--  <input type="hidden" ng-value="property[0].taxIsActive == 'true' ? true : false"> -->
                     <div class="col-md-3">
                     <div class="form-group">
                     <label>Tax Status</label>
                     <select name="taxIsActive" id="taxIsActive"   class="form-control" ng-required="true">
                     <option value="true"  ng-selected ="property[0].taxIsActive == 'true'">Active</option>
                     <option value="false" ng-selected ="property[0].taxIsActive == 'false'">InActive</option>
                     <!-- <option ng-repeat="pt in propertytypes" value="{{pt.DT_RowId}}" ng-selected ="property[0].propertyTypeId == pt.DT_RowId">{{pt.propertyTypeName}}</option> -->
                     </select>
                     </div>
                     </div>
                     </div>
                     </form>
                     </div>
                     </section>
                     <section id="property-map">
                        <h4 class="page-header">Property Address</h4>
                        <div class="row fontawesome-icon-list">
                           <div class="col-md-12 col-sm-4">
                              <div id="map" style="width:100%; height:400px;"></div>
                           </div>
                        </div>
                     </section>
                     <section id="video-player">
                        <h4 class="page-header"></h4>
                        <div class="row fontawesome-icon-list">
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4">
                              <button type="submit" ng-click="editProperty()" class="btn btn-primary btngreeen pull-right" onclick ="goToByScroll('page-header');">Save</button>
                           </div>
                        </div>
                     </section>
                     </form>
                  </div>
                  <!-- /.tab-content -->
                  <div class="tab-pane" id="tab-policy">
                     <form method="post" theme="simple" name="propertyPolicy">
                        <section id="new">
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Hotel Policy : </h4>
                                    <textarea id="propertyHotelPolicy" name="propertyHotelPolicy" class="form-control ckeditor" value="{{property[0].propertyHotelPolicy}}"  placeholder="Hotel Policy"   ng-required="true">
		                    		{{property[0].propertyHotelPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Standard Policy : </h4>
                                    <!-- 		                      			<label for="propertyPolicy">Standard Policy</label> -->
                                    <textarea id="propertyStandardPolicy" name="propertyStandardPolicy" class="ckeditor form-control" value="{{property[0].propertyStandardPolicy}}"  placeholder="Standard Policy"   ng-required="true">
		                    		{{property[0].propertyStandardPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Cancellation Policy : </h4>
                                    <!-- 		                      			<label for="propertyPolicy">Cancellation Policy</label> -->
                                    <textarea  id="propertyCancellationPolicy" name="propertyCancellationPolicy" class="ckeditor form-control" value="{{property[0].propertyCancellationPolicy}}"  placeholder="Cancellation Policy"   ng-required="true">
		                    		{{property[0].propertyCancellationPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section>
                           <h4 class="page-header"></h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4">
                                 <button type="submit" ng-click="editPropertyPolicy()" class="btn btn-primary btngreeen pull-right" onclick ="goToByScroll('page-header');">Save</button>
                              </div>
                           </div>
                        </section>
                     </form>
                  </div>
   
                  <div class="tab-pane" id="fa-icons3">
                     <section id="new">
                        <input type="hidden" id="photoCategoryId" name="photoCategoryId" value=""/>
                        <!-- <h4 class="page-header">66 New Icons in 4.4</h4> -->
                        <div class="row fontawesome-icon-list">
                           <div class="col-md-4" ng-repeat="cp in catphotos">
                              <div class="box box-solid">
                                 <div class="box-header with-border">
                                    <h3 class="box-title">					                 
                                       <input type="radio" name="propertyPhotoId" value="{{cp.DT_RowId}}" class="flat-red">
                                    </h3>
                                    <a href="#" ng-click="deletePhoto(cp.DT_RowId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash"></i></a>
                                 </div>
                                 <!-- /.box-header -->
                                 <div class="box-body">
                                    <img class="attachment-img " src="get-property-image-stream?propertyPhotoId={{cp.DT_RowId}}"  width="100" height="100" alt="attachment image"/>
                                 </div>
                                 <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                           </div>
                           <!-- ./col -->
                        </div>
                     </section>
                     <section id="video-player">
                        <h4 class="page-header"></h4>
                        <div class="row fontawesome-icon-list">
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4">
                              <button  ngf-select="upload($file,cp.photoCategoryId)" ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" class="btn btn-primary btngreen pull-right">Add Images</button>
                           </div>
                        </div>
                     </section>
                  </div>
      
               </div>
            </div>
            <!-- /.nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="modal" id="deletemodal">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" id="deletebtnclose" class="close" data-dismiss="modal"
                     aria-hidden="true">x</button>
                  <h4 class="modal-title">Delete Message</h4>
               </div>
               <div class="modal-body">
                  <div class="alert alert-success alert-dismissible">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <h4><i class="icon fa fa-check"></i>Your Photo has been deleted Sucessfully </h4>
                  </div>
               </div>
               <div class="modal-footer">
                  <a href="#" data-dismiss="modal" class="btn">Close</a>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
         <!-- /.modal-dalog -->
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
   var app = angular.module('myApp',['ngFileUpload']);
   app.controller('customersCtrl',['$scope',
                					'Upload',
               					'$timeout',
               					'$http', function($scope,Upload,$timeout,$http) {
   
   	$("#alert-message").hide();
   	
   	//$scope.progressbar = ngProgressFactory.createInstance();
   	//$scope.progressbar.setColor("green");
       //$scope.progressbar.start();
   	
         	$timeout(function(){
        	    // $scope.progressbar.complete();
              $scope.show = true;
              $("#pre-loader").css("display","none");
          }, 2000);
         	
         	$scope.unread = function() {
   		var notifiurl = "unreadnotifications.action";
   		$http.get(notifiurl).success(function(response) {
   			$scope.latestnoti = response.data;
   		});
   		};
   
   		
   		$scope.getProperty = function() {
   			
   			var url = "get-property";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.property = response.data;
   			
   			});
   		};
   		
   		$scope.getStates = function() {
   			
   			var url = "get-states";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.states = response.data;
   			
   			});
   		};
   		
   		$scope.getCountries = function() {
   			
   			var url = "get-countries";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.countries = response.data;
   			
   			});
   		};
   		
   		$scope.getPropertyPhotos = function() {
   			
   			var url = "get-property-photos";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.photos = response.data;
   			
   			});
   		};
   		
   		$scope.deletePhoto = function(rowid) {
   			
   			
   			var url = "delete-property-photo?propertyPhotoId="+rowid;
   			$http.get(url).success(function(response) {
   			    //console.log(response);
   			    $scope.getPropertyPhotos();
   			    $('#deletemodal').modal('toggle');
   			    $timeout(function(){
   			      	$('#deletebtnclose').click();
   			    }, 2000);
   				
   	
   			});
   			
   		};
   		$scope.editPropertyPolicy = function() {
   			var propertyHotelPolicy= CKEDITOR.instances.propertyHotelPolicy.getData();
   			var propertyStandardPolicy= CKEDITOR.instances.propertyStandardPolicy.getData();
   			var propertyCancellationPolicy= CKEDITOR.instances.propertyCancellationPolicy.getData();
   			
   			var fdata="&propertyHotelPolicy="+ propertyHotelPolicy
   			+ "&propertyStandardPolicy=" +propertyStandardPolicy
   			+"&propertyCancellationPolicy="+ propertyCancellationPolicy;
   			
   			$http(
   					{
   						method : 'POST',
   						data : fdata,
   						headers : {
   							'Content-Type' : 'application/x-www-form-urlencoded'
   						},
   						url : 'edit-property-policy'
   					}).then(function successCallback(response) {
   						
   						 
   						 $scope.getProperty();
   						$("#alert-message").show();
   				
   			}, function errorCallback(response) {
   	
   			}); 
   		}
   		
   		$scope.editProperty = function() {
   			
   		
   			
   			var fdata="&address1="+ $('#address1').val()
   					+ "&address2=" + $('#address2').val()
   					+"&city="+ $('#city').val()
   					+"&latitude="+$('#latitude').val()
   					+"&longitude="+$('#longitude').val()
   					+"&propertyTypeId=" + $('#propertyTypeId').val()
   					+"&propertyContact="+$('#propertyContact').val()
   					+"&propertyDescription="+$('#propertyDescription').val()
   					+"&propertyEmail="+$('#propertyEmail').val()
   					+"&propertyLogo="+$('#propertyLogo').val()
   					+"&propertyName="+$('#propertyName').val()
   					+"&resortManagerName="+$('#resortManagerName').val()
   					+"&resortManagerEmail="+$('#resortManagerEmail').val()
   					+"&resortManagerContact="+$('#resortManagerContact').val()
   					+"&reservationManagerName="+$('#reservationManagerName').val()
   					+"&reservationManagerEmail="+$('#reservationManagerEmail').val()
   					+"&reservationManagerContact="+$('#reservationManagerContact').val()
   					//+"&propertyPhone="+$('#propertyPhone').val()
   					+"&zipCode="+$('#zipCode').val() 
   					+"&countryCode="+$('#countryCode').val()
   					+"&taxValidFrom="+$('#taxValidFrom').val()
   					+"&taxValidTo="+$('#taxValidTo').val()
   					+"&taxIsActive="+$('#taxIsActive').val()
   					+"&stateCode="+$('#stateCode').val();

   			
   		$http(
   					{
   						method : 'POST',
   						data : fdata,
   						headers : {
   							'Content-Type' : 'application/x-www-form-urlencoded'
   						},
   						url : 'edit-property'
   					}).then(function successCallback(response) {
   						
   						 
   						 $scope.getProperty();
   						$("#alert-message").show();
   				
   			}, function errorCallback(response) {

   			}); 
   
   		};
   		
   		
   		 $scope.getPhotoCategory = function() {
   				
   				var url = "get-Photo-category";
   				$http.get(url).success(function(response) {
   					//console.log(response);
   					//alert(JSON.stringify(response.data));
   					$scope.photocategory = response.data;
   				
   				});
   			};
   			
   		
   		$scope.getCategoryPhotos = function(photoCategoryId) {
   	
   			document.getElementById("photoCategoryId").value = photoCategoryId;
   			 var url = "get-category-photos?photoCategoryId="+photoCategoryId							
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.catphotos = response.data;
   				//window.location.reload();
   		
   			}); 
   		};
   		
   			$scope.getCategoryPhotos1 = function() {
   			
   			//$('#adminId').val()	
   			//alert(photoCategoryId);
   			var  photoCategoryId = document.getElementById("photoCategoryId").value;
   			//alert( photoCategoryId);
   			 var url = "get-category-photos?photoCategoryId="+photoCategoryId	
   					// alert(url);
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.catphotos = response.data;
   				//window.location.reload();
   		
   			}); 
   		};
   		
   		
    	
   		$scope.getPropertyTypes = function() {
   			
   			var url = "get-property-types";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.propertytypes = response.data;
   			
   			});
   		};
                 
   		 $scope.getPropertyList = function() {
   			 
   	        	
   	        	
   		        var userId = $('#adminId').val();
   	 			var url = "get-user-properties?userId="+userId;
   	 			$http.get(url).success(function(response) {
   	 			    
   	 				$scope.props = response.data;
   	 	
   	 			});
   	 		};
   	 		
                $scope.change = function() {
          	   
   	        //alert($scope.id);
   	        
   	        var propertyId = $scope.id;	
          	    var url = "change-user-property?propertyId="+propertyId;
    			$http.get(url).success(function(response) {
    				
    				 window.location = '/ulopms/dashboard'; 
    				//$scope.change = response.data;
    	
    			});
   		       
   	 		};
   	 		
   			
   	    $scope.getPropertyList();
   		
   	    $scope.getPhotoCategory();  
   	    	
           $scope.getProperty();
   		
   		$scope.getStates();
   		
   		$scope.getCountries();
   		
   		$scope.getPropertyTypes();
   		
   		$scope.getPropertyPhotos();

           $scope.upload = function (file) {
           	//alert("file"+file);
           	var id = document.getElementById("photoCategoryId").value;
           	//alert("id"+id);
           	//alert("id"+photoCategoryId);	
               Upload.upload({
                   url: 'add-property-photo',
                   enableProgress: true, 
                   data: {myFile: file,'photoCategoryId':id}
               }).then(function (resp) {
               	
               	$scope.getCategoryPhotos1();
                   console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                   //$scope.getPropertyPhotos();
               }, function (resp) {
                   console.log('Error status: ' + resp.status);
               }, function (evt) {
               	
                   var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                   
                   console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                  
               });
              
               
           };
           
   	
   	//$scope.unread();
   	
   
   }]);
   
   
</script>
<script src="js1/upload/ng-file-upload-shim.min.js"></script>
<script src="js1/upload/ng-file-upload.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>

<script>
   $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('propertyDescription');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
     CKEDITOR.replace('propertyHotelPolicy');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('propertyStandardPolicy');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
     CKEDITOR.replace('propertyCancellationPolicy');
     // bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
</script>
<script async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg9dqRsKwRWXGdTKsUNHTHhfQqdSyip9k&callback=initMap"></script>
<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript">
   var markers = [
       {
           "title": 'ulohotels',
           "lat": '13.05406559587872',
           "lng": '80.19280683201782',
           "description": 'ULO Hotels is a trusted and fast growing hotel network with over 10 partners. We use the most advanced technology to offer best value promotions and hotel rates to business and leisure travelers.'
       }
   ];
   window.onload = function () {
       var mapOptions = {
           center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
           zoom: 0,
           mapTypeId: google.maps.MapTypeId.ROADMAP
       };
       var infoWindow = new google.maps.InfoWindow();
       var latlngbounds = new google.maps.LatLngBounds();
       var geocoder = geocoder = new google.maps.Geocoder();
       var map = new google.maps.Map(document.getElementById("map"), mapOptions);
       for (var i = 0; i < markers.length; i++) {
           var data = markers[i]
           var myLatlng = new google.maps.LatLng(data.lat, data.lng);
           var marker = new google.maps.Marker({
               position: myLatlng,
               map: map,
               title: data.title,
               draggable: true,
               animation: google.maps.Animation.DROP
           });
           (function (marker, data) {
               google.maps.event.addListener(marker, "click", function (e) {
                   infoWindow.setContent(data.description);
                   infoWindow.open(map, marker);
               });
               google.maps.event.addListener(marker, "dragend", function (e) {
                   var lat, lng, address;
                   geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                       if (status == google.maps.GeocoderStatus.OK) {
                           lat = marker.getPosition().lat();
                           lng = marker.getPosition().lng();
                           address = results[0].formatted_address;
                           $('#latitude').val(lat);
                           $('#longitude').val(lng);
                           
                           //alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);
                       }
                   });
               });
           })(marker, data);
           latlngbounds.extend(marker.position);
       }
       var bounds = new google.maps.LatLngBounds();
       map.setCenter(latlngbounds.getCenter());
       map.fitBounds(latlngbounds);
   }
</script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $( function() {
     $("#taxValidFrom").datepicker({minDate:0});
     $("#taxValidTo").datepicker({minDate:0});
     $("#checkin" ).datepicker({minDate:0});
     $("#checkout" ).datepicker({minDate:0});
     $("#expiryDate" ).datepicker({minDate:0});
   });
</script> 
<script>
   function goToByScroll(id){
      
       // Reove "link" from the ID
     id = id.replace("link", "");
       // Scroll
     $('html,body').animate({
         scrollTop: $("#"+id).offset().top},
         'slow');
   }
</script>