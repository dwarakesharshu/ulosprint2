<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div id="wrapper">


   <section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index">Home</a></li>
                        <li>Partners</li>
                    </ul>
                    <h3>Partners</h3>
                </div>
            </div> 
        </div>
    </section>

    

    <section class="section clearfix">
        <div class="container well">
            <div class="row">
                <div id="fullwidth" class="col-sm-12 pad-bot">

                    <!-- START CONTENT -->
                            <h6>1. Are your properties performing below 40% occupancy through the year?</h6>
                            <h6>2. Do you struggle to break even?</h6>
                            <h6>3. Are you trying to juggle quality, price and revenue and are having to let one or the other fall?</h6>
   
<br>

		<div class="row">	
   
<div class="col-md-6">
                            <h4 class="text-center" style="color:red;">Partner with us</h4>
                            <p class="text-center">We will figure out your property's USP, turn around your occupancy rate and guarantee revenue.</p>
                              <h4 class="text-center" style="color:red;">How?</h4>
                           
                              <ul class="list-style">
<li>Quality service. </li>
<li>Transparency across all operations</li>
<li>On time payments</li>
<li>Joint decisions</li>
<li>Brand loyalty</li>
<li>Increased occupancy rate</li>
</ul>

<h6 class="text-center" style="color:red;">We ask for only one thing. Teamwork.</h6>
</div>
			<div class="col-md-6">
				<!-- <iframe width="500" height="315" src="https://www.youtube.com/embed/iSlo6aNOUMk?ecver=1" frameborder="0" allowfullscreen></iframe> -->
			<iframe width="500" height="315" src="https://www.youtube.com/embed/ibamCpzTAfs" frameborder="0" allowfullscreen>
</iframe>
			</div>  
			<div class="col-md-12">
			<p>Call us at <a href="tel:9543592593">9543-592-593</a> or drop a mail to <a href="mailto:support@ulohotels.com">support@ulohotels.com</a> We know the internet is here to stay, but we believe some things need to have a human touch.</p>
			<p>We will visit your property and do a detailed analysis. We know how much time and effort you have invested in it and promise to treat it the same way. We will give your property a better presence on the internet, improve quality and employ advanced technology to offer the best experience for our guests.</p>
			<h4>Why should I partner with Ulo?</h4>
			<p>We use smarter technology, astute analytics and streamlined processes to deliver improved business value to your property, helping you serve guests in the best manner.</p>
			</div>
			
		</div>	                      
                
                    <!-- END CONTENT -->
                    <br>
                    <br>

<!-- Testimonial -->
<section class="section fullscreen background parallax" style="background-image:url('upload/parallax_03.jpg');" data-img-width="1920" data-img-height="586" data-diff="10">
        <div class="container">
             <div class="hotel-title text-center">
                <h4>OUR CUSTOMER REVIEWS</h4>
                 <hr>
            </div>
            <div id="testimonials">
                <div class="testi-item">
<!--                     <div class="testi"><img class="image img-responsive size-image" src="images/clients/1.jpg"></div>-->
                    <div class="hotel-title text-center">
                     <img class="wid-img" src="ulowebsite/images/clients/1.jpg" >
                       
                        <p>"We joined ULO and we have been getting constant bookings. We have been doing good since the day we joined Ulo."</p>
                        <h6>- Aravind, ULO Muthu Residency -</h6>
                    </div>
                </div><!-- end testi-item -->

                <div class="testi-item">
<!--                    <div class="testi"><img class="image img-responsive size-image" src="images/clients/2.jpg"></div>-->
                    <div class="hotel-title text-center">
                         <img class="wid-img" src="ulowebsite/images/clients/2.jpg">
                        
                        <p>"ULO Muthu Residency Is a Peaceful Hotel in Kodaikanal. Hotel View Is Very Superb. Food Is Very Good. I Am Enjoyed That Weekend In Kodaikanal.."</p>
                        <h6>- Pravin Kumar -</h6>
                    </div>
                </div><!-- end testi-item -->
                <div class="testi-item">
<!--                     <div class="testi"><img class="image img-responsive size-image" src="images/cli ents/3.jpg"></div>-->
                    <div class="hotel-title text-center">
                   <img class="wid-img" src="ulowebsite/images/clients/3.jpg" >
                       
                        <p>"Last week we had a chance to stay in ulo hotels kodaikanal rooms are well cleaned and room service is always available.valve for money good place for corporate trip"</p>
                        <h6>- Sabareesh Kumar -</h6>
                    </div>
                </div><!-- end testi-item -->
            </div><!-- end testimonials -->
        </div><!-- end container -->
    </section><!-- end section -->
<!--End of testimonial -->
<br>
                    <br>
		<h4 class="text-center">WE GUARANTEE THE FOLLOWING!</h4>
		<br>
		<div class="row text-center">
			<div class="col-md-4"><center><img src="ulowebsite/images/1.png" style="width:100px;" alt="Quality Service"/></center><h5>QUALITY SERVICE</h5></div>
			<div class="col-md-4"><center><img src="ulowebsite/images/2.png" style="width:160px;" alt="Transparency across all operations"/></center><h5>TRANSPARENCY ACROSS ALL OPERATIONS</h5></div>
			<div class="col-md-4"><center><img src="ulowebsite/images/3.png" style="width:70px;" alt="On time payments"/></center><h5>ON TIME PAYMENTS</h5></div>
		</div>	
		<div class="row text-center">
			<div class="col-md-4"><center><img src="ulowebsite/images/4.png" style="width:80px;" alt="Joint decisions"/></center><h5>JOINT DECISIONS</h5></div>
			<div class="col-md-4"><center><img src="ulowebsite/images/5.png" style="width:108px;" alt="Brand loyalty"/></center><h5>BRAND LOYALTY</h5></div>
			<div class="col-md-4"><center><img src="ulowebsite/images/6.png" style="width:92px;" alt="Increased occupancy rate"/></center><h5>INCREASED OCCUPANCY RATE</h5></div>
		</div>	
		</div>	
                </div><!-- end fullwidth -->
        <center> <button type="button" value="" id="joinnow" data-toggle="collapse" data-target="#join_us_form" class="btn btn-primary btn-lg">JOIN NOW</button> </center>
                <div class="collapse" id="join_us_form" >
                <div class="row">	
                    <div class="col-md-10 col-md-offset-1">
                   
                             <div id="contact_form" class="contact_form">
                                
                                <form id="contactform" class="row" action="partnerwithus" name="joinform" method="post">
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                    <input type="text" name="hotelName" id="hotelName" class="form-control" placeholder="Hotel Name" required> 
                                    </div>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                    <input type="text" name="room" id="room" class="form-control" placeholder="Room" required> 
                                    </div>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name " required> 
                                    </div>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                    <input type="text" name="email" id="email" class="form-control" placeholder="Email" required> 
                                    </div>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                    <input type="text" name="mobile" id="mobile" class="form-control" placeholder="Mobile" required > 
                                    </div>
                                    <input type="submit" value="SEND" id="submit" class="pull-left btn btn-primary btn-lg ">
                                 </form>    
                            </div><!-- end contact-form -->  
                           
                        </div><!-- end col -->
                        </div><!--end row -->
                       </div><!--end join us form--> 
            </div><!-- end row -->
     
    </section><!-- end section -->
</div> 
  <style>
  .center{
    width:200px;
    margin:0 auto;
    text-align:left;
}
  h5
  {
  font-size:14px;
  }
  #join_us_form
  {
    background-color: white;
    box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    margin:5% 0%;
    padding: 30px;
  }
 #testimonials p
{
margin-right:30px;
}

  </style>
  <script>
  $(document).ready(function(){
  	$('#join_us_form').hide();
  	$('#joinnow').click(function(){
  	   $('#join_us_form').toggle();
  	});
  });
  </script>
 <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
    