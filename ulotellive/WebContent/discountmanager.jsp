<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Discount Manager
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li class="active">Discount Manager</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
               <h3 class="box-title">Taxes</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
               <table class="table table-hover" id="example1">
                  <tr>
                     <th>Discount Name</th>
                     <th>Discoutnt percentage</th>
                     <th>Edit</th>
                     <th>Delete</th>
                  </tr>
                  <tr ng-repeat="dis in discounts">
                     <td>{{dis.discountName}}</td>
                     <td>{{dis.discountPercentage}}</td>
                     <td>
                        <a href="#editmodal" ng-click="getDiscount(dis.discountId)" data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
                     </td>
                     <td>
                        <a href="#" ng-click="deleteDiscount(dis.discountId)" data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
                     </td>
                  </tr>
                  <tr>
                  </tr>
               </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
               <ul class="pagination pagination-sm no-margin pull-right">
                  <a class="btn" href="#addmodal" ng-click="" data-toggle="modal">Add Discount</a>
               </ul>
            </div>
         </div>
         <!-- /.box -->
      </div>
   </div>
</section>
<!-- /.content -->
<div class="modal" id="addmodal">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" id="btnclose" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title">Add Discount</h4>
         </div>
         <form name="addForm">
            <div class="modal-body">
               <div id="message"></div>
               <div class="form-group">
                  <label>Coupon Code(Coupon Code Should Be Unique, Duplicates Are Not Allowed )</label>
                  <input type="text" class="form-control" name="discountName" value="" id="discountName" ng-model='discountName' ng-pattern="/^[a-zA-Z0-9]*$/" ng-required="true">
                  <span ng-show="addForm.discountName.$error.pattern" style="color:red">Not a valid Name!</span>
               </div>
               <div class=" form-group">
                  <label>Valid From :</label>
                  <div class="input-group date">
                     <label class="input-group-addon btn" for="startDate">
                     <span class="fa fa-calendar"></span>
                     </label>
                     <input type="text" id="startDate" type="text" class="form-control" name="startDate" value="" onkeypress="return false;" ng-required="true">
                  </div>
               </div>
               <div class="form-group">
                  <label>Valid To :</label>
                  <div class="input-group date">
                     <label class="input-group-addon btn" for="endDate">
                     <span class="fa fa-calendar"></span>
                     </label>
                     <input type="text" id="endDate" type="text" class="form-control" name="endDate" value="" onkeypress="return false;" ng-required="true">
                  </div>
               </div>
               <div class="form-group" id="percentDiv">
                  <label>Percentage</label>
                  <input type="text" class="form-control" name="percentage" id="percentage" value="" ng-model="percentage" ng-pattern="/^[0-9]/" ng-required="false">
                  <span ng-show="addForm.percentage.$error.pattern" style="color:red">Not a valid Percentage!</span>
               </div>
               <div class="form-group">
                  <label>No Of Coupons</label>
                  <input type="text" class="form-control" name="units" id="units" value="" ng-model="units" ng-pattern="/^[0-9]/" ng-required="true">
                  <span ng-show="addForm.units.$error.pattern" style="color:red">Not a valid Digit!</span>
               </div>
               <div class="form-group">
                  <label>Signup (You Can Activate Only One Code As Signup )</label>
                  <select name="isSignup" id="isSignup" class="form-control" ng-required="true">
                     <option value="false" ng-selected="d.isSignup == 'false'">InActive</option>
                     <option value="true" ng-selected="d.isSignup == 'true'">Active</option>
                     <!-- <option ng-repeat="pt in propertytypes" value="{{pt.DT_RowId}}" ng-selected ="property[0].propertyTypeId == pt.DT_RowId">{{pt.propertyTypeName}}</option> -->
                  </select>
               </div>
            </div>
            <div class="modal-footer">
               <a href="#" data-dismiss="modal" class="btn">Close</a>
               <button ng-click="addForm.$valid && addDiscount()" ng-disabled="addForm.$invalid" class="btn btn-primary">Save</button>
            </div>
         </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dalog -->
</div>
<div class="modal" id="editmodal">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" id="editbtnclose" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title">Edit Discount</h4>
         </div>
         <form name="editForm">
            <div class="modal-body" ng-repeat="d in discount">
               <div id="editmessage"></div>
               <input type="hidden" class="form-control" name="editDiscountId" id="editDiscountId" value="{{d.discountId}}">
               <div class="form-group">
                  <div class="form-group">
                     <label>Coupon Code</label>
                     <input type="text" class="form-control" name="editDiscountName" value="{{d.discountName}}" ng-model='d.discountName' id="editDiscountName" ng-pattern="/^[a-zA-Z0-9]*$/" ng-required="true">
                     <span ng-show="editForm.editDiscountName.$error.pattern" style="color:red">Not a valid Name!</span>
                  </div>
                  <div class=" form-group">
                     <label>Valid From :</label>
                     <div class="input-group date">
                        <label class="input-group-addon btn" for="editstartDate">
                        <span class="fa fa-calendar"></span>
                        </label>
                        <input type="text" id="editStartDate" type="text" class="form-control" name="editStartDate" value="{{d.startDate}}" onkeypress="return false;" ng-required="true">
                     </div>
                  </div>
                  <div class="form-group">
                     <label>Valid To :</label>
                     <div class="input-group date">
                        <label class="input-group-addon btn" for="editendDate">
                        <span class="fa fa-calendar"></span>
                        </label>
                        <input type="text" id="editEndDate" type="text" class="form-control" name="editEndDate" value="{{d.endDate}}" onkeypress="return false;" ng-required="true">
                     </div>
                  </div>
                  <div class="form-group">
                     <label> Percentage</label>
                     <input type="text" class="form-control" name="editPercentage" id="editPercentage" value="{{d.discountPercentage}}" ng-model='d.discountPercentage' ng-pattern="/^[0-9]/" ng-required="false">
                     <span ng-show="editForm.editPercentage.$error.pattern" style="color:red">Not a valid Percentage!</span>
                  </div>
                  <div class="form-group">
                     <label>No Of Coupons</label>
                     <input type="text" class="form-control" name="editUnits" id="editUnits" value="{{d.discountUnits}}" ng-model='d.discountUnits' ng-pattern="/^[0-9]/" ng-required="false">
                     <span ng-show="editForm.editUnits.$error.pattern" style="color:red">Not a valid Percentage!</span>
                  </div>
                  <div class="form-group">
                     <label>Signup</label>
                     <select name="editIsSignup" id="editIsSignup" class="form-control" ng-required="true">
                        <option value="false" ng-selected="d.isSignup == 'false'">InActive</option>
                        <option value="true" ng-selected="d.isSignup == 'true'">Active</option>
                        <!-- <option ng-repeat="pt in propertytypes" value="{{pt.DT_RowId}}" ng-selected ="property[0].propertyTypeId == pt.DT_RowId">{{pt.propertyTypeName}}</option> -->
                     </select>
                  </div>
               </div>
               <div class="modal-footer">
                  <a href="#" data-dismiss="modal" class="btn">Close</a>
                  <button type="submit" ng-click="editDiscount()" id="editbtn" class="btn btn-primary">update</button>
               </div>
         </form>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dalog -->
   </div>
</div>
</div>
<!-- /.content-wrapper -->
<script></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function($scope, $http, $timeout, ngProgressFactory) {
   
   
       $("#checkAll").click(function() {
           $('input:checkbox').not(this).prop('checked', this.checked);
       });
   
   
       $timeout(function() {
           // $scope.progressbar.complete();
           $scope.show = true;
           $("#pre-loader").css("display", "none");
       }, 2000);
   
   
   
       $scope.unread = function() {
           //var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
           var notifiurl = "unreadnotifications.action";
           $http.get(notifiurl).success(function(response) {
               $scope.latestnoti = response.data;
           });
       };
   
   
       $("#startDate").datepicker({
           dateFormat: 'mm/dd/yy',
           minDate: 0,
           onSelect: function(formattedDate) {
               var date1 = $('#startDate').datepicker('getDate');
               var date = new Date(Date.parse(date1));
               date.setDate(date.getDate() + 1);
               var newDate = date.toDateString();
               newDate = new Date(Date.parse(newDate));
               $('#endDate').datepicker("option", "minDate", newDate);
               $timeout(function() {
                   //scope.checkIn = formattedDate;
               });
           }
       });
   
       $("#endDate").datepicker({
           dateFormat: 'mm/dd/yy',
           minDate: 0,
           onSelect: function(formattedDate) {
               var date2 = $('#endDate').datepicker('getDate');
               $timeout(function() {
                   //scope.checkOut = formattedDate;
               });
           }
       });
   
       $("#editStartDate").datepicker({
           dateFormat: 'mm/dd/yy',
           minDate: 0,
           onSelect: function(formattedDate) {
               var date1 = $('#editStartDate').datepicker('getDate');
               var date = new Date(Date.parse(date1));
               date.setDate(date.getDate() + 1);
               var newDate = date.toDateString();
               newDate = new Date(Date.parse(newDate));
               $('#editEndDate').datepicker("option", "minDate", newDate);
               $timeout(function() {
                   //scope.checkIn = formattedDate;
               });
           }
       });
   
       $("#editEndDate").datepicker({
           dateFormat: 'mm/dd/yy',
           minDate: 0,
           onSelect: function(formattedDate) {
               var date2 = $('#editEndDate').datepicker('getDate');
               $timeout(function() {
                   //scope.checkOut = formattedDate;
               });
           }
       });
       $scope.editDiscount = function() {
   
   
           var fdata = "discountName=" + $('#editDiscountName').val() + "&propertyDiscountId=" + $('#editDiscountId').val() + "&startDate=" + $('#editStartDate').val() + "&endDate=" + $('#editEndDate').val() + "&percentage=" + $('#editPercentage').val() + "&isSignup=" + $('#editIsSignup').val() + "&discountUnits=" + $('#editUnits').val();
   
   
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: 'edit-discount'
           }).then(function successCallback(response) {
   
               var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Updated Succesfully.</div>';
               $('#editmessage').html(alert);
               $timeout(function() {
                   $('#editbtnclose').click();
               }, 2000);
               //window.location.reload();
           }, function errorCallback(response) {
   
   
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });
   
       };
   
       $scope.addDiscount = function() {
   
   
   
           var fdata = "&discountName=" + $('#discountName').val() + "&startDate=" + $('#startDate').val() + "&endDate=" + $('#endDate').val() + "&discountPercentage=" + $('#percentage').val() + "&isSignup=" + $('#isSignup').val() + "&discountUnits=" + $('#units').val()
   
   
           alert(fdata);
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: 'add-discount'
           }).then(function successCallback(response) {
   
               //alert(response);
               //console.log(response.data);
               $scope.res = response.data;
               if ($scope.res != "") {
   
                   var alertmsg = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
                   $('#message').html(alertmsg);
                   $timeout(function() {
                       $('#btnclose').click();
                   }, 2000);
   
                   $scope.getDiscounts();
   
               } else {
   
                   var alertmsg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Coupon Code Already Exist.</div>';
                   $('#message').html(alertmsg);
                   $timeout(function() {
                       $('#btnclose').click();
                   }, 2000);
   
                   $scope.getDiscounts();
   
               }
   
               //window.location.reload();
   
               //alert("update successfully");
           }, function errorCallback(response) {
   
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });
   
       };
   
       $scope.getAccommodations = function() {
   
           var url = "get-accommodations";
           $http.get(url).success(function(response) {
               //console.log(response);
               $scope.accommodations = response.data;
   
           });
       };
   
   
       $scope.changeType = function() {
   
           var type = $scope.discountType;
           //alert($scope.discountType);	
   
           if (type == 'inr') {
   
               $("#inrDiv").show(500);
               $("#percentDiv").hide(500);
           }
   
           if (type == 'percentage') {
   
               $("#inrDiv").hide(500);
               $("#percentDiv").show(500);
           }
   
       };
   
       $scope.getDiscounts = function() {
   
           var url = "get-discounts";
           $http.get(url).success(function(response) {
               //alert(response);
               $scope.discounts = response.data;
   
           });
       };
   
       $scope.getDiscount = function(rowid) {
   
           var url = "get-discount?propertyDiscountId=" + rowid;
           $http.get(url).success(function(response) {
               //alert(response);
               $scope.discount = response.data;
   
           });
       };
   
   
   
   
       $scope.deleteDiscount = function(rowid) {
   
   
           var url = "delete-discount?propertyDiscountId=" + rowid;
           $http.get(url).success(function(response) {
               //console.log(response);
               //$scope.getTaxes();
               // $('#deletemodal').modal('toggle');
               $timeout(function() {
                   $('#deletebtnclose').click();
               }, 2000);
               window.location.reload();
   
           });
   
       };
   
   
   
   
       $scope.getAccommodations();
   
       $scope.getDiscounts();
   
       //$scope.unread();
       //
   
   
   });
</script>
<style>
   #ui-datepicker-div {
   z-index: 99999!important;
   }
</style>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<%-- <script>
   $( function() { $("#startDate" ).datepicker({minDate:0}); $("#endDate" ).datepicker({minDate:0}); $("#checkin" ).datepicker({minDate:0}); $("#checkout" ).datepicker({minDate:0}); });
   </script> --%>