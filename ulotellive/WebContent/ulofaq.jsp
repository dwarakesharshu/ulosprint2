<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <style>

.container {
  max-width: 960px;
}

.panel-default>.panel-heading {
  color: #333;
  background-color: #fff;
  border-color: #e4e5e7;
  padding: 0;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-default>.panel-heading a {
  display: block;
  padding: 10px 15px;
}

.panel-default>.panel-heading a:after {
  content: "";
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-default>.panel-heading a[aria-expanded="true"] {
  background-color: #eee;
}

.panel-default>.panel-heading a[aria-expanded="true"]:after {
  content: "\2212";
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.panel-default>.panel-heading a[aria-expanded="false"]:after {
  content: "\002b";
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
}

.accordion-option {
  width: 100%;
  float: left;
  clear: both;
  margin: 15px 0;
}

.accordion-option .title {
  font-size: 20px;
  font-weight: bold;
  float: left;
  padding: 0;
  margin: 0;
}

.accordion-option .toggle-accordion {
  float: right;
  font-size: 16px;
  color: #6a6c6f;
}

.accordion-option .toggle-accordion:before {
  content: "Expand All";
}

.accordion-option .toggle-accordion.active:before {
  content: "Collapse All";
}
</style>
<div id="wrapper">
   <section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index">Home</a></li>
                        <li>Faq</li>
                    </ul>
                    <h3>Frequently Asked Questions</h3>
                </div>
            </div> 
        </div>
    </section>
<div class="container">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-target="#demo" aria-expanded="true" aria-controls="collapseOne">
         ABOUT ULO
        </a>
      </h4>
      </div>
      <div id="demo" class="collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
                     <h5>What are Ulo Hotels?</h5>
                   <p>We are India's First Quality Budget Hotel Chain, serving you professionally in excellent properties while ensuring  your stay is smooth and doesn't put a hole in your pocket. </p>
                   <h5>Why should you book with us?</h5>
                   <p>We give you excellent quality without burning your pockets, offer attractive discounts on every booking, 24*7 support, instant confirmation, easy cancellations and quick refunds.</p>
                    <h5>How are we different?</h5>
                   <p>We are in direct contact with the owners of all our hotels and have control over the property quality. We organize audits through the managers of the properties and ensure that you get the best of  everything.</p>      
      
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingTwo">
        <h4 class="panel-title">
      <a role="button" data-toggle="collapse" data-target="#demo1" aria-expanded="true" aria-controls="collapseOne">
          Bookings
        </a>
      </h4>
      </div>
       <div id="demo1" class="collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
          <h5>What are the different ways of making a reservation with you?</h5>
                   <p>You can make a booking through any of the following:
                
                   <li style="list-style-type:none">a. Through our website www.ulohotels.com </li>
                   <li style="list-style-type:none">b. Calling our customer care helpdesk at 9543-592-593</li>
                   <li style="list-style-type:none">c. Writing to us reservations@ulohotels.com</li></p>
                   <h5>What are the various payment modes available while booking via your website? </h5>
                   <p>You can make payment via:
                
                   <li style="list-style-type:none">a. Credit card, debit card, net-banking or e-wallets. </li>
                   <li style="list-style-type:none">b. You can pay a partial advance online and pay the rest in person during check-out. </li>
                   <li style="list-style-type:none">c. You could do an online transfer. Write to us at support@ulohotels.com for our account details.</li>
                   </p>
                   <h5>I am unable to make a reservation through the website. What should I do?  </h5>
                       <p>In cases where you are unable to complete the reservation through our website, please call us on 123456789 and we will be glad to assist you.</p>
                  <h5>What is the procedure after I make a booking through your website?</h5>
                     <p>After you are done booking, you will receive a confirmation mail with all relevant details: check in & checkout, directions & contact information of the hotel. In case you did not receive the mail, you can reach out to us on support@ulohotels.com or call us on  123456789 to confirm your booking.</p>
                     <h5>Are bookings made via travel websites like Yatra, makemytrip etc considered by you?</h5>
                     <p>Yes, bookings can be made via other online portals and travel websites. However, your booking and changes/cancellation is subject to their policies, terms and conditions. Ulo Hotels is not responsible for the above  or payments, discounts and other deals promised by other portals.</p>
                     <h5>Is there a separate policy for bulk bookings?</h5>
                     <p>You may call us on 9543-592-593 or leave your details and requests regarding bulk bookings and corporate booking with us on support@ulohotels.com.</p>      
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingThree">
        <h4 class="panel-title">
        <a  role="button"  data-toggle="collapse" data-target="#demo2" aria-expanded="true" aria-controls="collapseThree">
          MODIFICATION
        </a>
      </h4>
      </div>
       <div id="demo2" class="collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
            <h5>How can I cancel/amend my reservation?</h5> 
  <p>For all cancellations and amendments, please call us on 9543-592-593 if the booking is made via our website or through our customer care helpdesk. If your booking is made via other online travel websites like yatra.com etc, all modifications have to be made at the website where the booking was done.</p>
  <h5>How do I cancel my reservation?</h5>
   <p>Cancellation policies may vary depending upon the dates of your reservation. Please refer to the cancellation policy. You can cancel your booking online or call us at 123456789 for assistance.</p>
   <h5> What is the cancellation procedure?</h5>
    <p>For any cancellation or changes made up to 72 hours before the check-in date through Ulo hotels website, the entire booking amount will be refunded (within 7 working days) except the cancellation fee of Rs. 300/-. If any cancellation is made within 72 hours of check-in we will be unable to refund the amount paid.</p>
  <h5>What is the cancellation fee?</h5>
   <p>A service charge of Rs. 300 is charged to the guest on cancellation made up to 72 hours before the check-in date. The balance booking amount will be refunded within 7 working days.</p>
   <h5>Is it possible to modify my reservation?</h5>
<p>Yes, you can modify your bookings subjected to the availability of the rooms. A service fee of Rs. 300 is charged for the guest for the amendment.</p>
   <h5>What are your on-going offers and discounts?</h5>
    <p>We offer a flat 10% discount on all our bookings, year round.</p> 
        </div>
      </div>
    </div>
        <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFour">
        <h4 class="panel-title">
        <a role="button"  data-toggle="collapse" data-target="#demo3" aria-expanded="true" aria-controls="collapseFour">
          CHECK IN & CHECK OUT
        </a>
      </h4>
      </div>
      <div id="demo3" class="collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
        <h5>What is the standard check in & check out time</h5>
</p>Our check in time is 10 A.M and check out time is 9 A.M. For additional details, please contact the respective hotel directly or call our helpdesk at 9543-592-593</p>.
<h5>What documents are required at the time of check-in at the hotel? </h5>
</p>Booking confirmation E-mail/ SMS (with Booking ID) and a valid Photo ID (with address) is required for all the guests. Photo ID proofs accepted are Driver's License, Voters Card, Passport, Ration Card. 
Note: PAN Cards will not be accepted as valid Photo ID card.</p>
<h5>What if I am arriving earlier than 10 AM and want to leave post 9 AM? </h5>
<p>Unfortunately, early check-ins or  late check-outs are subjected to availability. However, we will certainly try to offer you an early check-in (free of charge) as long as we do not have other guests already staying in the booked room, or a late check out (free of charge), provided we do not have other guests arriving immediately</p>. 
<h5>Are complimentary breakfasts included? </h5>
<p>Yes, all Ulo properties offer complimentary breakfast. </p>
<h4> Other Amenities:- </h4>
<h5>What are the Other Amenities provided in Ulo Hotels? </h5>
<p>As our cherished guest, the following amenities are covered under our quality guarantee program: 
<li style="list-style-type:none">Clean, fresh linen</li>
<li style="list-style-type:none">Sterile, hygienic bath towels</li> 
<li style="list-style-type:none">Sanitary, fully equipped bathrooms</li>
<li style="list-style-type:none">Free Wi-fi* </li>
<li style="list-style-type:none">Complimentary bottled water</li> 
<li style="list-style-type:none">Complimentary breakfast</li>
<li style="list-style-type:none">Branded toiletries</li> 
*Not applicable for hill stations or areas where connectivity isn't available
</p>
<h5>What are the facilities available for food? </h5> 
<p>Breakfast is complimentary at all our hotels. For other meals and snacks, most of our hotels have room service available, wherein food is either prepared in house or can be facilitated from nearby restaurants on request.</p>
<h5>Is there any facility for laundry? </h5>
<p>Laundry facilities are available in all our hotels but it is charged at an extra price varying from hotel to hotel</p>. 
<h5>Does your hotel make arrangements for airport transportation? </h5>
<p>We can arrange the same at an extra cost in some cases, but we encourage you to try arranging transport yourself.</p>
<h5>Will I get help in transferring luggage and other bags to my room? </h5>
<p>Our hotel staff will be happy to help you with all your luggage transfer within the hotel premises. </p>
<h5>Is extra bed provided in all rooms? Is it included in room tariff? </h5>
<p>Extra mattress can be arranged  in all our hotels at an extra charge. This is subjected to the maximum permissible occupancy for the given room category.</p> 
<h5>Can I ask for a smoking room? </h5> 
<p>Most of our hotels have non-smoking rooms except for a few. For exact details, please drop  a mail or call us at 9543-592-593</p>.
<h5>What if I am dissatisfied with the hotel? </h5>
<p>We always do our best to ensure your stay with us is comfortable and safe. In the rare event, something isn't to your satisfaction, please contact us at 9543-592-593 or write to us at support@ulohotels.com to resolve the issue.</p> 

.
        </div>
      </div>
    </div>
            <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFour">
        <h4 class="panel-title">
        <a role="button"  data-toggle="collapse" data-target="#demo4" aria-expanded="true" aria-controls="collapseFour">
          OTHER PROVISIONS
        </a>
      </h4>
      </div>
      <div id="demo4" class="collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">

<h5>What are the Other Amenities provided in Ulo Hotels? </h5>
<p>As our cherished guest, the following amenities are covered under our quality guarantee program: 
<li style="list-style-type:none">Clean, fresh linen</li>
<li style="list-style-type:none">Sterile, hygienic bath towels</li> 
<li style="list-style-type:none">Sanitary, fully equipped bathrooms</li>
<li style="list-style-type:none">Free Wifi* </li>
<li style="list-style-type:none">Complimentary bottled water</li> 
<li style="list-style-type:none">Complimentary breakfast</li>
<li style="list-style-type:none">Branded toiletries</li> 
*Not applicable for hill stations or areas where connectivity isn't available
</p>
<h5>What are the facilities available for food? </h5> 
<p>Breakfast is complimentary at all our hotels. For other meals and snacks, most of our hotels have room service available, wherein food is either prepared in house or can be facilitated from nearby restaurants on request.</p>
<h5>Is there any facility for laundry? </h5>
<p>Laundry facilities are available in all our hotels but it is charged at an extra price varying from hotel to hotel</p>. 
<h5>Does your hotel make arrangements for airport transportation? </h5>
<p>We can arrange the same at an extra cost in some cases, but we encourage you to try arranging transport yourself.</p>
<h5>Will I get help in transferring luggage and other bags to my room? </h5>
<p>Our hotel staff will be happy to help you with all your luggage transfer within the hotel premises. </p>
<h5>Is extra bed provided in all rooms? Is it included in room tariff? </h5>
<p>Extra mattress can be arranged for in all our hotels at extra charge. This is subject to the maximum permissible occupancy for the given room category.</p> 
<h5>Can I ask for a smoking room? </h5> 
<p>Most of our hotels have non-smoking rooms except for a few. For exact details, please drop in a mail or call us at 9543-592-593</p>.
<h5>What if I am dissatisfied with the hotel? </h5>
<p>We always do our best to ensure your stay with us is comfortable and safe. In the rare event, something isn't to your satisfaction, please contact us at 9543-592-593 or write to us at support@ulohotels.com to resolve the issue.</p> 

.
        </div>
      </div>
    </div>
            <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFive">
        <h4 class="panel-title">
        <a role="button"  data-toggle="collapse" data-target="#demo5" aria-expanded="true" aria-controls="collapseFive">
        Partners
        </a>
      </h4>
      </div>
      <div id="demo5" class="collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
             <h5>Why should you work with us? </h5>
              <p>We believe excellent quality should be affordable. If you are associated with a hotel/property and believe in the same, we would love to partner with you. We will improve quality, increase your turnover and increase occupancy rate. </p>
                 <h5>How does Ulo work? </h5>
              <p>Ulo is designed to help budget hotel chains improve their quality level through various hands-on processes. We will pay a visit to your property and hire a General Manager to oversee the administration and quality control. We will review the day-to-day operations, improve the quality and ensure your occupancy rate is high and maintained through the year</p>
                 <h5> How does partnering with Ulo guarantee revenue?</h5>
              <p> We will set personalized targets for your property every month, which we will then achieve to meet the revenue standards.</p>
                 <h5>Will Ulo oversee the complete administration of the hotel? </h5>
              <p>Yes, you don't have to worry about the administration. We will appoint a General Manager for your hotel who will oversee the day-to-day operations of your property and report to us.  </p>
                 <h5>How will Ulo help us in other operations? </h5>
              <p>We will generate reports on an everyday basis in the Property Management System [PMS]. These reports will be  analyzed by our team to discover what is unique to your property, areas to improve, your performance and so on.  </p>
                 <h5>Can I access all the day-today reports? </h5>
              <p> Yes,  you can access all the reports through our PMS platform.</p>
                 <h5> How does Ulo handle payments?</h5>
              <p>We are directly linked to the OTA for our bookings. All payments from OTA will be released on the 15th of every calendar month to the hotel owners after deducting the agreed upon Ulo revenue. </p>
                    <h5> How will you be partnering with hotels?</h5>
              <p>We work on the Franchise model, where, we take control of the quality aspect of the property while you retain ownership of the property. We will help give your property a makeover, so to speak and increase occupancy rate. For more details please mail us at <a href="mailto:contracts@ulohotels.com">contracts@ulohotels.com.</a> </p>
                  
                    <h5> Will you perform an audit of the property prior to partnership?</h5>
              <p>By law, we are required to verify the property before making the contract. Once we are satisfied with the standards & all other criteria are met, we will draft the contract. </p>
               <h5>What are the documents required for partnering with Ulo? </h5>
               <p> We require the following for partnering with you:</p>
               <ul class="list-style">
               <li>a. Updated Property License </li>
               <li>b. Updated Property Tax</li>
               <li>c. If you are sub-lease owner, we require a no objection letter from the property owner.</li>
               </ul>
           <h5> Is there a minimum criteria required for partnering with Ulo?</h5>
          <p> Yes,  we require that your property has a minimum of at least 5 rooms.</p>
           <h5>Is there a minimum contract period for partnering with Ulo? </h5>
          <p>Yes, our contract has a minimum 3 years lock-in policy. If you are a lease owner, we require proof of your lease and an NOC (No Objection Certificate) from the owner of the property. </p>  
        </div>
      </div>
    </div>

  </div>
</div>
<script>
$(document).ready(function() {

  $(".toggle-accordion").on("click", function() {
    var accordionId = $(this).attr("accordion-id"),
      numPanelOpen = $(accordionId + ' .collapse.in').length;
    
    $(this).toggleClass("active");

    if (numPanelOpen == 0) {
      openAllPanels(accordionId);
    } else {
      closeAllPanels(accordionId);
    }
  })

  openAllPanels = function(aId) {
    console.log("setAllPanelOpen");
    $(aId + ' .panel-collapse:not(".in")').collapse('show');
  }
  closeAllPanels = function(aId) {
    console.log("setAllPanelclose");
    $(aId + ' .panel-collapse.in').collapse('hide');
  }
     
});
</script>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 

 <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>