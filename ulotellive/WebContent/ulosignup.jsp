<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
        <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
            <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
   <head>
      <!-- Meta Tag starts -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta name="author" content="">
      <title>ULO Hotels | A New Generation Quality Budget Hotel Chain.</title>
      <meta name="description" content="If you planning for a staycation & in a budget cut, reserve cheap of cozy hotels with special discounts & deals by online hotel booking from our Ulohotels."/>
      <meta name="keywords" content="Ulo Hotels, online hotel booking, budget hotel booking, hotels near me, online budget hotels">
      <meta name="robots" content="index, follow"/>
      <!-- Meta Tag Ends -->
      <!-- Schema.org markup for Google+ -->
      <meta itemprop="name" content="ULO Hotels | A New Generation Quality Budget Hotel Chain">
      <meta itemprop="description" content="If you planning for a staycation and in a budget cut, reserve cheap of cozy hotels with special discounts & deals by online hotel booking from our Ulohotels.">
      <meta itemprop="image" content="https://ulohotels.com/ulowebsite/images/logo.png">
      <!-- Twitter Card data -->
      <meta name="twitter:card" content="summary_large_image">
      <meta name="twitter:site" content="@Ulohotels">
      <meta name="twitter:title" content="ULO Hotels | A New Generation Quality Budget Hotel Chain">
      <meta name="twitter:description" content="If you planning for a staycation and in a budget cut, reserve cheap of cozy hotels with special discounts & deals by online hotel booking from our Ulohotels.">
      <meta name="twitter:creator" content="@Ulohotels">
      <meta name="twitter:image:src" content="https://ulohotels.com/ulowebsite/images/logo.png">
      <meta property="og:url" content="https://www.ulohotels.com/" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="ULO Hotels | A New Generation Quality Budget Hotel Chain" />
      <meta property="og:description" content="If you planning for a staycation and in a budget cut, reserve cheap of cozy hotels with special discounts & deals by online hotel booking from our Ulohotels." />
      <meta property="og:image" content="https://ulohotels.com/ulowebsite/images/logo.png" />
      <!-- Favicons -->
      <link rel="shortcut icon" href="ulowebsite/images/favicon.png" type="image/png" />
      <link rel="apple-touch-icon" href="ulowebsite/images/favicon.png" />
      <link rel="apple-touch-icon" sizes="72x72" href="ulowebsite/images/favicon.png" />
      <link rel="apple-touch-icon" sizes="114x114" href="ulowebsite/images/favicon.png" />
      <!-- Bootstrap -->
      <link href="ulowebsite/css/bootstrap.css" rel="stylesheet">
      <!-- Default Styles -->
      <link href="ulowebsite/css/style.css" rel="stylesheet">
      <!-- Custom Styles -->
      <link href="ulowebsite/css/custom.css" rel="stylesheet">
      <link href="ulowebsite/css/newstyle.css" rel="stylesheet">
      <link href="ulowebsite/css/jquery-ui.css" rel="stylesheet">
      <link href="ulowebsite/css/lightslider.css" rel="stylesheet">
      <link href="ulowebsite/css/dwarakesh.css" rel="stylesheet">
      <link href="ulowebsite/css/newstyle.css" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
      <script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M3RH7N5');</script>
<!-- End Google Tag Manager -->

      <style>
         .quickcontact {
         margin-top:10px;
         }
      </style>
   </head>

            <body ng-app="myApp" ng-controller="customersCtrl">
             
                <div id="wrapper">
       <div class="topbar">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7 logoindex">
                     <a href="index" style="padding:10px;">
                     <img src="ulowebsite/images/logo.png" alt="ulo-logo"></a>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2 headerbtn" style="border-right:1px dotted #DCDCDC;" >
                     <s:if test="#session.userId == null">
                        <a href="customer-login" class="btn btn-primary btn-xs  top-btn" style="padding:10px;margin-top:19px;margin-right:10px;margin-bottom:10px">login</a>
                        <a href="customer-signup" class="btn btn-primary btn-xs  top-btn" style="padding:10px;margin-top:19px;margin-bottom:10px">Signup</a>
                     </s:if>
                     <s:if test="#session.userId != null">
                        <div class="dropdown">
                           <button class="dropbtn" ><i class="fa fa-user"></i>&nbsp; <i class="icon-down-open-mini"></i></button>
                           <div class="dropdown-content">
                              <p>Hi! <%=session.getAttribute("userName") %></p>
                              <br>
                              <a href="ulouserprofile">Profile</a>
                              <a href="ulologout">Logout</a>
                           </div>
                        </div>
                     </s:if>
                  </div>
                  <div class="col-xs-12 col-sm-12 hidden-lg hidden-md">
                      <div class="col-xs-12 col-sm-12">
                        <span style="color:#114b70;font-size:14px;"><i class="fa fa-envelope" style="color:#88ba41;"></i>&nbsp;support@ulohotels.com  </span>
                     </div>
                     <div class="col-xs-12 col-sm-12">
                        <span style="color:#114b70;font-size:14px;"><i class="fa fa-phone" style="color:#88ba41;"></i>&nbsp; 9543 592 593 </span>
                     </div>
                  </div>
                  <div class="hidden-xs hidden-sm col-md-3 col-lg-3 ulocontact">
                     <span><i class="fa fa-envelope" style="color:#88ba41;"></i>&nbsp;support@ulohotels.com </span> <br>
                     <span><i class="fa fa-phone" style="color:#88ba41;"></i> &nbsp;+91-9543 592 593</span>
                  </div>
               </div>
            </div>
            <!-- end container -->
         </div>
         <!-- end topbar -->
                    <!-- end topbar -->
                    <section class="section fullscreen background parallax" style="background-image:url('ulowebsite/upload/hotel-bgnew.jpg');" data-img-width="1920" data-img-height="1133" data-diff="100">
                        <section class="clearfix section-bg">
                            <div class="col-md-7 col-md-offset-3">
                                <div class="form">
                                    <div id="signup">
                                        <h3>Welcome TO Ulo Hotels</h3>
                                           <s:if test="hasActionErrors()">
                                <div id="password_error_message" style="color:red;font-weight:normal;">
                                	<s:iterator value="actionErrors">
								  <s:property/>
								</s:iterator>
                                </div>
                                </s:if>
                                        <form action="ulosignup" method="post">

                                             <div class="field-wrap">
              <label>
                Name<span class="req">*</span>
              </label>
              <input type="text"  name ="userName"required autocomplete="off" />
            </div>
        
          

          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email"required  name ="emailId" autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input type="password"required  name ="password" autocomplete="off"/>
          </div>
          
          <input type="hidden" value="4" name="roleId">
          <input type="hidden" value="2" name="accessRightsId">
          
          <input type="submit" class="button button-block" value="Signup"/>

                                        </form>

                                    </div>

                                </div>
                                <!-- /form -->
                            </div>
                        </section>
                    </section>

                    <!-- start of footer -->
    <footer class="footer clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-3 col-xs-12">
                    <div class="widget">
                        <div class="widget-title">
                            <h3>GET IN TOUCH</h3>
                        </div><!-- end title -->
                        <div class="textwidget">
                          <p>
                                56,B2,Oyster Apartment, <br>
                                4th Avenue,19th Street,<br>
                                 Ashok Nagar,Chennai,<br>
                                Tamil Nadu 600083<br>
                                India<br>
                                <i class="fa fa-envelope"></i> reservations@ulohotels.com<br>
                                 <i class="fa fa-phone"></i> +91-9543 592 593</p>
                        </div><!-- end textwidget -->
                    </div><!-- end widget -->
                </div><!-- end col -->
                <div class="col-md-6 col-sm-5 col-xs-12">
                    <div class="widget ">
                        <div class="widget-title">
                            <h3>OUR USEFUL LINKS</h3>
                        </div><!-- end title -->
                        <div class="row">
                            <div class="col-md-6">
                        <div class="newsletterwidget">
                            <ul style="list-style-type:none;">
                               <!-- <li><a href="index.php">Home</a></li>-->
                                 
                             <li><a href="aboutulo">About</a></li>
                             
                                <li><a href="privacy">Privacy policy</a></li>
                               <li><a href="termsandconditions">Terms & conditions</a></li>
                                <li><a href="refundterms">Cancellation And Refund Policy</a></li>
                                <li><a href="team">Our Team</a></li>
                               
                            </ul>
                        </div>
                                </div>
                            <div class="col-md-6">
                        <div class="newsletterwidget">
                          <ul style="list-style-type:none;">
                             
                              <li><a href="partner-with-us">Partner With us</a></li>
                              <li><a href="ulo-faq">Frequently Asked Questions</a></li>
                              <li><a href="careers-ulo">Careers</a></li>
                              <li><a href="contactus">Contact</a></li>
                              
                             
                          </ul>
                        </div><!-- end newsletter widget -->
                            </div>
                    </div><!-- end widget -->
                        </div>
                </div><!-- end col -->
                  <div class="col-md-2 col-sm-3 col-xs-12 text-center">
                    <div class="">
                        <img src="ulowebsite/images/logo-ft.png" alt="">
                    </div><!-- end logo -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </footer><!-- end copyrights -->
                    <!-- end copyrights -->
                    <div class="copyrights clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <p>© 2017 Ulo Hotels Private Limited, All Rights Reserved.</p>
                                </div>
                                <!-- end col -->

                                <div class="col-md-6 text-right">
                                    <div class="social-footer">
                                        <a href="https://www.facebook.com/Ulohotels/" title="Facebook"><i class="icon-facebook"></i></a>
                                        <a href="https://twitter.com/ulohotels" title="Twitter"><i class="icon-twitter"></i></a>
                                        <a href="https://www.pinterest.com/ulohotels/" title="Pinterest"><i class="icon-pinterest"></i></a>
                                        <a href="https://goo.gl/ucr1n8" title="google-plus"><i class="icon-gplus"></i></a>
                                    </div>
                                    <!-- end social-footer -->
                                </div>
                                <!-- end col -->
                            </div>
                            <!-- end row -->
                        </div>
                        <!-- end container -->
                    </div>
                    <!-- end copyrights -->
                </div>
                <!-- end wrapper -->
            </body>
            <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
            <!-- jQuery UI 1.11.4 -->
            <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
            <script src="js1/ngprogress.js" type="text/javascript"></script>
            <script src="https://maps.google.com/maps/api/js?sensor=false"></script>
            <script src="ulowebsite/js/bootstrap.min.js"></script>
            <script src="ulowebsite/js/retina.js"></script>
            <script src="ulowebsite/js/sidebar.js"></script>
            <script src="ulowebsite/js/circle.js"></script>
            <script src="ulowebsite/js/progress.js"></script>
            <script src="ulowebsite/js/jquery.prettyPhoto.js"></script>
            <script src="ulowebsite/js/contact.js"></script>
            <script src="ulowebsite/js/parallax.js"></script>
            <script src="ulowebsite/js/owl.carousel.js"></script>
            <script src="ulowebsite/js/bootstrap-select.js"></script>
            <script src="ulowebsite/js/custom.js"></script>

            <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
            <script src="ulowebsite/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
            <script src="ulowebsite/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
            <script src="ulowebsite/js/revslider.js"></script>
            <script src="ulowebsite/js/lightslider.js"></script>

            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>

            
            <script>
                $(document).ready(function() {
                    $('.popup-text').magnificPopup({
                        type: 'inline',
                        preloader: false,
                        focus: '#name',
                        callbacks: {
                            beforeOpen: function() {
                                if ($(window).width() < 700) {
                                    this.st.focus = false;
                                } else {
                                    this.st.focus = '#name';
                                }
                            }
                        }
                    });
                });
            </script>
            <script>
                $('.form').find('input, textarea').on('keyup blur focus', function(e) {

                    var $this = $(this),
                        label = $this.prev('label');

                    if (e.type === 'keyup') {
                        if ($this.val() === '') {
                            label.removeClass('active highlight');
                        } else {
                            label.addClass('active highlight');
                        }
                    } else if (e.type === 'blur') {
                        if ($this.val() === '') {
                            label.removeClass('active highlight');
                        } else {
                            label.removeClass('highlight');
                        }
                    } else if (e.type === 'focus') {

                        if ($this.val() === '') {
                            label.removeClass('highlight');
                        } else if ($this.val() !== '') {
                            label.addClass('highlight');
                        }
                    }

                });
            </script>
            <style>
                .form {
                    background: rgba(0, 0, 0, 0.78);
                    padding: 40px;
                    max-width: 600px;
                    margin: 40px auto;
                    border-radius: 4px;
                    box-shadow: 0 4px 10px 4px #24333c;
                }
                
                .tab-group {
                    list-style: none;
                    padding: 0;
                    margin: 0 0 40px 0;
                }
                
                .tab-group:after {
                    content: "";
                    display: table;
                    clear: both;
                }
                
                .tab-group li a {
                    display: block;
                    text-decoration: none;
                    padding: 15px;
                    background: rgba(160, 179, 176, 0.25);
                    color: #a0b3b0;
                    font-size: 20px;
                    float: left;
                    width: 50%;
                    text-align: center;
                    cursor: pointer;
                    -webkit-transition: .5s ease;
                    transition: .5s ease;
                }
                
                .tab-group li a:hover {
                    background: #092f53;
                    color: #ffffff;
                }
                
                .tab-group .active a {
                    background: #88ba41;
                    color: #ffffff;
                }
                
                .tab-content > div:last-child {
                    display: none;
                }
                
                h3 {
                    text-align: center;
                    color: #ffffff;
                    font-weight: 300;
                    margin: 0 0 40px;
                }
                
                label {
                    position: absolute;
                    -webkit-transform: translateY(6px);
                    transform: translateY(6px);
                    left: 13px;
                    color: rgba(255, 255, 255, 0.5);
                    -webkit-transition: all 0.25s ease;
                    transition: all 0.25s ease;
                    -webkit-backface-visibility: hidden;
                    pointer-events: none;
                    font-size: 22px;
                }
                
                label .req {
                    margin: 2px;
                    color: #1ab188;
                }
                
                label.active {
                    -webkit-transform: translateY(50px);
                    transform: translateY(50px);
                    left: 2px;
                    font-size: 14px;
                }
                
                label.active .req {
                    opacity: 0;
                }
                
                label.highlight {
                    color: #ffffff;
                }
                
                input,
                textarea {
                    font-size: 22px;
                    display: block;
                    width: 100%;
                    height: 100%;
                    padding: 15px 10px;
                    background: none;
                    background-image: none;
                    border: 1px solid #a0b3b0;
                    color: #ffffff;
                    border-radius: 0;
                    -webkit-transition: border-color .25s ease, box-shadow .25s ease;
                    transition: border-color .25s ease, box-shadow .25s ease;
                }
                
                input:focus,
                textarea:focus {
                    outline: 0;
                    border-color: #1ab188;
                }
                
                textarea {
                    border: 2px solid #a0b3b0;
                    resize: vertical;
                }
                
                .field-wrap {
                    position: relative;
                    margin-bottom: 40px;
                }
                
                .top-row:after {
                    content: "";
                    display: table;
                    clear: both;
                }
                
                .top-row > div {
                    float: left;
                    width: 48%;
                    margin-right: 4%;
                }
                
                .top-row > div:last-child {
                    margin: 0;
                }
                
                .button {
                    border: 0;
                    outline: none;
                    border-radius: 0;
                    padding: 15px 0;
                    font-size: 15px;
                    font-weight: 600;
                    text-transform: uppercase;
                    letter-spacing: .1em;
                    background: #88ba41;
                    color: #ffffff;
                    -webkit-transition: all 0.5s ease;
                    transition: all 0.5s ease;
                    -webkit-appearance: none;
                }
                
                .button:hover,
                .button:focus {
                    background: #092f53;
                }
                
                .button-block {
                    display: block;
                    width: 100%;
                }
        

        .mar-b10 i{
    margin-bottom: 10px;
}
        .bold{
            font-weight:bold;
        }  
        
          </style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M3RH7N5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
            </html>