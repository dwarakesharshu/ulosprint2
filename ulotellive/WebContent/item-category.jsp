<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Meal-Plan
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Meal-Plan</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Plan</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										
										<th>Category Name</th>
										<th>Category Code</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
									<tr ng-repeat="c in categories">
									
										<td>{{c.categoryName}}</td>
										<td>{{c.categoryCode}}</td>
											<td>
											<a href="#editmodal" ng-click="getCategory(c.DT_RowId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
											
										</td>
											<td>
										
											<a href="#" ng-click="deleteCategory(c.DT_RowId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
										</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary" href="#addmodal" ng-click="enableTask(x.DT_RowId)"  data-toggle="modal" >Add Category</a>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Meal Plan</h4>
					</div>
					 <form name="myform">
					<div class="modal-body" >
					<div id="message"></div>
						<div class="form-group">
							<label>Category Name</label>
							<input type="text" class="form-control"  name="categoryName"  id="categoryName"  placeholder="Name" ng-model='categoryName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="myform.categoryName.$error.pattern">Not a valid Name!</span>
						     
						</div>
						<div class="form-group">
							<label>Category Code</label>
							<input type="text" class="form-control"  name="categoryCode"  id="categoryCode"  placeholder="Code" ng-model='categoryCode' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="myform.categoryCode.$error.pattern">Not a valid number!</span>
						</div>
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="myform.$valid && addCategory()" ng-disabled="myform.$invalid" class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Category</h4>
					</div>
					<div class="modal-body" ng-repeat="cat in category">
					<div id="editmessage" ></div>
					    <input type="hidden" name ="editCategoryId" id ="editCategoryId" value="{{cat.DT_RowId}}">
						<div class="form-group">
							<label>Category Name</label>
							<input type="text" class="form-control" name="editCategoryName"  value="{{cat.categoryName}}" id="editCategoryName"  placeholder="Name">
						</div>
						<div class="form-group">
							<label>Category Code</label>
							<input type="text" class="form-control" name="editCategoryCode"  value="{{cat.categoryCode}}" id="editCategoryCode"  placeholder="Code">
						</div>
						
					</div>
					<div class="modal-footer">
	                    
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="editCategory()" class="btn btn-primary">Save</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		<div class="modal" id="deletemodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="deletebtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Delete Message</h4>
					</div>
					<div class="modal-body">
					 <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i>Your Item Category has been deleted Sucessfully </h4>
                  
                 </div>
					    
						
					</div>
					<div class="modal-footer">
	                    
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
         
			

			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			$scope.getCategories = function() {

				var url = "get-categories";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.categories = response.data;
		
				});
			};
			
			
			$scope.addCategory = function(){
				//alert($('#taskId').val());
				
				var fdata = "categoryCode=" + $('#categoryCode').val()
				+ "&categoryName=" + $('#categoryName').val();
				
				
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-category'
						}).then(function successCallback(response) {
							
							$scope.getCategories();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
				            $('#message').html(alert);
							$timeout(function(){
					      	$('#btnclose').click();
					        }, 2000);
							
							//$('#btnclose').click();
				}, function errorCallback(response) {
					
				  
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			$scope.editCategory = function(){
				
				
				var fdata = "categoryCode=" + $('#editCategoryCode').val()
				+ "&categoryName=" + $('#editCategoryName').val() 
				+"&propertyItemCategoryId="+ $('#editCategoryId').val(); 
				
				
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-category'
						}).then(function successCallback(response) {
							
							$scope.getCategories();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Updated Succesfully.</div>';
				            $('#editmessage').html(alert);
							$timeout(function(){
					      	$('#editbtnclose').click();
					        }, 2000);
				}, function errorCallback(response) {
					
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
            
            
			
			
            
			$scope.getCategory = function(rowid) {
				
				
				var url = "get-category?propertyItemCategoryId="+rowid;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.category = response.data;
		
				});
				
			};
			
            $scope.deleteCategory = function(rowid) {
				
				
				var url = "delete-category?propertyItemCategoryId="+rowid;
				$http.get(url).success(function(response) {
				    //console.log(response);
				    $scope.getCategories();
				    $('#deletemodal').modal('toggle');
				    $timeout(function(){
				      	$('#deletebtnclose').click();
				    }, 2000);
					
		
				});
				
			};
			
			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
            $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
				
		    $scope.getPropertyList();
			
			$scope.getCategories();
			
			
			//$scope.unread();
			//
			
			
		});


		
		   
		        

		
	</script>
			<script>
		  $(function () {
		    $("#example1").DataTable();
		    $('#example2').DataTable({
		      "paging": true,
		      "lengthChange": false,
		      "searching": false,
		      "ordering": true,
		      "info": true,
		      "autoWidth": false
		    });
		  });
		</script>
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       