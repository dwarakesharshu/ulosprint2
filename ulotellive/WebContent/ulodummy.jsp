<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div id="wrapper">
    <section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index">Home</a></li>
                        <li>Payment</li>
                    </ul>
                    <h3>PAYMENT</h3>
                </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->

    <section class="section clearfix pad-bot">
        <div class="container">
            <div class="row">
                <div id="fullwidth" class="col-sm-12">

                    <!-- START CONTENT -->
                    <div class="row">
                        <div id="content" class="col-md-12 col-sm-12 col-xs-12">

                           <!--  <div class="price-range text-center clearfix row">
                                <div class="col-md-12">
                                    <img src="ulowebsite/images/progress.png" class="img-responsive" alt="">
                                </div>
                            </div> -->

                          

                            <div class="price-details">
                                <div class="hotel-title">
                                    <h5>YOUR BOOKING DETAILS <p style="color:red;font-size:12px;text-transform:none;">Please Click "ADD ROOM" To Add More Rooms </p></h5>
                                    
                                   <textarea name="accommodationTypes" style="display:none" id ="accommodationTypes"><%= session.getAttribute("accommodationTypes") %></textarea>
                                <!--  <input type="text"  size ="100" value = "<%= session.getAttribute("accommodationTypes") %>" id="accommodationTypes" name="accommodationTypes">-->
                                </div>
                                <input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
                              <div class="row">
                                    <div class="col-md-6">
                                        <div class="row" ng-repeat="av in availablities track by $index">
                                            <div class="col-sm-4">
                                             <h6 class="mar0">{{property[0].propertyName}}</h6>
                                              <span class="rating clearfix">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </span><!-- end rating -->
                                              <img src="accommodation-picture?propertyAccommodationId={{av.accommodationId}}" alt="" class="img-responsive img-thumbnail">
                                            </div><!-- end col -->
                                            <div class="col-sm-6 mar-top-10">
                                            <!-- shiva add av.tax -->
                                                <button class="btn btn-primary"   ng-hide="av.available == 0 "   ng-click="addRow(av.accommodationId,av.accommodationType,av.baseAmount,av.arrivalDate,av.departureDate,av.rooms,av.noOfAdults,av.noOfChild,av.minOccupancy,av.extraAdult,av.extraChild,av.available,$index,av.tax)">Add Room</button>
                                               
                                     <a type="button" class= "btn btn-danger btn-style" ng-show="av.available == 0 ">sold out</a>
                                                 
                                                <ul class="clearfix" >
                                                    <li><span class="bold">ROOM: </span>  {{av.accommodationType}}</li>
                                                    <li><span class="bold">CHECKIN:</span>  <%=session.getAttribute("checkIn") %></li>
                                                    <li><span class="bold">CHECKOUT:</span> <%=session.getAttribute("checkOut") %></li>
                                                    <li><span class="bold">STAY:</span>  Max {{av.maxOccupancy}} Person(s)</li>
                                                
                                                </ul><br>
                                           
                                            </div><!-- end col -->
                                        </div><!-- end row -->
                                    </div><!-- end col -->

                                    <div class="col-md-6">
                                        <div class="bookprice table-responsive">
                                        <h6 class="mar0 text-center">Booking Summary</h6>
                                            <table class="table " id="finalData" >
									
									<tr>
									    <th><strong>Type</strong></th>
										<th><strong>Rooms</strong></th>
										<th><strong>Adult</strong></th>
										<th><strong>Child</strong></th>
										<th><strong>Tax</strong></th>
										<th><strong>Fare</strong></th>
										<th><strong>Total</strong></th>
										
									</tr>
									<tr id="tr-{{$index}}" ng-repeat="rs in roomsSelected track by $index">
										<td><strong>{{rs.accommodationType}}</strong></td>
										<td class="text-center">{{rs.rooms}}</td>
										<!-- shiva update both ng-change rs.baseAmount,rs.diffDays,rs.rooms-->
									   <td><select  ng-model="ad.value" id="adultsAllow" ng-change="updateExtraAdult(ad.value,ch.value,$index,rs.minOccu,rs.baseAmount,rs.diffDays,rs.rooms)"><option ng-repeat="ad in range(1,rs.adultsAllow)" value="{{ad}}">{{ad}}</option></select></td>
									   <td><select ng-init="ch.value = 0" ng-model="ch.value" id="childAllow" ng-change="updateExtraAdult(ad.value,ch.value,$index,rs.minOccu,rs.baseAmount,rs.diffDays,rs.rooms); childAge($index,ch.value);" ><option ng-repeat="ch in range(0,rs.childAllow)" value="{{ch}}">{{ch}}</option></select></td>
									   <!--  <td id="output{{$index}}"></td>-->
									   
									    <!-- <td><button class="btn btn-xs btn-primary" ng-click ="removeRoom($index)">Remove</button></td> -->
									 <td>{{rs.tax}}</td>
									 <td>{{rs.total}}</td>
									 <td>{{rs.total + rs.tax}}</td>
									 <td><button class="btn btn-xs btn-primary" ng-click ="removeRoom($index,rs.indx,rs.available)"><i class="fa fa-times" aria-hidden="true"></i>
									 </button></td>
									</tr>
									<tr>
									<!-- <td></td>
									<td></td> -->
									<td style="border:none!important"></td>
									<td style="border:none!important"></td>
									<td style="border:none!important"></td>
									<td style="border:none!important"></td>
									<td style="border:none!important"></td>
									<td style="border:none!important"></td>
									<!--  <td class="danger">Sub Total:</td>-->
								 <td class=""  id="totalAmount">{{ getTotal() }} </td>
								<!--   <td class="danger" id="totalRooms">{{ getRoomsTotal() }} </td>
								 <td><input type="hidden" value="0" id="tax"></td>
								 <td><input type="hidden" value="0" id="securityDeposit"></td>-->
								 
								 <input type="hidden" id="bookingId" value="<s:property value="#session.bookingId" />" name="">
									</tr>
									 
								</table>
							
								  
								 
                                        </div><!-- end bookprice -->
                             
                                    </div><!-- end col -->
                                </div><!-- end row -->
                                <div class="col-md-6"></div>
                                        
                                         
					
                                            	<div class="col-sm-6 col-md-6"><br>
							<label for="coupon" class="col-lg-5"><h6>Do you have Coupon code?</h6> </label>
					<input type="text"  class="col-md-3"  name="coupon" id ="coupon" value="" placeholder="Enter Coupon"></input>
					<button class="btn btn-primary  col-lg-3 pull-right"   ng-hide=""   ng-click="applyDiscount()">Apply</button>
						</div>
							
                              
                              
                            </div><!-- end pricing-details -->
                              
                            <div class="clearfix"></div>
                            <br>
                            <hr>
                            <br>
                                       
                                
                      
                            
                                <div class="row">
                                    <div class="col-sm-10">
                                       

                                       <div class="row">
					<div id="guest">
					<div class="row">
					<div class="col-md-12">
					 <s:if test="#session.userId == null">
					   <h6 class="text-center">Already Have An Ulo Account ? Click Here To  <button class="btn btn-primary "  ng-click="showLogin();">Login</button> </h6>
					 
					  </s:if>
					</div>
					
					</div><br>
					 <div class="hotel-title">
                                            <h5 class="text-center">GUEST INFORMATION</h5>
                    
                                        </div>
<form id="infoform" name="myformval">

                      <input type="hidden" class="form-control" name="userId" id="userId" value="<s:property value="#session.userId"/>" placeholder="usrid">

                       <div class="form-group col-sm-6 col-lg-6">

                            <label for="firstName">Name <span style="color:red">*</span></label>

                            <input type="text" class="form-control" name="firstName" id="firstName" ng-model="firstName" value="{{user[0].o_username}}" placeholder="Name" ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">

                          <span ng-show="myformval.firstName.$error.pattern" style="color:red">Enter Your Valid Name </span>

                        </div>

                            

                        <div class="form-group col-sm-6 col-lg-6">

                            <label for="guestEmail">E-mail <span style="color:red">*</span></label>

                            <input type="email" class="form-control"  name="guestEmail"  id="guestEmail" ng-model="guestEmail" value="{{user[0].o_email}}" placeholder="Enter Your Email"  ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">

                        

                             <span ng-show="myformval.guestEmail.$error.pattern" style="color:red">Enter The Valid Email ID!</span>

                        </div>

                        <div class="form-group col-sm-6 col-lg-6">

                            <label for="mobile_phone">Mobile Phone <span style="color:red">*</span></label>

                            <input type="text" class="form-control"  name="mobilePhone"  id="mobilePhone"  ng-model="mobilePhone"  value="{{user[0].o_phone }}" placeholder="Enter The Mobile Number"  ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="10">

                           <span ng-show="myformval.mobilePhone.$error.pattern" style="color:red">Enter the Valid Mobile Number!</span>

                        </div>

                        <div class="form-group col-sm-6 col-lg-6">

                            <label for="landlinePhone">Alternate Number <span style="color:red">*</span></label>

                            <input type="text" class="form-control"  name="landlinePhone"  id="landlinePhone" ng-model="landlinePhone"  placeholder="Enter The Alternate Number "  ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="10">

                            <span ng-show="myformval.landlinePhone.$error.pattern" style="color:red">Enter the Valid Mobile Number!</span>

                        </div>

                              <div class="col-md-12 text-center">

                                       <!-- <p>By selecting to complete this booking I acknowledge that I have read and accept <a href="termsandconditions">terms & conditions</a>, and <a href="privacy">privacy policy</a>.</p> -->

                                <!-- <p>All Fields Are Mandatory</p> -->

                                       <!-- ng-click="confirmBooking()" -->

                                       <button type="submit" ng-disabled="myformval.$invalid" ng-click="booking()"  class="btn btn-primary btn-normal">PAY AND BOOK NOW</button>

                                       

                                   </div>        

                        </div>            

                         </form><!-- end form -->
							
				
						</div>
						
						<div class="row">
						<div class="form">
   <div id="login">
   <div class="row">
					  <button class="btn btn-primary"  ng-click="showGuest();">Book As A Guest</button> 
					</div><br>
      <h3>Welcome Back!</h3>
      <s:form id="myForm" action="ulodetailslogin" method="post" theme="simple" >
      
      
							
	 <div class="form-group col-sm-6 col-lg-6">
							<label for="guestEmail">E-mail</label>
							<input type="email" class="form-control"  name="username" id="username"  placeholder="Enter Your Email">
						    
						     
	 </div>
	 
	 <div class="form-group col-sm-6 col-lg-6">
							<label for="firstName">Password</label>
							<input type="password" class="form-control" name="password"  id="password"  placeholder="First Name"   maxlength="50">
						   
	  </div>
	  
         <s:if test="hasActionErrors()">
            <div id="password_error_message" style="color:red;font-weight:normal;">
               <s:iterator value="actionErrors">
                  <s:property/>
               </s:iterator>
            </div>
         </s:if>
         <s:if test="%{#session['countcap']>3}">
            <div id="captchashow">
               <table>
                  <tr>
                     <td>
                        <div class="g-recaptcha" data-sitekey="6LdrlSAUAAAAANGKJe_wASqoEv3fjw-94WdlPAPs"></div>
                     </td>
                  </tr>
                  <tr>
                     <td>
                       
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <s:property value="#sessionMap.count" />
                     </td>
                  </tr>
               </table>
            </div>
         </s:if>
         <p class="forgot" style=""><a href="forget">Forgot Password?</a></p>
         <input type="submit" class="btn btn-primary" value="LogIn"/>
      </s:form>
   </div>
</div>
<!-- /form -->
						</div>
                                    </div><!-- end col -->

                           
                                        </div><!-- end row -->

                                        <div class="clearfix"></div>

                                        <div class="row">
                                       
                                            </div><!-- end col -->
                                        </div><!-- end row -->

                                        <div class="clearfix"></div>
                                    </div><!-- end col 6 -->
                                </div><!-- end row -->
                                <br>
                                <hr>
                               
                          <!--       <div class="row text-center">
                                    <div class="col-md-12">
                                        <p>By selecting to complete this booking I acknowledge that I have read and accept <a href="termsandconditions">terms & conditions</a>, and <a href="privacy">privacy policy</a>.</p>
                                        <br>
                                        ng-click="confirmBooking()"
                                        <a href="#" ng-click="booking()"  class="btn btn-primary btn-normal btn-lg">PAY AND BOOK NOW</a>
                                        
                                    </div>
                                </div> -->
                           
                        </div><!-- end col -->
                    </div><!-- end row -->
                    <!-- END CONTENT -->

                </div><!-- end fullwidth -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section>
</div>
 <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

<script>

		var app = angular.module('myApp',['ngProgress']);
		app.controller('customersCtrl',function($scope, $http,$timeout,ngProgressFactory,$location) {
         
			 var propertyId = $('#propertyId').val(); 	
			 
			 $('#login').hide();
			 
			 $scope.showLogin = function(){
				
				 $('#guest').hide();
				 $('#login').show();
				 //alert("dsfd");
				 
				 };
			 
			 $scope.showGuest = function(){
						
					 $('#login').hide();
					 $('#guest').show();
					 //alert("dsfd");
					 
					 };
				 
			
			  $scope.getFrontAvailability = function(){
					
				  $scope.typeSelected = [];
				  $scope.accommodations = [];
				  var typeSeleted = new Array();
				  
					if($('#datepicker').val() == ""){
					   $('#datepicker').focus();
					}
					
					if($('#datepicker1').val() == ""){
						
						$('#datepicker1').focus();
						
					}
					//alert($('#propertyItemCategoryId').val());
					//$scope.accommodations.remove;
					
					var fdata = "&arrivalDate=" + $('#datepicker').val()
					+ "&departureDate=" + $('#datepicker1').val()
					+ "&propertyId=" + $('#propertyId').val()
		             
					
					
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'get-availability'
							}).success(function(response) {
								
								$scope.accommodations = response.data;
							    //console.log($scope.accommodations[0].accommodationId);
							    var id = parseInt($scope.accommodations[0].accommodationId);
							    var available = parseFloat($scope.accommodations[0].available);
							    console.log(id);
							    var newType = {'accommodationId':id ,'available':available};
							    $scope.typeSelected.push(newType);
					        	console.log();
							});

				};
	       	
	       	   $scope.manageTypes = function(id){
	    	  
	    	   if(document.getElementById(id).checked){
	    		   
	    		   var accId = parseInt($scope.accommodations[id].accommodationId);
				   var available = parseInt($scope.accommodations[id].available);
				   
	    		  // var chkval = document.getElementById(id).value;
	    		   var newType = {'accommodationId':accId ,'available':available};
				    $scope.typeSelected.push(newType);
	    	       
	    	   } 
	    	   else{
	    		   
	    		   $scope.typeSelected.splice(id);
	    		   //var chkval = document.getElementById(id).value;
		    	   //alert(chkval);
	    	   }
	    	 
	    		   
	    	};
	        	
	    	$scope.getType = function(){
                
	             var text = $('#accommodationTypes').val(); 
	             
	             // var text = '{"types":' + JSON.stringify($scope.typeSelected) + '}';
	              var data = JSON.parse(text);
	                
	                 
	                
	                
	                $http(
	                        {
	                            method : 'POST',
	                            data : data,
	                            //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
	                            dataType: 'json',
	                            headers : {
	                                 'Content-Type' : 'application/json; charset=utf-8'

	                            },
	                            
	                            //url : 'bookingdetails'
	                            url : 'get-type-selected'
	                            
	                        }).success(function(response) {
	                            
	                             
	                            
	                            $scope.availablities = response.data;
	                            
	                            //alert(dt.baseAmount);
	                            var indx = 0;
	                            //$scope.roomsSelected = [];
	                            //addRow(av.accommodationId,av.accommodationType,av.baseAmount,av.arrivalDate,av.departureDate,av.rooms,av.noOfAdults,av.noOfChild,av.minOccupancy,av.extraAdult,av.extraChild,av.available,$index,av.tax)      
	                         
	                            for (var i = 0; i < $scope.availablities.length; i++) 
	                               {
	                               var dt = $scope.availablities[i];
	                               $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,dt.rooms,dt.noOfAdults,dt.noOfChild,dt.minOccupancy,dt.extraAdult,dt.extraChild,dt.available,i,dt.tax);    
	                               }  
	                            //console.log($scope.booked[0].firstName);
	                            
	                            
	                        });
	              

	            };
	            
			$scope.booking = function()
			{
				var options = {
					   // "key": "rzp_live_XK8Wemj85ETgaW",
					   "key": "rzp_live_XK8Wemj85ETgaW",
					    "amount": parseInt(document.getElementById ("totalAmount").innerText) * 100, // 2000 paise = INR 20
					    "name": "Ulohotels.com",
					    "description": "Welcom to Ulohotels",
					    //"image": "/your_logo.png",
					    "handler": function (response){
					        //alert(response.razorpay_payment_id);
					         $scope.confirmBooking();
					    },
					    "prefill": {
					        "name": $('#firstName').val(),
					        "email": $('#guestEmail').val(),
					        "contact":  $('#mobilePhone').val()
					    },
					    "notes": {
					        "address": $('#mobilePhone').val()
					    },
					    "theme": {
					        "color": "#89b717"
					    }
					};
					var rzp1 = new Razorpay(options);
					rzp1.open();
					e.preventDefault();
			};
			
			 $scope.confirmBooking = function(){
					
				    //document.getElementById('rzp-button1').click(); 
					
					var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
					var data = JSON.parse(text);
					//alert(text);
					
					$http(
							{
								method : 'POST',
								data : data,
								//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
								dataType: 'json',
								headers : {
									 'Content-Type' : 'application/json; charset=utf-8'

								},
								url : 'add-booking'
								
								
							}).then(function successCallback(response) {
								
								
								var gdata = "firstName=" + $('#firstName').val()
								+ "&lastName=" + $('#lastName').val()
								+ "&emailId=" + $('#guestEmail').val()
								+ "&phone=" + $('#mobilePhone').val()
								+ "&landline=" + $('#landlinePhone').val()
								+ "&address1=" + $('#firstAddress').val()
								+ "&address1=" + $('#secondAddress').val()
								+ "&city=" + $('#guestCity').val()
								+ "&zipCode=" + $('#guestZipCode').val()
								+ "&countryCode=" + $('#countryCode').val()
								+ "&stateCode=" + $('#stateCode').val()
								
							//alert(gdata);
								$http(
							{
								method : 'POST',
								data : gdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'add-guest'
								
							}).then(function successCallback(response) {
								
							
							    console.log($scope.roomsSelected);
							   
								var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
								//alert(text);
								var data = JSON.parse(text);
								//var data = $scope.roomsSelected;
								
								
								
								$http(
							    {
								method : 'POST',
								data : data,
								//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
								dataType: 'json',
								headers : {
									 'Content-Type' : 'application/json; charset=utf-8'
									//'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'add-booking-details'
								//url : 'jsonTest'
							   }).then(function successCallback(response) {
								
								
							 // alert("booking has been done sucessfully");	
							     $scope.booked = response.data;
							     //alert(JSON.stringify($scope.booked));
							     console.log($scope.booked.data[0].bookingId);
							     window.location = '/paymentsuccess'; 
							    
							   }, function errorCallback(response) {
								
								  
								
							});
								
							}, function errorCallback(response) {
								
								  
								
							});
							
							}, function errorCallback(response) {
						
					  
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});

				};
				
				
				$scope.range = function(min, max, step){
				    step = step || 1;
				    var input = [];
				    for (var i = min; i <= max; i += step) input.push(i);
				    return input;
				 };

				 $scope.getStates = function() {
		             
		             var url = "get-states";
		             $http.get(url).success(function(response) {
		                 //console.log(response);
		                 //alert(JSON.stringify(response.data));
		                 $scope.states = response.data;
		             
		             });
		         };
		         
		         $scope.getCountries = function() {
		             
		             var url = "get-countries";
		             $http.get(url).success(function(response) {
		                 //console.log(response);
		                 //alert(JSON.stringify(response.data));
		                 $scope.countries = response.data;
		             
		             });
		         };
		        
		         $scope.roomsSelected = [];
		         
		         $scope.addRow = function(id,type,baseAmount,arrival, departure,rooms,adultsAllow,childAllow,minOccu,extraAdult,extraChild,available,indx,tax){	
		        	
		        	 if(available == 0){
		             	
		        		 
		        		     
		             	// alert("Rooms is not available");
		             	  //$("#submitBtn").attr("disabled","disabled");
		             	    //btn.disabled = true;
		        			 console.log("Disable");
		                 }
		                 else{
		        
		        	 
		         var type = type.replace(/\s/g,'');
		         var id = parseInt(id);
		         var minOccu = parseInt(minOccu);
		         var extraAdult = parseInt(extraAdult);
		         var extraChild = parseInt(extraChild);
		         
		         var adultsAllow = parseInt(adultsAllow);
		         var childAllow = parseInt(childAllow);
		        
		         var amount = parseFloat(baseAmount);
		         var rooms = parseInt(rooms);
		         
		         var date1 = new Date(arrival);
		         var date2 = new Date(departure);
		         var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
		         var randomNo = Math.random();
		         //shiva add var taxes
		         if(amount < 1000){
                     //alert("dsfsd");
                     var percent = 0;
                     var taxes = 0/100*amount* diffDays;
                     //alert(taxes);
                 }
                 else if(amount >= 1000 && amount <= 2500){
                     //alert("dsfsd");
                     var percent = 12;
                     //var taxes = 12/100*amount* diffDays;
                     var taxes = Math.round(percent / 100 * amount) * diffDays;
                    // alert(taxes);
                 }
                 else if(amount >= 2500 && amount <= 7000){
                     //alert("dsfsd");
                     var percent = 18;
                     var taxes = Math.round(percent / 100 * amount) * diffDays;
                     //alert(taxes);
                 }
                 else if(amount >= 7000 && amount <= 7000000){
                     //alert("dsfsd");
                     var percent = 28;
                     //var taxes = Math.round(percent)/100 *amount* diffDays;
                    var taxes = Math.round(percent / 100 * amount) * diffDays;
                     //alert( taxes);
                 }
		         //alert(diffDays);
		         var total = (diffDays*rooms*amount);
		         //shiva update newData diffDays only
		         var newData = {'arrival':arrival,'indx':indx,'available':available,'departure':departure,'accommodationId':id,'sourceId':1,'accommodationType':type, 'rooms': rooms,'diffDays':diffDays, 'baseAmount':amount, 'total':total ,'adultsAllow':adultsAllow,'childAllow':childAllow,'minOccu':minOccu,'extraAdult':extraAdult,'extraChild':extraChild,'adultsCount':0,'childCount':0,'refund':0,'statusId':2,'tax':taxes,'randomNo':randomNo};
		        	   $scope.roomsSelected.push(newData);
		        	   console.log($scope.roomsSelected);
		        	   $scope.availablities[indx].available = $scope.availablities[indx].available - 1 ;
		                 }
		        	};
		        	
		          $scope.getTotal = function(){
		        	    var total = 0;
		        	    var tax = 0;
		        	    for(var i = 0; i < $scope.roomsSelected.length; i++){
		        	        var accommodation = $scope.roomsSelected[i];
		        	        tax += (accommodation.tax);
		        	        total += (accommodation.total);
		        	    }
		        	    var all = (total+tax);
		        	    return all;
		        	} 	
		          
		          $scope.getRoomsTotal = function(){
		      	    var totalRooms = 0;
		      	    for(var i = 0; i < $scope.roomsSelected.length; i++){
		      	        var roomsCount = $scope.roomsSelected[i];
		      	        totalRooms += parseInt(roomsCount.rooms);
		      	    }
		      	    return totalRooms;
		      	  } 	
				
		          $scope.removeRoom = function(index,indx,available){
		        	  // alert(indx);
		        	  $scope.availablities[indx].available = parseInt($scope.availablities[indx].available) + 1; 
		         	 
		              $scope.roomsSelected.splice(index,1); 
		              	 
		        	  
		        	  
		            } 	
		          
				 $scope.getPropertyList = function() {
					 
			        	var userId = $('#adminId').val();
			 			var url = "get-user-properties?userId="+userId;
			 			$http.get(url).success(function(response) {
			 			    
			 				$scope.props = response.data;
			 	
			 			});
			 		};
			 		
			 	   $scope.applyDiscount = function() {
						 
				        	var discountName = $('#coupon').val();
				 			var url = "apply-discount?discountName="+discountName;
				 			$http.get(url).success(function(response) {
				 			    
				 				$scope.discount = response.data;
				 				if($scope.discount == ""){
				 				alert("coupon code invalid");
				 				}
				 				
				 				else{
				 					
				 					
				 					var percent = $scope.discount[0].discountPercentage;
				 					//alert(percent);
				 					var discountable = Math.round( percent )/100*$scope.getTotal();
				 					var  a = $scope.getTotal();
				 			        a= ($scope.getTotal()-discountable); 
				 					//console.log($scope.getTotal()-discountable);
				 					document.getElementById ("totalAmount").innerText = $scope.getTotal()-discountable;
				 					//alert(discounty);
				 							
				 					//alert($scope.getTotal());
				 				
				 				}
				 	
				 			});
				 		};
			 		
		        $scope.change = function() {
		 	   
			        //alert($scope.id);
			        
			        var propertyId = $scope.id;	
		 	        var url = "change-user-property?propertyId="+propertyId;
		 			$http.get(url).success(function(response) {
		 				
		 				 window.location = '/dashboard'; 
		 				//$scope.change = response.data;
		 	
		 			});
				       
			 		};
			 			
			 	$scope.update = function(rValue,indx) {
			 		
			 		
			 		$scope.rValue = rValue;
			 		$scope.roomsSelected[indx].rooms = rValue;
			 		$scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rValue;
			 		
			 		$scope.roomsSelected[indx].adultsAllow  = $scope.availablities[indx].noOfAdults * rValue;
			 		$scope.roomsSelected[indx].minOccu  = $scope.availablities[indx].minOccupancy * rValue;
			 		$scope.roomsSelected[indx].childAllow  = $scope.availablities[indx].noOfChild * rValue;
			 		
			 		   // console.log($scope.item.n, $scope.item.name)
			    }
			 	// shiva update start	
			  $scope.updateExtraAdult = function(adults,child,indx,minOccu,baseAmount,diffDays,rooms) {
				  
				  
				 // alert("min"+minOccu);
				 // alert("base"+baseAmount);
				//	alert("days"+diffDays);
							 //alert("room"+rooms);
							 var total = (diffDays*rooms*baseAmount);
				  adults = parseInt(adults);
				  child = parseInt(child);
				  minOccu = parseInt(minOccu);
				  var totalOccu = parseInt(adults+child);
				  var roomAmount = parseFloat(total);
				  $scope.roomsSelected[indx].adultsCount = adults;
				  $scope.roomsSelected[indx].childCount = child;
				  
				          if(totalOccu>minOccu){
					  
					      if(adults<=minOccu){
						  
						  var childLeft = totalOccu-minOccu;
						  
						  // alert("extraChild "+childLeft); 
						  
						  $scope.roomsSelected[indx].total =  (diffDays * childLeft * $scope.roomsSelected[indx].extraChild) + roomAmount;
						  // var bdprice1 =  (adultsLeft * $scope.roomsSelected[indx].extraAdult);
						 
						  }
						  
						  if(adults > minOccu){
					      
					      $scope.roomsSelected[indx].total = roomAmount;
					     
					      var adultsLeft = adults-minOccu; 
					      //alert("extraAdult "+adultsLeft);
					      
					      $scope.roomsSelected[indx].total = (diffDays * adultsLeft * $scope.roomsSelected[indx].extraAdult) + roomAmount;
					      
					      var left =  totalOccu-minOccu;
					      //alert(left);
						  var extraChd =   left-adultsLeft;
						  
						  $scope.roomsSelected[indx].total += (diffDays * extraChd * $scope.roomsSelected[indx].extraChild);
						  
						  //alert("extraChild "+ extraChd);
						  
						  }  
					  
				  }
				  
			
				  
				  else{
				  
				  $scope.roomsSelected[indx].total = roomAmount * $scope.roomsSelected[indx].rooms ;
					  
				  }
				       
				 }
			  
			    //shiva update end
		        $scope.childAge = function(indx,chValue) {
				
		        var val = chValue;
		        var row = document.getElementById("tr-"+indx);
		        
		       
		        $('#output'+indx).empty();
		        var output;
		        for (var i = 0, length = val; i < length; i++) {
		                  
		            output = "<select> <option value=''>Age</option> <option value='1'>1</option> <option value='2'>2</option> <option value='3'>3</option> </select> &nbsp &nbsp";
		            $('#output'+indx).append(output);
		        }
		        
		        
		       
		        
				 }
		      
		        
		   $scope.getBookedDetails = function(){
					
					//alert("hai");
					//alert($('#bookingId').val());
					//$('#bookingId').val();
					var bookingId = '297';
					var fdata = "bookingId=" + bookingId; 
					
		             
					
					
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'get-booked-details'
							}).success(function(response) {
								
								$scope.booked = response.data;
								//console.log($scope.booked[0].firstName);
								
								
							});

				};
				
				$scope.getProperty = function() {
					
					var url = "get-front-property?propertyId="+propertyId;
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.property = response.data;
					
					});
				};	
				
				$scope.getUser = function() {
					
					//alert($('#userId').val());
					
					
				//var userid = document.getElementById("userId").value;
					
					var url = "get-customer?auserId="+$('#userId').val();
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.user = response.data;
					
					});
				};
	       
			$scope.getUser();
			$scope.getType();
			$scope.getProperty();
		/*	$scope.getAccommodations();
			$scope.getPropertyPhotos();	
			$scope.getProperty();	     
			
		*/		

		});

		
	</script>
		

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
var options = {
    "key": "rzp_live_XK8Wemj85ETgaW",
    "amount": 20000, // 2000 paise = INR 20
    "name": "Merchant Name",
    "description": "Purchase Description",
    "image": "/your_logo.png",
    "handler": function (response){
        //alert(response.razorpay_payment_id);
         $scope.confirmBooking();
    },
    "prefill": {
        "name": "Harshil Mathur",
        "email": "harshil@razorpay.com"
    },
    "notes": {
        "address": "Hello World"
    },
    "theme": {
        "color": "#F37254 "
    }
};
var rzp1 = new Razorpay(options);

document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}
</script>
	
	 



  