<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div id="wrapper">
       <section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index">Home</a></li>
                        <li>Team</li>
                    </ul>
                    <h3>ULO Team</h3>
                    
                </div>
            </div> 
        </div>
    </section>

    

    <section class="section clearfix">
        <div class="container">
            <div class="row">
                <div id="fullwidth" class="col-sm-12 pad-bot">

                    <!-- START CONTENT -->
                    <div class="col-lg-12">
                 <h2 class="text-center"><img src="ulowebsite/images/team/viswa.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Viswanathan M</h4>
                   <h6 class="text-center">CEO</h6>
                 <p>Viswanathan, the Founder and CEO, has an experience of over 6 years in the travel and tourism sector. He enjoys interacting with people and is currently making Ulo Hotels a Pan India operation. Operations, marketing, strategy, financing, creation of company culture, human resources and more, he handles them all. He enjoys charting Ulo’s direction and believes in doing things differently and smartly. He is always ready for food, travel, impromptu plans and long drives. Today, he stands as a man who has travelled extensively, and is always on the lookout for the next destination or one more challenge.</p> 
                    </div>
                    <div class="col-lg-4">
                    <h2 class="text-center"><img src="ulowebsite/images/team/muruga.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                    <h4 class="text-center">Muruga j</h4>
                    <h6 class="text-center">chief strategist</h6>
                    <p style="text-align: justify;">Muruga J, the chief strategist, an alumnus of IIM Calcutta has nearly 2 decade's worth of experience in multiple domains with interests spread across real-estate, healthcare, sustainable farming and hospitality sectors. He believes creating something unique lies in thinking what nobody has yet thought, about that which everybody sees. His favorite thing to do is brainstorm both within and out of the office to figure out novel ways to streamline and scale up operations. He values collaboration and gets things done by mentoring the team. During the summers, you can find him heading up to the mountains to unwind amidst nature.</p>
                    </div>
                            <div class="col-lg-4">
                        <h2 class="text-center"><img src="ulowebsite/images/team/vino.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Vinodhini</h4>
                   <h6 class="text-center">DIRECTOR-OPERATIONS</h6>
                   <p style="text-align: justify;">Vinodhini graduated from Ethiraj College for Women, Chennai with a degree in Commerce. She is a people person and loves the idea of tying everything together for the clients. She loves making lists, planning holidays, making presentations, numbers, chocolate, choosing destinations, and... you get the idea. She is a romantic at heart and brings to the table her ability to know what people want and makes sure Ulo Hotels delivers, every time.</p>
                    </div>
                         <div class="col-lg-4">
                         <h2 class="text-center"><img src="ulowebsite/images/team/gopi.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Gopinath</h4>
                   <h6 class="text-center">SEnior developer</h6>
                   <p style="text-align: justify;">If Ulo was a software, Gopinath would be the data processor. With a Master degree in Computer Science from SRM,  he heads the web development team. He gets by with barely any words through the day, except when he is talking movies. He aspires to be an actor and has several interesting tit bits to share about the movie industry. He loves coding and is constantly talking to his monitor, with whom he shares a very tempestuous relationship. He is the brain behind the software and takes care of all things digital. He is always there to provide clarity in a crisis much like pulling a rabbit out a hat!</p>
                    </div>
         
                    </div>
                   
                   <div class="row">
                
                         <div class="col-lg-4">
                         <h2 class="text-center"><img src="ulowebsite/images/team/pravin.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Pravin</h4>
                   <h6 class="text-center">SEO analyst</h5>
                   <p style="text-align: justify;">Pravin is an MCA graduate and heads digital marketing. He plans, creates and implements marketing strategies to ensure that Ulo Hotels has a strong digital presence. He does everything from developing media strategies that puts Ulo Hotels in front of our target audience to pitching content and digging through the web analytics to unearth invaluable info about target consumers. He loves experimenting with strategy and genre, and can be found dissecting movies when not working.</p>
                    </div>
                           <div class="col-lg-4">
                     <h2 class="text-center"><img src="ulowebsite/images/team/dwarakesh.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Dwarakesh</h4>
                   <h6 class="text-center">Web Developer</h6>
                   <p style="text-align: justify;">DwaRakesh Arshu holds an engineering degree in Information Technology. With a love for coding and a flair of new computer languages, he joined Ulo Hotels right out of college. He is responsible for the software Ulo uses, how it works and how it can be made better. He takes care of getting data from the server, executing conditional statements, picking the best framework and tools to improve the velocity of the product development. He loves listening to music and reading in his spare time.</p>
                    </div>
                    <div class="col-lg-4">
                    <h2 class="text-center"><img src="ulowebsite/images/team/siva.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Siva</h4>
                   <h6 class="text-center">Web Developer</h6>
                   <p style="text-align: justify;">Siva Balan graduated from  with a degree in computer Science. He handles client relations and makes sure they are happy and within their budget. He enjoys coding and is proficient in various softwares. He is always reading up on related concepts and tries to improvise on the existing softwares used. He loves his coffee and his codes. He is always looking to improve and tries harder today than yesterday. </p>
                    </div>
                    </div>
              
                                    <div class="row">
                     <div class="col-lg-4">
                     <h2 class="text-center"><img src="ulowebsite/images/team/vikram.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Vikram</h4>
                   <h6 class="text-center">Reservation Manager</h6>
                   <p style="text-align: justify;">Vikram is the reservation manager for Coorg and Yelagiri. Having graduated with a degree in Computer Science, he makes sure there is effective management of reservations to increase occupancy and revenue. He follows up with clients, updates the products on the website, analysis the reservation charts to give information to the marketing team. He loves watching and playing cricket and tries to catch a game during the weekends. When not on the phone or playing cricket, he enjoys a good book..</p>
                    </div>
                    <div class="col-lg-4">
                    <h2 class="text-center"><img src="ulowebsite/images/team/raja.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Raja</h4>
                   <h6 class="text-center">Reservation Manager</h6>
                   <p style="text-align: justify;">Raja, a graduate with a degree in Information System Management is our reservation Manager for Kodaikanal, Kolli Hills and Ooty. He makes sure there is sufficient marketing done to create awareness about promotional offers. He brings to the table his ability to know what people want and makes sure Ulo Hotels delivers, every time. He ensures things are functional on a day-to-day basis at the client reach level, takes care of bookings, preparation of reports and other logistics related to reservations. He unwinds through gaming or listening to music. </p>
                    </div>
                        <div class="col-lg-4">
                    <h2 class="text-center"><img src="ulowebsite/images/team/naga.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Nagalingam</h4>
                   <h6 class="text-center">Contract Manager</h6>
                   <p style="text-align: justify;">Nagalingam is our contracts and affiliation manager. A computer Science graduate, he takes care of the logistics related to operations and scaling up. His primary concern is to develop a growth strategy focused both on increasing occupancy rate and customer satisfaction. He is responsible for generating new leads, market analysis, review management and after sales support. When not texting or emailing, he unwinds with music on bass speakers. </p>
                    </div>
         
                   </div>
        
                                                    <div class="row">
                                                          <div class="col-lg-4">
                    <h2 class="text-center"><img src="ulowebsite/images/team/rahul.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Rahul</h4>
                   <h6 class="text-center">Digital Marketing Executive</h6>
                   <p style="text-align: justify;">Rahul is a digital marketing specialist with a degree in information technology. Joining as a fresher, he is a mix of enthusiasm, creativity and humour. He specializes in SEO, SEM, SMM,Google AdWords and more. He is responsible for making sure Ulo reaches the targetaudience. He is adept at evaluating the needs of the consumer market, and understand show and where to acquire knowledge about consumer trends and demands. He enjoys yoga, blogging and watching movies. </p>
                    </div>
                     <div class="col-lg-4">
                     <h2 class="text-center"><img src="ulowebsite/images/team/bala.png" class="img-circle" alt="Cinque Terre" width="100" height="100"></h2>
                     <h4 class="text-center">Balamurgan</h4>
                   <h6 class="text-center">designer</h6>
                   <p style="text-align: justify;">Balamurugan our creative designer is a Computer Science graduate. He heads the graphic design team and creates the content for the marketing specialists. He has a strong knowledge of several different design software packages and understands branding and the kind of reach and target audience Ulo looks for. He works alongside developers and digital marketing specialists and enjoys developing the overall layout and production design for advertisements, brochures, magazines, and corporate reports. When not designing, you can find him scrolling through social media.</p>
                    </div>
      
                   </div>
                    <!-- END CONTENT -->

                </div><!-- end fullwidth -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->
</div>
 <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
    