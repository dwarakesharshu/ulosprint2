<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {{member[0].firstname + " " + member[0].lastname }} 
            <small>Subscription Expiry on {{member[0].subscription[0].expirydate}}</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Member</a></li>
            <li class="active">New</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	 <div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Member Info</a></li>
                  <li ><a href="#tab-subscription" data-toggle="tab">Subscription</a></li>
                  <li ><a href="#tab-nominee" data-toggle="tab">Nominee</a></li>
                </ul>
                <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
                  <form method="post" action="editfamilymember.action" enctype="multipart/form-data" theme="simple">
                    <s:hidden name="familyid" value="%{familyid}"></s:hidden>
                  	<section id="secimage">
                  		<div class="row fontawesome-icon-list">
                      	<div class="col-xs-12">
                      	 <div class="box box-widget widget-user-2">
                  		<div class="widget-user-header bg-yellow">
                  			<div class="widget-user-image">
                    			<img class="img-circle" src="<s:url action='familyprofilepicture.action?familyid='/><%=request.getParameter("familyid") %>" onclick="javascript:fileBrowse();" alt="User Avatar">
                    			<input type="file" name="myFile" id="myFile" accept=".png,.jpg" style="display:none;"/>
                  			</div><!-- /.widget-user-image -->
                 			 <h3 class="widget-user-username">{{member[0].firstname + " " + member[0].lastname}}</h3>
                  			<h5 class="widget-user-desc">{{member[0].relationship}}</h5>
                		</div>
                		</div>
                		</div>
                		</div>
                  	</section>
                    <section id="new">
                      <!-- <h4 class="page-header">General Settings</h4> -->
                      
                      <div class="row fontawesome-icon-list">
                      	<div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>Title</label>
	                      			<select name="title" id="title" class="form-control">
	                       			 <option value="Mr.">Mr.</option>
	                        			<option value="Miss.">Miss</option>
	                        			<option value="Mrs.">Mrs.</option>
	                      			</select>
	                    		</div>
	                        </div>
                        <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="firstname">First Name</label>
                      			<input type="text" class="form-control" name="firstname"  id="firstname" value="{{member[0].firstname}}" placeholder="firstname">
                    		</div>
						</div>
                        <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="lastname">Last Name</label>
                      			<input type="text" class="form-control" name="lastname" id="lastname" value="{{member[0].lastname}}" placeholder="lastname">
                    		</div>
                        </div>
                        <div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>Sex</label>
	                      			<select name="sex" id="sex" class="form-control">
	                       			 <option value="Male">Male</option>
	                        			<option value="Female">Female</option>
	                        			
	                      			</select>
	                    		</div>
	                        </div>
                        <div class="col-md-3 col-sm-4">
                        	 <div class="form-group">
                    			<label>Date of Birth</label>
                    			<div class="input-group">
                     			 <div class="input-group-addon">
                       				 <i class="fa fa-calendar"></i>
                      			</div>
                     			 <input type="text" class="form-control" id="dateofbirth" name="dateofbirth" value="{{member[0].dateofbirth}}"  data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
                    			</div><!-- /.input group -->
                 			 </div><!-- /.form group -->
                  
                        </div>
                        <div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>Sex</label>
	                      			<select name="bloodgroup" id="bloodgroup" value="{{member[0].bloodgroup}}" class="form-control">
	                       			 <option value="O+">O+</option>
	                        			<option value="O-">O-</option>
	                        			<option value="A+">A+</option>
	                        			<option value="A-">A-</option>
	                        			<option value="B+">B+</option>
	                        			<option value="B-">B-</option>
	                        			<option value="AB+">AB+</option>
	                        			<option value="AB-">AB-</option>
	                      			</select>
	                    		</div>
	                        </div>
                        
                      </div>
                    </section>

                    <section id="web-application">
                      <h4 class="page-header">Contact Details</h4>
	                      <div class="row fontawesome-icon-list">
	                        <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="address">Address</label>
                      			<input type="text" class="form-control" name="address"  id="address" value="{{member[0].address}}" placeholder="address">
                    		</div>
						</div>
	                        <div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>City</label>
	                      			
	                      			<input type="text" class="form-control" name="city"  id="city" value="{{member[0].city}}" placeholder="city">
	                    		</div>
	                        </div>
	                        
	                         <div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>state</label>
	                      			<select name="state" id="state"  value="{{member[0].state}}" class="form-control">
	                       				<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
	                        			<option value="Andhra Pradesh">Andhra Pradesh</option>
	                        			<option value="Arunachal Pradesh">Arunachal Pradesh</option>
	                        			<option value="Assam">Assam</option>
	                        			<option value="Bihar">Bihar</option>
	                        			<option value="Chandigarh">Chandigarh</option>
	                        			<option value="Chhattisgarh">Chhattisgarh</option>
	                        			<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
										<option value="Daman and Diu">Daman and Diu</option>
										<option value="Delhi">Delhi</option>
										<option value="Goa">Goa</option>
										<option value="Gujarat">Gujarat</option>
										<option value="Haryana">Haryana</option>
										<option value="Himachal Pradesh">Himachal Pradesh</option>
										<option value="Jammu and Kashmir">Jammu and Kashmir</option>
										<option value="Jharkhand">Jharkhand</option>
										<option value="Karnataka">Karnataka</option>
										<option value="Kerala">Kerala</option>
										<option value="Lakshadweep ">Lakshadweep </option>
										<option value="Madhya Pradesh">Madhya Pradesh</option>
										<option value="Maharashtra">Maharashtra</option>
										<option value="Manipur">Manipur</option>
										<option value="Meghalaya">Meghalaya</option>
										<option value="Mizoram">Mizoram</option>
										<option value="Nagaland">Nagaland</option>
										<option value="Odisha">Odisha</option>
										<option value="Puducherry ">Puducherry </option>
										<option value="Punjab">Punjab</option>
										<option value="Rajasthan">Rajasthan</option>
										<option value="Sikkim">Sikkim</option>
										<option value="Tamil Nadu">Tamil Nadu</option>
										<option value="Telangana">Telangana</option>
										<option value="Tripura">Tripura</option>
										<option value="Uttar Pradesh">Uttar Pradesh</option>
										<option value="Uttarakhand">Uttarakhand</option>
										<option value="West Bengal">West Bengal</option>
	                      			</select>
	                    		</div>
	                        </div>
	                         <div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>country</label>
	                      			<select name="country" id="country" value="{{member[0].country}}" class="form-control">
	                       				<option value="India">India</option>
	                      			</select>
	                    		</div>
	                        </div>
	                        <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="zipcode">zipcode</label>
                      			<input type="text" class="form-control" name="zipcode"  id="zipcode" value="{{member[0].zipcode}}" placeholder="zipcode">
                    		</div>
						</div>
						 <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="phone">phone</label>
                      			<input type="text" class="form-control" name="phone"  id="phone" value="{{member[0].phone}}" placeholder="phone">
                    		</div>
						</div>
						 <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="mobile">mobile</label>
                      			<input type="text" class="form-control" name="mobile"  id="mobile" value="{{member[0].mobile}}" placeholder="mobile">
                    		</div>
						</div>
						 <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="email">email</label>
                      			<input type="text" class="form-control" name="email"  id="email" value="{{member[0].email}}" placeholder="email">
                    		</div>
						</div>
						<div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="alteremail">alteremail</label>
                      			<input type="text" class="form-control" name="alteremail"  id="alteremail" value="{{member[0].alteremail}}" placeholder="alteremail">
                    		</div>
						</div>
                        <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="ice">ICE</label>
                      			<input type="text" class="form-control" name="ice"  id="ice" placeholder="ICE">
                    		</div>
						</div>
						<div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="icecontactno">ICE Contact No</label>
                      			<input type="text" class="form-control" name="icecontactno"  id="icecontactno" placeholder="ICE Contact No">
                    		</div>
						</div>
						<div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>Relationship</label>
	                      			<select name="relationship" id="relationship" value="{{member[0].relationship}}" class="form-control">
	                       				<option value="father">Father</option>
	                       				<option value="mother">Mother</option>
	                       				<option value="husband">Husband</option>
	                       				<option value="wife">Wife</option>
	                       				<option value="son">Son</option>
	                       				<option value="daughter">Daughter</option>
	                      			</select>
	                    		</div>
	                        </div>
	                        <div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>Preferred Language</label>
	                      			<select name="language" id="language" value="{{member[0].language}}" class="form-control">
	                       				<option value="hindi">Hindi</option>
										<option value="english">English</option>
										<option value="bengali">Bengali</option>
										<option value="telugu">Telugu</option>
										<option value="marathi">Marathi</option>
										<option value="tamil">Tamil</option>
										<option value="urdu">Urdu</option>
										<option value="kannada">Kannada</option>
										<option value="gujarati">Gujarati</option>
										<option value="odia">Odia</option>
										<option value="malayalam">Malayalam</option>
										<option value="sanskrit">Sanskrit</option>

	                      			</select>
	                    		</div>
	                        </div>
                    	</div>
                    </section>

					
                    

                    <section id="video-player">
                      <h4 class="page-header"></h4>
                      <div class="row fontawesome-icon-list">
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4">
                        	<button type="submit" class="btn btn-info pull-right">Save</button>
                        </div>
                        
                      </div>
                    </section>

                        </form>
              

                </div><!-- /.tab-content -->
              	  
              	  <div class="tab-pane" id="tab-subscription">
              	  	
                    <form method="post" action="addsubscription.action"  theme="simple">
                      <s:hidden name="familyid" value="%{familyid}"></s:hidden>
                      
                      <section id="subs-current">
                 		<h4 class="page-header">Current Subscription</h4>

				          <div class="row fontawesome-icon-list">
				            <div class="col-md-3 col-sm-4" ng-repeat="s in subscription">
				              <div class="box box-solid">
				                <div class="box-header with-border">
				                 <!--  <i class="fa fa-text-width"></i> -->
				                  <h3 class="box-title">
				                  
				                      	<input type="radio" name="subscriptionid" value="{{p.DT_RowId}}" class="flat-red">
				                    
					         
				                  {{p.packagename}}</h3>
				                </div><!-- /.box-header -->
				                <div class="box-body">
				                  <ul>
				                  		<li>Age Range - {{p.agerange}}</li>
				                  		<li>Gender  - {{p.gender}}</li>
				                  		<li>Location  - {{p.locationid}}</li>
				                  		<li>Rate/Month  - {{p.ratepermonth}}</li>
				                    <!-- <li ng-repeat="f in t.fields" ng-if="$index<10">{{p.fieldName}}</li> -->
				                    
				                  </ul>
				                </div><!-- /.box-body -->
				              </div><!-- /.box -->
				            </div><!-- ./col -->
				            
				          </div><!-- /.row -->
             
       
        </section><!-- /.content -->
        
                    <section id="subs-package">
                 		<h4 class="page-header">Available Subscriptions</h4>

				          <div class="row fontawesome-icon-list">
				            <div class="col-md-3 col-sm-4" ng-repeat="p in packages">
				              <div class="box box-solid">
				                <div class="box-header with-border">
				                 <!--  <i class="fa fa-text-width"></i> -->
				                  <h3 class="box-title">
				                  
				                      	<input type="radio" name="packageid" value="{{p.DT_RowId}}" class="flat-red">
				                    
					         
				                  {{p.packagename}}</h3>
				                </div><!-- /.box-header -->
				                <div class="box-body">
				                  <ul>
				                  		<li>Age Range - {{p.agerange}}</li>
				                  		<li>Gender  - {{p.gender}}</li>
				                  		<li>Location  - {{p.locationid}}</li>
				                  		<li>Rate/Month  - {{p.ratepermonth}}</li>
				                    <!-- <li ng-repeat="f in t.fields" ng-if="$index<10">{{p.fieldName}}</li> -->
				                    
				                  </ul>
				                </div><!-- /.box-body -->
				              </div><!-- /.box -->
				            </div><!-- ./col -->
				            
				          </div><!-- /.row -->
             
       
        </section><!-- /.content -->
        
        <section id="subs-month">
                      <h4 class="page-header">Subscription Duration</h4>
                      
                      <div class="row fontawesome-icon-list">
                      	<div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>Months</label>
	                      			<input type="text" class="form-control" name="months"  id="months" placeholder="Enter Months">
	                    		</div>
	                        </div>
	                        </div>
	                        </section>
         <section id="subs-save">
                      <h4 class="page-header"></h4>
                      <div class="row fontawesome-icon-list">
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4">
                        	<button type="submit" class="btn btn-info pull-right">Save</button>
                        </div>
                        
                      </div>
                    </section>
                    </form>
              	  </div>
              	  <div class="tab-pane" id="tab-nominee">
              	  	<section id="new">
                      <!-- <h4 class="page-header">General Settings</h4> -->
                      
                      <div class="row fontawesome-icon-list">
                      	<div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>Title</label>
	                      			<select name="title" id="title" class="form-control">
	                       			 <option value="Mr.">Mr.</option>
	                        			<option value="Miss.">Miss</option>
	                        			<option value="Mrs.">Mrs.</option>
	                      			</select>
	                    		</div>
	                        </div>
                        <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="firstname">First Name</label>
                      			<input type="text" class="form-control" name="firstname"  id="firstname" value="{{member[0].firstname}}" placeholder="firstname">
                    		</div>
						</div>
                        <div class="col-md-3 col-sm-4">
                        	<div class="form-group">
                      			<label for="lastname">Last Name</label>
                      			<input type="text" class="form-control" name="lastname" id="lastname" value="{{member[0].lastname}}" placeholder="lastname">
                    		</div>
                        </div>
                        <div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>Sex</label>
	                      			<select name="sex" id="sex" class="form-control">
	                       			 <option value="Male">Male</option>
	                        			<option value="Female">Female</option>
	                        			
	                      			</select>
	                    		</div>
	                        </div>
                        <div class="col-md-3 col-sm-4">
                        	 <div class="form-group">
                    			<label>Date of Birth</label>
                    			<div class="input-group">
                     			 <div class="input-group-addon">
                       				 <i class="fa fa-calendar"></i>
                      			</div>
                     			 <input type="text" class="form-control" id="dateofbirth" name="dateofbirth" value="{{member[0].dateofbirth}}"  data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
                    			</div><!-- /.input group -->
                 			 </div><!-- /.form group -->
                  
                        </div>
                        <div class="col-md-3 col-sm-4">
	                        	<div class="form-group">
	                      			<label>Sex</label>
	                      			<select name="bloodgroup" id="bloodgroup" value="{{member[0].bloodgroup}}" class="form-control">
	                       			 <option value="O+">O+</option>
	                        			<option value="O-">O-</option>
	                        			<option value="A+">A+</option>
	                        			<option value="A-">A-</option>
	                        			<option value="B+">B+</option>
	                        			<option value="B-">B-</option>
	                        			<option value="AB+">AB+</option>
	                        			<option value="AB-">AB-</option>
	                      			</select>
	                    		</div>
	                        </div>
                        
                      </div>
                    </section>
              	  </div>
              
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
          </div>
        
        
          
          
        </section><!-- /.content -->
      
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {


			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
			//$scope.progressbar.start();
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			//var taskurl = "http://localhost:8085/collaborative-workflow/gettasksbyuserlatest.action";
			var taskurl = "gettasksbyuserlatest.action";
			$http.get(taskurl).success(function(response) {
				$scope.latesttasks = response.data;
			});
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};


			
			
			
			
			$scope.getMember = function() {

				//var customurl = "http://localhost:8085/collaborative-workflow/gettasksbyuser.action?"+fdata;
				var url = "getfamilymember.action?familyid="+$("#familyid").val();
				//alert(customurl);
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.member = response.data;
					
					//$timeout(toggle_do, 10); 

				});
			};
			
			$scope.getPackages = function() {

				//var customurl = "http://localhost:8085/collaborative-workflow/gettasksbyuser.action?"+fdata;
				var url = "allpackages.action";
				//alert(customurl);
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.packages = response.data;
					
					//$timeout(toggle_do, 10); 

				});
			};

			


			
			$scope.getMember();

			$scope.unread();
			$scope.getPackages();
			
			
		});

	
		function fileBrowse()
		{
			document.getElementById("myFile").click();
		}
		   
		        

		
	</script>
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
       