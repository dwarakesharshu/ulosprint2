<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
   <head>
      <title>ULO Hotels</title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Bootstrap3.3.6 stylesheet -->
      <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
      <!-- main css stylesheet-->
      <link href="style.css" rel="stylesheet" type="text/css">
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
   </head>
   <body class="hold-transition login-page">
      <!-- Signin Page Start -->
      <div class="container  ">
      <div class="login-box  ">
         <div class="login-box-body">
            <div class="row">
               <div class="col-md-12">
                  <div class="signin-page-logo align-center"><img src="images/ulo-logo.png" height="100px" width="200px" class="img-responsive"/ ></a>
                  </div>
               </div>
               <div class="col-md-12">
                  <form id="myForm" action="forgetpassword.action" method="post" theme="simple">
                     <h5 class="text-center">Forgot Your Password?</h5>
                    
                      <div class="form-group has-feedback">
                     
                      <input type="text" name="emailId" id="emailId" class="form-control" type="email" placeholder="Email" required="required">
                      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                      </div>
                     <s:if test="hasActionErrors()">
                        <div id="password_error_message" style="color:red;font-weight:normal;">
                           <s:iterator value="actionErrors">
                              <s:property/>
                           </s:iterator>
                        </div>
                     </s:if>
                     <s:if test="hasActionMessages()"  >
                        <div id="password_error_message" style="color:green;font-weight:normal;">
                           <s:iterator value="actionMessages">
                              <s:property/>
                           </s:iterator>
                        </div>
                     </s:if>
                     <!-- <div><a href="Login.jsp">Return to Login</a></div> -->
                     <!-- <a href="index.html"> --><!--  <button  type="submit" class="login-btn">Login</button>--><!-- </a> -->
                     <div class="row">
                        <br>
                        <div class="col-xs-4">
                           <button  onclick="validate();" class="btn btn-primary btn-flat btn-block">Send</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- Signin Page End -->
      <!-- jQuery Version 2.1.1 -->
      <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
      <!-- bootstrap-3.3.6 script -->
      <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   </body>
</html>
